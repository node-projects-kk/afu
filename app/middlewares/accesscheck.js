const _                   		= require("underscore");
const userRepo            		= require('user/repositories/user.repository');
const permissionRepo		= require('permission/repositories/permission.repository');

module.exports = function (role,permission,cb) {
	permissionRepo.getByField({ 'operation': { $in: [permission] } },function (err, result) {
		if (err || result==null) {
			cb(err,false);
		}
		else {
			userRepo.findIsAccess(role,result._id,function (err, result) {
				if (err) {
					cb(err,false);
				}
				else {
					if(result) {
						cb(null,true);
					}
					else {
						cb(null,false);
					}
				}
			});
		}    
	})    
};