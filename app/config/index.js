const nodemailer = require('nodemailer');
const path = require('path');
const isProd = process.env.NODE_ENV === 'prod';

var firebase = require('firebase');
require('firebase/auth');
require('firebase/database');

module.exports = {
	jwtSecret: "MyS3cr3tK3Y",
	jwtSession: {
		session: false
	},
	PAGINATION_PERPAGE: 10,
	android_serverKey: 'AAAAb4syF_w:APA91bHuiTPO1LAppJTdTIPuYnVAURpxn6ymNznWfaqYGM57Ie0zX-K9Na3Fq_Qaot9SKV84bs6bBWxVRSI_dgcAcRZ9cpXy1nHDh2CmwwJLnkOzP1EPsYCSG1E1jlxmfk03kB-dxRF4',
	//ios_key: '/var/www/html_node/afu/app/config/key_file/AuthKey_7XGMBSUZ7H.p8',
	ios_key: path.join(__dirname, '/key_file/AuthKey_7XGMBSUZ7H.p8'),
	ios_keyId: '7XGMBSUZ7H',
	ios_teamId: 'H23W3EERLK',
	stripe_pub_key: 'pk_test_CbyKB49tW4HK64CWeGhhOKgm',
	stripe_secret_key: 'sk_test_WyhzyGDgDUnAkVqI6yFAVXE6',
	isProd,
	getPort: process.env.PORT || 1380,
	getAdminFolderName: process.env.ADMIN_FOLDER_NAME || 'admin',
	getApiFolderName: process.env.API_FOLDER_NAME || 'api',
	nonSecurePaths: ['/reset-password', '/login', '/store', '/verify', '/resendOtp', '/register'],
	transporter: nodemailer.createTransport({
		service: 'gmail',
		host: 'smtp.gmail.com',
		auth: {
			user: process.env.MAIL_USERNAME,
			pass: process.env.MAIL_PASSWORD,
		}
	}),
	STRIPE_SK: process.env.STRIPE_SK,
	TOK_BOX_API_KEY: '46410972',
	TOK_BOX_API_SECRET: '50b4560f8b3c33874c1f2a5277cab54f12e89e81',
	FIREBASE_APIKEY: 'AIzaSyAmsV_l_VeFCaZkFBM_SfHNfkcgUTI7eTs',
	FIREBASE_AUTHDOMAIN: 'twiliocalling-58e51.firebaseio.com',
	FIREBASE_DATABASE_URL: 'https://twiliocalling-58e51.firebaseio.com',
	FIREBASE_PROJECTID: 'twiliocalling-58e51',
	FIREBASE_STORAGE_BUCKET: 'twiliocalling-58e51.appspot.com',
	FIREBASE_MESSEGING_SENDERID: '479076685820'
	// s3: new aws.S3()

}