const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const faqController = require('faq/controllers/faq.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of faq
namedRouter.all('/faq*', auth.authenticate);

// admin faq list route
namedRouter.post("admin.faq.getall", '/faq/getall', async (req, res) => {
	try {
		const success = await faqController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.faq.list", '/faq/list',faqController.list);
namedRouter.get("admin.faq.create", '/faq/create', faqController.create);
namedRouter.post("admin.faq.store", '/faq/store', request_param.any(),faqController.store);
namedRouter.get("admin.faq.edit", '/faq/edit/:id',faqController.edit);
namedRouter.get("admin.faq.delete", '/faq/delete/:id',faqController.destroy);
namedRouter.post("admin.faq.update", '/faq/update',request_param.any(),faqController.update);
namedRouter.get("admin.faq.statusChange", '/faq/status-change/:id', request_param.any(),faqController.changeStatus);

//Export the express.Router() instance
module.exports = router;