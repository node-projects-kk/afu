const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const reportController = require('report/controllers/report.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of report
namedRouter.all('/report*', auth.authenticate);

// admin report list route

namedRouter.post("admin.report.getall", '/report/getall', async (req, res) => {
    try {
        const success = await reportController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
namedRouter.get("admin.report.list", '/report/list',reportController.list);
namedRouter.get("admin.report.edit", '/report/edit/:id',reportController.edit);
namedRouter.get("admin.report.delete", '/report/delete/:id',reportController.destroy);
namedRouter.post("admin.report.update", '/report/update',request_param.any(),reportController.update);





//Export the express.Router() instance
module.exports = router;