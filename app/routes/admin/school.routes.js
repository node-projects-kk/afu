const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const schoolController = require('school/controllers/school.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of school
namedRouter.all('/school*', auth.authenticate);

// admin school list route
namedRouter.post("admin.school.getall", '/school/getall', async (req, res) => {
	try {
		const success = await schoolController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.school.list", '/school/list',schoolController.list);
namedRouter.get("admin.school.create", '/school/create', schoolController.create);
namedRouter.post("admin.school.store", '/school/store', request_param.any(),schoolController.store);
namedRouter.get("admin.school.edit", '/school/edit/:id',schoolController.edit);
namedRouter.get("admin.school.delete", '/school/delete/:id',schoolController.destroy);
namedRouter.post("admin.school.update", '/school/update',request_param.any(),schoolController.update);
namedRouter.get("admin.school.statusChange", '/school/status-change/:id', request_param.any(),schoolController.changeStatus);

//Export the express.Router() instance
module.exports = router;