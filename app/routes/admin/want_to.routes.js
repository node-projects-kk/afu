const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const want_toController = require('want_to/controllers/want_to.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of want_to
namedRouter.all('/want_to*', auth.authenticate);

// admin want_to list route

namedRouter.post("admin.want_to.getall", '/want_to/getall', async (req, res) => {
    try {
        const success = await want_toController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
namedRouter.get("admin.want_to.list", '/want_to/list',want_toController.list);
namedRouter.get("admin.want_to.create", '/want_to/create', want_toController.create);
namedRouter.post("admin.want_to.store", '/want_to/store', request_param.any(),want_toController.store);
namedRouter.get("admin.want_to.edit", '/want_to/edit/:id',want_toController.edit);
namedRouter.get("admin.want_to.delete", '/want_to/delete/:id',want_toController.destroy);
namedRouter.post("admin.want_to.update", '/want_to/update',request_param.any(),want_toController.update);
namedRouter.get("admin.want_to.statusChange", '/want_to/status-change/:id', request_param.any(),want_toController.changeStatus);




//Export the express.Router() instance
module.exports = router;