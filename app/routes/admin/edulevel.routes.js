const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const edulevelController = require('education_level/controllers/edulevel.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of edulevel
namedRouter.all('/edulevel*', auth.authenticate);

// admin edulevel list route

namedRouter.post("admin.edulevel.getall", '/edulevel/getall', async (req, res) => {
    try {
        const success = await edulevelController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
namedRouter.get("admin.edulevel.list", '/edulevel/list',edulevelController.list);
namedRouter.get("admin.edulevel.create", '/edulevel/create', edulevelController.create);
namedRouter.post("admin.edulevel.store", '/edulevel/store', request_param.any(),edulevelController.store);
namedRouter.get("admin.edulevel.edit", '/edulevel/edit/:id',edulevelController.edit);
namedRouter.get("admin.edulevel.delete", '/edulevel/delete/:id',edulevelController.destroy);
namedRouter.post("admin.edulevel.update", '/edulevel/update',request_param.any(),edulevelController.update);
namedRouter.get("admin.edulevel.statusChange", '/edulevel/status-change/:id', request_param.any(),edulevelController.changeStatus);




//Export the express.Router() instance
module.exports = router;