const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const supportController = require('support/controllers/support.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of support
namedRouter.all('/support*', auth.authenticate);

// admin support list route
namedRouter.post("admin.support.getall", '/support/getall', async (req, res) => {
	try {
		const success = await supportController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.support.list", '/support/list',supportController.list);


//Export the express.Router() instance
module.exports = router;