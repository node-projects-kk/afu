const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const settingController = require('setting/controllers/setting.controller');

const multer = require('multer');
const request_param = multer();

//authentication section of setting
namedRouter.all('/setting*', auth.authenticate);

// admin setting list route

namedRouter.post("admin.setting.getall", '/setting/getall', async (req, res) => {
    try {
        const success = await settingController.getAll(req, res);
        // console.log("GETALL",success)
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
namedRouter.get("admin.setting.list", '/setting/list',settingController.list);
namedRouter.get("admin.setting.edit", '/setting/edit/:id',settingController.edit);
//namedRouter.get("admin.setting.delete", '/setting/delete/:id',settingController.destroy);
namedRouter.post("admin.setting.update", '/setting/update',request_param.any(),settingController.update);

//Export the express.Router() instance
module.exports = router;