const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const courseController = require('course/controllers/course.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of course
namedRouter.all('/course*', auth.authenticate);

// admin course list route
namedRouter.post("admin.course.getall", '/course/getall', async (req, res) => {
	try {
		const success = await courseController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.course.list", '/course/list',courseController.list);
namedRouter.get("admin.course.create", '/course/create', courseController.create);
namedRouter.post("admin.course.store", '/course/store', request_param.any(),courseController.store);
namedRouter.get("admin.course.edit", '/course/edit/:id',courseController.edit);
namedRouter.get("admin.course.delete", '/course/delete/:id',courseController.destroy);
namedRouter.post("admin.course.update", '/course/update',request_param.any(),courseController.update);
namedRouter.get("admin.course.statusChange", '/course/status-change/:id', request_param.any(),courseController.changeStatus);

//Export the express.Router() instance
module.exports = router;