const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const bookedDemoController = require('booked-demo/controllers/booked-demo.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of booked-demo
namedRouter.all('/booked-demo*', auth.authenticate);

// admin booked-demo list route
namedRouter.post("admin.booked-demo.getall", '/booked-demo/getall', async (req, res) => {
	try {
		const success = await bookedDemoController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.booked-demo.list", '/booked-demo/list',bookedDemoController.list);


//Export the express.Router() instance
module.exports = router;