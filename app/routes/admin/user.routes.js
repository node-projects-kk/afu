const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const multer = require('multer');
const userController = require('user/controllers/user.controller');
const auth = require("../../middlewares/auth")();

const Storage = multer.diskStorage({
	destination: (req, file, callback) => {
		if (file.fieldname === 'banner_image') {
			callback(null, "./public/uploads/user/banner")
		} else {
			callback(null, "./public/uploads/user");
		}
	},
	filename: (req, file, callback) => {
		callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(/\s/g, '_'));
	}
});

const uploadFile = multer({storage: Storage});
const request_param = multer();

// 

// login render route
namedRouter.get('admin.user.login', '/', userController.login);

// login process route
namedRouter.post("admin.login.process", '/login',userController.signin);

namedRouter.post("admin.forgotPass.process", '/forgotpassword',userController.forgotPassword);
/*
// @Route: Users Forgotpassowrd [Admin]
*/
//namedRouter.post('admin.forgotPass.process', '/forgotpassword', request_param.any(), async (req, res) => {
//	try {
//		const success = await userController.forgotPassword(req);
//		console.log("xcascasc",success); process.exit();
//		return success;
//		//req.flash('success', success.message);
//		//return res.redirect(namedRouter.urlFor('user.login'));
//	}
//	catch (error) {
//		console.log("47>>",error);
//		return error;
//		//throw error;
//		//req.flash('error', error.message);
//		//return res.redirect(namedRouter.urlFor('user.login'));
//	}
//});

namedRouter.get('admin.user.logout', "/logout", userController.logout);
namedRouter.all('/*', auth.authenticate);

/*
// @Route: Users Dashboard [Admin]
*/
// dashboard route
namedRouter.get("admin.user.dashboard", '/dashboard', userController.dashboard);

namedRouter.get('admin.user.dashboard.counts', '/dashboard/recordcounts', async(req, res) => {
	try{
		const success = await userController.dashboardCounts(req);
		res.send(success);
	} catch(error) {
		req.flash('error', error.message);
		return res.redirect(namedRouter.urlFor('admin.user.login'));
	}
});

namedRouter.get("admin.profile", '/profile/:id', request_param.any(),userController.viewmyprofile);
namedRouter.post("admin.updateProfile", '/update/profile', request_param.any(), userController.updateprofile);
namedRouter.get("admin.changepassword", '/change/password', userController.adminChangePassword);
namedRouter.post("admin.updateAdminPassword", '/update/admin-password', request_param.any(),userController.adminUpdatePassword);

/* user section start */
namedRouter.post("admin.allUsers", '/users/getAll/:utype', async (req, res) => {
	try {
		const success = await userController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

namedRouter.get("admin.users", '/users/:type', request_param.any(),userController.list);
namedRouter.get("admin.user.create", '/user/create', request_param.any(),userController.create);
namedRouter.post("admin.user.store", '/user/store', request_param.any(),userController.store);
namedRouter.get("admin.user.edit", '/user/edit/:id', request_param.any(),userController.edit);
namedRouter.get("admin.user.view", '/user/view/:id/:utype', request_param.any(),userController.view);
namedRouter.get("admin.user.delete", '/user/delete/:id',userController.destroy);
namedRouter.post("admin.user.update", '/user/update', request_param.any(),userController.update);
namedRouter.get("admin.user.changeStatus", '/user/change-status/:id', request_param.any(),userController.changeStatus);
/* end section */

// Export the express.Router() instance
module.exports = router;