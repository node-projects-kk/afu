const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const universityController = require('university/controllers/university.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of university
namedRouter.all('/university*', auth.authenticate);

// admin university list route
namedRouter.post("admin.university.getall", '/university/getall', async (req, res) => {
	try {
		const success = await universityController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.university.list", '/university/list',universityController.list);
namedRouter.get("admin.university.create", '/university/create', universityController.create);
namedRouter.post("admin.university.store", '/university/store', request_param.any(),universityController.store);
namedRouter.get("admin.university.edit", '/university/edit/:id',universityController.edit);
namedRouter.get("admin.university.delete", '/university/delete/:id',universityController.destroy);
namedRouter.post("admin.university.update", '/university/update',request_param.any(),universityController.update);
namedRouter.get("admin.university.statusChange", '/university/status-change/:id', request_param.any(),universityController.changeStatus);
namedRouter.get("admin.university.invite", '/university/invite', universityController.invite);
namedRouter.post("admin.university.invitesend", '/university/invitesend', universityController.inviteSend);
//Export the express.Router() instance
module.exports = router;