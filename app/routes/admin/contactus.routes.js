const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const contactusController = require('contactus/controllers/contactus.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of contactus
namedRouter.all('/contactus*', auth.authenticate);

// admin contactus list route
namedRouter.get("admin.contactus.list", '/contactus/list',contactusController.list);

namedRouter.post("admin.contactus.getall", '/contactus/getall', async (req, res) => {
	
    try {
        const success = await contactusController.getAll(req, res);
        res.send({"meta": success.meta, "data": success.data});
    } catch (error) {
        res.status(error.status).send(error);
    }
});
/*@Route:  contactus  Create*/
// namedRouter.get("admin.contactus.create", '/contactus/create',contactusController.create);
// namedRouter.post("admin.contactus.store", '/contactus/store', request_param.any(),contactusController.store);

/*@Route:  contactus  Edit*/
// namedRouter.get("admin.contactus.edit", '/contactus/edit/:id',contactusController.edit);

/*@Route:  contactus  Update*/
// namedRouter.post("admin.contactus.update", '/contactus/update',request_param.any(),contactusController.update);

/*@Route:  contactus  Delete*/
// namedRouter.get("admin.contactus.delete", '/contactus/delete/:id',contactusController.destroy);

// /*@Route:  contactus  Status Chenges*/
// namedRouter.get("admin.contactus.statusChange", '/contactus/status-change/:id',request_param.any(),contactusController.statusChange);

//Export the express.Router() instance


module.exports = router;