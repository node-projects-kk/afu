const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const packageController = require('package/controllers/package.controller');
const auth = require("../../middlewares/auth")();

const multer = require('multer');
const request_param = multer();

//authentication section of package
namedRouter.all('/package*', auth.authenticate);

// admin package list route
namedRouter.post("admin.package.getall", '/package/getall', async (req, res) => {
	try {
		const success = await packageController.getAll(req, res);
		res.send({"meta": success.meta, "data": success.data});
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});
namedRouter.get("admin.package.list", '/package/list',packageController.list);
namedRouter.get("admin.package.create", '/package/create', packageController.create);
namedRouter.post("admin.package.store", '/package/store', request_param.any(),packageController.store);
namedRouter.get("admin.package.edit", '/package/edit/:id',packageController.edit);
namedRouter.get("admin.package.delete", '/package/delete/:id',packageController.destroy);
namedRouter.post("admin.package.update", '/package/update',request_param.any(),packageController.update);
namedRouter.get("admin.package.statusChange", '/package/status-change/:id', request_param.any(),packageController.changeStatus);

//Export the express.Router() instance
module.exports = router;