const express = require('express');
const routeLabel = require('route-label');
const multer = require('multer');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');
const request_param = multer();
const supportController = require('webservice/support.controller');

namedRouter.all('/support*', auth.authenticateAPI);

/**
 * @api {post} /support/query Support Query
 * @apiVersion 1.0.0
 * @apiGroup Support
 * @apiHeader x-access-token User's Access token
 * @apiParam email Email Address
 * @apiParam message Message
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "email": "test@email.com",
        "message": "Lorem Ipsum",
        "status": "Active",
        "isDeleted": false,
        "_id": "5da72c55d9da54061984a152",
        "user_id": "5d81f293af7f7442362ab99e",
        "createdAt": "2019-10-16T14:42:29.280Z",
        "updatedAt": "2019-10-16T14:42:29.280Z",
        "__v": 0
    },
    "message": "Query placed successfully."
}
*/
namedRouter.post("api.support.query", '/support/query', request_param.any(), async (req, res) => {
	try {
		const success = await supportController.submitQuery(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('route error',error.message);
		res.status(error.status).send(error);
	}
});

module.exports = router;