const express = require('express');
const routeLabel = require('route-label');
const multer = require('multer');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');
const userController = require('webservice/user.controller');
const chatController = require('webservice/chat.controller');

//const request_param = multer();
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log("12>>", file);
        if ((file.fieldname).indexOf('profile_image') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/user')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/user');
            }
            cb(null, './public/uploads/user')
        }
        if ((file.fieldname).indexOf('logo') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/logo')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/logo');
            }
            cb(null, './public/uploads/logo')
        }
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
})
const request_param = multer({
    storage: storage
});

/**
* @api {post} /user/register  Mentor/Student Signup
* @apiVersion 1.0.0
* @apiGroup Users
* 
 * @apiSuccessExample {json} Success Mentor
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "profile_image": "profile_image_1563802441970_img2.jpg",
        "school_name": "",
        "logo": "logo_1563802441971_gc.jpg",
        "short_description": "test description",
        "address": "London",
        "university_name": "RKMV",
        "gender": "Male",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "education_level": "5d31c1a0822fa522c7abefca",
        "specialization": [
            {
                "title": "project one",
                "_id": "5d35bb4a38104765642d2b23"
            },
            {
                "title": "http://www.projectone.com",
                "_id": "5d35bb4a38104765642d2b22"
            }
        ],
        "service": [
            {
                "name": "Service One",
                "time": "2",
                "details": "service details one",
                "cost": "75",
                "_id": "5d35bb4a38104765642d2b25"
            },
            {
                "name": "Service Two",
                "time": "3",
                "details": "service details two",
                "cost": "100",
                "_id": "5d35bb4a38104765642d2b24"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "project": [],
        "activities": [],
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzViYjRhMzgxMDQ3NjU2NDJkMmIyMSIsImlhdCI6MTU2MzgwMjQ0MiwiZXhwIjoxNTYzODg4ODQyfQ.Ba9x4uJZva7d6zm2KOPjOHCSoM-br3M0uFKAKSZP1zY",
    "message": "Registration successfull."
}
 
* @apiSuccessExample {json} Success Student
 *{
    "status": 200,
    "data": {
        "full_name": "Ayan",
        "email": "ayan1@gmail.com",
        "password": "$2a$08$D/GDWDaQm1M5ykmC.pvXKeQzQBAXn8KWl33sx0Vyo47VeystRkqcu",
        "profile_image": "",
        "university_name": "",
        "year_of_study": "",
        "short_description": "",
        "social_id": "",
        "register_type": "normal",
        "school_name": "Nava Nalanda",
        "logo": "",
        "address": "",
        "gender": "Male",
        "phone": "",
        "dob": "",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "display_mobile_no": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d668a1d37aa6464abeeb44a",
        "language": "5d371e651ae2de8011c4acb5",
        "country": "5d306dc6822fa522c79acd1f",
        "education_level": "5d385f14d3486b7ae28e0e7b",
        "want_to": "5d31c5b8822fa522c7ac5585",
        "activities": [
            {
                "title": "Cooking",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44e"
            },
            {
                "title": "App Developing",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44d"
            },
            {
                "title": "Programming",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44c"
            },
            {
                "title": "Teaching",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44b"
            }
        ],
        "role": "5d30518c822fa522c7991274",
        "service": [],
        "createdAt": "2019-08-28T14:05:17.096Z",
        "updatedAt": "2019-08-28T14:05:17.096Z",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNjY4YTFkMzdhYTY0NjRhYmVlYjQ0YSIsImlhdCI6MTU2NzAwMTExNywiZXhwIjoxNTY3MDg3NTE3fQ.sV8Rmz3jdSLQR8tQPBXPdv-R8El1l5VcK0JixR0g81o",
    "message": "Registration successfull."
}
*/
namedRouter.post("api.user.register", '/user/register', request_param.any(), async (req, res) => {
    try {
        const success = await userController.register(req, res);
        //console.log("81>>",success);
        res.status(success.status).send(success);
    } catch (error) {
        console.log('85>>', error.message)
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /user/signin Signin Mentor/Student
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam email Student/Mentr Email 
 * @apiParam password Student/Mentor Password
 * @apiParam user_type User Type [student,mentor]
 * @apiParam social_id Social ID (If Social Login)
 * @apiParam register_type Register Type (facebook/google/normal/linkedin/instagram)
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra@gmail.com",
        "password": "$2a$08$gnoPVZI9yuN1hqDuLHj2ZOYEgrMjQqe70.exh7.3wZr2CdDcMwjAG",
        "profile_image": "",
        "school_name": "",
        "education_level": "Masters degree ",
        "want_to": "",
        "address": "London",
        "university_name": "RKMV",
        "gender": "Male",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "specialization": [
            {
                "title": "project one",
                "_id": "5d3188206d84b8436af1ce39"
            },
            {
                "title": "http://www.projectone.com",
                "_id": "5d3188206d84b8436af1ce38"
            }
        ],
        "service": [
            {
                "name": "Service One",
                "time": "2",
                "details": "service details one",
                "cost": "75",
                "_id": "5d3188206d84b8436af1ce3b"
            },
            {
                "name": "Service Two",
                "time": "3",
                "details": "service details two",
                "cost": "100",
                "_id": "5d3188206d84b8436af1ce3a"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "project": [],
        "activities": [],
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-07-19T09:06:40.023Z",
        "__v": 0
    },
    "isLoggedIn": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0",
    "message": "Login successfull."
}
*/
namedRouter.post("api.user.signin", '/user/signin', request_param.any(), async (req, res) => {
    try {
        const success = await userController.signin(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        console.log('route error', error.message);
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /user/forgotpassword Forgot Password
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiParam email User's account Email
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "New Password sent to your registered Email"
}
*/
namedRouter.post("api.user.forgotpassword", '/user/forgotpassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.forgotPassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {POST} /users/logout Logout User
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam user_id User Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "isLoggedIn": false,
    "message": "Logout Successfully"
}
*/
namedRouter.post("api.user.logout", '/users/logout', request_param.any(), async (req, res) => {
    try {
        const success = await userController.logout(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.all('/users*', auth.authenticateAPI);


/**
 * @api {Get} /users/orderHistory Order History List
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiHeader x-access-token User's Access token (Student)
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5db2a60cea50c357ba6b238d",
            "package_name": "Profile/CV Improvementt",
            "package_price": 100,
            "stripe_charge_id": "ch_1FXNLjCae2wvBG9BYr3pruuF",
            "customer_id": "cus_G3UKHRB4lOZnHB",
            "order_date": "2019-10-23T08:24:11.712Z",
            "package_id": "5d494830238c6455a392cd0f",
            "mentor_id": "5db2a304ea50c357ba6b2389",
            "student_id": "5db29da0ea50c357ba6b237d",
            "createdAt": "2019-10-25T07:36:44.032Z",
            "updatedAt": "2019-10-25T07:36:44.032Z",
            "__v": 0,
            "package_info": {
                "_id": "5d494830238c6455a392cd0f",
                "package_name": "Profile/CV Improvementt",
                "package_price": 100,
                "package_description": "Test Descriptionn",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:28:16.066Z",
                "updatedAt": "2019-09-20T09:46:25.100Z",
                "__v": 0
            },
            "mentor_info": {
                "_id": "5db2a304ea50c357ba6b2389",
                "full_name": "Perfect Mentor",
                "surname": "",
                "email": "perfect@email.com",
                "password": "$2a$08$43sRfDS59w1BmlFcBOYjfOaIs8a5PWMu84Z2I260cPKcaWeLdCTfW",
                "profile_image": "profile_image_1571988221226_IMG_0111.HEIC",
                "university_name": "My University.",
                "year_of_study": "1995",
                "short_description": "You Know What I Do. ",
                "social_id": "",
                "register_type": "normal",
                "school_name": "",
                "logo": "",
                "address": "",
                "gender": "Male",
                "phone": "",
                "dob": "",
                "deviceToken": null,
                "deviceType": "ios",
                "push_notification": true,
                "display_mobile_no": true,
                "onoffstatus": "yes",
                "isDeleted": false,
                "isActive": true,
                "language": "5d371e651ae2de8011c4acb5",
                "country": "5d306dc6822fa522c79acd1f",
                "course": "5d498ab0e72f7708815f0792",
                "education_level": "5d7d6c373f71307502bca5d4",
                "service": [
                    {
                        "_id": "5db2a304ea50c357ba6b238b",
                        "package_id": "5d494830238c6455a392cd0f"
                    },
                    {
                        "_id": "5db2a304ea50c357ba6b238a",
                        "package_id": "5d49480d238c6455a392cd0e"
                    }
                ],
                "role": "5d30518c822fa522c7991272",
                "activities": [],
                "createdAt": "2019-10-25T07:23:48.261Z",
                "updatedAt": "2019-10-25T07:53:59.224Z",
                "__v": 0
            }
        },
        {
            "_id": "5db29ec7ea50c357ba6b2384",
            "package_name": "testAFU001",
            "package_price": 300,
            "stripe_charge_id": "ch_1FXMriCae2wvBG9BCmoDpzhM",
            "customer_id": "cus_G3TpxvcIL8GZ3N",
            "order_date": "2019-10-23T08:24:11.712Z",
            "package_id": "5da56475b470be127dea9a05",
            "mentor_id": "5d9d75947f45992abe8293a7",
            "student_id": "5db29da0ea50c357ba6b237d",
            "createdAt": "2019-10-24T07:05:43.056Z",
            "updatedAt": "2019-10-25T07:05:43.056Z",
            "__v": 0,
            "package_info": {
                "_id": "5da56475b470be127dea9a05",
                "package_name": "testAFU001",
                "package_price": 300,
                "package_description": "lorem",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d9d75947f45992abe8293a7",
                "createdAt": "2019-10-15T06:17:25.782Z",
                "updatedAt": "2019-10-15T13:57:23.475Z",
                "__v": 0
            },
            "mentor_info": {
                "_id": "5d9d75947f45992abe8293a7",
                "full_name": "John Doe",
                "surname": "",
                "email": "john.doe@yahoo.com",
                "password": "$2a$08$8T0X7Mq2tSmyuPmAYwk0MeUos7.DDNMpBHaMHazzjmtJdSRNBml5u",
                "profile_image": "profile_image_1571205509710_userJonath.jpeg",
                "university_name": "University of Birmingham",
                "year_of_study": "2015",
                "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p",
                "social_id": "",
                "register_type": "normal",
                "school_name": "",
                "logo": "logo_1571126624325_univLogo.jpeg",
                "address": "UK",
                "gender": "Male",
                "phone": "9876543233",
                "dob": "",
                "deviceToken": "",
                "deviceType": "",
                "push_notification": true,
                "display_mobile_no": true,
                "onoffstatus": "yes",
                "isDeleted": false,
                "isActive": true,
                "language": "5d306346822fa522c79a2df2",
                "country": "5d306dc6822fa522c79acd21",
                "course": "5d498adae72f7708815f0793",
                "education_level": "5d7d69053f71307502bca5d2",
                "service": [
                    {
                        "_id": "5d9d75947f45992abe8293aa",
                        "package_id": "5d49453d94ac5553b49b5b74"
                    },
                    {
                        "_id": "5d9d75947f45992abe8293a9",
                        "package_id": "5d493b24f5cd7d4a7fc345f4"
                    },
                    {
                        "_id": "5d9d75947f45992abe8293a8",
                        "package_id": "5d49480d238c6455a392cd0e"
                    },
                    {
                        "_id": "5d9ee002bf9680442582b6c0",
                        "package_id": "5d9ee002bf9680442582b6bf"
                    },
                    {
                        "_id": "5d9ee0ecbf9680442582b6c2",
                        "package_id": "5d9ee0ecbf9680442582b6c1"
                    },
                    {
                        "_id": "5da45f08b470be127dea99f3",
                        "package_id": "5da45f08b470be127dea99f2"
                    },
                    {
                        "_id": "5da47d69b470be127dea99f6",
                        "package_id": "5da47d69b470be127dea99f5"
                    },
                    {
                        "_id": "5da47e63b470be127dea99f8",
                        "package_id": "5da47e63b470be127dea99f7"
                    },
                    {
                        "_id": "5da47e8fb470be127dea99fa",
                        "package_id": "5da47e8fb470be127dea99f9"
                    },
                    {
                        "_id": "5da482bcb470be127dea99fc",
                        "package_id": "5da482bcb470be127dea99fb"
                    },
                    {
                        "_id": "5da49263b470be127dea99fe",
                        "package_id": "5da49263b470be127dea99fd"
                    },
                    {
                        "_id": "5da55029b470be127dea9a00",
                        "package_id": "5da55029b470be127dea99ff"
                    },
                    {
                        "_id": "5da5506fb470be127dea9a02",
                        "package_id": "5da5506fb470be127dea9a01"
                    },
                    {
                        "_id": "5da56475b470be127dea9a06",
                        "package_id": "5da56475b470be127dea9a05"
                    },
                    {
                        "_id": "5da56482b470be127dea9a08",
                        "package_id": "5da56482b470be127dea9a07"
                    },
                    {
                        "_id": "5da5794eb470be127dea9a0b",
                        "package_id": "5da5794eb470be127dea9a0a"
                    },
                    {
                        "_id": "5da5a522b470be127dea9a0e",
                        "package_id": "5da5a522b470be127dea9a0d"
                    }
                ],
                "role": "5d30518c822fa522c7991272",
                "activities": [],
                "createdAt": "2019-10-09T05:52:20.929Z",
                "updatedAt": "2019-10-25T08:00:11.571Z",
                "__v": 0
            }
        }
    ],
    "message": "Order history fetched successfully."
}
*/
namedRouter.get("api.users.orderHistory", '/users/orderHistory', request_param.any(), async (req, res) => {
    try {
        const success = await userController.userOrderHistory(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {GET} /users/getprofile User's Profile
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "profile": {
                "_id": "5d3188206d84b8436af1ce37",
                "full_name": "Subhra",
                "email": "subhra@gmail.com",
                "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                "profile_image": "",
                "school_name": "",
                "education_level": "5d31c1a0822fa522c7abefca",
                "want_to": "",
                "address": "London",
                "university_name": "RKMV",
                "gender": "Male",
                "dob": "",
                "social_id": "",
                "register_type": "normal",
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "country": "5d306dc6822fa522c79acd21",
                "language": "5d306346822fa522c79a2dec",
                "specialization": [
                    {
                        "title": "project one",
                        "_id": "5d3188206d84b8436af1ce39"
                    },
                    {
                        "title": "http://www.projectone.com",
                        "_id": "5d3188206d84b8436af1ce38"
                    }
                ],
                "service": {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                },
                "role": "5d30518c822fa522c7991272",
                "project": [],
                "activities": [],
                "push_notification": true,
                "short_description": "test description",
                "createdAt": "2019-07-19T09:06:40.023Z",
                "updatedAt": "2019-07-19T13:35:04.720Z",
                "__v": 0,
                "user_role": {
                    "_id": "5d30518c822fa522c7991272",
                    "role": "mentor",
                    "desc": "This is the mentor role here",
                    "roleDisplayName": "Mentor"
                },
                "edu_level": {
                    "_id": "5d31c1a0822fa522c7abefca",
                    "level_name": "Level 01",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-04-15T08:24:40.827Z",
                    "__v": 0
                }
            },
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ]
        }
    ],
    "message": "Profile Info fetched Successfully"
}
*/

namedRouter.get('api.user.getprofile', '/users/getprofile', async (req, res) => {
    try {
        const success = await userController.getMyProfile(req);
        // console.log(success);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {GET} /users/dashboard Dashboard
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "count": {
            "user": 36,
            "referral": 5,
            "ambassador": 7
        },
        "top_users": [
            {
                "_id": "5d81ed77c7eb5b0871bbbf06",
                "full_name": "Oliver Queen",
                "profile_image": "profile_image_1568820826186_IMG_0008.JPG",
                "earning": 60,
                "packages_sold": 2,
                "active_from": "2019-09-18T08:40:23.781Z"
            },
            {
                "_id": "5d935fb6350a2b544c14a630",
                "full_name": "John Wells",
                "profile_image": "",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-01T14:16:22.438Z"
            },
            {
                "_id": "5d935d15350a2b544c14a62b",
                "full_name": "test4",
                "profile_image": "",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-01T14:05:09.800Z"
            },
            {
                "_id": "5d9dd457bf9680442582b6ba",
                "full_name": "Jonath James",
                "profile_image": "profile_image_1570624598932_userJonath.jpeg",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-09T12:36:39.172Z"
            },
            {
                "_id": "5da44059b470be127dea99ef",
                "full_name": "Aidan Carr",
                "profile_image": "",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-14T09:31:05.490Z"
            }
        ]
    },
    "message": "Dashboard Info fetched Successfully"
}
*/

namedRouter.get('api.user.dashboard', '/users/dashboard', async (req, res) => {
    try {
        const success = await userController.getDashboard(req);
        // console.log(success);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {GET} /users/detailsById/:id User Details By ID
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam id User Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d81ed77c7eb5b0871bbbf06",
        "profile": {
            "_id": "5d81ed77c7eb5b0871bbbf06",
            "full_name": "Oliver Queen",
            "surname": "",
            "email": "oliver@gmail.com",
            "password": "$2a$08$hCNBo3dZSB0/tu.GZSBrnue677N8cDE.si4iKjyF.T0uiabdAMWHG",
            "profile_image": "profile_image_1568820826186_IMG_0008.JPG",
            "university_name": "Cambridge",
            "year_of_study": "2005",
            "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's dd\nstandard dummy text ever since the 1500. \nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500. ",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "logo_1568820725763_IMG_0007.JPG",
            "address": "25th avenue",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": false,
            "display_mobile_no": false,
            "onoffstatus": "yes",
            "isDeleted": false,
            "isActive": true,
            "language": "5d371e651ae2de8011c4acb5",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498adae72f7708815f0793",
            "education_level": "5d385f1ad3486b7ae28e0e7c",
            "service": {
                "_id": "5d81ed77c7eb5b0871bbbf09",
                "package_id": "5d494830238c6455a392cd0f"
            },
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-09-18T08:40:23.781Z",
            "updatedAt": "2019-10-16T12:01:46.833Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "service_val": {
                "_id": "5d494830238c6455a392cd0f",
                "package_name": "Profile/CV Improvementt",
                "package_price": 100,
                "package_description": "Test Descriptionn",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:28:16.066Z",
                "updatedAt": "2019-09-20T09:46:25.100Z",
                "__v": 0
            },
            "edu_level": {
                "_id": "5d385f1ad3486b7ae28e0e7c",
                "level_name": "Master's Degree",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-24T13:37:30.482Z",
                "__v": 0
            },
            "country_val": {
                "_id": "5d306dc6822fa522c79acd1f",
                "country_name": "United Kingdom",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "language_val": {
                "_id": "5d371e651ae2de8011c4acb5",
                "title": "English",
                "code": "en",
                "isDefault": false,
                "isDeleted": false,
                "status": "Active"
            },
            "course_val": {
                "_id": "5d498adae72f7708815f0793",
                "course_name": "Business Studies",
                "status": "Active",
                "isDeleted": false,
                "education": "5d31c1d7822fa522c7abf330",
                "university": "5d497ab20866ed78651160a2",
                "createdAt": "2019-08-06T14:12:42.017Z",
                "updatedAt": "2019-09-10T10:02:54.003Z",
                "__v": 0
            }
        },
        "service": [
            {
                "_id": "5d84b9b954de315aee9fb09e",
                "package_id": "5d84b9b954de315aee9fb09d"
            },
            {
                "_id": "5d935e8f350a2b544c14a62f",
                "package_id": "5d935e8f350a2b544c14a62e"
            },
            {
                "_id": "5d81ed77c7eb5b0871bbbf07",
                "package_id": "5d49453d94ac5553b49b5b74"
            },
            {
                "_id": "5d81ed77c7eb5b0871bbbf09",
                "package_id": "5d494830238c6455a392cd0f"
            },
            {
                "_id": "5d81ed77c7eb5b0871bbbf08",
                "package_id": "5d49480d238c6455a392cd0e"
            },
            {
                "_id": "5d8cdd387a66451bb1acf1f8",
                "package_id": "5d8cdd387a66451bb1acf1f7"
            },
            {
                "_id": "5d84ba6b54de315aee9fb0a0",
                "package_id": "5d84ba6b54de315aee9fb09f"
            },
            {
                "_id": "5d8cdd797a66451bb1acf1fa",
                "package_id": "5d8cdd797a66451bb1acf1f9"
            },
            {
                "_id": "5d8deee77a66451bb1acf20d",
                "package_id": "5d8deee77a66451bb1acf20c"
            },
            {
                "_id": "5d8cdd127a66451bb1acf1f6",
                "package_id": "5d8cdd127a66451bb1acf1f5"
            },
            {
                "_id": "5d8def287a66451bb1acf20f",
                "package_id": "5d8def287a66451bb1acf20e"
            }
        ],
        "service_val": [
            {
                "_id": "5d935e8f350a2b544c14a62e",
                "package_name": "Package1",
                "package_price": "25",
                "package_description": "We",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-10-01T14:11:27.047Z",
                "updatedAt": "2019-10-01T14:11:27.047Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d49453d94ac5553b49b5b74",
                "package_name": "Personal Statement Check",
                "package_price": 40,
                "package_description": "Test ",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:15:41.563Z",
                "updatedAt": "2019-09-20T11:19:34.478Z",
                "__v": 0,
                "isPurchased": true
            },
            {
                "_id": "5d84ba6b54de315aee9fb09f",
                "package_name": "TTTEE",
                "package_price": 1000,
                "package_description": "Test hhhhhh",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-20T11:39:23.435Z",
                "updatedAt": "2019-09-26T15:36:36.318Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d8deee77a66451bb1acf20c",
                "package_name": "ABC",
                "package_price": "200",
                "package_description": "Test test",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-27T11:13:43.848Z",
                "updatedAt": "2019-09-27T11:13:43.848Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d49480d238c6455a392cd0e",
                "package_name": "Interview Practice/Guidance",
                "package_price": 75,
                "package_description": "Test Description",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:27:41.599Z",
                "updatedAt": "2019-09-19T09:03:46.719Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d8cdd387a66451bb1acf1f7",
                "package_name": "Hello World2",
                "package_price": "500",
                "package_description": "Test",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-26T15:46:00.926Z",
                "updatedAt": "2019-09-26T15:46:00.926Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d84b9b954de315aee9fb09d",
                "package_name": "YGDG",
                "package_price": 20,
                "package_description": "Lorem Ipsum",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-20T11:36:25.470Z",
                "updatedAt": "2019-09-26T15:35:46.348Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d494830238c6455a392cd0f",
                "package_name": "Profile/CV Improvementt",
                "package_price": 100,
                "package_description": "Test Descriptionn",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:28:16.066Z",
                "updatedAt": "2019-09-20T09:46:25.100Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d8cdd127a66451bb1acf1f5",
                "package_name": "Hello World Hello World",
                "package_price": 5002,
                "package_description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-26T15:45:22.479Z",
                "updatedAt": "2019-10-15T14:41:45.072Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d8cdd797a66451bb1acf1f9",
                "package_name": "Test 23",
                "package_price": "50",
                "package_description": "Rr",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-26T15:47:05.098Z",
                "updatedAt": "2019-09-26T15:47:05.098Z",
                "__v": 0,
                "isPurchased": false
            },
            {
                "_id": "5d8def287a66451bb1acf20e",
                "package_name": "Timbuc2",
                "package_price": "10",
                "package_description": "www",
                "status": "Active",
                "isDeleted": false,
                "user_id": "5d81ed77c7eb5b0871bbbf06",
                "createdAt": "2019-09-27T11:14:48.013Z",
                "updatedAt": "2019-09-27T11:14:48.013Z",
                "__v": 0,
                "isPurchased": false
            }
        ]
    },
    "ratingAverageInfo": 4,
    "message": "Profile Info fetched Successfully"
}
*/
namedRouter.get("api.user.details.id", '/users/detailsById/:id', request_param.any(), async (req, res) => {
    try {
        const success = await userController.getMentorProfile(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/updateprofile User's Profile Update
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam full_name Full Name
 * @apiParam email Email Address (required)
 * @apiParam profile_image Profile Image - [For Mentor]
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "SUBHRA",
        "email": "subhra@gmail.com",
        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
        "profile_image": "",
        "school_name": "",
        "logo": "",
        "short_description": "test description",
        "address": "London",
        "university_name": "RKMV",
        "gender": "Male",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "education_level": "5d31c1a0822fa522c7abefca",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "specialization": [
            {
                "title": "project one",
                "_id": "5d3188206d84b8436af1ce39"
            },
            {
                "title": "http://www.projectone.com",
                "_id": "5d3188206d84b8436af1ce38"
            }
        ],
        "service": [
            {
                "name": "Service One",
                "time": "2",
                "details": "service details one",
                "cost": "75",
                "_id": "5d3188206d84b8436af1ce3b"
            },
            {
                "name": "Service Two",
                "time": "3",
                "details": "service details two",
                "cost": "100",
                "_id": "5d3188206d84b8436af1ce3a"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "project": [],
        "activities": [],
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-07-22T14:04:43.614Z",
        "__v": 0
    },
    "message": "User updated Successfully"
}
*/

namedRouter.post('api.user.updateprofile', '/users/updateprofile', request_param.any(), async (req, res) => {
    try {
        const success = await userController.updateProfile(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/updateadditionalinfo Additional Information Update [Mentor]
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam logo University Logo
 * @apiParam address Address
 * @apiParam university_name University Name
 * @apiParam education_level Education ID
 * @apiParam language Language ID
 * @apiParam country Country ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "Josephine Darakjy",
        "email": "josephine_darakjy@darakjy.org",
        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
        "profile_image": "",
        "school_name": "",
        "logo": "logo_1565791258336_colin.jpg",
        "short_description": "test description",
        "address": "74 S Westgate St",
        "university_name": "Ghasidas University",
        "year_of_study": "",
        "gender": "Female",
        "phone": "1",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "education_level": "5d385f1ad3486b7ae28e0e7c",
        "country": "5d306dc6822fa522c79acd25",
        "language": "5d306346822fa522c79a2dfa",
        "specialization": [
            {
                "title": "Finance",
                "_id": "5d3188206d84b8436af1ce39"
            },
            {
                "title": "Information Systems",
                "_id": "5d3188206d84b8436af1ce38"
            }
        ],
        "service": [
            {
                "name": "Service One",
                "time": "2",
                "details": "service details one",
                "cost": "75",
                "_id": "5d3188206d84b8436af1ce3b"
            },
            {
                "name": "Service Two",
                "time": "3",
                "details": "service details two",
                "cost": "100",
                "_id": "5d3188206d84b8436af1ce3a"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "activities": [],
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-08-14T14:00:58.367Z",
        "__v": 0
    },
    "message": "Additional info updated Successfully"
}
*/
namedRouter.post('api.user.updateadditionalinfo', '/users/updateadditionalinfo', request_param.any(), async (req, res) => {
    try {
        const success = await userController.updateAdditionalInfo(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/changepassword Change User's Password
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiParam currentPassword user's Current password
 * @apiparam newPassword new password
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Password updated successfully"
}
*/
namedRouter.post("api.user.changepassword", '/users/changepassword', request_param.any(), async (req, res) => {
    try {
        const success = await userController.changePassword(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {GET} users/mentor/list Mentor List
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d31830d6d84b8436af1ce32",
            "full_name": "Kuldip",
            "email": "kk@gmail.com",
            "password": "$2a$08$vtcwsvoesd6jZtMo5wdp4OxKMBdwpzuHMNwMoGQZnIRIilVPEBMnm",
            "profile_image": "",
            "school_name": "",
            "education_level": "5d31c1a0822fa522c7abefca",
            "want_to": "",
            "address": "London",
            "university_name": "RKMV",
            "gender": "Male",
            "dob": "",
            "social_id": "",
            "register_type": "normal",
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "country": "5d306dc6822fa522c79acd21",
            "language": "5d306346822fa522c79a2dec",
            "specialization": [
                {
                    "title": "project one",
                    "_id": "5d31830d6d84b8436af1ce34"
                },
                {
                    "title": "http://www.projectone.com",
                    "_id": "5d31830d6d84b8436af1ce33"
                }
            ],
            "service": [
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d31830d6d84b8436af1ce36"
                },
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d31830d6d84b8436af1ce35"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "project": [],
            "activities": [],
            "push_notification": true,
            "short_description": "test description",
            "createdAt": "2019-07-19T08:45:01.100Z",
            "updatedAt": "2019-07-19T08:45:01.100Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "edu_level": {
                "_id": "5d31c1a0822fa522c7abefca",
                "level_name": "Level 01",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        },
        {
            "_id": "5d3188206d84b8436af1ce37",
            "full_name": "Subhra",
            "email": "subhra@gmail.com",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "school_name": "",
            "education_level": "5d31c1a0822fa522c7abefca",
            "want_to": "",
            "address": "London",
            "university_name": "RKMV",
            "gender": "Male",
            "dob": "",
            "social_id": "",
            "register_type": "normal",
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "country": "5d306dc6822fa522c79acd21",
            "language": "5d306346822fa522c79a2dec",
            "specialization": [
                {
                    "title": "project one",
                    "_id": "5d3188206d84b8436af1ce39"
                },
                {
                    "title": "http://www.projectone.com",
                    "_id": "5d3188206d84b8436af1ce38"
                }
            ],
            "service": [
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                },
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "project": [],
            "activities": [],
            "push_notification": true,
            "short_description": "test description",
            "createdAt": "2019-07-19T09:06:40.023Z",
            "updatedAt": "2019-07-19T13:35:04.720Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "edu_level": {
                "_id": "5d31c1a0822fa522c7abefca",
                "level_name": "Level 01",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        },
        {
            "_id": "5d3189f0719a7e481b879cb1",
            "full_name": "Soudip",
            "email": "soudip@gmail.com",
            "password": "$2a$08$zPkgkuWbLqIepAojvcOW9.svpljrsvlS9bf6cBR.lM2GCYY8bMtQm",
            "profile_image": "profile_image_1563527664279_img3.jpg",
            "school_name": "",
            "education_level": "5d31c1d7822fa522c7abf330",
            "want_to": "",
            "address": "London",
            "university_name": "RKMV",
            "gender": "Male",
            "dob": "",
            "social_id": "",
            "register_type": "normal",
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "country": "5d306dc6822fa522c79acd21",
            "language": "5d306346822fa522c79a2dec",
            "specialization": [
                {
                    "title": "project one",
                    "_id": "5d3189f0719a7e481b879cb3"
                },
                {
                    "title": "http://www.projectone.com",
                    "_id": "5d3189f0719a7e481b879cb2"
                }
            ],
            "service": [
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3189f0719a7e481b879cb5"
                },
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3189f0719a7e481b879cb4"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "project": [],
            "activities": [],
            "push_notification": true,
            "short_description": "test description",
            "createdAt": "2019-07-19T09:14:24.393Z",
            "updatedAt": "2019-07-19T09:14:24.393Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "edu_level": {
                "_id": "5d31c1d7822fa522c7abf330",
                "level_name": "Level 02",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.get("api.mentor.list", '/users/mentor/list', request_param.any(), async (req, res) => {
    try {
        const success = await userController.mentorList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {POST} /users/student/list Student List
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access Token
 * @apiParam course Course ID
 * @apiParam country Country ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d6e4b8db6c99e0151707649",
            "full_name": "SUBHRA",
            "email": "debarka@gmail.com",
            "password": "$2a$08$LqQ4HUblkLS5Ocp.HB8.Xu7Yjo0YiPdJw1SKOFkH69.jXk75503Q6",
            "profile_image": "",
            "university_name": "",
            "year_of_study": "",
            "short_description": "",
            "social_id": "",
            "register_type": "normal",
            "school_name": "South Point",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": false,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d371e651ae2de8011c4acb5",
            "country": "5d306dc6822fa522c79acd1f",
            "education_level": "5d385f3fd3486b7ae28e0e7d",
            "want_to": "5d31c5b8822fa522c7ac5585",
            "activities": [
                {
                    "title": "Cooking",
                    "tag": "",
                    "_id": "5d6e4b8db6c99e015170764a"
                }
            ],
            "role": "5d30518c822fa522c7991274",
            "service": [],
            "createdAt": "2019-09-03T11:16:29.173Z",
            "updatedAt": "2019-09-12T05:56:06.817Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991274",
                "role": "student",
                "desc": "This is the student role here",
                "roleDisplayName": "Student"
            },
            "edu_level": {
                "_id": "5d385f3fd3486b7ae28e0e7d",
                "level_name": "M.Pharm",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-07-24T13:38:07.443Z",
                "__v": 0
            },
            "want_tos": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "countries": {
                "_id": "5d306dc6822fa522c79acd1f",
                "country_name": "United Kingdom",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        },
        {
            "_id": "5d725382963b173d5d8319b7",
            "full_name": "Test Student",
            "email": "testStudent@gmail.com",
            "password": "$2a$08$HII7Malc5DtbthXYREclE..Es/pkgrU6aD.5gkAZduhuHGELQ4/wC",
            "profile_image": "",
            "university_name": "",
            "year_of_study": "",
            "short_description": "",
            "social_id": "",
            "register_type": "normal",
            "school_name": "Test",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d306346822fa522c79a2dee",
            "country": "5d306dc6822fa522c79acd1f",
            "education_level": "5d31c1d7822fa522c7abf330",
            "want_to": "5d31c5b8822fa522c7ac5585",
            "activities": [
                {
                    "title": "Test",
                    "tag": "",
                    "_id": "5d725382963b173d5d8319b8"
                }
            ],
            "role": "5d30518c822fa522c7991274",
            "service": [],
            "createdAt": "2019-09-06T12:39:30.938Z",
            "updatedAt": "2019-09-09T10:17:24.062Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991274",
                "role": "student",
                "desc": "This is the student role here",
                "roleDisplayName": "Student"
            },
            "want_tos": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "countries": {
                "_id": "5d306dc6822fa522c79acd1f",
                "country_name": "United Kingdom",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        }
    ],
    "message": "Student list fetched successfully"
}
*/
namedRouter.post("api.student.list", '/users/student/list', request_param.any(), async (req, res) => {
    try {
        const success = await userController.studentList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/mentor/search Mentor Search
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam country Mentor Country Id
 * @apiParam course Mentor Course Id
 * @apiParam university University Name
 * @apiSuccessExample {json} Success  -- Provide Country
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d3189f0719a7e481b879cb1",
            "name": "Art Venere",
            "email": "art@venere.org",
            "password": "$2a$08$zPkgkuWbLqIepAojvcOW9.svpljrsvlS9bf6cBR.lM2GCYY8bMtQm",
            "profile_image": "profile_image_1563527664279_img3.jpg",
            "address": "London",
            "university_name": "Oxford",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3189f0719a7e481b879cb4"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3189f0719a7e481b879cb5"
                }
            ],
            "specialization": [
                {
                    "title": "Entrepreneurship",
                    "_id": "5d3189f0719a7e481b879cb2"
                },
                {
                    "title": "Consulting",
                    "_id": "5d3189f0719a7e481b879cb3"
                }
            ]
        },
        {
            "_id": "5d3188206d84b8436af1ce37",
            "name": "Josephine Darakjy",
            "email": "josephine_darakjy@darakjy.org",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "address": "London",
            "university_name": "Chembridge",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ],
            "specialization": [
                {
                    "title": "Information Systems",
                    "_id": "5d3188206d84b8436af1ce38"
                },
                {
                    "title": "Finance",
                    "_id": "5d3188206d84b8436af1ce39"
                }
            ]
        }
    ],
    "message": "Mentor list fetched successfully"
}
* @apiSuccessExample {json} Success  -- Provide Country & Course
*{
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "name": "Josephine Darakjy",
            "email": "josephine_darakjy@darakjy.org",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "address": "London",
            "university_name": "Chembridge",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ],
            "specialization": [
                {
                    "title": "Finance",
                    "_id": "5d3188206d84b8436af1ce39"
                }
            ]
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.post("api.mentor.search", '/users/mentor/search', request_param.any(), async (req, res) => {
    try {
        const success = await userController.mentorSearch(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/job/search Serach by Multiple Criteria
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiHeader x-access-token User's Access token
 * @apiParam country Country Name
 * @apiParam course Course name of Student
  * @apiParam university University name
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Student list fetched successfully"
}
*/
namedRouter.post("api.job.search", '/users/job/search', request_param.any(), async (req, res) => {
    try {
        const success = await userController.jobSearch(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/changenotification Change Notification (Push Notification/Display Mobile No)
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam push_notification Push Notification
 * @apiParam display_mobile_no Display Mobile No
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "full_name": "Josephine Darakjy",
        "email": "josephine_darakjy@darakjy.org",
        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
        "profile_image": "",
        "school_name": "",
        "logo": "logo_1565791258336_colin.jpg",
        "short_description": "test description",
        "address": "74 S Westgate St",
        "university_name": "Ghasidas University",
        "year_of_study": "",
        "gender": "Female",
        "phone": "1",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": false,
        "display_mobile_no": false,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "education_level": "5d385f1ad3486b7ae28e0e7c",
        "country": "5d306dc6822fa522c79acd25",
        "language": "5d306346822fa522c79a2dfa",
        "specialization": [
            {
                "title": "Finance",
                "_id": "5d3188206d84b8436af1ce39"
            },
            {
                "title": "Information Systems",
                "_id": "5d3188206d84b8436af1ce38"
            }
        ],
        "service": [
            {
                "name": "Service One",
                "time": "2",
                "details": "service details one",
                "cost": "75",
                "_id": "5d3188206d84b8436af1ce3b"
            },
            {
                "name": "Service Two",
                "time": "3",
                "details": "service details two",
                "cost": "100",
                "_id": "5d3188206d84b8436af1ce3a"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "activities": [],
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-08-16T09:14:15.232Z",
        "__v": 0
    },
    "message": "Settings update successfully"
}
*/
namedRouter.post("api.user.changenotify", '/users/changenotification', request_param.any(), async (req, res) => {
    try {
        const success = await userController.changeNotification(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/requestcallback Request Call back API
 * @apiVersion 1.0.0
 * @apiGroup Student
 * @apiHeader x-access-token User's Access token
 * @apiHeader mentor_id Mentor ID
 * @apiParam name Name
 * @apiParam phone_no Phone no
 * @apiParam callback_time Callback Time
 * @apiParam message Message
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [],
    "message": "Request call back send successfully"
}

*/
namedRouter.post("api.user.requestcallback", '/users/requestcallback', request_param.any(), async (req, res) => {
    try {
        const success = await userController.requestCallBack(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/placeorder Order Place
 * @apiVersion 1.0.0
 * @apiGroup Order
 * @apiHeader x-access-token User's Access token
 * @apiParam package_id Package ID
 * @apiParam mentor_id Mentor ID
 * @apiParam student_id Student ID
 * @apiParam package_name Package Name
 * @apiParam package_price Package Price
 * @apiParam card_number Card Number
 * @apiParam exp_year EXP Year
 * @apiParam exp_month EXP Month
 * @apiParam cvv Cvv number
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "package_name": "TTTEE",
        "package_price": "12",
        "stripe_charge_id": "ch_1FTrOmCae2wvBG9B3nd507d4",
        "customer_id": "cus_Fzr6WpyzhpFr6k",
        "order_date": "2019-10-15T11:44:56.968Z",
        "_id": "5da5dd61f8d878069434670b",
        "mentor_id": "5d9d75947f45992abe8293a7",
        "student_id": "5d81f293af7f7442362ab99e",
        "package_id": "5d84ba6b54de315aee9fb09f",
        "createdAt": "2019-10-15T14:53:21.205Z",
        "updatedAt": "2019-10-15T14:53:21.205Z",
        "__v": 0
    },
    "message": "Package has purchased successfully"
}
*/
namedRouter.post("api.user.order", '/users/placeorder', request_param.any(), async (req, res) => {
    try {
        const success = await userController.placeOrder(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/addmentee Add Mentee API
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 *{
    "status": 201,
    "data": {
        "isDeleted": false,
        "_id": "5d7a6baee5cf952225b936d3",
        "student_id": "5d6e4b8db6c99e0151707649",
        "mentor_id": "5d6e75ff91b6735dc5206a35",
        "createdAt": "2019-09-12T16:00:46.425Z",
        "updatedAt": "2019-09-12T16:00:46.425Z",
        "__v": 0
    },
    "message": "Mentee added successfully"
}
*/
namedRouter.post("api.user.addmentee", '/users/addmentee', request_param.any(), async (req, res) => {
    try {
        const success = await userController.addMentees(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/listmentee List Mentee API
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "_id": "5d7a6baee5cf952225b936d3",
        "isDeleted": false,
        "student_id": "5d6e4b8db6c99e0151707649",
        "mentor_id": "5d6e75ff91b6735dc5206a35",
        "createdAt": "2019-09-12T16:00:46.425Z",
        "updatedAt": "2019-09-12T16:00:46.425Z",
        "__v": 0,
        "students": {
            "_id": "5d6e4b8db6c99e0151707649",
            "full_name": "SUBHRA",
            "email": "debarka@gmail.com",
            "password": "$2a$08$LqQ4HUblkLS5Ocp.HB8.Xu7Yjo0YiPdJw1SKOFkH69.jXk75503Q6",
            "profile_image": "",
            "university_name": "",
            "year_of_study": "",
            "short_description": "",
            "social_id": "",
            "register_type": "normal",
            "school_name": "South Point",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": false,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d371e651ae2de8011c4acb5",
            "country": "5d306dc6822fa522c79acd1f",
            "education_level": "5d385f3fd3486b7ae28e0e7d",
            "want_to": "5d31c5b8822fa522c7ac5585",
            "activities": [
                {
                    "title": "Cooking",
                    "tag": "",
                    "_id": "5d6e4b8db6c99e015170764a"
                }
            ],
            "role": "5d30518c822fa522c7991274",
            "service": [],
            "createdAt": "2019-09-03T11:16:29.173Z",
            "updatedAt": "2019-09-13T14:53:31.882Z",
            "__v": 0
        },
        "mentors": {
            "_id": "5d6e75ff91b6735dc5206a35",
            "full_name": "Kalyan Dey",
            "email": "kalyan@gmail.com",
            "password": "$2a$08$r3kKfuqAwQ/c2vqUpof0LOyWFC7tmrZxxeuMA1cilMpUcsO2MHL/S",
            "profile_image": "profile_image_1568286068752_IMG_0004.JPG",
            "university_name": "Leiden University",
            "year_of_study": "1996",
            "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "logo_1568288877179_IMG_0002.JPG",
            "address": "test address here",
            "gender": "Male",
            "phone": "2535353578",
            "dob": "",
            "deviceToken": null,
            "deviceType": "ios",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "education_level": "5d385f0dd3486b7ae28e0e7a",
            "language": "5d306346822fa522c79a2df0",
            "country": "5d306dc6822fa522c79acd21",
            "course": "5d498ab0e72f7708815f0792",
            "service": [
                {
                    "_id": "5d78c788860d521eb8de35e1",
                    "package_id": "5d494830238c6455a392cd0f"
                },
                {
                    "_id": "5d6e75ff91b6735dc5206a38",
                    "package_id": "5d493b24f5cd7d4a7fc345f4"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-09-03T14:17:35.539Z",
            "updatedAt": "2019-09-16T06:59:51.942Z",
            "__v": 0
        }
    },
    "message": "Mentees list fetched successfully"
}
*/
namedRouter.get("api.user.listmentee", '/users/listmentee', request_param.any(), async (req, res) => {
    try {
        const success = await userController.listMentees(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /users/removementee Remove Mentee API
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam id list ID
 * @apiSuccessExample {json} Success
 *{
    "status": 201,
    "data": {
        "isDeleted": false,
        "_id": "5d7f67a249dca17ca6031943",
        "mentor_id": "5d6e75ff91b6735dc5206a35",
        "createdAt": "2019-09-16T10:44:50.718Z",
        "updatedAt": "2019-09-16T10:44:50.718Z",
        "__v": 0
    },
    "message": "Mentee has removed successfully"
}
*/
namedRouter.post("api.user.removementee", '/users/removementee', request_param.any(), async (req, res) => {
    try {
        const success = await userController.removeMentees(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /users/add-mentor Add Mentor API
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "isDeleted": false,
        "_id": "5d89d71ff334720751633d35",
        "mentor_id": "5d832e36bb10351e14c6028a",
        "student_id": "5d88983bd0ed0d4b3e09132a",
        "createdAt": "2019-09-24T08:43:12.000Z",
        "updatedAt": "2019-09-24T08:43:12.000Z",
        "__v": 0
    },
    "message": "Mentor added successfully"
}
*/
namedRouter.post("api.user.addmentor", '/users/add-mentor', request_param.any(), async (req, res) => {
    try {
        const success = await userController.addMentors(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /users/list-mentor List Mentor API
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d89d71ff334720751633d35",
            "isDeleted": false,
            "mentor_id": "5d832e36bb10351e14c6028a",
            "student_id": "5d88983bd0ed0d4b3e09132a",
            "createdAt": "2019-09-24T08:43:12.000Z",
            "updatedAt": "2019-09-24T08:43:12.000Z",
            "__v": 0,
            "students": {
                "_id": "5d88983bd0ed0d4b3e09132a",
                "full_name": "Jamie Ramsay",
                "surname": "",
                "email": "jamie@gmail.com",
                "password": "$2a$08$gi1huC2.BbXK3Ji7VrEK9u4MODJIq4fa507V9ehI/1GZsqMHKB/.a",
                "profile_image": "",
                "university_name": "",
                "year_of_study": "",
                "short_description": "",
                "social_id": "",
                "register_type": "normal",
                "school_name": "Dollar Academy",
                "logo": "",
                "address": "",
                "gender": "Male",
                "phone": "",
                "dob": "",
                "deviceToken": "",
                "deviceType": "",
                "push_notification": true,
                "display_mobile_no": true,
                "onoffstatus": "yes",
                "isDeleted": false,
                "isActive": true,
                "language": {
                    "_id": "5d371e651ae2de8011c4acb5",
                    "title": "English",
                    "code": "en",
                    "isDefault": false,
                    "isDeleted": false,
                    "status": "Active"
                },
                "country": "5d306dc6822fa522c79acd1f",
                "education_level": {
                    "_id": "5d7d69053f71307502bca5d2",
                    "level_name": "Foundation (Pre-University)",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-09-14T22:26:13.415Z",
                    "__v": 0
                },
                "course": "5d777517050bc65b2077285e",
                "activities": [
                    {
                        "title": "Cooking",
                        "tag": "",
                        "_id": "5d88983bd0ed0d4b3e09132b"
                    }
                ],
                "role": "5d30518c822fa522c7991274",
                "service": [],
                "createdAt": "2019-09-23T10:02:35.953Z",
                "updatedAt": "2019-09-23T10:03:30.257Z",
                "__v": 0,
                "countries": {
                    "_id": "5d306dc6822fa522c79acd1f",
                    "country_name": "United Kingdom",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-04-15T08:24:40.827Z",
                    "__v": 0
                },
                "course_details": {
                    "_id": "5d777517050bc65b2077285e",
                    "course_name": "Arts & Design",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-09-10T10:04:07.339Z",
                    "updatedAt": "2019-09-10T10:04:07.339Z",
                    "__v": 0
                }
            },
            "mentors": {
                "_id": "5d832e36bb10351e14c6028a",
                "full_name": "David Jones",
                "surname": "",
                "email": "david@mailinator.com",
                "password": "$2a$08$vqTOFNCoopL9tHCcIpnhlu2I7FL7OJnSUfyVpHKyHj0t2TAsmqa/y",
                "profile_image": "profile_image_1568878133957_image-c1ecf950-6956-48ff-bff6-6f875afe7239.jpg",
                "university_name": "OXFORD UNIVERSITY",
                "year_of_study": "2016",
                "short_description": "Hi , I am a mentor , studying at Oxford University at Business Administration. Loren ipsum is simply dummy text.fdhgd getdth dhshf ydfhfg gdgdgx xgxgxg xgfhfgf",
                "social_id": "",
                "register_type": "normal",
                "school_name": "",
                "logo": "logo_1568881498571_image-aa073627-7b8a-455c-b5ab-f511cd0dd0e9.jpg",
                "address": "Baker Street , London",
                "gender": "Male",
                "phone": "123",
                "dob": "",
                "deviceToken": "",
                "deviceType": "",
                "push_notification": true,
                "display_mobile_no": true,
                "onoffstatus": "yes",
                "isDeleted": false,
                "isActive": true,
                "language": "5d371e651ae2de8011c4acb5",
                "country": "5d306dc6822fa522c79acd1f",
                "course": "5d498adae72f7708815f0793",
                "education_level": "5d7d6c373f71307502bca5d4",
                "service": [
                    {
                        "_id": "5d832e36bb10351e14c6028c",
                        "package_id": "5d494830238c6455a392cd0f"
                    },
                    {
                        "_id": "5d832e36bb10351e14c6028b",
                        "package_id": "5d49453d94ac5553b49b5b74"
                    }
                ],
                "role": "5d30518c822fa522c7991272",
                "activities": [],
                "createdAt": "2019-09-19T07:28:54.142Z",
                "updatedAt": "2019-09-19T10:20:59.652Z",
                "__v": 0
            }
        }
    ],
    "message": "Mentors list fetched successfully"
}
*/
namedRouter.get("api.user.listmentor", '/users/list-mentor', request_param.any(), async (req, res) => {
    try {
        const success = await userController.listMentors(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /users/remove-mentor Remove Mentor API
 * @apiVersion 1.0.0
 * @apiGroup Users
 * @apiHeader x-access-token User's Access token
 * @apiParam id list ID
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "isDeleted": false,
        "_id": "5d89d71ff334720751633d35",
        "mentor_id": "5d832e36bb10351e14c6028a",
        "student_id": "5d88983bd0ed0d4b3e09132a",
        "createdAt": "2019-09-24T08:43:12.000Z",
        "updatedAt": "2019-09-24T08:43:12.000Z",
        "__v": 0
    },
    "message": "Mentor has removed successfully"
}
*/
namedRouter.post("api.user.removementor", '/users/remove-mentor', request_param.any(), async (req, res) => {
    try {
        const success = await userController.removeMentors(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});




module.exports = router;