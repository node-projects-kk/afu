const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const contactUsController = require('webservice/contactus.controller');
const request_param = multer();


/**
 * @api {post} /contactus/form/save store contact us form
 * @apiVersion 1.0.0
 * @apiGroup ContactUs
 * @apiParam {String} name Name
 * @apiParam {String} phone Phone
 * @apiParam {String} email Email
 * @apiParam {String} message Message
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "name": "test",
        "phone": "7665725",
        "email": "abc@gmail.com",
        "message": "hello",
        "isDeleted": false,
        "status": "Active",
        "_id": "5da6cd1203ef4f0b8c7da30d",
        "createdAt": "2019-10-16T07:56:02.869Z",
        "__v": 0
    },
    "message": "ContactUs Form submitted and Email Send successfully."
}
*/
namedRouter.post("api.contact.form.save", '/contactus/form/save',request_param.any(), async (req, res) => {
    try {
        const success = await contactUsController.contactusformSave(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

// Export the express.Router() instance
module.exports = router;