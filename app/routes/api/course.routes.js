const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const courseController = require('webservice/course.controller');


/**
 * @api {get} /course/list Course Name API
 * @apiVersion 1.0.0
 * @apiGroup Course
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d498ab0e72f7708815f0792",
            "course_name": "Pharmacy",
            "status": "Active",
            "isDeleted": false,
            "education": "5d385f3fd3486b7ae28e0e7d",
            "university": "5d498a26e72f7708815f0791",
            "createdAt": "2019-08-06T14:12:00.886Z",
            "updatedAt": "2019-08-09T05:54:22.266Z",
            "__v": 0
        },
        {
            "_id": "5d498adae72f7708815f0793",
            "course_name": "Human Resourse",
            "status": "Active",
            "isDeleted": false,
            "education": "5d31c1d7822fa522c7abf330",
            "university": "5d497ab20866ed78651160a2",
            "createdAt": "2019-08-06T14:12:42.017Z",
            "updatedAt": "2019-08-08T11:41:55.548Z",
            "__v": 0
        }
    ],
    "message": "Courses fetched Successfully"
}
*/
namedRouter.get("api.course.list", '/course/list', async (req, res) => {
    try {
        const success = await courseController.getList(req);
        res.status(success.status).send(success);
    }
    catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {get} /course/autocomplete-list?keyword= Course Autocomplete API
 * @apiVersion 1.0.0
 * @apiGroup Course
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d498ab0e72f7708815f0792",
            "course_name": "Pharmacy",
            "status": "Active",
            "isDeleted": false,
            "education": "5d385f3fd3486b7ae28e0e7d",
            "university": "5d498a26e72f7708815f0791",
            "createdAt": "2019-08-06T14:12:00.886Z",
            "updatedAt": "2019-08-09T05:54:22.266Z",
            "__v": 0
        },
        {
            "_id": "5d498adae72f7708815f0793",
            "course_name": "Human Resourse",
            "status": "Active",
            "isDeleted": false,
            "education": "5d31c1d7822fa522c7abf330",
            "university": "5d497ab20866ed78651160a2",
            "createdAt": "2019-08-06T14:12:42.017Z",
            "updatedAt": "2019-08-08T11:41:55.548Z",
            "__v": 0
        }
    ],
    "message": "Courses fetched Successfully"
}
*/
namedRouter.get("api.course.autocomplete.list", '/course/autocomplete-list', async (req, res) => {
    try {
        const success = await courseController.getAutocompleteList(req);
        res.status(success.status).send(success);
    }
    catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;