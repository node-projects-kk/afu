const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const mentorController = require('webservice/mentor.controller');
const fs = require('fs');
const request_param = multer();

/**
 * @api {Post} /user/getRoleInfo Get User Role Info
 * @apiVersion 1.0.0
 * @apiGroup Mentor
 * @apiParam {ObjectId} user_id Pass User Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "desc": "This is the mentor role here",
        "_id": "5d30518c822fa522c7991272",
        "role": "mentor",
        "roleDisplayName": "Mentor",
        "id": "5d30518c822fa522c7991272"
    },
    "message": "Role fetched Successfully."
}
*/
namedRouter.post('api.user.getRoleInfo', '/user/getRoleInfo', request_param.any(), async (req, res) => {
    try {
        const success = await mentorController.getRoleInfoBasedOnId(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


namedRouter.all('/mentor*', auth.authenticateAPI);


/**
 * @api {GET} /mentor/dashboard Mentor Dashboard
 * @apiVersion 1.0.0
 * @apiGroup Mentor
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "mentee_count": 2,
        "referral_count": 2
    },
    "message": "Data fetched Successfully."
}
*/
namedRouter.get('api.mentor.dashboard', '/mentor/dashboard', async (req, res) => {
    try {
        const success = await mentorController.getDashboardForMentor(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {GET} /mentor/referralList Mentor Referral List
 * @apiVersion 1.0.0
 * @apiGroup Mentor
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5da96cf47a13dd0f11787aa7",
            "mentor_id": "5da96852df86fa6f3a59c2ab",
            "mentee_id": "5d886c46d0ed0d4b3e091319",
            "createdAt": "2019-09-18T10:59:11.960Z",
            "mentee_info": {
                "_id": "5d886c46d0ed0d4b3e091319",
                "full_name": "Ryan Walker",
                "email": "ryan@gmail.com",
                "profile_image": ""
            }
        },
        {
            "_id": "5da88cec7a13dd0f116c60a2",
            "mentor_id": "5da96852df86fa6f3a59c2ab",
            "mentee_id": "5d835f7f9bbb257b8ad90f10",
            "createdAt": "2019-09-17T10:59:11.960Z",
            "mentee_info": {
                "_id": "5d835f7f9bbb257b8ad90f10",
                "full_name": "Emily gomes",
                "email": "emily_juxecxf_gomes@tfbnw.net",
                "profile_image": ""
            }
        }
    ],
    "message": "Referral information fetched Successfully."
}
*/
namedRouter.get("api.mentor.referralList", '/mentor/referralList', async (req, res) => {
    try {
        const success = await mentorController.getMentorReferralList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

/**
 * @api {GET} /mentor/student/list Mentor Student List & Search
 * @apiVersion 1.0.0
 * @apiGroup Mentor
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} country Pass Country Id
 * @apiParam {ObjectId} course Pass Course Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5da983c04af4213dc238f623",
            "full_name": "Isabela Gomes",
            "role": "student",
            "profile_image": "",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd27",
            "country_name": "Netherlands",
            "isDeleted": false,
            "createdAt": "2019-10-18T07:07:36.885Z"
        },
        {
            "_id": "5da9d6b7bec36e3308dc847f",
            "full_name": "Bill Aniston",
            "role": "student",
            "profile_image": "",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "isDeleted": false,
            "createdAt": "2019-09-18T09:02:11.904Z"
        }
    ],
    "message": "Students fetched successfully."
}
*/
namedRouter.post("api.mentor.student.list", '/mentor/student/list', request_param.any(), async (req, res) => {
    try {
        const success = await mentorController.getStudentListWithSearch(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {Post} /mentor/student/cvAccept Mentor CV Accept
 * @apiVersion 1.0.0
 * @apiGroup Mentor
 * @apiHeader x-access-token Mentor Access token
 * @apiParam {String} file_link Pass File Link
 * @apiParam {ObjectId} student_id Pass Student Id
 * @apiParam {Date} date Pass ISO Date
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "Oliver Queen",
        "surname": "",
        "email": "oliver@gmail.com",
        "password": "$2a$08$qAf6zKEKUTLmtz6WKqz3I.xM5/tkKUNrxU9Ai3guob4EnYQD3np2q",
        "profile_image": "profile_image_1568820826186_IMG_0008.JPG",
        "university_name": "University of Oxford",
        "year_of_study": "2005",
        "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's dd\nstandard dummy text ever since the 1500. \nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500. ",
        "social_id": "",
        "register_type": "normal",
        "school_name": "",
        "is_ambassador": false,
        "logo": "logo_1568820725763_IMG_0007.JPG",
        "address": "25th avenue",
        "gender": "Male",
        "phone": "",
        "dob": "",
        "deviceToken": "fxtfZwbaAWY:APA91bERc884Vc2Nxg4MTcOSbWhvoT-uD2pnccqY2onT4Bd7H2215zG6q8yTeC5p70sri4B9mZB4pIx5gtIBpic21HDLtrSOeSTNn1KDts_OK3iZZKp_C4_WDTDBS5sHWruFdcvjh4-K",
        "deviceType": "android",
        "push_notification": true,
        "display_mobile_no": true,
        "onoffstatus": "yes",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d81ed77c7eb5b0871bbbf06",
        "language": "5d371e651ae2de8011c4acb5",
        "country": "5d306dc6822fa522c79acd1f",
        "course": "5d498adae72f7708815f0793",
        "education_level": "5d385f1ad3486b7ae28e0e7c",
        "service": [
            {
                "_id": "5db990ad12ae0b0ca433d666",
                "package_id": "5d49453d94ac5553b49b5b74"
            },
            {
                "_id": "5db992d40197060d491c2c26",
                "package_id": "5db992d40197060d491c2c25"
            },
            {
                "_id": "5dc172ece6fc140a994a7a59",
                "package_id": "5d493b24f5cd7d4a7fc345f4"
            },
            {
                "_id": "5dc17b28d2f2f032d4d7b38f",
                "package_id": "5d49480d238c6455a392cd0e"
            },
            {
                "_id": "5dc17b7fd2f2f032d4d7b391",
                "package_id": "5dc17b7fd2f2f032d4d7b390"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "activities": [],
        "createdAt": "2019-09-18T08:40:23.781Z",
        "updatedAt": "2019-11-05T15:09:37.175Z",
        "__v": 0,
        "university_id": "5da87d8e60ea3463240240b9",
        "cv_list": [
            {
                "file_link": "https://lh3.googleusercontent.com/k67JmxkY7_Ki4tZUQN5TpWFCz1lCFLo5_iaSSoOUed6VdZDFOgyldiAPCuBw71y5LvZ_KkQ=w640-h400-e365",
                "date": "2019-11-05T20:32:30.151Z",
                "student_id": "5da9e91ebec36e3308dc8482"
            },
            {
                "file_link": "https://lh3.googleusercontent.com/k67JmxkY7_Ki4tZUQN5TpWFCz1lCFLo5_iaSSoOUed6VdZDFOgyldiAPCuBw71y5LvZ_KkQ=w640-h400-e365",
                "date": "2019-11-04T20:35:30.151Z",
                "student_id": "5dbacaf037082e0b880dc712"
            },
            {
                "file_link": "http://www.typo3manual.com/fileadmin/_processed_/a/2/csm_filelinks_v8_ae0e191b9d.jpg",
                "date": "2019-11-04T20:35:30.151Z",
                "student_id": "5dbacaf037082e0b880dc712"
            }
        ]
    },
    "message": "CV saved successfully."
}
*/
namedRouter.post("api.mentor.student.cvAccept", '/mentor/student/cvAccept', request_param.any(), async (req, res) => {
    try {
        const success = await mentorController.saveStudentCVInfo(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

module.exports = router;