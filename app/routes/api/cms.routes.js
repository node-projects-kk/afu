const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const cmsController = require('webservice/cms.controller');


/**
 * @api {get} /cms/:slug CMS by slug
 * @apiVersion 1.0.0
 * @apiGroup CMS
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": {
    "title": "Contact Us",
    "slug": "Contact-Us",
    "content": "<p>Thank you for your order. We will be there ASAP OK?</p>",
    "status": "Active",
    "_id": "5d31d5e3822fa522c7add5fd"
  },
  "message": "CMS fetched Successfully"
}
*/
namedRouter.get("api.cms.slug", '/cms/:slug', async (req, res) => {
    try {
        const success = await cmsController.getCMSBySlug(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;