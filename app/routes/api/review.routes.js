const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const reviewController = require('webservice/review.controller');

namedRouter.all('/reviews*', auth.authenticateAPI);
/**
 * @api {get} /reviews/list/:packageId Review list
 * @apiVersion 1.0.0
 * @apiGroup Review
 * @apiHeader x-access-token User's Access token (Student)
 * @apiParam {ObjectId} packageId Pass Package Id in params
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5db403b69f79fb06defae9fe",
            "rating": 5,
            "review_comment": "Test Comment !!",
            "package_id": "5da56475b470be127dea9a05",
            "package_name": "testAFU001",
            "package_price": 300,
            "package_description": "lorem",
            "package_status": "Active",
            "user_id": "5d83933c70fc2c655c3e02db",
            "user_name": "Will smith",
            "user_email": "will123@gmail.com",
            "createdAt": "2019-10-26T08:28:38.854Z"
        },
        {
            "_id": "5db3f712272dea057b6f5eca",
            "rating": 5,
            "review_comment": "Sample Comment !!",
            "package_id": "5da56475b470be127dea9a05",
            "package_name": "testAFU001",
            "package_price": 300,
            "package_description": "lorem",
            "package_status": "Active",
            "user_id": "5db29da0ea50c357ba6b237d",
            "user_name": "Prithviraj Acharya",
            "user_email": "prithviraj@email.com",
            "createdAt": "2019-10-25T07:34:42.061Z"
        }
    ],
    "message": "Review fetched successfully."
}
*/
namedRouter.get("api.reviews.list", '/reviews/list/:packageId', async (req, res) => {
    try {
        const success = await reviewController.getReviewList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

/**
 * @api {post} /reviews/add  Add Review
 * @apiVersion 1.0.0
 * @apiGroup Review
 * @apiHeader x-access-token User's Access token (Student)
 * @apiParam {Number} rating Rating Count
 * @apiParam {String} review_comment Pass Review Comment
 * @apiParam {ObjectId} review_package_id Pass the Package Id
 * @apiParam {ObjectId} mentor_id Pass the Mentor Id of this Specific Package
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "rating": 4,
        "review_comment": "Test Comment !!",
        "_id": "5db4269c767ff00dade6bf51",
        "review_package_id": "5da56475b470be127dea9a05",
        "mentor_id": "5d9d75947f45992abe8293a7",
        "review_from_user_id": "5d83933c70fc2c655c3e02db",
        "createdAt": "2019-10-26T10:57:32.509Z",
        "updatedAt": "2019-10-26T10:57:32.509Z",
        "__v": 0
    },
    "message": "Review added successfully."
}
*/
namedRouter.post("api.reviews.add", '/reviews/add', request_param.any(), async (req, res) => {
    try {
        const success = await reviewController.addReview(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;