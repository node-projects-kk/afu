const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const universityController = require('webservice/university.controller');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        // console.log("12>>",file);
        if ((file.fieldname).indexOf('profile_image') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/user')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/user');
            }
            cb(null, './public/uploads/user')
        }
        if ((file.fieldname).indexOf('logo') > -1) {
            // check if directory exists
            if (!fs.existsSync('./public/uploads/logo')) {
                // if not create directory
                fs.mkdirSync('./public/uploads/logo');
            }
            cb(null, './public/uploads/logo')
        }
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
    }
})

// const request_param = multer();
// const uploadFile = multer({
//     storage: Storage
// });


const request_param = multer({
    storage: storage
});

/**
 * @api {post} /university/login Login
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiParam email University Email
 * @apiParam password Password
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "university_name": "University of Cambridge",
        "address": "The Old Schools, Trinity Ln, Cambridge CB2 1TN, United Kingdom",
        "city": "Cambridge",
        "state": "Cambridge",
        "postal_code": "12345",
        "email": "cambridge@test.com",
        "password": "$2a$08$pmxYe2eoPIAqnH7wFYoo1.vpglLy4ElUUY0CKaLLrCAi3xvrY4XRu",
        "phone": "1234568990",
        "website": "https://www.cam.ac.uk",
        "registration_no": "CM1254222210022",
        "deviceToken": "",
        "deviceType": "web",
        "status": "Active",
        "isDeleted": false,
        "_id": "5da863334114084d6168ceeb",
        "country": "5d306dc6822fa522c79acd1f",
        "createdAt": "2019-10-17T12:48:51.369Z",
        "updatedAt": "2019-10-17T12:48:51.369Z"
    },
    "isLoggedIn": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkYTg2MzMzNDExNDA4NGQ2MTY4Y2VlYiIsImlhdCI6MTU3MTMxODA4OSwiZXhwIjoxNTcxNDA0NDg5fQ.9znTvvzcrkAOiGjkJa8oaPMuxGk4N7E8UzQD3pYbr9A",
    "message": "Login successful."
}
*/
namedRouter.post("api.university.login", '/university/login', request_param.any(), async (req, res) => {
    try {
        const success = await universityController.login(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        console.log('route error', error.message);
        res.status(error.status).send(error);
    }
});


/**
 * @api {get} /university/list University Name API
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d497ab20866ed78651160a2",
            "university_name": "University of Birmingham",
            "address": "Birmingham B15 2TT",
            "city": "Edgbaston",
            "state": "Birmingham",
            "postal_code": "7891",
            "email": "oxford@gmail.com",
            "phone": "789456123",
            "website": "www.oxford.com",
            "registration_no": "78456123",
            "status": "Active",
            "isDeleted": false,
            "country": "5d306dc6822fa522c79acd1f",
            "createdAt": "2019-08-06T13:03:46.702Z",
            "updatedAt": "2019-08-09T06:07:38.157Z",
            "__v": 0
        },
        {
            "_id": "5d498a26e72f7708815f0791",
            "university_name": "University of Bristol",
            "address": "London",
            "city": "London",
            "state": "London",
            "postal_code": "EH12",
            "email": "chembridge@gmail.com",
            "phone": "4523698563",
            "website": "www.chembridge.com",
            "registration_no": "452136523654",
            "status": "Active",
            "isDeleted": false,
            "country": "5d306dc6822fa522c79acd1f",
            "createdAt": "2019-08-06T14:09:42.237Z",
            "updatedAt": "2019-09-11T12:48:00.898Z",
            "__v": 0
        }
    ],
    "message": "Universities fetched successfully"
}
*/
namedRouter.get("api.university.list", '/university/list', async (req, res) => {
    try {
        const success = await universityController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {get} /university/autocomplete-list?keyword= University Autocomplete List API
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d497ab20866ed78651160a2",
            "university_name": "University of Birmingham",
            "address": "Birmingham B15 2TT",
            "city": "Edgbaston",
            "state": "Birmingham",
            "postal_code": "7891",
            "email": "oxford@gmail.com",
            "phone": "789456123",
            "website": "www.oxford.com",
            "registration_no": "78456123",
            "status": "Active",
            "isDeleted": false,
            "country": "5d306dc6822fa522c79acd1f",
            "createdAt": "2019-08-06T13:03:46.702Z",
            "updatedAt": "2019-08-09T06:07:38.157Z",
            "__v": 0
        },
        {
            "_id": "5d498a26e72f7708815f0791",
            "university_name": "University of Bristol",
            "address": "London",
            "city": "London",
            "state": "London",
            "postal_code": "EH12",
            "email": "chembridge@gmail.com",
            "phone": "4523698563",
            "website": "www.chembridge.com",
            "registration_no": "452136523654",
            "status": "Active",
            "isDeleted": false,
            "country": "5d306dc6822fa522c79acd1f",
            "createdAt": "2019-08-06T14:09:42.237Z",
            "updatedAt": "2019-09-11T12:48:00.898Z",
            "__v": 0
        }
    ],
    "message": "Universities fetched successfully"
}
*/
namedRouter.get("api.university.autocomplete.list", '/university/autocomplete-list', async (req, res) => {
    try {
        const success = await universityController.getAutocompleteList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

namedRouter.all('/university*', auth.authenticateAPI);

/**
 * @api {GET} /university/getprofile Get Profile
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiHeader x-access-token University's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5da87d8e60ea3463240240b8",
        "university_name": "University of Oxford",
        "profile_image": "",
        "address": "Oxford OX1 2JD, United Kingdom",
        "city": "Oxford",
        "state": "Oxford",
        "postal_code": "12345",
        "email": "oxford@test.com",
        "phone": "1234568990",
        "website": "www.test.com",
        "registration_no": "OXF12200025",
        "country": "5d306dc6822fa522c79acd1f",
        "country_name": "United Kingdom"
    },
    "message": "Profile Info fetched Successfully"
}
*/

namedRouter.get('api.university.getprofile', '/university/getprofile', async (req, res) => {
    try {
        const success = await universityController.getUniversityProfile(req);
        // console.log(success);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /university/updateprofile Update Profile
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiHeader x-access-token University's Access token
 * @apiParam university_name University Name
 * @apiParam address Address
 * @apiParam city City
 * @apiParam state State
 * @apiParam postal_code Postal Code
 * @apiParam phone Phone Number
 * @apiParam website Website
 * @apiParam registration_no Registration Number
 * @apiParam profile_image Profile Image
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5da87d8e60ea3463240240b9",
        "university_name": "University of Oxford",
        "address": "Oxford OX1 2JD, United Kingdom",
        "city": "Oxford",
        "state": "Oxford",
        "postal_code": "12345",
        "email": "abcd@univ.com",
        "phone": "22222222221",
        "website": "www.test.com",
        "registration_no": "OXF12200025",
        "deviceToken": "",
        "deviceType": "web",
        "status": "Active",
        "isDeleted": false,
        "country": "5d306dc6822fa522c79acd1f",
        "user_id": "5da87d8e60ea3463240240b8",
        "createdAt": "2019-10-17T14:41:18.680Z",
        "updatedAt": "2019-10-18T14:10:45.203Z"
    },
    "message": "Profile updated successfully"
}
*/

namedRouter.post('api.university.updateprofile', '/university/updateprofile', request_param.any(), async (req, res) => {
    try {
        const success = await universityController.updateProfile(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {GET} /university/logout Logout
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiParam university_id University Id
 * @apiHeader x-access-token University's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "isLoggedIn": false,
    "message": "Logout Successfully"
}
*/
namedRouter.get("api.university.logout", '/university/logout', request_param.any(), async (req, res) => {
    try {
        const success = await universityController.logout(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        console.log(error.message);
        res.status(error.status).send(error);
    }
});


/**
 * @api {GET} /university/dashboard Dashboard
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiHeader x-access-token University's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "count": {
            "mentor": 36,
            "referral": 0,
            "ambassador": 0
        },
        "top_users": [
            {
                "_id": "5d81ed77c7eb5b0871bbbf06",
                "full_name": "Oliver Queen",
                "profile_image": "profile_image_1568820826186_IMG_0008.JPG",
                "earning": 60,
                "packages_sold": 2,
                "active_from": "2019-09-18T08:40:23.781Z"
            },
            {
                "_id": "5d935fb6350a2b544c14a630",
                "full_name": "John Wells",
                "profile_image": "",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-01T14:16:22.438Z"
            },
            {
                "_id": "5d935d15350a2b544c14a62b",
                "full_name": "test4",
                "profile_image": "",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-01T14:05:09.800Z"
            },
            {
                "_id": "5d9dd457bf9680442582b6ba",
                "full_name": "Jonath James",
                "profile_image": "profile_image_1570624598932_userJonath.jpeg",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-09T12:36:39.172Z"
            },
            {
                "_id": "5da44059b470be127dea99ef",
                "full_name": "Aidan Carr",
                "profile_image": "",
                "earning": 0,
                "packages_sold": 0,
                "active_from": "2019-10-14T09:31:05.490Z"
            }
        ]
    },
    "message": "Dashboard Info fetched Successfully"
}
*/

namedRouter.get('api.university.dashboard', '/university/dashboard', async (req, res) => {
    try {
        // console.log('university dashboard');
        const success = await universityController.getDashboard(req, res);
        // console.log(success);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /university/mentor/list Mentor List with Search
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiHeader x-access-token User's Access token
 * @apiParam country Mentor Country Id (Optional)
 * @apiParam course Mentor Course Id (Optional)
 * @apiParam university University Name (Optional)
 * @apiSuccessExample {json} Success  -- Provide Country
 *{
    "status": 200,
    "data": [
        {
            "_id": "5da96852df86fa6f3a59c2ab",
            "full_name": "Annabel Smith",
            "role": "mentor",
            "profile_image": "profile_image_1571383378077_IMG_1571383061674",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "students": 1,
            "earning": 1023,
            "packages_sold": 1,
            "active_from": "2019-10-18T07:22:58.298Z",
            "isDeleted": false,
            "isActive": true
        },
        {
            "_id": "5d81ed77c7eb5b0871bbbf06",
            "full_name": "Oliver Queen",
            "role": "mentor",
            "profile_image": "profile_image_1568820826186_IMG_0008.JPG",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "students": 1,
            "earning": 60,
            "packages_sold": 2,
            "active_from": "2019-09-18T08:40:23.781Z",
            "isDeleted": false,
            "isActive": true
        }
    ],
    "message": "Mentor list fetched successfully"
}
* @apiSuccessExample {json} Success  -- Provide Country & Course
*{
    "status": 200,
    "data": [
        {
            "_id": "5da96852df86fa6f3a59c2ab",
            "full_name": "Annabel Smith",
            "role": "mentor",
            "profile_image": "profile_image_1571383378077_IMG_1571383061674",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "students": 1,
            "earning": 1023,
            "packages_sold": 1,
            "active_from": "2019-10-18T07:22:58.298Z",
            "isDeleted": false,
            "isActive": true
        },
        {
            "_id": "5d81ed77c7eb5b0871bbbf06",
            "full_name": "Oliver Queen",
            "role": "mentor",
            "profile_image": "profile_image_1568820826186_IMG_0008.JPG",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "students": 1,
            "earning": 60,
            "packages_sold": 2,
            "active_from": "2019-09-18T08:40:23.781Z",
            "isDeleted": false,
            "isActive": true
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.post("api.university.mentor.list", '/university/mentor/list', request_param.any(), async (req, res) => {
    try {
        const success = await universityController.mentorList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /university/student/list Student List with Search
 * @apiVersion 1.0.0
 * @apiGroup University
 * @apiHeader x-access-token User's Access token
 * @apiParam country Student Country Id (Optional)
 * @apiParam course Student Course Id (Optional)
 * @apiParam university University Name (Optional)
 * @apiSuccessExample {json} Success  -- Provide Country
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d81f293af7f7442362ab99e",
            "full_name": "Bill Aniston",
            "role": "student",
            "profile_image": "",
            "mentor_name": "Oliver Queen",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "active_from": "2019-09-18T09:02:11.904Z",
            "isDeleted": false,
            "isActive": true
        }
    ],
    "message": "Student list fetched successfully"
}
* @apiSuccessExample {json} Success  -- Provide Country & Course
*{
    "status": 200,
    "data": [
        {
            "_id": "5d81f293af7f7442362ab99e",
            "full_name": "Bill Aniston",
            "role": "student",
            "profile_image": "",
            "mentor_name": "Oliver Queen",
            "course_id": "5d498adae72f7708815f0793",
            "course_name": "Business Studies",
            "country_id": "5d306dc6822fa522c79acd1f",
            "country_name": "United Kingdom",
            "active_from": "2019-09-18T09:02:11.904Z",
            "isDeleted": false,
            "isActive": true
        }
    ],
    "message": "Student list fetched successfully"
}
*/
namedRouter.post("api.university.student.list", '/university/student/list', request_param.any(), async (req, res) => {
    try {
        const success = await universityController.studentList(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

module.exports = router;