const express = require('express');
const routeLabel = require('route-label');
const multer = require('multer');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');
const staticController = require('webservice/static.controller');
const chatController = require('webservice/chat.controller');

//const request_param = multer();
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		console.log("12>>",file);
		if ((file.fieldname).indexOf('profile_image') > -1) {
			// check if directory exists
			if (!fs.existsSync('./public/uploads/user')) {
				// if not create directory
				fs.mkdirSync('./public/uploads/user');
			}
			cb(null, './public/uploads/user')
		}
		if ((file.fieldname).indexOf('logo') > -1) {
			// check if directory exists
			if (!fs.existsSync('./public/uploads/logo')) {
				// if not create directory
				fs.mkdirSync('./public/uploads/logo');
			}
			cb(null, './public/uploads/logo')
		}
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
	}
})
const request_param = multer({storage: storage});


/**
 * @api {get} /splash Splash Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Splash Page fetch successfully."
}
*/
namedRouter.get("api.static.splash", '/splash', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.splash(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /loadingpage Loading page student
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Loading Page fetch successfully."
}
*/
namedRouter.get("api.static.loadingpage", '/loadingpage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.loadingpage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /expertise Expertise Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Expertise Page fetch successfully."
}
*/
namedRouter.get("api.static.expertise", '/expertise', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.expertise(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /yourwork Your work Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Yourwork Page fetch successfully."
}
*/
namedRouter.get("api.static.yourwork", '/yourwork', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.yourwork(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /extracurry Extra Curriculum Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Extracurriculam Page fetch successfully."
}
*/
namedRouter.get("api.static.extracurry", '/extracurry', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.extracurry(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /youreducation Your Education Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Your educcation Page fetch successfully."
}
*/
namedRouter.get("api.static.youreducation", '/youreducation', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.youreducation(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /aboutme About Me Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Aboutme Page fetch successfully."
}
*/
namedRouter.get("api.static.aboutme", '/aboutme', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.aboutme(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /logo Logo API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "logo": "logo_1565791258336_colin.jpg",
  }
    "message": "Logo fetch successfull."
}
*/
namedRouter.get("api.static.logo", '/logo', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.logo(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /title Title API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "title": "Apply For University",
  }
    "message": "Title fetch successfull."
}
*/
namedRouter.get("api.static.title", '/title', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.title(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /welcomepage Welcome Page
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Welcome Page fetch successfully."
}
*/
namedRouter.get("api.static.welcomepage", '/welcomepage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.welcomepage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /welcometext Welcome Text
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"text":"Welcome to AFU"
  }
    "message": "Welcome Page text fetch successfully."
}
*/
namedRouter.get("api.static.welcometext", '/welcometext', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.welcometext(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /usertypeselection User Type Selection
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
   }
    "message": "User Type Selection Page fetch successfully."
}
*/
namedRouter.get("api.static.usertypeselection", '/usertypeselection', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.usertypeselection(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /usertype Usertype API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"type":"Mentor,Student"
  }
    "message": "Usertype fetch successfully."
}
*/
namedRouter.get("api.static.usertype", '/usertype', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.usertype(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /signuppage Signup Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Sign up page fetch successfully."
}
*/
namedRouter.get("api.static.signuppage", '/signuppage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.signuppage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /studentsigninpage 
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Student Sign In Page fetch successfull."
}
*/
namedRouter.get("api.static.studentsigninpage", '/studentsigninpage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentsigninpage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /mentorsigninpage 
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Mentor Sign In Page fetch successfull."
}
*/
namedRouter.get("api.static.mentorsigninpage", '/mentorsigninpage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorsigninpage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/signin Student Login
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiParam email Student Email 
 * @apiParam password Student Password
 * @apiParam social_id Social ID (If Social Login)
 * @apiParam register_type Register Type (facebook/google/normal/linkedin/instagram)
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra@gmail.com",
        "password": "$2a$08$gnoPVZI9yuN1hqDuLHj2ZOYEgrMjQqe70.exh7.3wZr2CdDcMwjAG",
        "profile_image": "",
        "school_name": "",
        "education_level": "Masters degree ",
        "want_to": "",
        "address": "London",
        "university_name": "RKMV",
        "gender": "Male",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "specialization": [
            {
                "title": "project one",
                "_id": "5d3188206d84b8436af1ce39"
            },
            {
                "title": "http://www.projectone.com",
                "_id": "5d3188206d84b8436af1ce38"
            }
        ],
        "role": "5d30518c822fa522c7991274",
        "project": [],
        "activities": [],
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-07-19T09:06:40.023Z",
        "__v": 0
    },
    "isLoggedIn": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0",
    "message": "Login successfull Student."
}
*/
namedRouter.post("api.student.signin", '/student/signin', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentsignin(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/signin Mentor Login
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiParam email Mentor Email 
 * @apiParam password Mentor Password
 * @apiParam social_id Social ID (If Social Login)
 * @apiParam register_type Register Type (facebook/google/normal/linkedin/instagram)
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "Kalyan",
        "email": "kalyan@gmail.com",
        "password": "$2a$08$gnoPVZI9yuN1hqDuLHj2ZOYEgrMjQqe70.exh7.3wZr2CdDcMwjAG",
        "profile_image": "",
        "school_name": "",
        "education_level": "Masters degree ",
        "want_to": "",
        "address": "London",
        "university_name": "RKMV",
        "gender": "Male",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "specialization": [
            {
                "title": "project one",
                "_id": "5d3188206d84b8436af1ce39"
            },
            {
                "title": "http://www.projectone.com",
                "_id": "5d3188206d84b8436af1ce38"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "project": [],
        "activities": [],
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-07-19T09:06:40.023Z",
        "__v": 0
    },
    "isLoggedIn": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0",
    "message": "Login successfull Mentor."
}
*/
namedRouter.post("api.mentor.signin", '/mentor/signin', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorsignin(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /mentor/step1  Mentor Signup step One
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Mentor
 * {
    "status": 200,
    "data": {
        "full_name": "Kalyan",
        "email": "kalyan@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "school_name": "",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "message": "Registration step one successfull."
}
*/
namedRouter.post("api.mentor.step1", '/mentor/step1', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorstep1(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /mentor/step2  Mentor Signup step Two
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Mentor
 * {
    "status": 200,
    "data": {
        "full_name": "Kalyan",
        "email": "kalyan@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzViYjRhMzgxMDQ3NjU2NDJkMmIyMSIsImlhdCI6MTU2MzgwMjQ0MiwiZXhwIjoxNTYzODg4ODQyfQ.Ba9x4uJZva7d6zm2KOPjOHCSoM-br3M0uFKAKSZP1zY",
    "message": "Registration step two successfull."
}
*/
namedRouter.post("api.mentor.step2", '/mentor/step2', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorstep2(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /mentor/step3  Mentor Signup step Three
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Mentor
 * {
    "status": 200,
    "data": {
        "full_name": "Kalyan",
        "email": "kalyan@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "message": "Registration step three successfull."
}
*/
namedRouter.post("api.mentor.step3", '/mentor/step3', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorstep3(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /mentor/step4  Mentor Signup step Four
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Mentor
 * {
    "status": 200,
       "data": {
        "full_name": "Kalyan",
        "email": "kalyan@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "profile_image": "profile_image_1563802441970_img2.jpg",
        "school_name": "",
        "logo": "logo_1563802441971_gc.jpg",
        "short_description": "test description",
        "address": "London",
        "university_name": "RKMV",
        "gender": "Male",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "education_level": "5d31c1a0822fa522c7abefca",
        "specialization": [
            {
                "title": "project one",
                "_id": "5d35bb4a38104765642d2b23"
            },
            {
                "title": "http://www.projectone.com",
                "_id": "5d35bb4a38104765642d2b22"
            }
        ],
        "service": [
            {
                "name": "Service One",
                "time": "2",
                "details": "service details one",
                "cost": "75",
                "_id": "5d35bb4a38104765642d2b25"
            },
            {
                "name": "Service Two",
                "time": "3",
                "details": "service details two",
                "cost": "100",
                "_id": "5d35bb4a38104765642d2b24"
            }
        ],
        "role": "5d30518c822fa522c7991272",
        "project": [],
        "activities": [],
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzViYjRhMzgxMDQ3NjU2NDJkMmIyMSIsImlhdCI6MTU2MzgwMjQ0MiwiZXhwIjoxNTYzODg4ODQyfQ.Ba9x4uJZva7d6zm2KOPjOHCSoM-br3M0uFKAKSZP1zY",
    },
    "message": "Registration successfull."
}
*/
namedRouter.post("api.mentor.step4", '/mentor/step4', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorstep4(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /student/step1  Student Signup step One
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Student
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "school_name": "",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "message": "Registration step one successfull."
}
*/
namedRouter.post("api.student.step1", '/student/step1', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentstep1(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /student/step2  Student Signup step Two
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Student
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "message": "Registration  step2 successfull."
}
*/
namedRouter.post("api.student.step2", '/student/step2', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentstep2(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /student/step3  Student Signup step Three
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Student
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "country": "5d306dc6822fa522c79acd21",
        "language": "5d306346822fa522c79a2dec",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "message": "Registration step3 successfull."
}
*/
namedRouter.post("api.student.step3", '/student/step3', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentstep3(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /student/step4  Student Signup step Four
* @apiVersion 1.0.0
* @apiGroup REST API
* 
 * @apiSuccessExample {json} Success Student
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$D/GDWDaQm1M5ykmC.pvXKeQzQBAXn8KWl33sx0Vyo47VeystRkqcu",
        "profile_image": "",
        "university_name": "",
        "year_of_study": "",
        "short_description": "",
        "social_id": "",
        "register_type": "normal",
        "school_name": "Nava Nalanda",
        "logo": "",
        "address": "",
        "gender": "Male",
        "phone": "",
        "dob": "",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "display_mobile_no": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d668a1d37aa6464abeeb44a",
        "language": "5d371e651ae2de8011c4acb5",
        "country": "5d306dc6822fa522c79acd1f",
        "education_level": "5d385f14d3486b7ae28e0e7b",
        "want_to": "5d31c5b8822fa522c7ac5585",
        "role": "5d30518c822fa522c7991274",
        "service": [],
        "createdAt": "2019-08-28T14:05:17.096Z",
        "updatedAt": "2019-08-28T14:05:17.096Z",
        "__v": 0
    },
    "message": "Registration  step4 successfull."
}
*/
namedRouter.post("api.student.step4", '/student/step4', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentstep4(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /student/finalstep  Student Sign up Final step
* @apiVersion 1.0.0
* @apiGroup REST API
* 
* @apiSuccessExample {json} Success Student
 *{
    "status": 200,
    "data": {
        "full_name": "Ayan",
        "email": "ayan1@gmail.com",
        "password": "$2a$08$D/GDWDaQm1M5ykmC.pvXKeQzQBAXn8KWl33sx0Vyo47VeystRkqcu",
        "profile_image": "",
        "university_name": "",
        "year_of_study": "",
        "short_description": "",
        "social_id": "",
        "register_type": "normal",
        "school_name": "Nava Nalanda",
        "logo": "",
        "address": "",
        "gender": "Male",
        "phone": "",
        "dob": "",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "display_mobile_no": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d668a1d37aa6464abeeb44a",
        "language": "5d371e651ae2de8011c4acb5",
        "country": "5d306dc6822fa522c79acd1f",
        "education_level": "5d385f14d3486b7ae28e0e7b",
        "want_to": "5d31c5b8822fa522c7ac5585",
        "activities": [
            {
                "title": "Cooking",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44e"
            },
            {
                "title": "App Developing",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44d"
            },
            {
                "title": "Programming",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44c"
            },
            {
                "title": "Teaching",
                "tag": "",
                "_id": "5d668a1d37aa6464abeeb44b"
            }
        ],
        "role": "5d30518c822fa522c7991274",
        "service": [],
        "createdAt": "2019-08-28T14:05:17.096Z",
        "updatedAt": "2019-08-28T14:05:17.096Z",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNjY4YTFkMzdhYTY0NjRhYmVlYjQ0YSIsImlhdCI6MTU2NzAwMTExNywiZXhwIjoxNTY3MDg3NTE3fQ.sV8Rmz3jdSLQR8tQPBXPdv-R8El1l5VcK0JixR0g81o",
    "message": "Registration successfull."
}
*/
namedRouter.post("api.student.finalstep", '/student/finalstep', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentfinalstep(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/assignedjob View assigned Job by mentee API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Job list fetched successfully"
}
*/
namedRouter.post("api.mentor.assignjob", '/mentor/assignedjob', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorassignjob(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/job/mymentees My Mentees
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Mentees fetched successfully"
}
*/
namedRouter.post("api.mentor.mymentees", '/mentor/job/mymentees', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentormymentees(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/deletementees Delete Mentees
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
    ],
    "message": "Mentees deleted successfully"
}
*/
namedRouter.post("api.mentor.deletementees", '/mentor/deletementees', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentordeletementees(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/subscribedmentees List Subscribed Mentees
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "subscribe":true,
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        },
        {
            "_id": "5d526b57a47f2f2ab1d6569b",
            "name": "James Bond",
            "email": "james@gmail.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "subscribe":true,
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Mentees fetched successfully"
}
*/
namedRouter.post("api.mentor.subscribedmentees", '/mentor/subscribedmentees', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorsubscribedmentees(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/menteessearch Filter mentees API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam country Country Name
 * @apiParam course Course name of Student
  * @apiParam university University name
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Mentees list fetched successfully"
}
*/
namedRouter.post("api.student.menteessearch", '/student/menteessearch', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentmenteessearch(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/filter Filter Mentor API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d3189f0719a7e481b879cb1",
            "name": "Art Venere",
            "email": "art@venere.org",
            "password": "$2a$08$zPkgkuWbLqIepAojvcOW9.svpljrsvlS9bf6cBR.lM2GCYY8bMtQm",
            "profile_image": "profile_image_1563527664279_img3.jpg",
            "address": "London",
            "university_name": "Oxford",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3189f0719a7e481b879cb4"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3189f0719a7e481b879cb5"
                }
            ],
            "specialization": [
                {
                    "title": "Entrepreneurship",
                    "_id": "5d3189f0719a7e481b879cb2"
                },
                {
                    "title": "Consulting",
                    "_id": "5d3189f0719a7e481b879cb3"
                }
            ]
        },       
    ],
    "message": "Mentor list fetched successfully"
}
* @apiSuccessExample {json} Success  -- Provide Address & Specialization
*{
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "name": "Josephine Darakjy",
            "email": "josephine_darakjy@darakjy.org",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "address": "London",
            "university_name": "Chembridge",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ],
            "specialization": [
                {
                    "title": "Finance",
                    "_id": "5d3188206d84b8436af1ce39"
                }
            ]
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.post("api.mentor.filter", '/mentor/filter', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorfilter(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/country Search By Country API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam country Country Name
* @apiSuccessExample {json} Success  -- Provide Address & Specialization
*{
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "name": "Josephine Darakjy",
            "email": "josephine_darakjy@darakjy.org",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "address": "London",
            "university_name": "Chembridge",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ],
            "specialization": [
                {
                    "title": "Finance",
                    "_id": "5d3188206d84b8436af1ce39"
                }
            ]
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.post("api.mentor.country", '/mentor/country', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorcountry(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/course Search By Course API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam course Course Name
* @apiSuccessExample {json} Success
*{
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "name": "Josephine Darakjy",
            "email": "josephine_darakjy@darakjy.org",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "address": "London",
            "university_name": "Chembridge",
            "gender": "Male",
            "short_description": "test description",
            "language": "English",
            "country": "United Kingdom",
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ],
            "specialization": [
                {
                    "title": "Finance",
                    "_id": "5d3188206d84b8436af1ce39"
                }
            ]
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.post("api.mentor.course", '/mentor/course', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorcourse(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {GET} /student/getprofilestudent Student Profile
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "profile": {
                "_id": "5d3188206d84b8436af1ce37",
                "full_name": "Subhra",
                "email": "subhra@gmail.com",
                "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                "profile_image": "",
                "school_name": "",
                "education_level": "5d31c1a0822fa522c7abefca",
                "want_to": "",
                "address": "London",
                "university_name": "RKMV",
                "gender": "Male",
                "dob": "",
                "social_id": "",
                "register_type": "normal",
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "country": "5d306dc6822fa522c79acd21",
                "language": "5d306346822fa522c79a2dec",
                "specialization": [
                    {
                        "title": "project one",
                        "_id": "5d3188206d84b8436af1ce39"
                    },
                    {
                        "title": "http://www.projectone.com",
                        "_id": "5d3188206d84b8436af1ce38"
                    }
                ],
                "service": {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                },
                "role": "5d30518c822fa522c7991272",
                "project": [],
                "activities": [],
                "push_notification": true,
                "short_description": "test description",
                "createdAt": "2019-07-19T09:06:40.023Z",
                "updatedAt": "2019-07-19T13:35:04.720Z",
                "__v": 0,
                "user_role": {
                    "_id": "5d30518c822fa522c7991272",
                    "role": "mentor",
                    "desc": "This is the mentor role here",
                    "roleDisplayName": "Mentor"
                },
                "edu_level": {
                    "_id": "5d31c1a0822fa522c7abefca",
                    "level_name": "Level 01",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-04-15T08:24:40.827Z",
                    "__v": 0
                }
            },
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ]
        }
    ],
    "message": "Student Profile Info fetched Successfully"
}
*/
namedRouter.get("api.student.profile", '/student/getprofilestudent', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.getprofilestudent(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {GET} /mentor/getprofilementor Single Mentor Profile
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "profile": {
                "_id": "5d3188206d84b8436af1ce37",
                "full_name": "Kalyan",
                "email": "kalyan@gmail.com",
                "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                "profile_image": "",
                "school_name": "",
                "education_level": "5d31c1a0822fa522c7abefca",
                "want_to": "",
                "address": "London",
                "university_name": "RKMV",
                "gender": "Male",
                "dob": "",
                "social_id": "",
                "register_type": "normal",
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "country": "5d306dc6822fa522c79acd21",
                "language": "5d306346822fa522c79a2dec",
                "specialization": [
                    {
                        "title": "project one",
                        "_id": "5d3188206d84b8436af1ce39"
                    },
                    {
                        "title": "http://www.projectone.com",
                        "_id": "5d3188206d84b8436af1ce38"
                    }
                ],
                "service": {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                },
                "role": "5d30518c822fa522c7991272",
                "project": [],
                "activities": [],
                "push_notification": true,
                "short_description": "test description",
                "createdAt": "2019-07-19T09:06:40.023Z",
                "updatedAt": "2019-07-19T13:35:04.720Z",
                "__v": 0,
                "user_role": {
                    "_id": "5d30518c822fa522c7991272",
                    "role": "mentor",
                    "desc": "This is the mentor role here",
                    "roleDisplayName": "Mentor"
                },
                "edu_level": {
                    "_id": "5d31c1a0822fa522c7abefca",
                    "level_name": "Level 01",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-04-15T08:24:40.827Z",
                    "__v": 0
                }
            },
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ]
        }
    ],
    "message": "Mentor Profile Info fetched Successfully"
}
*/
namedRouter.get("api.mentor.profile", '/mentor/getprofilementor', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.getprofilementor(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {GET} /mentor/getprofiledetails Mentor Details
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d3188206d84b8436af1ce37",
            "profile": {
                "_id": "5d3188206d84b8436af1ce37",
                "full_name": "Kalyan",
                "email": "kalyan@gmail.com",
                "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                "profile_image": "",
                "school_name": "",
                "education_level": "5d31c1a0822fa522c7abefca",
                "want_to": "",
                "address": "London",
                "university_name": "RKMV",
                "gender": "Male",
                "dob": "",
                "social_id": "",
                "register_type": "normal",
                "deviceToken": "",
                "deviceType": "",
                "isDeleted": false,
                "isActive": true,
                "country": "5d306dc6822fa522c79acd21",
                "language": "5d306346822fa522c79a2dec",
                "specialization": [
                    {
                        "title": "project one",
                        "_id": "5d3188206d84b8436af1ce39"
                    },
                    {
                        "title": "http://www.projectone.com",
                        "_id": "5d3188206d84b8436af1ce38"
                    }
                ],
                "service": {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                },
                "role": "5d30518c822fa522c7991272",
                "project": [],
                "activities": [],
                "push_notification": true,
                "short_description": "test description",
                "createdAt": "2019-07-19T09:06:40.023Z",
                "updatedAt": "2019-07-19T13:35:04.720Z",
                "__v": 0,
                "user_role": {
                    "_id": "5d30518c822fa522c7991272",
                    "role": "mentor",
                    "desc": "This is the mentor role here",
                    "roleDisplayName": "Mentor"
                },
                "edu_level": {
                    "_id": "5d31c1a0822fa522c7abefca",
                    "level_name": "Level 01",
                    "status": "Active",
                    "isDeleted": false,
                    "createdAt": "2019-04-15T08:24:40.827Z",
                    "__v": 0
                }
            },
            "service": [
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                },
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                }
            ]
        }
    ],
    "message": "Mentor Profile Info fetched Successfully"
}
*/
namedRouter.get("api.mentor.details", '/mentor/getprofiledetails', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.getprofiledetails(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/addInterest Add Interest Tag API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 *@apiHeader x-access-token User's Access token
 *@apiParam tag name Tag Name Array
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [{
	"tag":"Interest One"
  },
  {
	"tag":"Interest Two"
  }],
    "message": "Add tag of Interest successfully."
}
*/
namedRouter.post("api.student.addInterest", '/student/addInterest', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.addInterest(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/addActivity Add Activity Tag API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 *@apiHeader x-access-token User's Access token
 *@apiParam tag name Tag Name Array
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [{
	"tag":"Activity One"
  },
  {
	"tag":"Activity Two"
  }],
    "message": "Add activity tag successfully."
}
*/
namedRouter.post("api.student.addActivity", '/student/addActivity', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.addActivity(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/addClubs Add Club Tag API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 *@apiHeader x-access-token User's Access token
 *@apiParam tag name Tag Name Array
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [{
	"tag":"Club One"
  },
  {
	"tag":"Club Two"
  }],
    "message": "Add clubs tag successfully."
}
*/
namedRouter.post("api.student.addClubs", '/student/addClubs', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.addClubs(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/addSocieties Add Societies Tag API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 *@apiHeader x-access-token User's Access token
 *@apiParam tag name Tag Name Array
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [{
	"tag":"Society One"
  },
  {
	"tag":"Society Two"
  }],
    "message": "Add society tag successfully."
}
*/
namedRouter.post("api.student.addSocieties", '/student/addSocieties', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.addSocieties(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/job/search/country Search By Location
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam country Country Name
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Job fetched successfully"
}
*/
namedRouter.post("api.mentor.jobsearchcountry", '/mentor/job/search/country', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorjobsearchcountry(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/job/search/course Search by Course
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam course Course name
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "Royal Grammar School",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": "",
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Student list fetched successfully"
}
*/
namedRouter.post("api.mentor.jobsearch", '/mentor/job/search/course', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorjobsearchcourse(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /mentor/job/search/university Search by university
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam university University Name
  * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a6",
            "name": "Albina Glick",
            "email": "albina@glick.com",
            "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
            "profile_image": "",
            "school_name": "",
            "short_description": "Test Description",
            "address": "128 Bransten Rd",
            "university_name": ""Oxford,
            "gender": "Male",
            "phone": "7894561236",
            "activities": [
                [
                    {
                        "title": "Activity One",
                        "tag": "Interest",
                        "_id": "5d526b57a47f2f2ab1d657a8"
                    },
                    {
                        "title": "Activity Two",
                        "tag": "Club",
                        "_id": "5d526b57a47f2f2ab1d657a7"
                    }
                ]
            ],
            "specialization": [
                {
                    "_id": "5d498adae72f7708815f0793",
                    "course_name": "Human Resourse",
                    "status": "Active",
                    "isDeleted": false,
                    "education": "5d31c1d7822fa522c7abf330",
                    "university": "5d497ab20866ed78651160a2",
                    "createdAt": "2019-08-06T14:12:42.017Z",
                    "updatedAt": "2019-08-08T11:41:55.548Z",
                    "__v": 0
                }
            ],
            "country": "Germany",
            "language": "French",
            "want_to": {
                "_id": "5d31c5b8822fa522c7ac5585",
                "name": "Study medicine at university",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            },
            "project": [
                {
                    "_id": "5d526b57a47f2f2ab1d657aa",
                    "project_name": "project two",
                    "project_url": "http://www.projecttwo.com",
                    "project_details": "project details two",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T07:48:39.390Z",
                    "__v": 0
                },
                {
                    "_id": "5d526b57a47f2f2ab1d657a9",
                    "project_name": "Apply For University",
                    "project_url": "www.afu.com",
                    "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                    "status": "Active",
                    "isDeleted": false,
                    "user_id": "5d526b57a47f2f2ab1d657a6",
                    "createdAt": "2019-08-13T07:48:39.390Z",
                    "updatedAt": "2019-08-13T09:37:32.578Z",
                    "__v": 0
                }
            ]
        }
    ],
    "message": "Student list fetched successfully"
}
*/
namedRouter.post("api.mentor.jobsearchuniversity", '/mentor/job/search/university', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.mentorjobsearchuniversity(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {POST} /mentor/listing Mentor Listing
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d31830d6d84b8436af1ce32",
            "full_name": "Kuldip",
            "email": "kk@gmail.com",
            "password": "$2a$08$vtcwsvoesd6jZtMo5wdp4OxKMBdwpzuHMNwMoGQZnIRIilVPEBMnm",
            "profile_image": "",
            "school_name": "",
            "education_level": "5d31c1a0822fa522c7abefca",
            "want_to": "",
            "address": "London",
            "university_name": "RKMV",
            "gender": "Male",
            "dob": "",
            "social_id": "",
            "register_type": "normal",
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "country": "5d306dc6822fa522c79acd21",
            "language": "5d306346822fa522c79a2dec",
            "specialization": [
                {
                    "title": "project one",
                    "_id": "5d31830d6d84b8436af1ce34"
                },
                {
                    "title": "http://www.projectone.com",
                    "_id": "5d31830d6d84b8436af1ce33"
                }
            ],
            "service": [
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d31830d6d84b8436af1ce36"
                },
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d31830d6d84b8436af1ce35"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "project": [],
            "activities": [],
            "push_notification": true,
            "short_description": "test description",
            "createdAt": "2019-07-19T08:45:01.100Z",
            "updatedAt": "2019-07-19T08:45:01.100Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "edu_level": {
                "_id": "5d31c1a0822fa522c7abefca",
                "level_name": "Level 01",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        },
        {
            "_id": "5d3188206d84b8436af1ce37",
            "full_name": "Subhra",
            "email": "subhra@gmail.com",
            "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
            "profile_image": "",
            "school_name": "",
            "education_level": "5d31c1a0822fa522c7abefca",
            "want_to": "",
            "address": "London",
            "university_name": "RKMV",
            "gender": "Male",
            "dob": "",
            "social_id": "",
            "register_type": "normal",
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "country": "5d306dc6822fa522c79acd21",
            "language": "5d306346822fa522c79a2dec",
            "specialization": [
                {
                    "title": "project one",
                    "_id": "5d3188206d84b8436af1ce39"
                },
                {
                    "title": "http://www.projectone.com",
                    "_id": "5d3188206d84b8436af1ce38"
                }
            ],
            "service": [
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3188206d84b8436af1ce3b"
                },
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3188206d84b8436af1ce3a"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "project": [],
            "activities": [],
            "push_notification": true,
            "short_description": "test description",
            "createdAt": "2019-07-19T09:06:40.023Z",
            "updatedAt": "2019-07-19T13:35:04.720Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "edu_level": {
                "_id": "5d31c1a0822fa522c7abefca",
                "level_name": "Level 01",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        },
        {
            "_id": "5d3189f0719a7e481b879cb1",
            "full_name": "Soudip",
            "email": "soudip@gmail.com",
            "password": "$2a$08$zPkgkuWbLqIepAojvcOW9.svpljrsvlS9bf6cBR.lM2GCYY8bMtQm",
            "profile_image": "profile_image_1563527664279_img3.jpg",
            "school_name": "",
            "education_level": "5d31c1d7822fa522c7abf330",
            "want_to": "",
            "address": "London",
            "university_name": "RKMV",
            "gender": "Male",
            "dob": "",
            "social_id": "",
            "register_type": "normal",
            "deviceToken": "",
            "deviceType": "",
            "isDeleted": false,
            "isActive": true,
            "country": "5d306dc6822fa522c79acd21",
            "language": "5d306346822fa522c79a2dec",
            "specialization": [
                {
                    "title": "project one",
                    "_id": "5d3189f0719a7e481b879cb3"
                },
                {
                    "title": "http://www.projectone.com",
                    "_id": "5d3189f0719a7e481b879cb2"
                }
            ],
            "service": [
                {
                    "name": "Service One",
                    "time": "2",
                    "details": "service details one",
                    "cost": "75",
                    "_id": "5d3189f0719a7e481b879cb5"
                },
                {
                    "name": "Service Two",
                    "time": "3",
                    "details": "service details two",
                    "cost": "100",
                    "_id": "5d3189f0719a7e481b879cb4"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "project": [],
            "activities": [],
            "push_notification": true,
            "short_description": "test description",
            "createdAt": "2019-07-19T09:14:24.393Z",
            "updatedAt": "2019-07-19T09:14:24.393Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "edu_level": {
                "_id": "5d31c1d7822fa522c7abf330",
                "level_name": "Level 02",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-04-15T08:24:40.827Z",
                "__v": 0
            }
        }
    ],
    "message": "Mentor list fetched successfully"
}
*/
namedRouter.post("api.mentor.listing", '/mentor/listing', request_param.any(),  async (req, res) => {
	try {
		console.log("8asca>>");
		const success = await staticController.mentorlisting(req,res);
		
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {get} /student/requestcallbackpage Request Call back Page
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {},
    "message": "Request call back page fetched"
}
*/
namedRouter.get("api.static.requestcallbackpage", '/student/requestcallbackpage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.requestcallbackpage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /message/reportmessage Report Message API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"_id":"5d3188206d84b8436af1ce37",
	"mentor_id":"5d306dc6822fa522c79acd21",
	"student_id":"5d306346822fa522c79a2dec",
	"count":5,
	"message_details":[
		{
			message:"message one"
		},
		{
			message:"message two"
		}
	]
  }
    "message": "Report message list successfully"
}
*/
namedRouter.post("api.static.reportmessage", '/message/reportmessage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.reportmessage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /message/delete Delete Message API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Delete message successfully"
}
*/
namedRouter.post("api.mentor.messagedelete", '/message/delete', request_param.any(),  async (req, res) => {
	try {
		console.log("8asca>>");
		const success = await staticController.messagedelete(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/offlineonline Offline Online Icon
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam online_status Online Status
  * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "full_name": "Josephine Darakjy",
        "email": "josephine_darakjy@darakjy.org",
        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
        "online_status":true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-08-16T09:14:15.232Z",
        "__v": 0
    },
    "message": "Settings update successfully"
}
*/
namedRouter.post("api.student.offlineonline", '/student/offlineonline', request_param.any(), async (req, res) => {
	try {
		console.log("xcasdca");
		const success = await staticController.offlineonlinestatus(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log("xcasdca");
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /message/changestatus Read/Unread status Message API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"readstatus":"yes"
	"_id":"5d3188206d84b8436af1ce37"
  }
    "message": "message status updated successfully"
}
*/
namedRouter.post("api.mentor.messagestatuschange", '/message/changestatus', request_param.any(),  async (req, res) => {
	try {
		console.log("8asca>>");
		const success = await staticController.messagestatuschange(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /message Message Page API
 * @apiVersion 1.0.0
 * @apiGroup REST API
  * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"_id":"5d3188206d84b8436af1ce37",
	"mentor_id":"5d306dc6822fa522c79acd21",
	"student_id":"5d306346822fa522c79a2dec"
  }
    "message": "Create message API"
}
*/
namedRouter.post("api.mentor.message", '/message', request_param.any(),  async (req, res) => {
	try {
		console.log("8asca>>");
		const success = await staticController.messagePage(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /chat/new/create Chat page
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "chat_token": "dPucrZQBwqIi0o8TlTwsseaYr2sjkTEWHJHer8PGtVI",
        "chat_date": "2019-07-23T10:46:57.731Z",
        "_id": "5d36e5ba24bf1f6694936c0e",
        "mentor_id": "5d31830d6d84b8436af1ce32",
        "student_id": "5d309257ae2ba07b64ea59f4",
        "__v": 0
    },
    "message": "Chat start"
}
*/
namedRouter.post('api.chat.newcreate','/chat/new/create',request_param.any(),function(req,res){
	staticController.createChatNew(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {post} /chat/sendattachment Send Attachment API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
  * @apiParam file File
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": "1563802441970_ton.pdf",
    "message": "Attachment send successfully"
}
*/
namedRouter.post('api.chat.sendattachment','/chat/sendattachment',request_param.any(),function(req,res){
	staticController.chatSendAttachment(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {post} /chat/sendpicture Send Picture API
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam file Image
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": "image_1563802441970_img2.jpg",
    "message": "Picture send successfully"
}
*/
namedRouter.post('api.chat.sendpicture','/chat/sendpicture',request_param.any(),function(req,res){
	staticController.chatSendPicture(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {get} /chat/createToken/:sessionId Intregate Tokbox chat
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Chat token create successfully"
}
*/
namedRouter.get('api.chat.createToken','/chat/createToken/:sessionId',request_param.any(),function(req,res){
	staticController.createToken(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {get} /chat/createSessionToken Intregate firebase chat
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Chat started successfully"
}
*/
namedRouter.get('api.chat.createSessionToken','/chat/createSessionToken',request_param.any(),function(req,res){
	staticController.createSessionToken(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {GET} /messagelist Message List(Inbox)
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"_id":"5d3188206d84b8436af1ce37",
	"mentor_id":"5d306dc6822fa522c79acd21",
	"student_id":"5d306346822fa522c79a2dec",
	"count":5,
	"message_details":[
		{
			message:"hi"
		},
		{
			message:"how are you"
		}
	]
  }
    "message": "Message list fetched successfully"
}
*/
namedRouter.get("api.user.messagelist", '/messagelist', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.messagelist(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /chat/sremovechat Remove chat
 * @apiVersion 1.0.0
 * @apiGroup REST API
 * @apiHeader x-access-token User's Access token
 * @apiParam chat_id Chat ID
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "chat_token": "cxxph92ZaCwW1LEmir9wCkmGNIiQ9xtsQ5FV4t9EAkU",
        "chat_date": "2019-09-06T10:57:44.989Z",
        "isDeleted": true,
        "_id": "5d723bddc5524b394b342861",
        "mentor_id": "5d6e75ff91b6735dc5206a35",
        "student_id": "5d6e4b8db6c99e0151707649",
        "__v": 0
    },
    "message": "Chat deleted successfully"
}
*/
namedRouter.post('api.chat.sremovechat','/chat/sremovechat',request_param.any(),function(req,res){
	staticController.sremoveChat(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});


module.exports = router;