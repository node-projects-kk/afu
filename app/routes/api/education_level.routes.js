const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const educationLevelController = require('webservice/education_level.controller');


/**
 * @api {get} /education-level/list Highest education API
 * @apiVersion 1.0.0
 * @apiGroup Education Level
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": [
    {
      "_id": "5d31c1a0822fa522c7abefca",
      "level_name": "Level 01",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    },
    {
      "_id": "5d31c1d7822fa522c7abf330",
      "level_name": "Level 02",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    }
  ],
  "message": "Education levels fetched Successfully"
}
*/
namedRouter.get("api.education.level.list", '/education-level/list', async (req, res) => {
    try {
        const success = await educationLevelController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {get} /education-level/autocomplete-list?keyword= Highest education Autocomplete API
 * @apiVersion 1.0.0
 * @apiGroup Education Level
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": [
    {
      "_id": "5d31c1a0822fa522c7abefca",
      "level_name": "Level 01",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    },
    {
      "_id": "5d31c1d7822fa522c7abf330",
      "level_name": "Level 02",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    }
  ],
  "message": "Education levels fetched Successfully"
}
*/
namedRouter.get("api.education.level.autocomplete.list", '/education-level/autocomplete-list', async (req, res) => {
  try {
      const success = await educationLevelController.getAutocompleteList(req);
      res.status(success.status).send(success);
  } catch (error) {
      res.status(error.status).send(error.message);
  }
});


module.exports = router;