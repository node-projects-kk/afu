const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const wantToController = require('webservice/want_to.controller');


/**
 * @api {get} /want-to/list I want to API
 * @apiVersion 1.0.0
 * @apiGroup Want To
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": [
    {
      "_id": "5d31c5b8822fa522c7ac5585",
      "name": "Study medicine at university",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    },
    {
      "_id": "5d31c5c5822fa522c7ac5628",
      "name": "Mentor prospective medical students",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    }
  ],
  "message": "I want to fetched Successfully"
}
*/
namedRouter.get("api.wantto.list", '/want-to/list', async (req, res) => {
    try {
        const success = await wantToController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;