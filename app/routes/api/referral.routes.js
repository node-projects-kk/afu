const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const referralController = require('webservice/referral.controller');

// namedRouter.all('/reviews*', auth.authenticateAPI);

/**
 * @api {get} /referral/list Referral List
 * @apiVersion 1.0.0
 * @apiGroup Referral
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": {
                "mentor_id": "5d6a5b3a53099465479367d8"
            },
            "mentor_info": {
                "_id": "5d6a5b3a53099465479367d8",
                "full_name": "aidancarr",
                "email": "aidan@gmail.com",
                "profile_image": null
            },
            "mentee_info": [
                {
                    "_id": "5d80dc8187db0a224dc01a15",
                    "full_name": "Aidan Cart",
                    "email": "aidan@lox-digital.com"
                },
                {
                    "_id": "5d81f293af7f7442362ab99e",
                    "full_name": "Bill Aniston",
                    "email": "bill@gmail.com"
                },
                {
                    "_id": "5d835f7f9bbb257b8ad90f10",
                    "full_name": "Emily gomes",
                    "email": "emily_juxecxf_gomes@tfbnw.net"
                },
                {
                    "_id": "5d886c46d0ed0d4b3e091319",
                    "full_name": "Ryan Walker",
                    "email": "ryan@gmail.com"
                }
            ],
            "last_refer_date": "2019-09-18T10:59:11.960Z",
            "count": 4
        },
        {
            "_id": {
                "mentor_id": "5d81ed77c7eb5b0871bbbf06"
            },
            "mentor_info": {
                "_id": "5d81ed77c7eb5b0871bbbf06",
                "full_name": "Oliver Queen",
                "email": "oliver@gmail.com",
                "profile_image": "profile_image_1568820826186_IMG_0008.JPG"
            },
            "mentee_info": [
                {
                    "_id": "5d80dc8187db0a224dc01a15",
                    "full_name": "Aidan Cart",
                    "email": "aidan@lox-digital.com"
                }
            ],
            "last_refer_date": "2019-09-20T10:59:11.960Z",
            "count": 1
        }
    ],
    "message": "Referral information fetched successfully."
}
*/
namedRouter.get("api.referral.list", '/referral/list', request_param.any(), async (req, res) => {
    try {
        const success = await referralController.referralListOfMentor(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;