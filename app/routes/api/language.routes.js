const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const languageController = require('webservice/language.controller');


/**
 * @api {get} /language/list Native Language API
 * @apiVersion 1.0.0
 * @apiGroup Language
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": [
    {
      "_id": "5d306346822fa522c79a2dec",
      "title": "English",
      "code": "en",
      "isDefault": true,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2dee",
      "title": "Hindi",
      "code": "hin",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2df0",
      "title": "Marathi",
      "code": "mr",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2df2",
      "title": "Telugu",
      "code": "tl",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2df4",
      "title": "Tamil",
      "code": "tm",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2df6",
      "title": "Bengali",
      "code": "bn",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2df8",
      "title": "Japanese",
      "code": "ja",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    },
    {
      "_id": "5d306346822fa522c79a2dfa",
      "title": "Spanish",
      "code": "sp",
      "isDefault": false,
      "isDeleted": false,
      "status": "Active"
    }
  ],
  "message": "Languages fetched Successfully"
}
*/
namedRouter.get("api.language.list", '/language/list', async (req, res) => {
    try {
        const success = await languageController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;