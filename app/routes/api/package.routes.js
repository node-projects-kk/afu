const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const packageController = require('webservice/package.controller');

/**
 * @api {get} /package/list Package list
 * @apiVersion 1.0.0
 * @apiGroup Package
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d493b24f5cd7d4a7fc345f4",
            "package_name": "UKCAT/BMAT Guidance",
            "package_description": "Test Description",
            "package_price": 50,
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-05-07T16:08:47.952Z",
            "updatedAt": "2019-08-06T09:25:45.263Z",
            "__v": 0
        },
        {
            "_id": "5d49453d94ac5553b49b5b74",
            "package_name": "Personal Statement Check",
            "package_price": 25,
            "package_description": "Test Description",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-08-06T09:15:41.563Z",
            "updatedAt": "2019-08-06T09:16:27.165Z",
            "__v": 0
        },
        {
            "_id": "5d49480d238c6455a392cd0e",
            "package_name": "Interview Practice/Guidance",
            "package_price": 75,
            "package_description": "Test Description",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-08-06T09:27:41.599Z",
            "updatedAt": "2019-08-06T09:27:41.599Z",
            "__v": 0
        },
        {
            "_id": "5d494830238c6455a392cd0f",
            "package_name": "Profile/CV Improvement",
            "package_price": 100,
            "package_description": "Test Description",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-08-06T09:28:16.066Z",
            "updatedAt": "2019-08-06T09:28:16.066Z",
            "__v": 0
        }
    ],
    "message": "Packages fetched successfully"
}
*/
namedRouter.get("api.packageg.list", '/package/list', async (req, res) => {
    try {
        const success = await packageController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

namedRouter.all('/packages*', auth.authenticateAPI);

/**
 * @api {get} /packages/list/user Package list by User
 * @apiVersion 1.0.0
 * @apiGroup Package
 * @apiHeader x-access-token User's Access token [Mentor]
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "default": [],
        "custom": [
            {
                "_id": "5d493b24f5cd7d4a7fc345f4",
                "package_name": "UKCAT/BMAT Guidance",
                "package_description": "Test Description",
                "package_price": 50,
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-05-07T16:08:47.952Z",
                "updatedAt": "2019-08-06T09:25:45.263Z",
                "__v": 0
            },
            {
                "_id": "5d49453d94ac5553b49b5b74",
                "package_name": "Personal Statement Check",
                "package_price": 25,
                "package_description": "Test Description",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:15:41.563Z",
                "updatedAt": "2019-08-06T09:16:27.165Z",
                "__v": 0
            },
            {
                "_id": "5d49480d238c6455a392cd0e",
                "package_name": "Interview Practice/Guidance",
                "package_price": 75,
                "package_description": "Test Description",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:27:41.599Z",
                "updatedAt": "2019-08-06T09:27:41.599Z",
                "__v": 0
            },
            {
                "_id": "5d494830238c6455a392cd0f",
                "package_name": "Profile/CV Improvement",
                "package_price": 100,
                "package_description": "Test Description",
                "status": "Active",
                "isDeleted": false,
                "createdAt": "2019-08-06T09:28:16.066Z",
                "updatedAt": "2019-08-06T09:28:16.066Z",
                "__v": 0
            }
        ]
    },
    "message": "Packages fetched successfully"
}
*/
namedRouter.get("api.packageuser.list.user", '/packages/list/user', async (req, res) => {
    try {
        const success = await packageController.getListUser(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {get} /packages/detail/user/:package_id Package Detail by User
 * @apiVersion 1.0.0
 * @apiGroup Package
 * @apiParam package_id Package Id
 * @apiHeader x-access-token User's Access token [Student]
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d49453d94ac5553b49b5b74",
        "package_name": "Personal Statement Check",
        "package_price": 40,
        "package_description": "Test ",
        "status": "Active",
        "isDeleted": false,
        "createdAt": "2019-08-06T09:15:41.563Z",
        "updatedAt": "2019-09-20T11:19:34.478Z",
        "__v": 0,
        "isPurchased": true
    },
    "message": "Packages fetched successfully"
}
*/
namedRouter.get("api.packageuser.detail.user", '/packages/detail/user/:package_id', async (req, res) => {
    try {
        const success = await packageController.getPackageDetailByUser(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {post} /packages/add Add Package
 * @apiVersion 1.0.0
 * @apiGroup Package
 * @apiHeader x-access-token User's Access token [Mentor]
 * @apiParam {String} package_name Package Name
 * @apiParam {Double} package_price Package Price
 * @apiParam {String} package_start_time Package Start Time
 * @apiParam {String} package_end_time Package End Time
 * @apiParam {String} package_description Package Description
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "package_name": "Custom Package One",
        "package_price": "100",
        "package_description": "Custom Package Description",
        "status": "Active",
        "isDeleted": false,
        "_id": "5d53e883a00ac3639ac2f395",
        "user_id": "5d3188206d84b8436af1ce37",
        "createdAt": "2019-08-14T10:54:59.713Z",
        "updatedAt": "2019-08-14T10:54:59.713Z",
        "__v": 0
    },
    "message": "Packages added successfully"
}
*/
namedRouter.post("api.package.add", '/packages/add', request_param.any(), async (req, res) => {
    try {
        const success = await packageController.addCustomPackage(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

/**
 * @api {post} /packages/update Update Package
 * @apiVersion 1.0.0
 * @apiGroup Package
 * @apiHeader x-access-token User's Access token [Mentor]
 * @apiParam package_name Package Name
 * @apiParam package_price Package Price
 * @apiParam package_start_time Package Start Time
 * @apiParam package_end_time Package End Time
 * @apiParam package_description Package Description
* @apiParam package_id Package Id
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d53e883a00ac3639ac2f395",
        "package_name": "Custom Package One1",
        "package_price": 125,
        "package_description": "Custom Package Description1",
        "status": "Active",
        "isDeleted": false,
        "user_id": "5d3188206d84b8436af1ce37",
        "createdAt": "2019-08-14T10:54:59.713Z",
        "updatedAt": "2019-08-14T11:06:10.601Z",
        "__v": 0
    },
    "message": "Packages updated Successfully"
}
*/
namedRouter.post("api.package.update", '/packages/update', request_param.any(), async (req, res) => {
    try {
        const success = await packageController.updateCustomPackage(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

/**
 * @api {post} /packages/delete Delete Package
 * @apiVersion 1.0.0
 * @apiGroup Package
 * @apiHeader x-access-token User's Access token [Mentor]
* @apiParam package_id Package Id
 * @apiSuccessExample {json} Success
 * {
    "status": 201,
    "data": {
        "_id": "5d53e883a00ac3639ac2f395",
        "package_name": "Custom Package One1",
        "package_price": 125,
        "package_description": "Custom Package Description1",
        "status": "Active",
        "isDeleted": false,
        "user_id": "5d3188206d84b8436af1ce37",
        "createdAt": "2019-08-14T10:54:59.713Z",
        "updatedAt": "2019-08-14T11:06:10.601Z",
        "__v": 0
    },
    "message": "You cannot delete this as this package already been used"
}
*/
namedRouter.post("api.package.delete", '/packages/delete', request_param.any(), async (req, res) => {
    try {
        const success = await packageController.deleteCustomPackage(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

module.exports = router;