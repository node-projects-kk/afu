const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const schoolController = require('webservice/school.controller');


/**
 * @api {get} /school/list School Name API
 * @apiVersion 1.0.0
 * @apiGroup School
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d775426f43b21800aa0f759",
            "school_name": "City of London School for Girls",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-04-15T08:24:40.827Z",
            "__v": 0
        },
        {
            "_id": "5d775426f43b21800aa0f75b",
            "school_name": "City of London School1",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-04-15T08:24:48.004Z",
            "__v": 0
        }
    ],
    "message": "Schools fetched Successfully"
}
*/
namedRouter.get("api.school.list", '/school/list', async (req, res) => {
    try {
        const success = await schoolController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;