const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const chatController = require('webservice/chat.controller');

namedRouter.all('/chats*', auth.authenticateAPI);

/**
 * @api {post} /chats/create Create Chat Token
 * @apiVersion 1.0.0
 * @apiGroup Chat
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} mentor_id Mentor ID
 * @apiParam {ObjectId} student_id Send this Student ID only for App end.
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "chat_token": "0a3oMk3vLFVT1RDKMmm1d7C662lsWTzje2V7i1TAacv",
        "student_id": null,
        "university_id": "5da87d8e60ea3463240240b9",
        "token_created_for": "web",
        "chat_date": "2019-10-21T08:45:07.350Z",
        "isDeleted": false,
        "_id": "5dad703ee9f23909569e2675",
        "mentor_id": "5d8e00f77a66451bb1acf21a",
        "__v": 0
    },
    "message": "Chat token created successfully"
}
*/
namedRouter.post("api.chat.create", '/chats/create', request_param.any(), async (req, res) => {
    try {
        const success = await chatController.createChatToken(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

/**
 * @api {get} /chats/create_session Create Chat Session for Tokbox
 * @apiVersion 1.0.0
 * @apiGroup Chat
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": "2_MX40NjQwODA4Mn5-MTU2NjM5NTEwNTc4Nn53c25ESXRKUVMyckNIVjZPdHpKWjZ5cmZ-UH4",
    "message": "Session ID created successfully."
}
*/

namedRouter.get('api.chat.createSession', '/chats/create_session', request_param.any(), function (req, res) {
    chatController.createChatSession(req).then(function (success) {
            res.send(success);
        },
        function (failure) {
            res.status(failure.status).send(failure);
        });
});

/**
 * @api {post} /chats/sendpush Send push when start video chat
 * @apiVersion 1.0.0
 * @apiGroup Chat
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Push send successfully"
}
*/

namedRouter.post('api.chat.sendpush', '/chats/sendpush', request_param.any(), function (req, res) {
    chatController.sendPushForChat(req).then(function (success) {
            res.send(success);
        },
        function (failure) {
            res.status(failure.status).send(failure);
        });
});

namedRouter.post("api.chat.rejectpush", '/chats/rejectpush', request_param.any(), async (req, res) => {
    try {
        const success = await chatController.rejectPushForChat(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

namedRouter.post('api.chat.fetchchat', '/chats/fetchchat', request_param.any(), function (req, res) {
    chatController.fetchChat(req).then(function (success) {
            res.send(success);
        },
        function (failure) {
            res.status(failure.status).send(failure);
        });
});

/**
 * @api {get} /chats/listwithchat Mentor list with last chat
 * @apiVersion 1.0.0
 * @apiGroup Chat
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d66813137aa6464abeeb447",
            "full_name": "Ayan",
            "email": "ayan@gmail.com",
            "password": "$2a$08$SaBs.UO6zDJ1PpO3tuVQEOG0WKtTKYKOSFsTxGzBbZmOBLGYQpguy",
            "profile_image": null,
            "university_name": "Cambridge",
            "year_of_study": "2005",
            "short_description": "Hello World",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "C2B85B7666D266BB2DF0BFC67543D2C190D2EFD5B8A1E06A294F98D9ACFCEB1B",
            "deviceType": "ios",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d371e651ae2de8011c4acb5",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498ab0e72f7708815f0792",
            "service": [
                {
                    "_id": "5d66813137aa6464abeeb449",
                    "package_id": "5d493b24f5cd7d4a7fc345f4"
                },
                {
                    "_id": "5d66813137aa6464abeeb448",
                    "package_id": "5d49480d238c6455a392cd0e"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-08-28T13:27:13.749Z",
            "updatedAt": "2019-08-30T16:57:11.225Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5d6ce68a05dfad0d5046f864",
                "chat_token": "sPxAjqrD1m6Gjlvexe9LFAMLXRHXeOfIrsbdysRUz1w",
                "chat_date": "2019-09-02T06:10:26.277Z",
                "mentor_id": "5d66813137aa6464abeeb447",
                "student_id": "5d66ac581a9c611506dc1fde",
                "__v": 0
            },
            "last_message": {
                "content": "Hiii",
                "imageUrl": "",
                "isSeen": false,
                "receiverId": "5d66813137aa6464abeeb447",
                "senderId": "5d66ac581a9c611506dc1fde",
                "time": "Mon Sep 02 2019 21:32:31 GMT+0530 (India Standard Time)",
                "videoUrl": ""
            }
        },
        {
            "_id": "5d67c05f5961c44cb159e458",
            "full_name": "AyanM",
            "email": "ayanMentor@gmail.com",
            "password": "$2a$08$/tq0jW1SCK15FlQK/Duw9OoJCI331PCRKj2vZeAGCAW8Bb6mHJSiu",
            "profile_image": null,
            "university_name": "Stanford",
            "year_of_study": "2005",
            "short_description": "Hello world",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d371e651ae2de8011c4acb5",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498ab0e72f7708815f0792",
            "service": [
                {
                    "_id": "5d67c05f5961c44cb159e459",
                    "package_id": "5d493b24f5cd7d4a7fc345f4"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-08-29T12:09:03.742Z",
            "updatedAt": "2019-08-29T12:10:41.133Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5d6ce98305dfad0d5046f865",
                "chat_token": "He9GAgFhS3Pm3j5Vl6BlVKlV3NsXiFUUphivQTtROeh",
                "chat_date": "2019-09-02T06:10:26.277Z",
                "mentor_id": "5d67c05f5961c44cb159e458",
                "student_id": "5d66ac581a9c611506dc1fde",
                "__v": 0
            },
            "last_message": {}
        },
        {
            "_id": "5d67c0e75961c44cb159e45a",
            "full_name": "Fefefe",
            "email": "dd@gmail.com",
            "password": "$2a$08$Ugm5fN032YTO8X8OHG/1ZOAE7xA2CuEJTTYcr6hUwmeHG34Nci4ja",
            "profile_image": null,
            "university_name": "Grgg",
            "year_of_study": "1990",
            "short_description": "Grgregrg",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d306346822fa522c79a2dee",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498ab0e72f7708815f0792",
            "service": [
                {
                    "_id": "5d67c0e75961c44cb159e45b",
                    "package_id": "5d49453d94ac5553b49b5b74"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-08-29T12:11:19.958Z",
            "updatedAt": "2019-08-29T12:12:10.715Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5d6ce9a605dfad0d5046f866",
                "chat_token": "CJIqRrzwu3DmFciYaGL1vd0Ihajcd2dyqZCDxtruMYl",
                "chat_date": "2019-09-02T06:10:26.277Z",
                "mentor_id": "5d67c0e75961c44cb159e45a",
                "student_id": "5d66ac581a9c611506dc1fde",
                "__v": 0
            },
            "last_message": {}
        },
        {
            "_id": "5d67c13c5961c44cb159e45c",
            "full_name": "Ddwedwq",
            "email": "a@t.com",
            "password": "$2a$08$iqlCNNnjisOXAMhZVHbuGO7jXrFRKLPZgfDFJkgmuI0l0PC9QxdlW",
            "profile_image": null,
            "university_name": "Grgrt",
            "year_of_study": "1991",
            "short_description": "Regret",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d306346822fa522c79a2dee",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498ab0e72f7708815f0792",
            "service": [
                {
                    "_id": "5d67c13c5961c44cb159e45d",
                    "package_id": "5d49480d238c6455a392cd0e"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-08-29T12:12:44.501Z",
            "updatedAt": "2019-08-29T12:14:02.922Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5d6ce9d205dfad0d5046f867",
                "chat_token": "yRgTdPR3d5y758bSKec4stAUAYXYtYMi22e8gNoOAhv",
                "chat_date": "2019-09-02T06:10:26.277Z",
                "mentor_id": "5d67c13c5961c44cb159e45c",
                "student_id": "5d66ac581a9c611506dc1fde",
                "__v": 0
            },
            "last_message": {}
        },
        {
            "_id": "5d67c1ab5961c44cb159e45e",
            "full_name": "Fefeef",
            "email": "e@r.com",
            "password": "$2a$08$zFBCKuP6RhKWuR4W5P6xgum0lUe2IqWekSU0g.oVAVzpIYDHkkUuW",
            "profile_image": null,
            "university_name": "Gg",
            "year_of_study": "1990",
            "short_description": "Gregory",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d306346822fa522c79a2dee",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498ab0e72f7708815f0792",
            "service": [
                {
                    "_id": "5d67c1ab5961c44cb159e45f",
                    "package_id": "5d494830238c6455a392cd0f"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-08-29T12:14:35.135Z",
            "updatedAt": "2019-08-29T12:15:04.520Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            }
        },
        {
            "_id": "5d6a5b3a53099465479367d8",
            "full_name": "aidancarr",
            "email": "aidan@gmail.com",
            "password": "$2a$08$Fay5Mw5on8zSnqDS9GYnM.VPlf5T.zfzTIulMhOVTEO7ABOmXE/Cm",
            "profile_image": null,
            "university_name": "Edinburgh ",
            "year_of_study": "1992",
            "short_description": "I am a mentor signing up for an account.",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "isDeleted": false,
            "isActive": true,
            "language": "5d371e651ae2de8011c4acb5",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498adae72f7708815f0793",
            "service": [
                {
                    "_id": "5d6a5b3a53099465479367dc",
                    "package_id": "5d49453d94ac5553b49b5b74"
                },
                {
                    "_id": "5d6a5b3a53099465479367db",
                    "package_id": "5d493b24f5cd7d4a7fc345f4"
                },
                {
                    "_id": "5d6a5b3a53099465479367da",
                    "package_id": "5d49480d238c6455a392cd0e"
                },
                {
                    "_id": "5d6a5b3a53099465479367d9",
                    "package_id": "5d494830238c6455a392cd0f"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-08-31T11:34:18.674Z",
            "updatedAt": "2019-08-31T12:41:22.845Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            }
        }
    ],
    "message": "Mentor list fetched successfully3"
}
*/
namedRouter.get("api.chat.listwithchat", '/chats/listwithchat', async (req, res) => {
    try {
        const success = await chatController.mentorListChat(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});


/**
 * @api {post} /chats/update Update last chat
 * @apiVersion 1.0.0
 * @apiGroup Chat
 * @apiHeader x-access-token User's Access token
 * @apiParam chat_id Chat ID
 * @apiParam last_message Last message
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "last_message": "test message123",
        "chat_date": "2019-09-03T08:02:31.488Z",
        "_id": "5d6e1e1cd84a7b479c966975",
        "chat_id": "5d36e5ba24bf1f6694936c0e",
        "user_id": "5d66ac581a9c611506dc1fde",
        "__v": 0
    },
    "message": "Chat save successfully"
}
*/
namedRouter.post("api.chat.updatechat", '/chats/update', request_param.any(), async (req, res) => {
    try {
        const success = await chatController.updateChat(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /chats/remove Remove chat
 * @apiVersion 1.0.0
 * @apiGroup Chat
 * @apiHeader x-access-token User's Access token
 * @apiParam chat_id Chat ID
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "chat_token": "cxxph92ZaCwW1LEmir9wCkmGNIiQ9xtsQ5FV4t9EAkU",
        "chat_date": "2019-09-06T10:57:44.989Z",
        "isDeleted": true,
        "_id": "5d723bddc5524b394b342861",
        "mentor_id": "5d6e75ff91b6735dc5206a35",
        "student_id": "5d6e4b8db6c99e0151707649",
        "__v": 0
    },
    "message": "Chat deleted successfully"
}
*/
namedRouter.post("api.chat.removechat", '/chats/remove', request_param.any(), async (req, res) => {
    try {
        const success = await chatController.removeChat(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

/**
 * @api {post} /chats/createTokenForWebsite Create Chat Token
 * @apiVersion 1.0.0
 * @apiGroup Website Chat
 * @apiHeader x-access-token User's Access token
 * @apiParam {ObjectId} mentor_id Pass Mentor ID
 * @apiParam {ObjectId} university_id Pass University ID
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "chat_token": "6hdH5ImPHMYwzGpdVZAf0a4MEf6CYbQnM2Rru078XOO",
        "mentor_id": "5d8e00f77a66451bb1acf21a",
        "student_id": null,
        "university_id": "5da87d8e60ea3463240240b8",
        "chat_date": "2019-10-21T13:32:24.710Z",
        "isDeleted": false,
        "_id": "5dadc4ccc04b3c10ec66b555",
        "__v": 0
    },
    "message": "Chat token created successfully."
}
*/
namedRouter.post("api.chat.createTokenForWebsite", '/chats/createTokenForWebsite', request_param.any(), async (req, res) => {
    try {
        const success = await chatController.createChatTokenWeb(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});

/**
 * @api {get} /chats/listwithchatForWebsite Mentor List with last chat
 * @apiVersion 1.0.0
 * @apiGroup Website Chat
 * @apiHeader x-access-token User's Access token (User: Mentor or University)
 * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": [
        {
            "_id": "5dae0cb6cb9f1e51ce615a5e",
            "full_name": "Brad Pitt",
            "surname": "",
            "email": "Brad.pitt@uclan.ac.uk",
            "password": "$2a$08$Yj8KYOK.XK3Q1rc7b6.OO.aivAGiSg0Z2pzTuXk3EDEu85T4L6BMi",
            "profile_image": "",
            "university_name": "uni of central lancs",
            "year_of_study": "2018",
            "short_description": "Hi I'm Brad and I'm studying Social Sciences. I'm from London but have a placement year in Hollywood. ",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "",
            "address": "",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "fSN1piuV1wM:APA91bFS5Nb-4tq6PnWqyuTZPgNY_QQkCcylOUI5kfd6dlsFgdnYjtDbXZKWmt7pwRbiV9pco6O31ibI1_mVIFavXgddgePRHGnVzHGBHD719qUCD2Z_O4CLue5bgO2APSKVi-KaixFa",
            "deviceType": "android",
            "push_notification": true,
            "display_mobile_no": false,
            "onoffstatus": "yes",
            "isDeleted": false,
            "isActive": true,
            "language": "5d4002e4d13b0a4e7c99fd41",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498ab0e72f7708815f0792",
            "education_level": "5d78ebc3860d521eb8de35e5",
            "service": [
                {
                    "_id": "5dae0cb6cb9f1e51ce615a5f",
                    "package_id": "5d49453d94ac5553b49b5b74"
                },
                {
                    "_id": "5dae16c1cb9f1e51ce615a61",
                    "package_id": "5dae16c1cb9f1e51ce615a60"
                },
                {
                    "_id": "5dae185fcb9f1e51ce615a63",
                    "package_id": "5dae185fcb9f1e51ce615a62"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-10-21T19:53:26.263Z",
            "updatedAt": "2019-10-21T20:50:12.871Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5dae9352cb9f1e51ce615a64",
                "chat_token": "VEqG0oehRKxrYDmnUqNK8d5TTSHJtDkoqwuVsB0hMSB",
                "mentor_id": "5dae0cb6cb9f1e51ce615a5e",
                "student_id": null,
                "university_id": "5da87d8e60ea3463240240b8",
                "chat_date": "2019-10-21T15:43:51.846Z",
                "isDeleted": false,
                "__v": 0
            },
            "last_message": {
                "content": "ggggfbvb",
                "documentName": "",
                "documentUrl": "",
                "imageUrl": "",
                "isSeen": false,
                "receiverId": "5dae0cb6cb9f1e51ce615a5e",
                "senderId": "5da87d8e60ea3463240240b8",
                "time": "Tue Oct 22 2019 11:55:45 GMT+0530 (India Standard Time)",
                "videoUrl": "",
                "id": "-Lrm87voApfSGqKaaKac"
            },
            "hasChat": true
        },
        {
            "_id": "5da96852df86fa6f3a59c2ab",
            "full_name": "Annabel Smith",
            "surname": "",
            "email": "annas@yopmail.com",
            "password": "$2a$08$mtEmw/tCgaHvaNtnTyXm5OOIC5FLEJyAP6KpJvwwzvLN58gB7p4nW",
            "profile_image": "profile_image_1571383378077_IMG_1571383061674",
            "university_name": "Com University ",
            "year_of_study": "1993",
            "short_description": "Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text. Lorem Ipsum is simply du",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "logo_1571388042719_IMG_0430.JPG",
            "address": "test address ",
            "gender": "Male",
            "phone": "",
            "dob": "",
            "deviceToken": "c50U5lyOvLo:APA91bFw_55Y9S1HI0rW9ZLna0Ewug3WghPcTR3wbF9JpcOGOjLbiJvOYoRjxwLL4_zdwwWdxwb1pWMETFdp6XX1I7nONznQt2oAh6JnqdK3ek36fO5Ur7P6ne5ZjLYM_CNojIydoOkL",
            "deviceType": "android",
            "push_notification": true,
            "display_mobile_no": true,
            "onoffstatus": "yes",
            "isDeleted": false,
            "isActive": true,
            "language": "5d306346822fa522c79a2dfa",
            "country": "5d306dc6822fa522c79acd1f",
            "course": "5d498adae72f7708815f0793",
            "education_level": "5d385f1ad3486b7ae28e0e7c",
            "service": [
                {
                    "_id": "5da96852df86fa6f3a59c2ac",
                    "package_id": "5d49480d238c6455a392cd0e"
                },
                {
                    "_id": "5da97b0c009d8c4088b0226f",
                    "package_id": "5da97b0c009d8c4088b0226e"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-10-18T07:22:58.298Z",
            "updatedAt": "2019-10-18T09:34:52.762Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5dae97dccb9f1e51ce615a65",
                "chat_token": "V8FPkMwg1D4I0uFizjI2UD9zR8KOsT0C1FxAxOvd2Kq",
                "mentor_id": "5da96852df86fa6f3a59c2ab",
                "student_id": null,
                "university_id": "5da87d8e60ea3463240240b8",
                "chat_date": "2019-10-21T15:43:51.846Z",
                "isDeleted": false,
                "__v": 0
            },
            "last_message": {},
            "hasChat": false
        },
        {
            "_id": "5d9d75947f45992abe8293a7",
            "full_name": "John Doe",
            "surname": "",
            "email": "john.doe@yahoo.com",
            "password": "$2a$08$8T0X7Mq2tSmyuPmAYwk0MeUos7.DDNMpBHaMHazzjmtJdSRNBml5u",
            "profile_image": "profile_image_1571205509710_userJonath.jpeg",
            "university_name": "University of Birmingham",
            "year_of_study": "2015",
            "short_description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop p",
            "social_id": "",
            "register_type": "normal",
            "school_name": "",
            "logo": "logo_1571126624325_univLogo.jpeg",
            "address": "UK",
            "gender": "Male",
            "phone": "9876543233",
            "dob": "",
            "deviceToken": "",
            "deviceType": "",
            "push_notification": true,
            "display_mobile_no": true,
            "onoffstatus": "yes",
            "isDeleted": false,
            "isActive": true,
            "language": "5d306346822fa522c79a2df2",
            "country": "5d306dc6822fa522c79acd21",
            "course": "5d498adae72f7708815f0793",
            "education_level": "5d7d69053f71307502bca5d2",
            "service": [
                {
                    "_id": "5d9d75947f45992abe8293aa",
                    "package_id": "5d49453d94ac5553b49b5b74"
                },
                {
                    "_id": "5d9d75947f45992abe8293a9",
                    "package_id": "5d493b24f5cd7d4a7fc345f4"
                },
                {
                    "_id": "5d9d75947f45992abe8293a8",
                    "package_id": "5d49480d238c6455a392cd0e"
                },
                {
                    "_id": "5d9ee002bf9680442582b6c0",
                    "package_id": "5d9ee002bf9680442582b6bf"
                },
                {
                    "_id": "5d9ee0ecbf9680442582b6c2",
                    "package_id": "5d9ee0ecbf9680442582b6c1"
                },
                {
                    "_id": "5da45f08b470be127dea99f3",
                    "package_id": "5da45f08b470be127dea99f2"
                },
                {
                    "_id": "5da47d69b470be127dea99f6",
                    "package_id": "5da47d69b470be127dea99f5"
                },
                {
                    "_id": "5da47e63b470be127dea99f8",
                    "package_id": "5da47e63b470be127dea99f7"
                },
                {
                    "_id": "5da47e8fb470be127dea99fa",
                    "package_id": "5da47e8fb470be127dea99f9"
                },
                {
                    "_id": "5da482bcb470be127dea99fc",
                    "package_id": "5da482bcb470be127dea99fb"
                },
                {
                    "_id": "5da49263b470be127dea99fe",
                    "package_id": "5da49263b470be127dea99fd"
                },
                {
                    "_id": "5da55029b470be127dea9a00",
                    "package_id": "5da55029b470be127dea99ff"
                },
                {
                    "_id": "5da5506fb470be127dea9a02",
                    "package_id": "5da5506fb470be127dea9a01"
                },
                {
                    "_id": "5da56475b470be127dea9a06",
                    "package_id": "5da56475b470be127dea9a05"
                },
                {
                    "_id": "5da56482b470be127dea9a08",
                    "package_id": "5da56482b470be127dea9a07"
                },
                {
                    "_id": "5da5794eb470be127dea9a0b",
                    "package_id": "5da5794eb470be127dea9a0a"
                },
                {
                    "_id": "5da5a522b470be127dea9a0e",
                    "package_id": "5da5a522b470be127dea9a0d"
                }
            ],
            "role": "5d30518c822fa522c7991272",
            "activities": [],
            "createdAt": "2019-10-09T05:52:20.929Z",
            "updatedAt": "2019-10-22T06:42:46.831Z",
            "__v": 0,
            "user_role": {
                "_id": "5d30518c822fa522c7991272",
                "role": "mentor",
                "desc": "This is the mentor role here",
                "roleDisplayName": "Mentor"
            },
            "chat_details": {
                "_id": "5daea4accb9f1e51ce615a66",
                "chat_token": "3zowxC8cBK6tydeBqGcbKQWWO7gemZ9XxTrxLSPMTbV",
                "mentor_id": "5d9d75947f45992abe8293a7",
                "student_id": null,
                "university_id": "5da87d8e60ea3463240240b8",
                "chat_date": "2019-10-21T15:43:51.846Z",
                "isDeleted": false,
                "__v": 0
            },
            "last_message": {
                "content": "hello university",
                "documentName": "",
                "documentUrl": "",
                "imageUrl": "",
                "isSeen": false,
                "receiverId": "5da87d8e60ea3463240240b8",
                "senderId": "5d9d75947f45992abe8293a7",
                "time": "Tue Oct 22 2019 12:12:43 GMT+0530 (India Standard Time)",
                "videoUrl": "",
                "id": "-LrmC0SvUELjU5twnLLB"
            },
            "hasChat": true
        }
    ],
    "message": "Chat list fetched successfully"
}
*/
namedRouter.get("api.chat.listwithchatForWebsite", '/chats/listwithchatForWebsite', async (req, res) => {
    try {
        const success = await chatController.mentorListChatForWebsite(req, res);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error);
    }
});

module.exports = router;