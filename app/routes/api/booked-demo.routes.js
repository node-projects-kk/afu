const express = require('express');
const routeLabel = require('route-label');
const multer = require('multer');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');
const request_param = multer();
const bookedDemoController = require('webservice/booked-demo.controller');

/**
 * @api {post} /book/demo Book Demo
 * @apiVersion 1.0.0
 * @apiGroup Book Demo
 * @apiParam full_name Full Name
 * @apiParam institution Institution
 * @apiParam job_title Job Title
 * @apiParam email Email Address
 * @apiParam phone Phone Number
 * @apiParam message Message
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": " Jerald Laury",
        "institution": " Test Org",
        "job_title": "Marketing Manager",
        "email": " jerald.laury@gmail.com",
        "phone": "1234567890",
        "message": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        "status": "Active",
        "isDeleted": false,
        "_id": "5da704ac77626758a0e70d0c",
        "createdAt": "2019-10-16T11:53:16.344Z",
        "updatedAt": "2019-10-16T11:53:16.344Z",
        "__v": 0
    },
    "message": "Demo booked successfully."
}
*/
namedRouter.post("api.book.demo", '/book/demo', request_param.any(), async (req, res) => {
	try {
		const success = await bookedDemoController.bookDemo(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('route error',error.message);
		res.status(error.status).send(error);
	}
});

module.exports = router;