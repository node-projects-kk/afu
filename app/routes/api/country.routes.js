const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const countryController = require('webservice/country.controller');


/**
 * @api {get} /country/list Country Name API
 * @apiVersion 1.0.0
 * @apiGroup Country
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": [
    {
      "_id": "5d306dc6822fa522c79acd1f",
      "country_name": "Australia",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    },
    {
      "_id": "5d306dc6822fa522c79acd21",
      "country_name": "India",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:48.004Z",
      "__v": 0
    },
    {
      "_id": "5d306dc6822fa522c79acd23",
      "country_name": "USA",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-05-06T07:56:19.232Z",
      "__v": 0
    },
    {
      "_id": "5d306dc6822fa522c79acd27",
      "country_name": "Canada",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-05-16T22:59:52.197Z",
      "__v": 0
    }
  ],
  "message": "Countries fetched Successfully"
}
*/
namedRouter.get("api.country.list", '/country/list', async (req, res) => {
    try {
        const success = await countryController.getList(req);
        res.status(success.status).send(success);
    } catch (error) {
        res.status(error.status).send(error.message);
    }
});


/**
 * @api {get} /country/autocomplete-list Country Autocomplete API
 * @apiVersion 1.0.0
 * @apiGroup Country
 * @apiSuccessExample {json} Success
 * {
  "status": 200,
  "data": [
    {
      "_id": "5d306dc6822fa522c79acd1f",
      "country_name": "Australia",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:40.827Z",
      "__v": 0
    },
    {
      "_id": "5d306dc6822fa522c79acd21",
      "country_name": "India",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-04-15T08:24:48.004Z",
      "__v": 0
    },
    {
      "_id": "5d306dc6822fa522c79acd23",
      "country_name": "USA",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-05-06T07:56:19.232Z",
      "__v": 0
    },
    {
      "_id": "5d306dc6822fa522c79acd27",
      "country_name": "Canada",
      "status": "Active",
      "isDeleted": false,
      "createdAt": "2019-05-16T22:59:52.197Z",
      "__v": 0
    }
  ],
  "message": "Countries fetched Successfully"
}
*/
namedRouter.get("api.country.autocomplete.list", '/country/autocomplete-list', async (req, res) => {
  try {
      const success = await countryController.getAutocompleteList(req);
      res.status(success.status).send(success);
  } catch (error) {
      res.status(error.status).send(error.message);
  }
});


module.exports = router;