const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const projectController = require('webservice/project.controller');

namedRouter.all('/projects*', auth.authenticateAPI);

/**
 * @api {post} /projects/add Project Add
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiHeader x-access-token User's Access token
 * @apiParam project_url Project ID
 * @apiParam project_name Project Name
 * @apiParam project_url Project URL
 * @apiParam project_details Project Details
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d526b57a47f2f2ab1d657a9",
        "project_name": "Apply For University",
        "project_url": "www.afu.com",
        "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
        "status": "Active",
        "isDeleted": false,
        "user_id": "5d526b57a47f2f2ab1d657a6",
        "createdAt": "2019-08-13T07:48:39.390Z",
        "updatedAt": "2019-08-13T09:37:32.578Z",
        "__v": 0
    },
    "message": "Projects added Successfully"
}
*/
namedRouter.post("api.project.add", '/projects/add',request_param.any(),async (req, res) => {
	try {
		const success = await projectController.updateProject(req);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error.message);
	}
});


/**
 * @api {get} /projects/list Project List
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5d526b57a47f2f2ab1d657a9",
            "project_name": "project one",
            "project_url": "http://www.projectone.com",
            "project_details": "project details one",
            "status": "Active",
            "isDeleted": false,
            "user_id": {
                "_id": "5d526b57a47f2f2ab1d657a6",
                "full_name": "Albina Glick",
                "email": "albina@glick.com"
            },
            "createdAt": "2019-08-13T07:48:39.390Z",
            "updatedAt": "2019-08-13T07:48:39.390Z",
            "__v": 0
        },
        {
            "_id": "5d526b57a47f2f2ab1d657aa",
            "project_name": "project two",
            "project_url": "http://www.projecttwo.com",
            "project_details": "project details two",
            "status": "Active",
            "isDeleted": false,
            "user_id": {
                "_id": "5d526b57a47f2f2ab1d657a6",
                "full_name": "Albina Glick",
                "email": "albina@glick.com"
            },
            "createdAt": "2019-08-13T07:48:39.390Z",
            "updatedAt": "2019-08-13T07:48:39.390Z",
            "__v": 0
        }
    ],
    "message": "Projects fetched Successfully"
}
*/
namedRouter.get("api.project.list", '/projects/list', async (req, res) => {
	try {
		const success = await projectController.getList(req);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error.message);
	}
});

/**
 * @api {post} /projects/update Project Update
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiHeader x-access-token User's Access token
 * @apiParam project_url Project ID
 * @apiParam project_name Project Name
 * @apiParam project_url Project URL
 * @apiParam project_details Project Details
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "_id": "5d526b57a47f2f2ab1d657a9",
        "project_name": "Apply For University",
        "project_url": "www.afu.com",
        "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
        "status": "Active",
        "isDeleted": false,
        "user_id": "5d526b57a47f2f2ab1d657a6",
        "createdAt": "2019-08-13T07:48:39.390Z",
        "updatedAt": "2019-08-13T09:37:32.578Z",
        "__v": 0
    },
    "message": "Projects updated Successfully"
}
*/
namedRouter.post("api.project.update", '/projects/update',request_param.any(),async (req, res) => {
	try {
		const success = await projectController.updateProject(req);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error.message);
	}
});

/**
 * @api {post} /projects/delete Project Delete
 * @apiVersion 1.0.0
 * @apiGroup Project
 * @apiHeader x-access-token User's Access token
 * @apiParam project_url Project ID
 * @apiSuccessExample {json} Success
 * {
    "status": 201,
    "data": {
        "_id": "5d526b57a47f2f2ab1d657a9",
        "project_name": "Apply For University",
        "project_url": "www.afu.com",
        "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
        "status": "Active",
        "isDeleted": false,
        "user_id": "5d526b57a47f2f2ab1d657a6",
        "createdAt": "2019-08-13T07:48:39.390Z",
        "updatedAt": "2019-08-13T09:37:32.578Z",
        "__v": 0
    },
    "message": "You cannot delete this as this project already been used"
}
*/
namedRouter.post("api.project.delete", '/projects/delete',request_param.any(),async (req, res) => {
	try {
		const success = await projectController.deleteProject(req);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error.message);
	}
});

module.exports = router;