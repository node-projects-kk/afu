const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const multer = require('multer');
const request_param = multer();
const faqController = require('webservice/faq.controller');


/**
 * @api {get} /faq/list List
 * @apiVersion 1.0.0
 * @apiGroup Faq
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [
        {
            "_id": "5da6da14c869c329f4a0b06a",
            "title": "What is Lorem Ipsum?",
            "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-10-16T08:51:32.751Z",
            "updatedAt": "2019-10-16T09:11:17.518Z",
            "__v": 0
        },
        {
            "_id": "5da6df81a63671397005a957",
            "title": "Where does it come from?",
            "content": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.",
            "status": "Active",
            "isDeleted": false,
            "createdAt": "2019-10-16T09:14:41.581Z",
            "updatedAt": "2019-10-16T09:25:33.996Z",
            "__v": 0
        }
    ],
    "message": "Faqs fetched Successfully"
}
*/
namedRouter.get("api.faq.list", '/faq/list', async (req, res) => {
    try {
        const success = await faqController.getList(req);
        res.status(success.status).send(success);
    }
    catch (error) {
        res.status(error.status).send(error.message);
    }
});


module.exports = router;