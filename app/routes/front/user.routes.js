const express = require('express');
const routeLabel = require('route-label');
const multer = require('multer');
const router = express.Router();
const namedRouter = routeLabel(router);
const fs = require('fs');
const userController = require('webservice/user.controller');
const chatController = require('webservice/chat.controller');
const staticController = require('webservice/static.controller');

//const request_param = multer();
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		console.log("12>>",file);
		if ((file.fieldname).indexOf('profile_image') > -1) {
			// check if directory exists
			if (!fs.existsSync('./public/uploads/user')) {
				// if not create directory
				fs.mkdirSync('./public/uploads/user');
			}
			cb(null, './public/uploads/user')
		}
		if ((file.fieldname).indexOf('logo') > -1) {
			// check if directory exists
			if (!fs.existsSync('./public/uploads/logo')) {
				// if not create directory
				fs.mkdirSync('./public/uploads/logo');
			}
			cb(null, './public/uploads/logo')
		}
	},
	filename: function (req, file, cb) {
		cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname)
	}
})
const request_param = multer({storage: storage});



/**
 * @api {post} /chat/new/create Chat page
 * @apiVersion 1.0.0
 * @apiGroup REST API CHAT
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "chat_token": "dPucrZQBwqIi0o8TlTwsseaYr2sjkTEWHJHer8PGtVI",
        "chat_date": "2019-07-23T10:46:57.731Z",
        "_id": "5d36e5ba24bf1f6694936c0e",
        "mentor_id": "5d31830d6d84b8436af1ce32",
        "student_id": "5d309257ae2ba07b64ea59f4",
        "__v": 0
    },
    "message": "Chat start"
}
*/
namedRouter.post('api.chat.newcreate','/chat/new/create',request_param.any(),function(req,res){
	staticController.createChatNew(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {get} /chat/createSessionToken Intregate firebase chat
 * @apiVersion 1.0.0
 * @apiGroup REST API CHAT
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Chat started successfully"
}
*/
namedRouter.get('api.chat.createSessionToken','/chat/createSessionToken',request_param.any(),function(req,res){
	staticController.createSessionToken(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {get} /chat/createToken/:sessionId Intregate Tokbox chat
 * @apiVersion 1.0.0
 * @apiGroup REST API CHAT
 * @apiHeader x-access-token User's Access token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": [],
    "message": "Chat token create successfully"
}
*/
namedRouter.get('api.chat.createToken','/chat/createToken/:sessionId',request_param.any(),function(req,res){
	staticController.createToken(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {post} /chat/sendattachment Send Attachment API
 * @apiVersion 1.0.0
 * @apiGroup REST API CHAT
 * @apiHeader x-access-token User's Access token
 * @apiParam file File
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": "1563802441970_ton.pdf",
    "message": "Attachment send successfully"
}
*/
namedRouter.post('api.chat.sendattachment','/chat/sendattachment',function(req,res){
	staticController.chatSendAttachment(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {post} /chat/sendpicture Send Picture API
 * @apiVersion 1.0.0
 * @apiGroup REST API CHAT
 * @apiHeader x-access-token User's Access token
  * @apiParam file Image
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": "image_1563802441970_img2.jpg",
    "message": "Picture send successfully"
}
*/
namedRouter.post('api.chat.sendpicture','/chat/sendpicture',request_param.any(),function(req,res){
	staticController.chatSendPicture(req).then(function (success) {
		res.send(success);
	},
	function (failure) {
		res.status(failure.status).send(failure);
	});
});

/**
 * @api {post} /message/reportmessage Report Message API
 * @apiVersion 1.0.0
 * @apiGroup REST API MESSAGE
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"_id":"5d3188206d84b8436af1ce37",
	"mentor_id":"5d306dc6822fa522c79acd21",
	"student_id":"5d306346822fa522c79a2dec",
	"count":5,
	"message_details":[
		{
			message:"message one"
		},
		{
			message:"message two"
		}
	]
  }
    "message": "Report message list successfully"
}
*/
namedRouter.post("api.static.reportmessage", '/message/reportmessage', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.reportmessage(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /message/delete Delete Message API
 * @apiVersion 1.0.0
 * @apiGroup REST API MESSAGE
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
  }
    "message": "Delete message successfully"
}
*/
namedRouter.post("api.mentor.messagedelete", '/message/delete', request_param.any(),  async (req, res) => {
	try {
		console.log("8asca>>");
		const success = await staticController.messagedelete(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /message/changestatus Read/Unread status Message API
 * @apiVersion 1.0.0
 * @apiGroup REST API MESSAGE
 * @apiHeader x-access-token User's Access token
 * @apiParam mentor_id Mentor ID 
 * @apiParam student_id Student ID
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"readstatus":"yes"
	"_id":"5d3188206d84b8436af1ce37"
  }
    "message": "message status updated successfully"
}
*/
namedRouter.post("api.mentor.messagestatuschange", '/message/changestatus', request_param.any(),  async (req, res) => {
	try {
		console.log("8asca>>");
		const success = await staticController.messagestatuschange(req,res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/offlineonline Offline Online Icon
 * @apiVersion 1.0.0
 * @apiGroup REST API USER
 * @apiHeader x-access-token User's Access token
 * @apiParam online_status Online Status
  * @apiSuccessExample {json} Success
 *{
    "status": 200,
    "data": {
        "full_name": "Josephine Darakjy",
        "email": "josephine_darakjy@darakjy.org",
        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
        "online_status":true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d3188206d84b8436af1ce37",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-19T09:06:40.023Z",
        "updatedAt": "2019-08-16T09:14:15.232Z",
        "__v": 0
    },
    "message": "Settings update successfully"
}
*/
namedRouter.post("api.student.offlineonline", '/student/offlineonline', request_param.any(), async (req, res) => {
	try {
		console.log("xcasdca");
		const success = await staticController.offlineonlinestatus(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log("xcasdca");
		res.status(error.status).send(error);
	}
});

/**
 * @api {post} /student/signin Student Login
 * @apiVersion 1.0.0
 * @apiGroup REST API USER
 * @apiParam email Student Email 
 * @apiParam password Student Password
 * @apiParam social_id Social ID (If Social Login)
 * @apiParam register_type Register Type (facebook/google/normal/linkedin/instagram)
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "school_name": "",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "isLoggedIn": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0",
    "message": "Login successfull Student."
}
*/
namedRouter.post("api.student.signin", '/student/signin', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentsignin(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
* @api {post} /student/step1  Signup API step One
* @apiVersion 1.0.0
* @apiGroup REST API USER
* @apiParam full_name Student Name 
* @apiParam email Student Email 
 * @apiParam password Student Password
 * @apiParam social_id Social ID (If Social Login)
 * @apiParam register_type Register Type (facebook/google/normal/linkedin/instagram)
* 
 * @apiSuccessExample {json} Success Student
 * {
    "status": 200,
    "data": {
        "full_name": "Subhra",
        "email": "subhra11@gmail.com",
        "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
        "school_name": "",
        "dob": "",
        "social_id": "",
        "register_type": "normal",
        "deviceToken": "",
        "deviceType": "",
        "push_notification": true,
        "isDeleted": false,
        "isActive": true,
        "_id": "5d35bb4a38104765642d2b21",
        "role": "5d30518c822fa522c7991272",
        "createdAt": "2019-07-22T13:34:02.101Z",
        "updatedAt": "2019-07-22T13:34:02.101Z",
        "__v": 0
    },
    "message": "Registration step one successfull."
}
*/
namedRouter.post("api.student.step1", '/student/step1', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.studentstep1(req,res);
		//console.log("81>>",success);
		res.status(success.status).send(success);
	}
	catch (error) {
		console.log('85>>', error.message)
		res.status(error.status).send(error);
	}
});

/**
 * @api {GET} /messagelist Message List(Inbox)
 * @apiVersion 1.0.0
 * @apiGroup REST API MESSAGE
 * @apiHeader x-access-token User's Access Token
 * @apiSuccessExample {json} Success
 * {
    "status": 200,
    "data": {
	"_id":"5d3188206d84b8436af1ce37",
	"mentor_id":"5d306dc6822fa522c79acd21",
	"student_id":"5d306346822fa522c79a2dec",
	"count":5,
	"message_details":[
		{
			message:"hi"
		},
		{
			message:"how are you"
		}
	]
  }
    "message": "Message list fetched successfully"
}
*/
namedRouter.get("api.user.messagelist", '/messagelist', request_param.any(), async (req, res) => {
	try {
		const success = await staticController.messagelist(req, res);
		res.status(success.status).send(success);
	}
	catch (error) {
		res.status(error.status).send(error);
	}
});


module.exports = router;