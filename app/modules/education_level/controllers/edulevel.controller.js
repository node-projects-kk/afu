const edulevelRepo = require('education_level/repositories/education_level.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class edulevelController {
	constructor() {
		this.edulevel = [];
	}

	async create(req, res) {
		try {
			res.render('education_level/views/create.ejs', {
				page_name: 'edulevel-list',
				page_title: 'Create Education Level',
				user: req.user,
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let edulevel =  await edulevelRepo.getByField({ 'level_name': { $regex: req.body.level_name, $options: 'i' } ,'isDeleted':false});
			//let edulevel = await edulevelRepo.getByField({ 'level_name': req.body.level_name, 'isDeleted': false });
			//console.log("29>>",edulevel); process.exit();
			
			if (_.isEmpty(edulevel)) {
				let edulevelSave = await edulevelRepo.save(req.body); 
				if(edulevelSave){
					req.flash('success', "Education level created successfully.");
					res.redirect(namedRouter.urlFor('admin.edulevel.list'));
				}    
			}
			else {
				req.flash('error', "This education level already exist!");
				res.redirect(namedRouter.urlFor('admin.edulevel.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  edulevel update page
    */
    async edit (req, res){
        try
        {
            let result = {};
            let edulevel = await edulevelRepo.getById(req.params.id);
            if (!_.isEmpty(edulevel)) {
                result.edulevel_data = edulevel;
                res.render('education_level/views/edit.ejs', {
                    page_name: 'edulevel-management',
                    page_title: 'Edit Education Level',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry education level not found!");
                res.redirect(namedRouter.urlFor('admin.edulevel.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: edulevel update action
    */
    async update (req, res){
        try {
            const edulevelId = req.body.edulevel_id;
           // let edulevel = await edulevelRepo.getByField({'level_name':req.body.level_name,_id:{$ne:edulevelId}});
           let edulevel =  await edulevelRepo.getByField({ 'level_name': { $regex: req.body.level_name, $options: 'i' } ,_id:{$ne:edulevelId}});
			
            if (_.isEmpty(edulevel)) {
                    let edulevelUpdate = await edulevelRepo.updateById(req.body,edulevelId)
                    if(edulevelUpdate) {
                        req.flash('success', "Education level updated successfully");
                        res.redirect(namedRouter.urlFor('admin.edulevel.list'));
                    }
                    
                }else{
                req.flash('error', "Education level is already available!");
                res.redirect(namedRouter.urlFor('admin.edulevel.edit', { id: edulevelId }));
            }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the edulevels from DB
    */
    async list (req, res){
            try
            {
                res.render('education_level/views/list.ejs', {
                    page_name: 'edulevel-list',
                    page_title: 'Education Level List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let edulevel = await edulevelRepo.getAll(req);
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": edulevel.pageCount, "perpage": req.body.pagination.perpage, "total": edulevel.totalCount, "sort": sortOrder, "field": sortField};
			return {status: 200, meta: meta, data:edulevel.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: edulevel status change action
    */
	async changeStatus (req, res){
		try {
			//console.log("147>>",req.params.id);
			let edulevel = await edulevelRepo.getById(req.params.id);
			//console.log("149>>",edulevel); process.exit();
			if(!_.isEmpty(edulevel)){
				let edulevelStatus = (edulevel.status == "Active") ? "Inactive" : "Active";
				let edulevelUpdate= await edulevelRepo.updateById({"status": edulevelStatus }, req.params.id);
				req.flash('success', "Education level status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.edulevel.list'));
			}
			else {
				req.flash('error', "Sorry education level not found");
				res.redirect(namedRouter.urlFor('admin.edulevel.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: edulevel delete
    */
    async destroy (req, res){
        try{
            let edulevelDelete = await edulevelRepo.delete(req.params.id)
            if(!_.isEmpty(edulevelDelete)){
                req.flash('success','Education level removed successfully');
                res.redirect(namedRouter.urlFor('admin.edulevel.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };

}

module.exports = new edulevelController();