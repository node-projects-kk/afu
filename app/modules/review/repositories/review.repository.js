const mongoose = require('mongoose');
const Review = require('review/models/review.model');
const perPage = config.PAGINATION_PERPAGE;

class ReviewRepository {
	constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({
				"isDeleted": false
			});

			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({
					'review_name': {
						$regex: req.body.query.generalSearch,
						$options: 'i'
					}
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({
					"status": req.body.query.Status
				});
			}
			conditions['$and'] = and_clauses;

			var sortOperator = {
				"$sort": {}
			};
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				} else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			} else {
				sortOperator["$sort"]['_id'] = -1;
			}

			var aggregate = Review.aggregate([{
					$match: conditions
				},
				sortOperator
			]);

			var options = {
				page: req.body.pagination.page,
				limit: req.body.pagination.perpage
			};
			let allReview = await Review.aggregatePaginate(aggregate, options);
			return allReview;
		} catch (e) {
			throw (e);
		}
	}

	async getAllReview(params) {
		try {
			const _params = {
				...params,
			};
			return await Review.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await Review.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			const _params = {
				...params,
			};
			return await Review.findOne(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			const _params = {
				...params,
				"isDeleted": false,
			};
			return await Review.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllReviewByField(id) {
		try {
			return await Review.find({
				'mentor_id': mongoose.Types.ObjectId(id)
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await Review.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getReviewCount() {
		try {
			return await Review.count({
				"isDeleted": false
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			const _save = new Review(data);
			return await _save.save();
		} catch (error) {
			return error;
		}
	}

	async getActiveReview(params) {
		try {
			const _params = {
				...params,
				"isDeleted": false,
			};
			return await Review.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			await Review.findById(id).lean().exec();
			return await Review.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllReviewByPackageId(id) {
		try {
			return await Review.aggregate([{
					$match: {
						"review_package_id": mongoose.Types.ObjectId(id),
					}
				},
				{
					$lookup: {
						from: 'packages',
						localField: 'review_package_id',
						foreignField: '_id',
						as: 'package_info'
					}
				},
				{
					$unwind: {
						path: "$package_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'users',
						localField: 'review_from_user_id',
						foreignField: '_id',
						as: 'user_info'
					}
				},
				{
					$unwind: {
						path: "$user_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$project: {
						_id: "$_id",
						rating: "$rating",
						review_comment: "$review_comment",
						package_id: "$review_package_id",
						package_name: "$package_info.package_name",
						package_price: "$package_info.package_price",
						package_description: "$package_info.package_description",
						package_status: '$package_info.status',
						user_id: "$review_from_user_id",
						user_name: "$user_info.full_name",
						user_email: "$user_info.email",
						createdAt: "$createdAt"
					}
				},
				{
					$match: {
						"package_status": "Active"
					}
				},
				{
					$sort: {
						'createdAt': -1
					}
				}
			]);
		} catch (error) {
			return error;
		}
	}

}
module.exports = new ReviewRepository();