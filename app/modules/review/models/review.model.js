const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const ReviewSchema = new Schema({
	rating: {
		type: Number,
		default: 0
	},
	review_comment: {
		type: String,
		default: ''
	},
	review_from_user_id: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	mentor_id: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	review_package_id: {
		type: Schema.Types.ObjectId,
		ref: 'Package'
	},
}, {
	timestamps: true
});

// For pagination
ReviewSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Review', ReviewSchema);