const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const ReferralSchema = new Schema({
	mentor_id: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	mentee_id: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: [true, false]
	},
	status: {
		type: String,
		default: 'Active',
		enum: ['Active', 'Inactive']
	},
	createdAt: {
		type: Date,
		default: Date.now()
	},
});

// For pagination
ReferralSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Referral', ReferralSchema);