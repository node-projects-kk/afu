const mongoose = require('mongoose');
const Referral = require('referral/models/referral.model');
const perPage = config.PAGINATION_PERPAGE;

class ReferralRepository {
	constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({
				"isDeleted": false
			});

			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({
					'review_name': {
						$regex: req.body.query.generalSearch,
						$options: 'i'
					}
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({
					"status": req.body.query.Status
				});
			}
			conditions['$and'] = and_clauses;

			var sortOperator = {
				"$sort": {}
			};
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				} else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			} else {
				sortOperator["$sort"]['_id'] = -1;
			}

			var aggregate = Referral.aggregate([{
					$match: conditions
				},
				sortOperator
			]);

			var options = {
				page: req.body.pagination.page,
				limit: req.body.pagination.perpage
			};
			let allReferral = await Referral.aggregatePaginate(aggregate, options);
			return allReferral;
		} catch (e) {
			throw (e);
		}
	}

	async getAllReferral(params) {
		try {
			const _params = {
				...params,
			};
			return await Referral.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await Referral.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			const _params = {
				...params,
			};
			return await Referral.findOne(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			const _params = {
				...params,
				"isDeleted": false,
			};
			return await Referral.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await Referral.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getReferralCount() {
		try {
			return await Referral.count({
				"isDeleted": false
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			const _save = new Referral(data);
			return await _save.save();
		} catch (error) {
			return error;
		}
	}

	async getActiveReferral(params) {
		try {
			const _params = {
				...params,
				"isDeleted": false,
			};
			return await Referral.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			await Referral.findById(id).lean().exec();
			return await Referral.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllReferralList(req) {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"status": "Active"
			});
			//and_clauses.push({"status": "Active"});
			// and_clauses.push({
			// 	"mentor_id": mongoose.Types.ObjectId(id)
			// });

			conditions['$and'] = and_clauses;

			let referrals = await Referral.aggregate([{
					$sort: {
						createdAt: -1
					}
				},
				{
					"$lookup": {
						"from": "users",
						"localField": "mentor_id",
						"foreignField": "_id",
						"as": "mentor_info"
					},
				},
				{
					$unwind: "$mentor_info"
				},
				{
					"$lookup": {
						"from": "users",
						"localField": "mentee_id",
						"foreignField": "_id",
						"as": "mentee_info"
					},
				},
				{
					$unwind: "$mentee_info"
				},
				{
					$match: conditions
				},
				{
					$project: {
						_id: 1,
						mentor_id: 1,
						mentee_id: 1,
						createdAt: 1,
						'mentor_info._id': 1,
						'mentor_info.full_name': 1,
						'mentor_info.email': 1,
						'mentor_info.profile_image': 1,
						'mentee_info._id': 1,
						'mentee_info.full_name': 1,
						'mentee_info.email': 1,
					}
				},
				{
					$group: {
						_id: {
							mentor_id: "$mentor_id"
						},
						mentor_info: {
							$first: "$mentor_info"
						},
						mentee_info: {
							$addToSet: "$mentee_info"
						},
						last_refer_date: {
							$first: "$createdAt"
						},
						count: {
							$sum: 1
						}
					}
				},
			]).exec();
			return referrals;
		} catch (e) {
			throw (e);
		}
	}

	async getMentorReferralCount(id) {
		try {
			let referralCount = await Referral.find({
				'mentor_id': mongoose.Types.ObjectId(id),
				'isDeleted': false
			}).count();
			return referralCount;
		} catch (e) {
			return e;
		}
	}

	async getMentorWiseReferralList(id) {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"status": "Active"
			});

			and_clauses.push({
				"mentor_id": mongoose.Types.ObjectId(id)
			});

			conditions['$and'] = and_clauses;

			let referrals = await Referral.aggregate([{
					$sort: {
						createdAt: -1
					}
				},
				{
					$match: conditions
				},
				{
					"$lookup": {
						"from": "users",
						"localField": "mentee_id",
						"foreignField": "_id",
						"as": "mentee_info"
					},
				},
				{
					$unwind: "$mentee_info"
				},

				{
					$project: {
						_id: 1,
						mentor_id: 1,
						mentee_id: 1,
						createdAt: 1,
						'mentee_info._id': 1,
						'mentee_info.full_name': 1,
						'mentee_info.email': 1,
						'mentee_info.profile_image': 1,
					}
				},
			]).exec();
			return referrals;
		} catch (e) {
			throw (e);
		}
	}


}

module.exports = new ReferralRepository();