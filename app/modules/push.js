var Q = require('q');
var FCM = require('fcm-push');
var apn = require('apn');
var config = require('../config')
//var pushModel = require('../../models/pushModel')

// ================= for ANDROID push Notification ==========================================
var serverKey = config.android_serverKey;
var fcm = new FCM(serverKey);
// ================= for ANDROID push Notification ==========================================


// ================= IOS push Notification ==========================================
var options = {
	token: {
		key: config.ios_key,
		keyId: config.ios_keyId,
		teamId: config.ios_teamId
	},
	production: false
};

//console.log("23>>",options);
var apnProviderForDriver = new apn.Provider(options);
// ================= IOS push Notification ==========================================


exports.sendPush = function (deviceType, deviceToken, title, from, to, params) {
	var deferred = Q.defer();
	//console.log("DeviceType="+deviceType);
	//console.log("DeviceToken="+deviceToken);
	//console.log("Params=" + params); 
	if (deviceType.toLowerCase() == "ios") { //console.log("here ios");

		var note = new apn.Notification();
		note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
		note.badge = 1;
		note.title = title;
		note.payload = params;
		note.body = params.text;
		note.topic = "com.webskitters.AFU";

		apnProviderForDriver.send(note, deviceToken).then((result) => {

			// console.log("44>>",note);

			//let _pushModel = new pushModel();  
			//_pushModel.title = title;
			//_pushModel.to = to;
			//_pushModel.message = params;
			//_pushModel.date = new Date();
			//
			//_pushModel.save(function (err) {
			//	if (err)
			//		deferred.reject({ "status": 500, data: [], "message": err.message });
			//	deferred.resolve({ "status": 200, data: [], "message": "Saved Successfully" });
			//});
			// console.log("Successfully sent with response: ", result);
			//console.log(result.failed);
		});
	} else {
		var message = {
			to: deviceToken,
			collapse_key: 'new_messages',
			data: {
				apikey: params.apiKey,
				session: params.sessionvalue,
				token: params.token,
				type: params.type,
				student_id: params.student_id,
				mentor_id: params.mentor_id
			},
			notification: {
				title: title,
				body: params.token,
				sound: 'default',
				tag: params.sessionvalue,
				click_action: params.clickaction,
			}
		};

		// console.log("76>>",message);

		fcm.send(message).then(function (response) {
				//response = JSON.parse(response);
				//let _pushModel = new pushModel();
				//_pushModel.title = title;
				//_pushModel.from = from;
				//_pushModel.to = to;
				//_pushModel.message = params;
				//_pushModel.date = new Date();
				//
				//_pushModel.save(function (err) {
				//	if (err)
				//		deferred.reject({ "status": 500, data: [], "message": err.message });
				//	deferred.resolve({ "status": 200, data: [response], "message": "Saved Successfully" });
				//});
				// console.log("Successfully sent with response: ", response);
			})
			.catch(function (err) {
				// console.log("102>>",err);
				//console.log("Something has gone wrong!");
				deferred.reject({
					"status": 500,
					data: [],
					"message": err
				});
			});
	}
	//return deferred.promise;
};