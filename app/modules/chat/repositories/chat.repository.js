const mongoose = require('mongoose');
const Chat = require('chat/models/chat.model');
const perPage = config.PAGINATION_PERPAGE;

const chatRepository = {

    getAll: async  (searchQuery, sortOrder = {
        '_id': -1
    }, page) => {
        
            try {
                const query = [{
                    //"status": "Active"
                }];
                // serach by keyword
                if (_.has(searchQuery, "keyword")) {
                    if (searchQuery.keyword != '') {
                        let search1 = searchQuery.keyword.trim();
                        const search = search1.charAt(0).toUpperCase() + search1.slice(1);
    
                        query.push({
                            "$or": [{
                                'title': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'status': search
                            }]
                        });
                    }
                }
                searchQuery = {
                    "$and": query
                };
                const aggregate = Chat.aggregate([{
                    "$sort": sortOrder
                },
                {
                    $project: {
                        _id: "$_id",
                        title: "$title",
                        content: "$content",
                        status: "$status",
                        slug: "$slug"
                    }
                },
                {
                    $match: searchQuery
                },
                ]);
                return await Chat.aggregatePaginate(aggregate, {
                    page: page,
                    limit: perPage
                }); // { data, pageCount, totalCount }
            } catch (error) {
                throw error;
            }
        
    },

    getById: async (id) => {
        let chat = await Chat.findById(id).lean().exec();
        try {
            if (!chat) {
                return null;
            }
            return chat;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let chat = await Chat.findOne(params).exec();
        try {
            if (!chat) {
                return null;
            }
            return chat;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let chat = await Chat.find(params).exec();
        try {
            if (!chat) {
                return null;
            }
            return chat;

        } catch (e) {
            return e;
        }
    },

    getChatCount: async (params) => {
        try {
            let chatCount = await Chat.countDocuments(params);
            if (!chatCount) {
                return null;
            }
            return chatCount;
        } catch (e) {
            return e;
        }

    },

	save: async (data) => {
		try {
			let user = await Chat.create(data);
			if (!user) {
				return null;
			}
			return user;  
		}
		catch (e){
			return e;  
		}     
	},
	
    delete: async (id) => {
        try {
            let chat = await Chat.findById(id);
            if (chat) {
                let chatDelete = await Chat.remove({ _id: id }).exec();
                if (!chatDelete) {
                    return null;
                }
                return chatDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let chat = await Chat.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            if (!chat) {
                return null;
            }
            return chat;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },


};

module.exports = chatRepository;