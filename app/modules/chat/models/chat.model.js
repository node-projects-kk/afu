const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const deleted = [true, false];

const ChatSchema = new Schema({
  chat_token: {
    type: String,
    default: ''
  },
  mentor_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  student_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    default: null
  },
  university_id: {
    type: Schema.Types.ObjectId,
    ref: 'University',
    default: null
  },
  chat_date: {
    type: Date,
    default: Date.now()
  },
  isDeleted: {
    type: Boolean,
    default: false,
    enum: deleted
  }
});

// For pagination
ChatSchema.plugin(mongooseAggregatePaginate);
// create the model for users and expose it to our app
module.exports = mongoose.model('Chat', ChatSchema);