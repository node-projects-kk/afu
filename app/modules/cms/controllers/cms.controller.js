const cmsRepo = require('cms/repositories/cms.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var slugify = require('slugify');


class cmsController {
    constructor() {
        this.cms = [];
        
    }

    /*
    // @Method: edit
    // @Description:  Coupon update page
    */
    async edit (req, res){
        try
        {
            let result = {};
            let cms = await cmsRepo.getById(req.params.id);
            if (!_.isEmpty(cms)) {
                result.cms_data = cms;
                res.render('cms/views/edit.ejs', {
                    page_name: 'cms-list',
                    page_title: 'Update cms',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry CMS not found!");
                res.redirect(namedRouter.urlFor('admin.cms.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: coupon update action
    */
	async update (req, res){
		try {
			const cmsId = req.body.id;
			let cms = await cmsRepo.getByField({'title':req.body.title,_id:{$ne:cmsId}});
			req.body.slug= slugify(req.body.title, { replacement: '-',remove: null,lower: true });
		
			if (_.isEmpty(cms)) {
				let cmsIdUpdate = cmsRepo.updateById(req.body,cmsId)
				if(cmsIdUpdate) {
					req.flash('success', "CMS updated successfully");
					res.redirect(namedRouter.urlFor('admin.cms.list'));
				}   
			}
			else{
				req.flash('error', "CMS is already available!");
				res.redirect(namedRouter.urlFor('admin.cms.edit', { id: cmsId }));
			}    
		}
		catch(e){
			return res.status(500).send({message: e.message});  
		}     
	};


    /* @Method: list
    // @Description: To get all the users from DB
    */
    async list (req, res){
            try
            {
                res.render('cms/views/list.ejs', {
                    page_name: 'cms-list',
                    page_title: 'CMS List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let cms = await cmsRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
		
			let meta = {
				"page": req.body.pagination.page,
				"pages": cms.pageCount,
				"perpage": req.body.pagination.perpage,
				"total": cms.totalCount,
				"sort": sortOrder,
				"field": sortField
			};
		
			return {status: 200, meta: meta, data:cms.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: coupon status change action
    */
	async statusChange (req, res){
		try {
			let cms = await cmsRepo.getById(req.params.id);
			//console.log("123>>",cms);
			if(!_.isEmpty(cms)){
				let cmsStatus = (cms.status == "Active") ? "Inactive" : "Active";
				let cmsUpdate = cmsRepo.updateById({ "status": cmsStatus }, req.params.id);
				req.flash('success', "CMS status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.cms.list'));
			}
			else {
				req.flash('error', "Sorry CMS not found");
				res.redirect(namedRouter.urlFor('admin.cms.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy (req, res){
        try{
            let cmsDelete = await cmsRepo.delete(req.params.id)
            if(!_.isEmpty(cmsDelete)){
                req.flash('success','CMS removed successfully');
                res.redirect(namedRouter.urlFor('admin.cms.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };
}

module.exports = new cmsController();