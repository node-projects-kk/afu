const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const deleted = [true, false];
const status = ["Active", "Inactive"];

const SettingSchema = new Schema({
  setting_name: { type: String, default: '' },
  setting_slug: { type: String, default: '' },
  setting_value: { type: String, default: '' },
   isDeleted: { type: Boolean, default: false, enum: deleted },
});

// For pagination
SettingSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Setting', SettingSchema);