const settingRepo = require('setting/repositories/setting.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const fs = require('fs');
var slugify = require('slugify');


class settingController {
    constructor() {
        this.setting = [];
        
    }

	/*
	// @Method: edit
	// @Description:  Setting update page
	*/
	async edit (req, res){
		try{
			let result = {};
			let setting = await settingRepo.getById(req.params.id);
			if (!_.isEmpty(setting)) {
				result.setting_data = setting;
				res.render('setting/views/edit.ejs', {
					page_name: 'setting-management',
					page_title: 'Update setting',
					user: req.user,
					response: result
				});
			}
			else {
				req.flash('error', "Sorry Setting not found!");
				res.redirect(namedRouter.urlFor('admin.setting.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

	/* @Method: update
	// @Description: coupon update action
	*/
	async update (req, res){
		try {
			const settingId = req.body.id;
			let coupon = await settingRepo.getByField({'setting_name':req.body.setting_name,_id:{$ne:settingId}});
			req.body.setting_slug= slugify(req.body.setting_name, { replacement: '-',remove: null,lower: true }); 
		
			//console.log("53>>",req.body.setting_slug);
			//console.log("54>>",req.body); process.exit();
		
			if (_.isEmpty(coupon)) {
				let settingIdUpdate = settingRepo.updateById(req.body,settingId)
				if(settingIdUpdate) {
					req.flash('success', "Setting updated successfully");
					res.redirect(namedRouter.urlFor('admin.setting.list'));
				}   
			}
			else{
				req.flash('error', "Setting is already available!");
				res.redirect(namedRouter.urlFor('admin.setting.edit', { id: settingId }));
			}    
		}
		catch(e){
			return res.status(500).send({message: e.message});  
		}     
	};

	/* @Method: list
	// @Description: To get all the users from DB
	*/
    async list (req, res){
            try
            {
                res.render('setting/views/list.ejs', {
                    page_name: 'setting-management',
                    page_title: 'Setting List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{ console.log("89>>","AAA");
			let setting = await settingRepo.getAll(req);
			//console.log("91>>",setting); process.exit();
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
		
			let meta = {
				"page": req.body.pagination.page,
				"pages": setting.pageCount,
				"perpage": req.body.pagination.perpage,
				"total": setting.totalCount,
				"sort": sortOrder,
				"field": sortField
			};
			//console.log("109>>",setting.data); process.exit();
			return {status: 200, meta: meta, data:setting.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: coupon status change action
    */
	async statusChange (req, res){
		try {
			let setting = await settingRepo.getById(req.params.id);
			console.log("123>>",setting);
			if(!_.isEmpty(setting)){
				let settingStatus = (setting.status == "Active") ? "Inactive" : "Active";
				let settingUpdate = settingRepo.updateById({ "status": settingStatus }, req.params.id);
				req.flash('success', "Setting status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.setting.list'));
			}
			else {
				req.flash('error', "Sorry Setting not found");
				res.redirect(namedRouter.urlFor('admin.setting.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: coupon delete
    */
    async destroy (req, res){
        try{
           
            let settingDelete = await settingRepo.delete(req.params.id)
            if(!_.isEmpty(settingDelete)){
                req.flash('success','Setting removed successfully');
                res.redirect(namedRouter.urlFor('admin.setting.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };
}

module.exports = new settingController();