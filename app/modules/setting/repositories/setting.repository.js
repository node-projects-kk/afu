const mongoose = require('mongoose');
const Setting = require('setting/models/setting.model');
const role = require('role/models/role.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;

const settingRepository = {

	getAll: async (req) => {
		
		try {
			var conditions = {};
			var and_clauses = [];
		
			and_clauses.push({"isDeleted": false});
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				
				//and_clauses.push({'title': {	$regex: req.body.query.generalSearch,$options: 'i'}});
				
				and_clauses.push({
					$or: [
						{ 'setting_name': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'setting_value': { $regex:  req.body.query.generalSearch, $options: 'i'} }
					]
				});
			}
			
			//if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
			//	and_clauses.push({"status": req.body.query.Status});
			//}
			conditions['$and'] = and_clauses;
		
			var sortOperator = {"$sort": {}};
			
			if (_.has(req.body, 'sort')) { 
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				//sortOperator["$sort"]['_id'] = -1;
				sortOperator["$sort"]['setting_name'] = 1;
			}
			console.log("48>>",sortOperator); 
			var aggregate = Setting.aggregate([
							{$match: conditions},
							sortOperator
			]);
			//console.log("53>>",aggregate); 
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allSetting = await Setting.aggregatePaginate(aggregate, options);
			return allSetting;
		}
		catch (e) {
			throw (e);
		}
	},


   

    getById: async (id) => {
        let setting = await Setting.findById(id).exec();
        try {
            if (!setting) {
                return null;
            }
            return setting;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let setting = await Setting.findOne(params).exec();
        try {
            if (!setting) {
                return null;
            }
            return setting;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let setting = await Setting.find(params).exec();
        try {
            if (!setting) {
                return null;
            }
            return setting;

        } catch (e) {
            return e;
        }
    },

    getSettingCount: async (params) => {
        try {
            let settingCount = await Setting.countDocuments(params);
            if (!settingCount) {
                return null;
            }
            return settingCount;
        } catch (e) {
            return e;
        }

    },




    delete: async (id) => {
        try {
            let setting = await Setting.findById(id);
            if (setting) {
                let settingDelete = await Setting.remove({ _id: id }).exec();
                if (!settingDelete) {
                    return null;
                }
                return settingDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let setting = await Setting.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            if (!setting) {
                return null;
            }
            return setting;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },






};

module.exports = settingRepository;