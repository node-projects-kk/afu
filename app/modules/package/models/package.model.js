const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const status = ["Active", "Inactive"];

const PackageSchema = new Schema({
  package_name : { type: String, default: '' },
  package_price : { type:Schema.Types.Double, default:0.00 },
  package_description:{type:String,default:''},
  package_start_time:{type:String,default:''},
  package_end_time:{type:String,default:''},
  user_id:{type: Schema.Types.ObjectId,ref: 'User'},
  status: { type: String, default: "Active", enum: status },
  isDeleted: { type: Boolean, default: false, enum: deleted },
},{timestamps:true});

// For pagination
PackageSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Package', PackageSchema);