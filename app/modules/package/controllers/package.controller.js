const packageRepo = require('package/repositories/package.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class packageController {
	constructor() {
		this.package = [];
	}

	async create(req, res) {
		try {
			res.render('package/views/create.ejs', {
				page_name: 'package-list',
				page_title: 'Create Package',
				user: req.user,
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let packagelist =  await packageRepo.getByField({ 'package_name': { $regex: req.body.package_name, $options: 'i' } ,'isDeleted':false});
			//console.log("29>>",package); process.exit();
			
			if (_.isEmpty(packagelist)) {
				req.body.package_price = parseFloat(req.body.package_price);
				let packageSave = await packageRepo.save(req.body); 
				if(packageSave){
					req.flash('success', "Package created successfully.");
					res.redirect(namedRouter.urlFor('admin.package.list'));
				}    
			}
			else {
				req.flash('error', "This package already exist!");
				res.redirect(namedRouter.urlFor('admin.package.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  package update page
    */
	async edit (req, res){
		try{
			let result = {};
			let packagelist = await packageRepo.getById(req.params.id);
			
			if (!_.isEmpty(packagelist)) {
				result.package_data = packagelist;
				res.render('package/views/edit.ejs', {
					page_name: 'package-management',
					page_title: 'Edit Package',
					user: req.user,
					response: result
				});
			}
			else {
				req.flash('error', "Sorry package not found!");
				res.redirect(namedRouter.urlFor('admin.package.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

	/* @Method: update
	// @Description: package update action
	*/
	async update (req, res){
		try {
			const packageId = req.body.package_id;
			//  let package = await packageRepo.getByField({'package_name':req.body.package_name,_id:{$ne:packageId}});
			let packagelist =  await packageRepo.getByField({ 'package_name': { $regex: req.body.package_name, $options: 'i' } ,_id:{$ne:packageId}});
			if (_.isEmpty(packagelist)) {
				req.body.package_price = parseFloat(req.body.package_price);
				let packageUpdate = await packageRepo.updateById(req.body,packageId);
				if(packageUpdate) {
					req.flash('success', "Package updated successfully");
					res.redirect(namedRouter.urlFor('admin.package.list'));
				}
			}
			else{
				req.flash('error', "Package is already available!");
				res.redirect(namedRouter.urlFor('admin.package.edit', { id: packageId }));
			}    
		}
		catch(e){
			return res.status(500).send({message: e.message});  
		}
	};



    /* @Method: list
    // @Description: To get all the packages from DB
    */
    async list (req, res){
            try
            {
                res.render('package/views/list.ejs', {
                    page_name: 'package-list',
                    page_title: 'Package List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let packagelist = await packageRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": packagelist.pageCount, "perpage": req.body.pagination.perpage, "total": packagelist.totalCount, "sort": sortOrder, "field": sortField};
			return {status: 200, meta: meta, data:packagelist.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: package status change action
    */
	async changeStatus (req, res){
		try {
			//console.log("147>>",req.params.id);
			let packagelist = await packageRepo.getById(req.params.id);
			//console.log("149>>",package); process.exit();
			if(!_.isEmpty(packagelist)){
				let packageStatus = (packagelist.status == "Active") ? "Inactive" : "Active";
				let packageUpdate= await packageRepo.updateById({"status": packageStatus }, req.params.id);
				req.flash('success', "Package status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.package.list'));
			}
			else {
				req.flash('error', "Sorry package not found");
				res.redirect(namedRouter.urlFor('admin.package.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: package delete
    */
	async destroy (req, res){
		try {
			const packageDelete = await packageRepo.updateById({'isDeleted': true}, req.params.id);
			if(!_.isEmpty(packageDelete)){
				req.flash('success','Package removed successfully');
				res.redirect(namedRouter.urlFor('admin.package.list'));
			} 
		}
		catch (error) {
			return res.status(500).send({message: e.message});  
		}
	};

}

module.exports = new packageController();