const Package = require('package/models/package.model');
const perPage = config.PAGINATION_PERPAGE;

class PackageRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({ 'package_name': { $regex: req.body.query.generalSearch, $options: 'i' } });
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
	
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  Package.aggregate([
								{ $match: conditions },
								sortOperator
							]);
	
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allPackage = await Package.aggregatePaginate(aggregate, options);
			return allPackage;
		}
		catch (e) {
			throw (e);
		}
	}
    
    
    
    async getAllPackage(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Package.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await Package.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Package.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Package.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Package.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getPackageCount() {
        try {
            return await Package.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new Package(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActivePackage(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Package.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Package.findById(id).lean().exec();
            return await Package.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new PackageRepository();