const mongoose = require('mongoose');
const Order = require('order/models/order.model');
const perPage = config.PAGINATION_PERPAGE;

class OrderRepository {
	constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({
				"isDeleted": false
			});

			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({
					'order_name': {
						$regex: req.body.query.generalSearch,
						$options: 'i'
					}
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({
					"status": req.body.query.Status
				});
			}
			conditions['$and'] = and_clauses;

			var sortOperator = {
				"$sort": {}
			};
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				} else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			} else {
				sortOperator["$sort"]['_id'] = -1;
			}

			var aggregate = Order.aggregate([{
					$match: conditions
				},
				sortOperator
			]);

			var options = {
				page: req.body.pagination.page,
				limit: req.body.pagination.perpage
			};
			let allOrder = await Order.aggregatePaginate(aggregate, options);
			return allOrder;
		} catch (e) {
			throw (e);
		}
	}

	async getAllOrder(params) {
		try {
			const _params = {
				...params,
			};
			return await Order.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getById(id) {
		try {
			return await Order.findById(id).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			const _params = {
				...params,
			};
			return await Order.findOne(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getAllByField(params) {
		try {
			const _params = {
				...params
			};
			return await Order.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async updateById(data, id) {
		try {
			return await Order.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async getOrderCount() {
		try {
			return await Order.count({
				"isDeleted": false
			});
		} catch (error) {
			return error;
		}
	}

	async save(data) {
		try {
			const _save = new Order(data);
			return await _save.save();
		} catch (error) {
			return error;
		}
	}

	async getActiveOrder(params) {
		try {
			const _params = {
				...params,
				"isDeleted": false,
			};
			return await Order.find(_params).lean().exec();
		} catch (error) {
			return error;
		}
	}

	async delete(id) {
		try {
			await Order.findById(id).lean().exec();
			return await Order.deleteOne({
				_id: id
			}).lean().exec();
		} catch (error) {
			return error;
		}
	}


	async getOrderHistoryByStudentId(studentId) {
		try {
			return await Order.aggregate([{
					$match: {
						'student_id': mongoose.Types.ObjectId(studentId)
					}
				},
				{
					$lookup: {
						from: 'packages',
						localField: 'package_id',
						foreignField: '_id',
						as: 'package_info'
					}
				},
				{
					$unwind: {
						path: "$package_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'users',
						localField: 'mentor_id',
						foreignField: '_id',
						as: 'mentor_info'
					}
				},
				{
					$unwind: {
						path: "$mentor_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$sort: {
						createdAt: -1
					}
				}
			]);
		} catch (e) {
			throw (e);
		}
	}




}

module.exports = new OrderRepository();