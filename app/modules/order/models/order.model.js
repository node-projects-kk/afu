const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const OrderSchema = new Schema({
	package_id : { type: Schema.Types.ObjectId,ref: 'Package' },
	mentor_id : { type: Schema.Types.ObjectId,ref: 'User'},
	student_id:{type: Schema.Types.ObjectId,ref: 'User'},
	package_name : { type: String, default: '' },
	package_price : { type:Schema.Types.Double, default:0.00 },
	stripe_charge_id : { type: String, default: '' },
	customer_id : { type: String, default: '' },
	order_date: { type: Date, default: Date.now() },
},{timestamps:true});

// For pagination
OrderSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Order', OrderSchema);