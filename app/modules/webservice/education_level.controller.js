const educationLevelRepo = require('education_level/repositories/education_level.repository');

/* @Method: getList
// @Description: Education Level list
*/
exports.getList = async req => {
    try {
        const educationLevelData = await educationLevelRepo.getAllEducationLevel({status: 'Active'});
        return { "status": 200,data: educationLevelData,message: 'Education levels fetched Successfully'};
    } catch (error) {
        return { "status": 500, data: [], "message": error.message };
    }
};


/* @Method: getAutocompleteList
// @Description: Education Autocomplete list API
*/
exports.getAutocompleteList = async req => {
	try {
		const searchTxt = req.query.keyword;
		const educationLevelData = await educationLevelRepo.getAllEducationLevel({status: 'Active',"level_name": {"$regex": searchTxt, "$options": "i"}});
		return { "status": 200,data: educationLevelData,message: 'Education levels fetched successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
    }
};