const wantToRepo = require('want_to/repositories/want_to.repository');

/* @Method: getList
// @Description: I Want to list
*/
exports.getList = async req => {
    try {
        const wantToData = await wantToRepo.getAllWantTo({status: 'Active'});
        return { "status": 200,data: wantToData,message: 'I want to fetched Successfully'};
    } catch (error) {
        return { "status": 500, data: [], "message": error.message };
    }
};