const contactusRepo = require('contactus/repositories/contactus.repository');
const settingsRepo = require('setting/repositories/setting.repository');
const mailer = require('../../helpers/mailer.js');

class contactusController {
  constructor() {}

	async contactusformSave(req,res){
		try {
			let save = await contactusRepo.contactUsFormSave(req.body);
			let locals = {
				name: save.name,
				phone: save.phone,
				email: save.email,
				message: save.message
			};

			const setting_data = await settingsRepo.getAllByField({"isDeleted":false});
			var settingObj = {};
			if (!_.isEmpty(setting_data)) {
				setting_data.forEach(function (element) {
					settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
				});
			}
		
			if(save){
				if (!_.isEmpty(save)) {
					let isMailSend = await mailer.sendMail(settingObj.Contact_Email, req.body.email, 'Contact Form Submitted', 'contact-us', locals);
					return { status: 200, data: save , message: `ContactUs Form submitted and Email Send successfully.`};
				}
			}
			else{
				return { status: 201, data: [], message: `There are some problem at this moment.` };
			}
		}
		catch (error) {
			return res.status(500).send({message: error.message});
		}
	}


}

module.exports = new contactusController();