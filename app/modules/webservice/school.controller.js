const schoolRepo = require('school/repositories/school.repository');
/* @Method: getList
// @Description: School list
*/
exports.getList = async req => {
	try {
		const schoolData = await schoolRepo.getAllSchool({status: 'Active'});
		return { "status": 200,data: schoolData,message: 'Schools fetched Successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
	}
};