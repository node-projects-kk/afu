const languageRepo = require('language/repositories/language.repository');

/* @Method: getList
// @Description: Language list
*/
exports.getList = async req => {
    try {
        const languageData = await languageRepo.getAllLanguage({status: 'Active'});
        return { "status": 200,data: languageData,message: 'Languages fetched Successfully'};
    } catch (error) {
        return { "status": 500, data: [], "message": error.message };
    }
};