const universityRepo = require('university/repositories/university.repository');
const universityModel = require('university/models/university.model.js');
const userRepo = require('user/repositories/user.repository');
const mentorRepo = require('user/repositories/mentor.repository');
const roleRepo = require('role/repositories/role.repository');
const settingRepo = require('setting/repositories/setting.repository');
const userModel = require('user/models/user.model.js');
const User = new userModel();
const async = require('async');
const mongoose = require('mongoose');
const mailer = require('../../helpers/mailer.js');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jsonwebtoken');
const University = new universityModel();

const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');

class universityController {
	constructor() {
		this.user = [];
	}

	async login(req, res) {
		try {
			let checkData = await userRepo.getUserByField({
				"email": req.body.email,
				"isDeleted": false
			});

			if (!_.isEmpty(checkData)) {

				if (checkData.isActive && checkData.isActive === true) {
					let isPasswordMatched = bcrypt.compareSync(req.body.password, checkData.password);
					if (!isPasswordMatched) {
						return {
							status: 201,
							data: [],
							isLoggedIn: false,
							"message": 'Password not matched.'
						};
					} else {
						const payload = {
							id: checkData._id
						};
						// console.log('payload', payload);
						const token = jwt.sign(payload, config.jwtSecret, {
							expiresIn: 86400
						});
						if (_.has(req.body, 'deviceType') && !_.isEmpty(req.body.deviceType)) {
							let userData = {
								'deviceType': req.body.deviceType,
								'deviceToken': req.body.deviceToken
							};
							const updatedUser = await userRepo.updateById(userData, checkData._id);
							checkData.deviceType = updatedUser.deviceType;
							checkData.deviceToken = updatedUser.deviceToken;
						}
						return {
							status: 200,
							data: checkData,
							isLoggedIn: true,
							token: token,
							"message": 'Login successful.'
						};
					}
				} else {
					return {
						status: 201,
						data: [],
						isLoggedIn: false,
						"message": 'University is inactive.'
					};
				}
			} else {
				return {
					status: 201,
					data: [],
					isLoggedIn: false,
					"message": 'University not found.'
				};
			}
		} catch (e) {
			return {
				"status": 500,
				data: [],
				"message": e.message
			};
		}
	}

	async logout(req, res) {
		try {
			var university_id = req.user._id;

			const result = await userRepo.getById(university_id);

			if (!_.isEmpty(result)) {
				var updateObj = {
					"deviceToken": "",
					"deviceType": ""
				};
				const updatedObj = await userRepo.updateById(updateObj, university_id);
				const payload = {
					id: university_id
				};
				const token = jwt.sign(payload, config.jwtSecret, {
					expiresIn: 0
				});
				return {
					status: 200,
					data: [],
					isLoggedIn: false,
					"message": 'Logout Successfully'
				};
			} else {
				return {
					status: 201,
					data: [],
					"message": 'No University Found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async getUniversityProfile(req) {
		try {

			const university = await userRepo.getByIdUniversity(req.user._id);

			let data = university;
			return {
				status: 200,
				data,
				message: 'Profile Info fetched Successfully'
			};
		} catch (e) {
			console.log(e.message);
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async updateProfile(req) {
		try {
			let userId = req.user._id;
			let user = await userRepo.getByField({
				'email': req.body.email,
				'isDeleted': false,
				'_id': {
					$ne: mongoose.Types.ObjectId(userId)
				}
			});
			if (_.isEmpty(user)) {
				const data = await userRepo.getById(req.user._id);
				if (_.has(req, 'files')) {
					if (req.files.length > 0) {
						let item = req.files[0];
						// async.forEach(req.files, function (item, cb) {
						if (item.fieldname == "profile_image") {
							if (!_.isEmpty(data)) {
								if (!_.isEmpty(data) && data.profile_image != '') {
									if (fs.existsSync('public/uploads/user/' + data.profile_image)) {
										const upl_resume = fs.unlinkSync('public/uploads/user/' + data.profile_image);
									}
									if (fs.existsSync('public/uploads/user/thumb/' + data.profile_image)) {
										const upl_thumb_resume = fs.unlinkSync('public/uploads/user/thumb/' + data.profile_image);
									}
								}
							} else {
								req.body.profile_image = data.profile_image;
							}
							gm('public/uploads/user/' + item.filename).resize(100).write('public/uploads/user/thumb/' + item.filename, function (err) {
								if (err) throw new Error(err.message);
							});
							req.body.profile_image = item.filename;
							// cb();
						}
						if (item.fieldname == "logo") {
							if (!_.isEmpty(data)) {
								if (!_.isEmpty(data) && data.logo != '') {
									if (fs.existsSync('public/uploads/logo/' + data.logo)) {
										const upl_resume = fs.unlinkSync('public/uploads/logo/' + data.logo);
									}
									if (fs.existsSync('public/uploads/logo/thumb/' + data.logo)) {
										const upl_thumb_resume = fs.unlinkSync('public/uploads/logo/thumb/' + data.logo);
									}
								}
							} else {
								req.body.logo = data.logo;
							}
							gm('public/uploads/logo/' + item.filename).resize(100).write('public/uploads/logo/thumb/' + item.filename, function (err) {
								if (err) throw new Error(err.message);
							});
							req.body.logo = item.filename;
							cb();
						}
						// }, function (err) {

						// });
					}
				}
				const result2 = await userRepo.updateById(req.body, req.user._id);
				if (!_.isEmpty(result2)) {
					const university = await universityRepo.getByField({
						'user_id': mongoose.Types.ObjectId(req.user._id)
					});
					const unvRes = await universityRepo.updateById(req.body, university._id);
					return {
						status: 200,
						data: unvRes,
						"message": "Profile updated successfully"
					};
				} else {
					return {
						status: 201,
						data: [],
						"message": "Something went wrong."
					};
				}
			} else {
				return {
					status: 201,
					data: {},
					"message": "This email address is already exist!"
				};
			}

		} catch (e) {
			console.log(e);
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async getDashboard(req, res) {
		try {
			const user = await userRepo.getById(req.user._id);

			var count_data = {};

			var top_users = await userRepo.getTopMentors(5);
			const mentorCount = await userRepo.getMentorCount();

			count_data.mentor = mentorCount[0].count;
			count_data.referral = 0;
			count_data.ambassador = 0;

			var result = {
				'count': count_data,
				'top_users': top_users
			}

			return {
				status: 200,
				data: result,
				message: 'Dashboard Info fetched Successfully'
			};
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	/* @Method: getList
	// @Description: University list API
	*/
	async getList(req) {
		try {
			const universityData = await universityRepo.getAllByField({
				status: 'Active'
			});
			return {
				"status": 200,
				data: universityData,
				message: 'Universities fetched successfully'
			};
		} catch (error) {
			return {
				"status": 500,
				data: [],
				"message": error.message
			};
		}
	}

	/* @Method: getAutocompleteList
	// @Description: University Autocomplete list API
	*/
	async getAutocompleteList(req) {
		try {
			const searchTxt = req.query.keyword;
			const universityData = await universityRepo.getAllByField({
				status: 'Active',
				"university_name": {
					"$regex": searchTxt,
					"$options": "i"
				}
			});
			return {
				"status": 200,
				data: universityData,
				message: 'Universities fetched successfully'
			};
		} catch (error) {
			return {
				"status": 500,
				data: [],
				"message": error.message
			};
		}
	}

	async mentorList(req, res) {
		try {
			let user_id = req.user._id;

			let mlist = await userRepo.getMentorsListWithSearch(req.body);

			if (!_.isEmpty(mlist)) {
				return {
					status: 200,
					data: mlist,
					"message": 'Mentor list fetched successfully'
				};
			} else {
				return {
					status: 200,
					data: [],
					message: 'No matching mentor found'
				};
			}
		} catch (e) {
			console.log("272>>", error.message);
			return res.status(500).send({
				"message": e.message
			});
		}
	}
	async studentList(req, res) {
		try {
			let user_id = req.user._id;

			let mlist = await userRepo.getStudentsListWithSearch(req.body);

			if (!_.isEmpty(mlist)) {
				return {
					status: 200,
					data: mlist,
					"message": 'Student list fetched successfully'
				};
			} else {
				return {
					status: 200,
					data: [],
					message: 'No matching mentor found'
				};
			}
		} catch (e) {
			console.log("272>>", error.message);
			return res.status(500).send({
				"message": e.message
			});
		}
	}
}


module.exports = new universityController();