const faqRepo = require('faq/repositories/faq.repository');
/* @Method: getList
// @Description: Faq List
*/
exports.getList = async req => {
	try {
		const faqData = await faqRepo.getAllFaq({status: 'Active'});
		return { "status": 200,data: faqData,message: 'Faqs fetched Successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
	}
};