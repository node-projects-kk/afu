const mentorRepo = require('user/repositories/mentor.repository');
const userRepo = require('user/repositories/user.repository');
const referralRepo = require('referral/repositories/referral.repository');
const roleRepo = require('role/repositories/role.repository');
const settingRepo = require('setting/repositories/setting.repository');
const async = require('async');
const mongoose = require('mongoose');
const mailer = require('../../helpers/mailer.js');

class mentorController {
  constructor() {}

  async getDashboardForMentor(req, res) {
    try {
      let menteeCount = await mentorRepo.getMenteeCount(req.user._id);
      let referralCount = await referralRepo.getMentorReferralCount(req.user._id);
      let result = {
        'mentee_count': menteeCount,
        'referral_count': referralCount,
      };
      return {
        status: 200,
        data: result,
        message: 'Data fetched Successfully.'
      }
    } catch (e) {
      return res.status(500).send({
        "message": e.message
      });
    }
  }

  async getMentorReferralList(req, res) {
    try {
      let getMentorReferralInfo = await referralRepo.getMentorWiseReferralList(req.user._id);
      if (!_.isEmpty(getMentorReferralInfo)) {
        return {
          status: 200,
          data: getMentorReferralInfo,
          message: 'Referral information fetched Successfully.'
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'There are no data at this moment.'
        }
      }
    } catch (error) {
      return res.status(500).send({
        "message": error.message
      });
    }
  }

  async getStudentListWithSearch(req, res) {
    try {
      let getStudentInfo = await mentorRepo.getMenteeListWithSearch(req.body, req.user._id);
      if (!_.isEmpty(getStudentInfo)) {
        return {
          status: 200,
          data: getStudentInfo,
          message: 'Students fetched successfully.'
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'There are no matched Students at this moment.'
        }
      }
    } catch (error) {
      console.log('erererere', error);
      return res.status(500).send({
        "message": error.message
      });
    }
  }

  async getRoleInfoBasedOnId(req, res) {
    try {
      let getUserInfo = await userRepo.getById(req.body.user_id);
      if (!_.isEmpty(getUserInfo)) {
        return {
          status: 200,
          data: getUserInfo.role,
          message: 'Role fetched Successfully.'
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'Please check the User Id that you have provided.'
        }
      }
    } catch (error) {
      return res.status(500).send({
        "message": error.message
      });
    }
  }

  async saveStudentCVInfo(req, res) {
    try {
      let setUpAnObject = {
        'file_link': req.body.file_link,
        'student_id': req.body.student_id,
        'date': req.body.date,
      };
      let updateMentorCVInfo = await userRepo.updateById({
        $push: {
          "cv_list": setUpAnObject
        }
      }, req.user._id);
      if (updateMentorCVInfo) {
        return {
          status: 200,
          data: updateMentorCVInfo,
          message: 'CV saved successfully.'
        }
      } else {
        return {
          status: 201,
          data: [],
          message: 'We are unable to save the CV, Please try again.'
        }
      }
    } catch (error) {
      return res.status(500).send({
        status: 500,
        message: error.message
      });
    }
  }


}


module.exports = new mentorController();