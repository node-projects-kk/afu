var Q = require('q');
const chatRepo = require('chat/repositories/chat.repository');
const userRepo = require('user/repositories/user.repository');
const TokenGenerator = require('uuid-token-generator');
const userModel = require('user/models/user.model');
const chatHistoryRepo = require('chathistory/repositories/chathistory.repository');
const chatStatusRepo = require('chatstatus/repositories/chatstatus.repository');
const mongoose = require('mongoose');
var tokBoxAPIKey = config.TOK_BOX_API_KEY;
var tokBoxAPISecret = config.TOK_BOX_API_SECRET;
var push = require('../push');
const async = require('async');
const pushHelper = require('../../helpers/native_push');

// const firebase = require('firebase');
// require('firebase/auth');
// require('firebase/database');
// const app = firebase.initializeApp({
// 	apiKey: config.FIREBASE_APIKEY,
// 	authDomain: config.FIREBASE_AUTHDOMAIN,
// 	databaseURL: config.FIREBASE_DATABASE_URL,
// 	projectId: config.FIREBASE_PROJECTID,
// 	storageBucket: config.FIREBASE_STORAGE_BUCKET,
// 	messagingSenderId: config.FIREBASE_MESSEGING_SENDERID
// });

var OpenTok = require('opentok'),
	opentok = new OpenTok(tokBoxAPIKey, tokBoxAPISecret);

/* 
// @Method: createChatToken
// @Description: Create chat token
*/
exports.createChatToken = async req => {
	try {
		let mentor_id = req.body.mentor_id;
		let student_id = req.body.student_id;
		const checkExist = await chatRepo.getByField({
			"mentor_id": mentor_id,
			"student_id": student_id
		});

		if (_.isEmpty(checkExist)) {
			const tokgen = new TokenGenerator(256, TokenGenerator.BASE62);
			req.body.chat_token = tokgen.generate();
			const result = await chatRepo.save(req.body);
			if (result) {
				return {
					status: 200,
					data: result,
					message: 'Chat token created successfully'
				};
			}
		} else {
			return {
				status: 200,
				data: checkExist,
				message: 'Chat token fetched successfully'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: {},
			"message": error.message
		}
	}
};

exports.sendPushForChat = function (req, res) {
	var deferred = Q.defer();
	//try{
	//	console.log("57>>",req.body);
	//	let loggedInUserId = req.user._id;
	//	let mentor_id = req.body.mentor_id;
	//	let student_id = req.body.student_id;
	//	let call_to = req.body.call_to;
	//	let result = await chatStatusRepo.getByField({"mentor_id":mentor_id,"student_id":student_id,"call_status":"free"});
	//	console.log("acasc>>", result);
	//
	//}
	//catch(error){
	//	console.log(">>", error);
	//	throw error;
	//}
	//process.exit();
	//console.log("57>>",req.body);
	let loggedInUserId = req.user._id;
	let mentor_id = req.body.mentor_id;
	let student_id = req.body.student_id;
	let call_to = req.body.call_to;
	//
	//let result = await chatStatusRepo.getByField({"mentor_id":mentor_id,"student_id":student_id,"call_status":"free"});
	//console.log("acasc>>", result);


	//chatStatusRepo.getByField({"mentor_id":mentor_id,"student_id":student_id,"call_status":"free"},function(errstat,resultstat){
	//	console.log("64>>",errstat);
	//	if (errstat) {
	//		deferred.reject({"status": 500, data: [], message: errstat});
	//	}
	//	else{
	//		if (!_.isEmpty(resultstat)) { console.log("if");
	if (call_to == "mentor") {
		// console.log("1");
		userModel.findById({
			"_id": mentor_id
		}, function (errmentor, resultmentor) {
			if (errmentor) {
				deferred.reject({
					"status": 500,
					data: [],
					message: errmentor
				});
			} else {
				if (!_.isEmpty(resultmentor)) {
					var mentorName = resultmentor.full_name;
					let deviceTypeMentor = resultmentor.deviceType;
					let deviceTokenMentor = resultmentor.deviceToken;
					if (!_.isEmpty(deviceTokenMentor) && !_.isEmpty(deviceTypeMentor)) {
						var sessionOptions = {
							mediaMode: 'routed',
							archiveMode: 'always'
						};
						opentok.createSession(sessionOptions, function (err, session) {
							if (err) {
								deferred.reject({
									status: 500,
									data: {},
									message: err.message
								});
							} else {
								var sessionValue = session.sessionId;
								var token = opentok.generateToken(sessionValue);
								if (deviceTypeMentor == "ios") {
									var paramsM = {
										"type": "push_send",
										"sessionvalue": sessionValue,
										"token": token,
										"apiKey": tokBoxAPIKey,
										"mentor_id": mentor_id,
										"student_id": student_id
									};
								} else {
									var paramsM = {
										"type": "push_send",
										"clickaction": "StartCallActivity",
										"sessionvalue": sessionValue,
										"token": token,
										"apiKey": tokBoxAPIKey,
										"mentor_id": mentor_id,
										"student_id": student_id
									};
								}
								let server = push.sendPush(deviceTypeMentor, deviceTokenMentor, title = '', from = '', to = '', paramsM);
								req.body.mentor_id = mentor_id;
								req.body.student_id = student_id;
								req.body.call_status = "busy";

								//chatStatusRepo.save(req.body,function(errsave,resultsave){
								//	if (errsave) {
								//		deferred.reject({ status: 500, data: {}, message: errsave.message });
								//	}
								//	else{
								userModel.findById({
									"_id": student_id
								}, function (errstudent, resultstudent) {
									if (errstudent) {
										deferred.reject({
											"status": 500,
											data: [],
											message: errstudent
										});
									} else {
										var studentName = resultstudent.full_name;
										deferred.resolve({
											"status": 200,
											data: [],
											"sessionvalue": sessionValue,
											"token": token,
											"apiKey": tokBoxAPIKey,
											"student_name": studentName,
											"mentor_name": mentorName,
											message: 'Push send successfully to mentor'
										});
									}
								});
								//	}
								//});
							}
						});
					} else {
						deferred.reject({
							"status": 201,
							data: [],
							message: "Device type or token not found for mentor"
						});
					}
				} else {
					deferred.reject({
						"status": 201,
						data: [],
						message: "Mentor not found"
					});
				}
			}
		});
	} else if (call_to == "student") {
		// console.log("2");
		userModel.findById({
			"_id": student_id
		}, function (errstudent, resultstudent) {
			if (errstudent) {
				deferred.reject({
					"status": 500,
					data: [],
					message: errstudent
				});
			} else {
				if (!_.isEmpty(resultstudent)) {
					var studentName = resultstudent.full_name;
					let deviceTypeStudent = resultstudent.deviceType;
					let deviceTokenStudent = resultstudent.deviceToken;
					if (!_.isEmpty(deviceTokenStudent) && !_.isEmpty(deviceTypeStudent)) {
						var sessionOptions = {
							mediaMode: 'routed',
							archiveMode: 'always'
						};
						opentok.createSession(sessionOptions, function (err, session) {
							if (err) {
								deferred.reject({
									status: 500,
									data: {},
									message: err.message
								});
							} else {
								var sessionValue = session.sessionId;
								var token = opentok.generateToken(sessionValue);
								if (deviceTypeStudent == "ios") {
									var paramsS = {
										"type": "push_send",
										"sessionvalue": sessionValue,
										"token": token,
										"apiKey": tokBoxAPIKey,
										"mentor_id": mentor_id,
										"student_id": student_id
									};
								} else {
									var paramsS = {
										"type": "push_send",
										"clickaction": "StartCallActivity",
										"sessionvalue": sessionValue,
										"token": token,
										"apiKey": tokBoxAPIKey,
										"mentor_id": mentor_id,
										"student_id": student_id
									};
								}
								let server = push.sendPush(deviceTypeStudent, deviceTokenStudent, title = '', from = '', to = '', paramsS);
								req.body.mentor_id = mentor_id;
								req.body.student_id = student_id;
								req.body.call_status = "busy";

								//chatStatusRepo.save(req.body,function(errsave,resultsave){
								//	if (errsave) {
								//		deferred.reject({ status: 500, data: {}, message: errsave.message });
								//	}
								//	else{
								userModel.findById({
									"_id": mentor_id
								}, function (errmentor, resultmentor) {
									if (errmentor) {
										deferred.reject({
											"status": 500,
											data: [],
											message: errmentor
										});
									} else {
										var mentorName = resultmentor.full_name;
										deferred.resolve({
											"status": 200,
											data: [],
											"sessionvalue": sessionValue,
											"token": token,
											"apiKey": tokBoxAPIKey,
											"mentor_name": mentorName,
											"student_name": studentName,
											message: 'Push send successfully to student'
										});
									}
								});
								//	}
								//});
							}
						});
					} else {
						deferred.reject({
							"status": 201,
							data: [],
							message: "Device type or token not found for mentor"
						});
					}
				} else {
					deferred.reject({
						"status": 201,
						data: [],
						message: "Student not found"
					});
				}
			}
		});
	}
	//		}
	//		else{
	//			deferred.reject({"status": 201, data: [], message: "Mentor or student are busy in another call"});
	//		}
	//	}
	//});
	// console.log("xascac");
	return deferred.promise;
};

exports.rejectPushForChat = async req => {
	try {
		let loggedInUserId = req.user._id;
		let mentor_id = req.body.mentor_id;
		let student_id = req.body.student_id;
		let call_reject_by = req.body.call_reject_by;
		if (call_reject_by == "mentor") {
			let studentResult = await userRepo.getById({
				"_id": student_id
			});
			if (!_.isEmpty(studentResult)) {
				let deviceTypeStudent = studentResult.deviceType;
				let deviceTokenStudent = studentResult.deviceToken;
				if (!_.isEmpty(deviceTypeStudent) && !_.isEmpty(deviceTokenStudent)) {
					if (deviceTypeStudent == "ios") {
						var paramsS = {
							"type": "reject_push",
							"mentor_id": mentor_id,
							"student_id": student_id
						};
					} else {
						var paramsS = {
							"type": "reject_push",
							"mentor_id": mentor_id,
							"student_id": student_id,
							"clickaction": "RejectCallActivity"
						};
					}
					let server = await push.sendPush(deviceTypeStudent, deviceTokenStudent, title = '', from = '', to = '', paramsS);
					// console.log(">>", server);
					return {
						status: 200,
						data: [],
						message: "Mentor is busy for the time being"
					}
				} else {
					return {
						status: 201,
						data: [],
						message: "Student device token or device type not found"
					}
				}
			} else {
				return {
					status: 201,
					data: [],
					message: "Student not found"
				}
			}
		} else if (call_reject_by == "student") {
			let mentorResult = await userRepo.getById({
				"_id": mentor_id
			});
			if (!_.isEmpty(mentorResult)) {
				let deviceTypeMentor = mentorResult.deviceType;
				let deviceTokenMentor = mentorResult.deviceToken;
				if (!_.isEmpty(deviceTypeMentor) && !_.isEmpty(deviceTokenMentor)) {
					if (deviceTypeMentor == "ios") {
						var paramsM = {
							"type": "reject_push",
							"mentor_id": mentor_id,
							"student_id": student_id
						};
					} else {
						var paramsM = {
							"type": "reject_push",
							"mentor_id": mentor_id,
							"student_id": student_id,
							"clickaction": "RejectCallActivity"
						};
					}
					let server = await push.sendPush(deviceTypeMentor, deviceTokenMentor, title = '', from = '', to = '', paramsM);
					// console.log(">>", server);
					return {
						status: 200,
						data: [],
						message: "Student is busy for the time being"
					}
				} else {
					return {
						status: 201,
						data: [],
						message: "Mentor device token or device type not found"
					}
				}
			} else {
				return {
					status: 201,
					data: [],
					message: "Mentor not found"
				}
			}
		}
	} catch (e) {
		console.log("250>>", e.message);
		return res.status(500).send({
			"message": e.message
		});
	}
};

exports.createChatSession = function (req, res) {
	var deferred = Q.defer();
	opentok.createSession(function (err, session) {
		if (err) {
			deferred.reject({
				status: 500,
				data: {},
				message: err.message
			});
		} else {
			var archiveOptions = {
				name: 'Important Presentation',
				hasVideo: true, // Record audio only
				outputMode: 'individual'
			};
			var sessionValue = session.sessionId;
			opentok.startArchive(session.sessionId, archiveOptions, function (errarchive, archive) {
				if (errarchive) {
					deferred.reject({
						status: 500,
						data: {},
						message: errarchive.message
					});
				} else {
					// The id property is useful to save to a database
					// console.log("new archive:" + archive.id);
					deferred.resolve({
						status: 200,
						data: sessionValue,
						archive: archive,
						"message": "Session ID created successfully."
					});
				}
			});
		}
	});
	return deferred.promise;
}

exports.createToken = function (req, res) {
	var deferred = Q.defer();
	var sessionValue = req.params.sessionId;
	var token = opentok.generateToken(sessionValue);
	deferred.resolve({
		"status": 200,
		data: token,
		"message": "Token created successfully."
	});
	return deferred.promise;
}

exports.createSessionToken = function (req, res) {
	var deferred = Q.defer();
	var sessionOptions = {
		mediaMode: 'routed',
		archiveMode: 'always'
	};
	opentok.createSession(sessionOptions, function (err, session) {
		if (err) {
			deferred.reject({
				status: 500,
				data: {},
				message: err.message
			});
		} else {
			var sessionValue = session.sessionId;
			var token = opentok.generateToken(sessionValue);
			deferred.resolve({
				status: 200,
				apiKey: tokBoxAPIKey,
				sessionId: sessionValue,
				token: token
			});
		}
	});
	return deferred.promise;
}

/* 
// @Method: createChatToken
// @Description: Create chat token
*/
exports.fetchChat = async req => {
	var deferred = Q.defer();
	var chatReference = firebase_app.database().ref("chats");
	let chat_id = req.body.chat_id;
	const listener = chatReference.child('chatToken1').on('value', dataSnapshot => {
		var items = [];
		dataSnapshot.forEach(child => {
			items.push({
				chatId: child.key,
				message: child.val().content,
				senderId: child.val().senderId,
				imageUrl: child.val().imageUrl,
				videoUrl: child.val().videoUrl,
				time: child.val().time
			})
		});

		var chatHistory = items.reverse();
		// console.log("164>>", chatHistory);

		if (!_.isEmpty(chatHistory)) {
			var lastChat = chatHistory[0];
			// console.log("<><><><>", lastChat);
			// console.log(">>>>>", {
			// 	data: items.reverse()
			// }); //process.exit();
			req.body.user_id = lastChat.senderId;
			req.body.chat_id = chat_id;
			req.body.last_message = lastChat.content;

			chatHistoryRepo.save(req.body, function (err, savedChatHistory) {
				if (err) {
					deferred.reject({
						"status": 500,
						data: [],
						"message": err.message
					});
				} else {
					deferred.resolve({
						status: 200,
						data: savedChatHistory,
						message: "Chat saved successfully"
					});
				}
			});
		} else {
			deferred.reject({
				"status": 201,
				data: [],
				"message": "No chat found"
			});
		}
	});
	return deferred.promise;
};

exports.mentorListChat = async req => {
	try {
		let newArr = [];
		let mlist, userRole, isChat;
		let user_id = req.user._id;
		let user_details = await userRepo.getById({
			"_id": user_id
		});
		if (!_.isEmpty(user_details)) {
			userRole = user_details.role.role;
		}

		if (userRole == "mentor") {
			isChat = await chatRepo.getAllByField({
				"mentor_id": user_id
			});
			if (!_.isEmpty(isChat) && !_.isNull(isChat) && !_.isUndefined(isChat)) {
				for (var j = 0; j < isChat.length; j++) {
					userList = await userRepo.getUserDetails(isChat[j].student_id, "student", user_id);
					// console.log("381>>", userList)
					newArr.push(userList);
				}
			} else {
				return {
					status: 201,
					data: [],
					"hasChat": false,
					"message": 'No chat found'
				};
			}
		} else {
			isChat = await chatRepo.getAllByField({
				"student_id": user_id
			});
			// console.log("391>>", isChat);
			if (!_.isEmpty(isChat) && !_.isNull(isChat) && !_.isUndefined(isChat)) {
				for (var k = 0; k < isChat.length; k++) {
					userList = await userRepo.getUserDetails(isChat[k].mentor_id, "mentor", user_id);
					// console.log("381>>", userList)
					newArr.push(userList);
				}
			} else {
				return {
					status: 201,
					data: [],
					"hasChat": false,
					"message": 'No chat found'
				};
			}
		}

		// console.log("402>>>>>", newArr); //process.exit();

		if (!_.isEmpty(newArr) && !_.isNull(newArr) && !_.isUndefined(newArr)) {
			for (var i = 0; i < newArr.length; i++) {
				if (!_.isEmpty(newArr[i].chat_details) && !_.isNull(newArr[i].chat_details) && !_.isUndefined(newArr[i].chat_details)) {
					let chat_token = newArr[i].chat_details.chat_token;
					// console.log("407>>", chat_token);
					var chatReference = firebase_app.database().ref("chats");
					const snapshot = await chatReference.child(chat_token).orderByChild('timestamp').limitToLast(1).once('value');
					const value = snapshot.val();
					if (!_.isNull(value)) {
						var key = Object.keys(value)[0];
						newArr[i].last_message = value[key];
					} else {
						newArr[i].last_message = {};
					}
				}
			}
			// console.log("420>>", newArr);
			return {
				status: 200,
				data: newArr,
				"hasChat": true,
				"message": 'Chat list fetched successfully'
			};
		}
	} catch (e) {
		// console.log("250>>", e);
		return res.status(500).send({
			"message": e.message
		});
	}
};

exports.updateChat = async req => {
	try {
		let user_id = req.user._id;
		let chat_id = req.body.chat_id;
		let last_message = req.body.last_message;
		let checkExistChat = await chatHistoryRepo.getByField({
			"chat_id": chat_id,
			"user_id": user_id
		});
		if (!_.isEmpty(checkExistChat) && !_.isNull(checkExistChat) && !_.isUndefined(checkExistChat)) {
			let id = checkExistChat._id;
			let updateval = {
				"last_message": last_message
			};
			let updateresult = await chatHistoryRepo.updateById(updateval, {
				"_id": id
			});
			if (!_.isEmpty(updateresult)) {
				return {
					status: 200,
					data: updateresult,
					message: "Chat save successfully"
				}
			}
		} else {
			req.body.user_id = user_id;
			req.body.chat_id = chat_id;
			req.body.last_message = last_message;
			let saveresult = await chatHistoryRepo.save(req.body);
			if (!_.isEmpty(saveresult)) {
				return {
					status: 200,
					data: saveresult,
					message: "Chat save successfully"
				}
			}
		}
	} catch (e) {
		console.log("508>>", e.message);
		return res.status(500).send({
			"message": e.message
		});
	}
};

exports.removeChat = async req => {
	try {
		let user_id = req.user._id;
		console.log("564>>", user_id); //process.exit();
		let chat_id = req.body.chat_id;
		let checkExistChat = await chatRepo.getByField({
			"_id": chat_id
		});
		if (!_.isEmpty(checkExistChat) && !_.isNull(checkExistChat) && !_.isUndefined(checkExistChat)) {
			let updateval = {
				"isDeleted": true
			};
			let updateresult = await chatRepo.updateById(updateval, {
				"_id": chat_id
			});
			if (!_.isEmpty(updateresult)) {
				return {
					status: 200,
					data: updateresult,
					message: "Chat deleted successfully"
				}
			}
		} else {
			return {
				status: 201,
				data: [],
				message: "No Chat found"
			}
		}
	} catch (e) {
		return res.status(500).send({
			"message": e.message
		});
	}
};

/* 
// @Method: createChatTokenWeb
// @Description: Create chat token for web
*/
exports.createChatTokenWeb = async req => {
	try {
		const checkExist = await chatRepo.getByField({
			"mentor_id": req.body.mentor_id,
			"university_id": req.body.university_id
		});

		if (_.isEmpty(checkExist)) {
			const token = new TokenGenerator(256, TokenGenerator.BASE62);
			req.body.chat_token = token.generate();
			const result = await chatRepo.save(req.body);
			if (result) {
				return {
					status: 200,
					data: result,
					message: 'Chat token created successfully.'
				};
			}
		} else {
			return {
				status: 200,
				data: checkExist,
				message: 'Existing Chat token fetched successfully.'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: {},
			"message": error.message
		}
	}
};

/* 
// @Method: mentorListChatForWebsite
// @Description:Chat list for web
*/
exports.mentorListChatForWebsite = async req => {
	try {
		let newArr = [];
		let mlist, userRole, isChat;
		let user_id = req.user._id;
		let user_details = await userRepo.getById({
			"_id": user_id
		});

		if (!_.isEmpty(user_details)) {
			userRole = user_details.role.role;
		}

		if (userRole == "mentor") {
			isChat = await chatRepo.getAllByField({
				"mentor_id": user_id,
				"university_id": {
					"$ne": null
				}
			});
			if (!_.isEmpty(isChat) && !_.isNull(isChat) && !_.isUndefined(isChat)) {
				for (var j = 0; j < isChat.length; j++) {
					userList = await userRepo.getUserDetailsForWebsite(isChat[j].university_id, "university", user_id);
					newArr.push(userList);
				}
			} else {
				return {
					status: 201,
					data: [],
					"hasChat": false,
					"message": 'No chat found'
				};
			}
		} else {
			isChat = await chatRepo.getAllByField({
				"university_id": user_id
			});

			if (!_.isEmpty(isChat) && !_.isNull(isChat) && !_.isUndefined(isChat)) {
				for (var k = 0; k < isChat.length; k++) {
					userList = await userRepo.getUserDetailsForWebsite(isChat[k].mentor_id, "mentor", user_id);
					// console.log("381>>", userList)
					newArr.push(userList);
				}
			} else {
				return {
					status: 201,
					data: [],
					"hasChat": false,
					"message": 'No chat found'
				};
			}
		}

		// console.log("402>>>>>", newArr);

		if (!_.isEmpty(newArr) && !_.isNull(newArr) && !_.isUndefined(newArr)) {
			for (var i = 0; i < newArr.length; i++) {
				if (!_.isEmpty(newArr[i].chat_details) && !_.isNull(newArr[i].chat_details) && !_.isUndefined(newArr[i].chat_details)) {
					let chat_token = newArr[i].chat_details.chat_token;
					var chatReference = firebase_app.database().ref("chats");
					const snapshot = await chatReference.child(chat_token).orderByChild('timestamp').limitToLast(1).once('value');
					const value = snapshot.val();
					if (!_.isNull(value)) {
						var key = Object.keys(value)[0];
						// newArr[i].last_message = value[key];
						newArr[i].last_message = value[key];
						newArr[i].last_message.id = key;
						newArr[i].hasChat = true;
					} else {
						newArr[i].last_message = {};
						newArr[i].hasChat = false;
					}
				}
			}
			// console.log("420>>", newArr);
			return {
				status: 200,
				data: newArr,
				// "hasChat": true,
				"message": 'Chat list fetched successfully'
			};
		}
	} catch (e) {
		console.log("250>>", e);
		return res.status(500).send({
			"message": e.message
		});
	}
};

let messagesRef = firebase_app.database().ref('chats');
messagesRef.orderByChild('createdAt').on("child_changed", async function (snapshot) {
	var changedPost = snapshot.val();
	var keys1 = Object.keys(snapshot.val() || {});
	var lastIdInSnapshot1 = keys1[keys1.length - 1];
	if (snapshot.val()[lastIdInSnapshot1] && !_.isEmpty(snapshot.val()[lastIdInSnapshot1])) {
		let document = snapshot.val()[lastIdInSnapshot1];
		let chatRecord = await chatRepo.getByField({
			$or: [{
				mentor_id: document.receiverId,
				student_id: document.senderId
			}, {
				mentor_id: document.senderId,
				student_id: document.receiverId
			}]
		});

		let chatToken = '';
		if (chatRecord) {
			chatToken = chatRecord.chat_token;
		}

		let {
			deviceType,
			deviceToken
		} = await userRepo.getById(document.receiverId);

		let {
			full_name,
			profile_image,
			phone
		} = await userRepo.getById(document.senderId);

		let deviceDetails = {
			deviceType,
			deviceToken
		};

		let sendPushNew = await pushHelper.sendPushForChat(deviceDetails, {
			title: `New Message from ${full_name}`,
			body: `${document.content}`,
			clickAction: 'userclickchat'
		}, {
			sender_id: document.senderId,
			full_name,
			chatToken,
			type: 'message'
		});
	}

});