const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const settingRepo = require('setting/repositories/setting.repository');
const languageRepo = require('language/repositories/language.repository');
const projectRepo = require('project/repositories/project.repository');
const orderRepo = require('order/repositories/order.repository');
const menteeRepo = require('user/repositories/mentee.repository');
const userModel = require('user/models/user.model.js');
const mailer = require('../../helpers/mailer.js');
const gm = require('gm').subClass({
    imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const async = require('async');
const perPage = config.PAGINATION_PERPAGE;
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const {
    join
} = require('path');
const {
    promisify
} = require('util');
const {
    readFile
} = require('fs');
const User = new userModel();

class staticController {
    constructor() {
        this.user = [];
    }

    async splash(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Splash Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async loadingpage(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Loading Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async expertise(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Expertise Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async yourwork(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Yourwork Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async extracurry(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Extracurriculam Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async youreducation(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Your educcation Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async aboutme(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Aboutme Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async logo(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "logo": "logo_1565791258336_colin.jpg"
                },
                message: "Logo fetch successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async title(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "title": "Apply For University"
                },
                message: "Title fetch successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async welcomepage(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Welcome Page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async welcometext(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "text": "Welcome to AFU"
                },
                message: "Welcome Page text fetch successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async usertypeselection(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "User Type Selection Page fetch successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async usertype(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "type": "Mentor,Student"
                },
                message: "Usertype fetch successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async signuppage(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Sign up page fetch successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentsigninpage(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Student Sign In Page fetch successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorsigninpage(req, res) {
        try {
            return {
                status: 201,
                data: {},
                message: "Mentor Sign In Page fetch successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentsignin(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Subhra",
                    "email": "subhra11@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "school_name": "",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                "isLoggedIn": true,
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0",
                message: "Login successfull Student."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorsignin(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "kalyan",
                    "email": "kalyan@gmail.com",
                    "password": "$2a$08$gnoPVZI9yuN1hqDuLHj2ZOYEgrMjQqe70.exh7.3wZr2CdDcMwjAG",
                    "profile_image": "",
                    "school_name": "",
                    "education_level": "Masters degree ",
                    "want_to": "",
                    "address": "London",
                    "university_name": "RKMV",
                    "gender": "Male",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d3188206d84b8436af1ce37",
                    "country": "5d306dc6822fa522c79acd21",
                    "language": "5d306346822fa522c79a2dec",
                    "specialization": [{
                            "title": "project one",
                            "_id": "5d3188206d84b8436af1ce39"
                        },
                        {
                            "title": "http://www.projectone.com",
                            "_id": "5d3188206d84b8436af1ce38"
                        }
                    ],
                    "role": "5d30518c822fa522c7991272",
                    "project": [],
                    "activities": [],
                    "createdAt": "2019-07-19T09:06:40.023Z",
                    "updatedAt": "2019-07-19T09:06:40.023Z",
                    "__v": 0
                },
                "isLoggedIn": true,
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0",
                message: "Login successfull Mentor."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorstep1(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Subhra",
                    "email": "subhra11@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "school_name": "",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration step one successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorstep2(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Kalyan",
                    "email": "kalyan@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "country": "5d306dc6822fa522c79acd21",
                    "language": "5d306346822fa522c79a2dec",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration step one successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorstep3(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Kalyan",
                    "email": "kalyan@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "country": "5d306dc6822fa522c79acd21",
                    "language": "5d306346822fa522c79a2dec",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration step three successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorstep4(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Kalyan",
                    "email": "kalyan@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "profile_image": "profile_image_1563802441970_img2.jpg",
                    "school_name": "",
                    "logo": "logo_1563802441971_gc.jpg",
                    "short_description": "test description",
                    "address": "London",
                    "university_name": "RKMV",
                    "gender": "Male",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "country": "5d306dc6822fa522c79acd21",
                    "language": "5d306346822fa522c79a2dec",
                    "education_level": "5d31c1a0822fa522c7abefca",
                    "specialization": [{
                            "title": "project one",
                            "_id": "5d35bb4a38104765642d2b23"
                        },
                        {
                            "title": "http://www.projectone.com",
                            "_id": "5d35bb4a38104765642d2b22"
                        }
                    ],
                    "service": [{
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d35bb4a38104765642d2b25"
                        },
                        {
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d35bb4a38104765642d2b24"
                        }
                    ],
                    "role": "5d30518c822fa522c7991272",
                    "project": [],
                    "activities": [],
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentstep1(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Subhra",
                    "email": "subhra11@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "school_name": "",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration  step one successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentstep2(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Subhra",
                    "email": "subhra11@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "country": "5d306dc6822fa522c79acd21",
                    "language": "5d306346822fa522c79a2dec",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration  step2 successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentstep3(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Subhra",
                    "email": "subhra11@gmail.com",
                    "password": "$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye",
                    "dob": "",
                    "social_id": "",
                    "register_type": "normal",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d35bb4a38104765642d2b21",
                    "country": "5d306dc6822fa522c79acd21",
                    "language": "5d306346822fa522c79a2dec",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-22T13:34:02.101Z",
                    "updatedAt": "2019-07-22T13:34:02.101Z",
                    "__v": 0
                },
                message: "Registration  step3 successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentstep4(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Subhra",
                    "email": "subhra11@gmail.com",
                    "password": "$2a$08$D/GDWDaQm1M5ykmC.pvXKeQzQBAXn8KWl33sx0Vyo47VeystRkqcu",
                    "profile_image": "",
                    "university_name": "",
                    "year_of_study": "",
                    "short_description": "",
                    "social_id": "",
                    "register_type": "normal",
                    "school_name": "Nava Nalanda",
                    "logo": "",
                    "address": "",
                    "gender": "Male",
                    "phone": "",
                    "dob": "",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "display_mobile_no": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d668a1d37aa6464abeeb44a",
                    "language": "5d371e651ae2de8011c4acb5",
                    "country": "5d306dc6822fa522c79acd1f",
                    "education_level": "5d385f14d3486b7ae28e0e7b",
                    "want_to": "5d31c5b8822fa522c7ac5585",
                    "role": "5d30518c822fa522c7991274",
                    "service": [],
                    "createdAt": "2019-08-28T14:05:17.096Z",
                    "updatedAt": "2019-08-28T14:05:17.096Z",
                    "__v": 0
                },
                message: "Registration  step4 successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentfinalstep(req, res) {
        try {
            return {
                status: 201,
                data: {
                    "full_name": "Ayan",
                    "email": "ayan1@gmail.com",
                    "password": "$2a$08$D/GDWDaQm1M5ykmC.pvXKeQzQBAXn8KWl33sx0Vyo47VeystRkqcu",
                    "profile_image": "",
                    "university_name": "",
                    "year_of_study": "",
                    "short_description": "",
                    "social_id": "",
                    "register_type": "normal",
                    "school_name": "Nava Nalanda",
                    "logo": "",
                    "address": "",
                    "gender": "Male",
                    "phone": "",
                    "dob": "",
                    "deviceToken": "",
                    "deviceType": "",
                    "push_notification": true,
                    "display_mobile_no": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d668a1d37aa6464abeeb44a",
                    "language": "5d371e651ae2de8011c4acb5",
                    "country": "5d306dc6822fa522c79acd1f",
                    "education_level": "5d385f14d3486b7ae28e0e7b",
                    "want_to": "5d31c5b8822fa522c7ac5585",
                    "activities": [{
                            "title": "Cooking",
                            "tag": "",
                            "_id": "5d668a1d37aa6464abeeb44e"
                        },
                        {
                            "title": "App Developing",
                            "tag": "",
                            "_id": "5d668a1d37aa6464abeeb44d"
                        },
                        {
                            "title": "Programming",
                            "tag": "",
                            "_id": "5d668a1d37aa6464abeeb44c"
                        },
                        {
                            "title": "Teaching",
                            "tag": "",
                            "_id": "5d668a1d37aa6464abeeb44b"
                        }
                    ],
                    "role": "5d30518c822fa522c7991274",
                    "service": [],
                    "createdAt": "2019-08-28T14:05:17.096Z",
                    "updatedAt": "2019-08-28T14:05:17.096Z",
                    "__v": 0
                },
                "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkNjY4YTFkMzdhYTY0NjRhYmVlYjQ0YSIsImlhdCI6MTU2NzAwMTExNywiZXhwIjoxNTY3MDg3NTE3fQ.sV8Rmz3jdSLQR8tQPBXPdv-R8El1l5VcK0JixR0g81o",
                message: "Registration successfull."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorassignjob(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d526b57a47f2f2ab1d657a6",
                    "name": "Albina Glick",
                    "email": "albina@glick.com",
                    "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                    "profile_image": "",
                    "school_name": "Royal Grammar School",
                    "short_description": "Test Description",
                    "address": "128 Bransten Rd",
                    "university_name": "",
                    "gender": "Male",
                    "phone": "7894561236",
                    "activities": [
                        [{
                                "title": "Activity One",
                                "tag": "Interest",
                                "_id": "5d526b57a47f2f2ab1d657a8"
                            },
                            {
                                "title": "Activity Two",
                                "tag": "Club",
                                "_id": "5d526b57a47f2f2ab1d657a7"
                            }
                        ]
                    ],
                    "specialization": [{
                        "_id": "5d498adae72f7708815f0793",
                        "course_name": "Human Resourse",
                        "status": "Active",
                        "isDeleted": false,
                        "education": "5d31c1d7822fa522c7abf330",
                        "university": "5d497ab20866ed78651160a2",
                        "createdAt": "2019-08-06T14:12:42.017Z",
                        "updatedAt": "2019-08-08T11:41:55.548Z",
                        "__v": 0
                    }],
                    "country": "Germany",
                    "language": "French",
                    "want_to": {
                        "_id": "5d31c5b8822fa522c7ac5585",
                        "name": "Study medicine at university",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-04-15T08:24:40.827Z",
                        "__v": 0
                    },
                    "project": [{
                            "_id": "5d526b57a47f2f2ab1d657aa",
                            "project_name": "project two",
                            "project_url": "http://www.projecttwo.com",
                            "project_details": "project details two",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T07:48:39.390Z",
                            "__v": 0
                        },
                        {
                            "_id": "5d526b57a47f2f2ab1d657a9",
                            "project_name": "Apply For University",
                            "project_url": "www.afu.com",
                            "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T09:37:32.578Z",
                            "__v": 0
                        }
                    ]
                }],
                message: "Job list fetched successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentormymentees(req, res) {
        try {
            // console.log("scdsac");
            return {
                status: 200,
                data: [{
                    "_id": "5d526b57a47f2f2ab1d657a6",
                    "name": "Albina Glick",
                    "email": "albina@glick.com",
                    "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                    "profile_image": "",
                    "school_name": "Royal Grammar School",
                    "short_description": "Test Description",
                    "address": "128 Bransten Rd",
                    "university_name": "",
                    "gender": "Male",
                    "phone": "7894561236",
                    "activities": [
                        [{
                                "title": "Activity One",
                                "tag": "Interest",
                                "_id": "5d526b57a47f2f2ab1d657a8"
                            },
                            {
                                "title": "Activity Two",
                                "tag": "Club",
                                "_id": "5d526b57a47f2f2ab1d657a7"
                            }
                        ]
                    ],
                    "specialization": [{
                        "_id": "5d498adae72f7708815f0793",
                        "course_name": "Human Resourse",
                        "status": "Active",
                        "isDeleted": false,
                        "education": "5d31c1d7822fa522c7abf330",
                        "university": "5d497ab20866ed78651160a2",
                        "createdAt": "2019-08-06T14:12:42.017Z",
                        "updatedAt": "2019-08-08T11:41:55.548Z",
                        "__v": 0
                    }],
                    "country": "Germany",
                    "language": "French",
                    "want_to": {
                        "_id": "5d31c5b8822fa522c7ac5585",
                        "name": "Study medicine at university",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-04-15T08:24:40.827Z",
                        "__v": 0
                    },
                    "project": [{
                            "_id": "5d526b57a47f2f2ab1d657aa",
                            "project_name": "project two",
                            "project_url": "http://www.projecttwo.com",
                            "project_details": "project details two",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T07:48:39.390Z",
                            "__v": 0
                        },
                        {
                            "_id": "5d526b57a47f2f2ab1d657a9",
                            "project_name": "Apply For University",
                            "project_url": "www.afu.com",
                            "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T09:37:32.578Z",
                            "__v": 0
                        }
                    ]
                }],
                message: "Mentees fetched successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentordeletementees(req, res) {
        try {
            return {
                status: 200,
                data: [],
                message: "Mentees deleted successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorsubscribedmentees(req, res) {
        try {
            return {
                status: 200,
                data: [{
                        "_id": "5d526b57a47f2f2ab1d657a6",
                        "name": "Albina Glick",
                        "email": "albina@glick.com",
                        "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                        "profile_image": "",
                        "school_name": "Royal Grammar School",
                        "short_description": "Test Description",
                        "address": "128 Bransten Rd",
                        "university_name": "",
                        "subscribe": true,
                        "gender": "Male",
                        "phone": "7894561236",
                        "activities": [
                            [{
                                    "title": "Activity One",
                                    "tag": "Interest",
                                    "_id": "5d526b57a47f2f2ab1d657a8"
                                },
                                {
                                    "title": "Activity Two",
                                    "tag": "Club",
                                    "_id": "5d526b57a47f2f2ab1d657a7"
                                }
                            ]
                        ],
                        "specialization": [{
                            "_id": "5d498adae72f7708815f0793",
                            "course_name": "Human Resourse",
                            "status": "Active",
                            "isDeleted": false,
                            "education": "5d31c1d7822fa522c7abf330",
                            "university": "5d497ab20866ed78651160a2",
                            "createdAt": "2019-08-06T14:12:42.017Z",
                            "updatedAt": "2019-08-08T11:41:55.548Z",
                            "__v": 0
                        }],
                        "country": "Germany",
                        "language": "French",
                        "want_to": {
                            "_id": "5d31c5b8822fa522c7ac5585",
                            "name": "Study medicine at university",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        },
                        "project": [{
                                "_id": "5d526b57a47f2f2ab1d657aa",
                                "project_name": "project two",
                                "project_url": "http://www.projecttwo.com",
                                "project_details": "project details two",
                                "status": "Active",
                                "isDeleted": false,
                                "user_id": "5d526b57a47f2f2ab1d657a6",
                                "createdAt": "2019-08-13T07:48:39.390Z",
                                "updatedAt": "2019-08-13T07:48:39.390Z",
                                "__v": 0
                            },
                            {
                                "_id": "5d526b57a47f2f2ab1d657a9",
                                "project_name": "Apply For University",
                                "project_url": "www.afu.com",
                                "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                                "status": "Active",
                                "isDeleted": false,
                                "user_id": "5d526b57a47f2f2ab1d657a6",
                                "createdAt": "2019-08-13T07:48:39.390Z",
                                "updatedAt": "2019-08-13T09:37:32.578Z",
                                "__v": 0
                            }
                        ]
                    },
                    {
                        "_id": "5d526b57a47f2f2ab1d6569b",
                        "name": "James Bond",
                        "email": "james@gmail.com",
                        "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                        "profile_image": "",
                        "school_name": "Royal Grammar School",
                        "short_description": "Test Description",
                        "address": "128 Bransten Rd",
                        "university_name": "",
                        "subscribe": true,
                        "gender": "Male",
                        "phone": "7894561236",
                        "activities": [
                            [{
                                    "title": "Activity One",
                                    "tag": "Interest",
                                    "_id": "5d526b57a47f2f2ab1d657a8"
                                },
                                {
                                    "title": "Activity Two",
                                    "tag": "Club",
                                    "_id": "5d526b57a47f2f2ab1d657a7"
                                }
                            ]
                        ],
                        "specialization": [{
                            "_id": "5d498adae72f7708815f0793",
                            "course_name": "Human Resourse",
                            "status": "Active",
                            "isDeleted": false,
                            "education": "5d31c1d7822fa522c7abf330",
                            "university": "5d497ab20866ed78651160a2",
                            "createdAt": "2019-08-06T14:12:42.017Z",
                            "updatedAt": "2019-08-08T11:41:55.548Z",
                            "__v": 0
                        }],
                        "country": "Germany",
                        "language": "French",
                        "want_to": {
                            "_id": "5d31c5b8822fa522c7ac5585",
                            "name": "Study medicine at university",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        },
                        "project": [{
                                "_id": "5d526b57a47f2f2ab1d657aa",
                                "project_name": "project two",
                                "project_url": "http://www.projecttwo.com",
                                "project_details": "project details two",
                                "status": "Active",
                                "isDeleted": false,
                                "user_id": "5d526b57a47f2f2ab1d657a6",
                                "createdAt": "2019-08-13T07:48:39.390Z",
                                "updatedAt": "2019-08-13T07:48:39.390Z",
                                "__v": 0
                            },
                            {
                                "_id": "5d526b57a47f2f2ab1d657a9",
                                "project_name": "Apply For University",
                                "project_url": "www.afu.com",
                                "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                                "status": "Active",
                                "isDeleted": false,
                                "user_id": "5d526b57a47f2f2ab1d657a6",
                                "createdAt": "2019-08-13T07:48:39.390Z",
                                "updatedAt": "2019-08-13T09:37:32.578Z",
                                "__v": 0
                            }
                        ]
                    }
                ],
                message: "Mentees fetched successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async studentmenteessearch(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d526b57a47f2f2ab1d657a6",
                    "name": "Albina Glick",
                    "email": "albina@glick.com",
                    "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                    "profile_image": "",
                    "school_name": "Royal Grammar School",
                    "short_description": "Test Description",
                    "address": "128 Bransten Rd",
                    "university_name": "",
                    "gender": "Male",
                    "phone": "7894561236",
                    "activities": [
                        [{
                                "title": "Activity One",
                                "tag": "Interest",
                                "_id": "5d526b57a47f2f2ab1d657a8"
                            },
                            {
                                "title": "Activity Two",
                                "tag": "Club",
                                "_id": "5d526b57a47f2f2ab1d657a7"
                            }
                        ]
                    ],
                    "specialization": [{
                        "_id": "5d498adae72f7708815f0793",
                        "course_name": "Human Resourse",
                        "status": "Active",
                        "isDeleted": false,
                        "education": "5d31c1d7822fa522c7abf330",
                        "university": "5d497ab20866ed78651160a2",
                        "createdAt": "2019-08-06T14:12:42.017Z",
                        "updatedAt": "2019-08-08T11:41:55.548Z",
                        "__v": 0
                    }],
                    "country": "Germany",
                    "language": "French",
                    "want_to": {
                        "_id": "5d31c5b8822fa522c7ac5585",
                        "name": "Study medicine at university",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-04-15T08:24:40.827Z",
                        "__v": 0
                    },
                    "project": [{
                            "_id": "5d526b57a47f2f2ab1d657aa",
                            "project_name": "project two",
                            "project_url": "http://www.projecttwo.com",
                            "project_details": "project details two",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T07:48:39.390Z",
                            "__v": 0
                        },
                        {
                            "_id": "5d526b57a47f2f2ab1d657a9",
                            "project_name": "Apply For University",
                            "project_url": "www.afu.com",
                            "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T09:37:32.578Z",
                            "__v": 0
                        }
                    ]
                }],
                message: "Mentees list fetched successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorfilter(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d3188206d84b8436af1ce37",
                    "name": "Josephine Darakjy",
                    "email": "josephine_darakjy@darakjy.org",
                    "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                    "profile_image": "",
                    "address": "London",
                    "university_name": "Chembridge",
                    "gender": "Male",
                    "short_description": "test description",
                    "language": "English",
                    "country": "United Kingdom",
                    "service": [{
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d3188206d84b8436af1ce3a"
                        },
                        {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        }
                    ],
                    "specialization": [{
                        "title": "Finance",
                        "_id": "5d3188206d84b8436af1ce39"
                    }]
                }],
                message: "Mentees deleted successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorcountry(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d3188206d84b8436af1ce37",
                    "name": "Josephine Darakjy",
                    "email": "josephine_darakjy@darakjy.org",
                    "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                    "profile_image": "",
                    "address": "London",
                    "university_name": "Chembridge",
                    "gender": "Male",
                    "short_description": "test description",
                    "language": "English",
                    "country": "United Kingdom",
                    "service": [{
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d3188206d84b8436af1ce3a"
                        },
                        {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        }
                    ],
                    "specialization": [{
                        "title": "Finance",
                        "_id": "5d3188206d84b8436af1ce39"
                    }]
                }],
                message: "Mentor list fetched successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorcourse(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d3188206d84b8436af1ce37",
                    "name": "Josephine Darakjy",
                    "email": "josephine_darakjy@darakjy.org",
                    "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                    "profile_image": "",
                    "address": "London",
                    "university_name": "Chembridge",
                    "gender": "Male",
                    "short_description": "test description",
                    "language": "English",
                    "country": "United Kingdom",
                    "service": [{
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d3188206d84b8436af1ce3a"
                        },
                        {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        }
                    ],
                    "specialization": [{
                        "title": "Finance",
                        "_id": "5d3188206d84b8436af1ce39"
                    }]
                }],
                message: "Mentor list fetched successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async getprofilestudent(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d3188206d84b8436af1ce37",
                    "profile": {
                        "_id": "5d3188206d84b8436af1ce37",
                        "full_name": "Subhra",
                        "email": "subhra@gmail.com",
                        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                        "profile_image": "",
                        "school_name": "",
                        "education_level": "5d31c1a0822fa522c7abefca",
                        "want_to": "",
                        "address": "London",
                        "university_name": "RKMV",
                        "gender": "Male",
                        "dob": "",
                        "social_id": "",
                        "register_type": "normal",
                        "deviceToken": "",
                        "deviceType": "",
                        "isDeleted": false,
                        "isActive": true,
                        "country": "5d306dc6822fa522c79acd21",
                        "language": "5d306346822fa522c79a2dec",
                        "specialization": [{
                                "title": "project one",
                                "_id": "5d3188206d84b8436af1ce39"
                            },
                            {
                                "title": "http://www.projectone.com",
                                "_id": "5d3188206d84b8436af1ce38"
                            }
                        ],
                        "service": {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        },
                        "role": "5d30518c822fa522c7991272",
                        "project": [],
                        "activities": [],
                        "push_notification": true,
                        "short_description": "test description",
                        "createdAt": "2019-07-19T09:06:40.023Z",
                        "updatedAt": "2019-07-19T13:35:04.720Z",
                        "__v": 0,
                        "user_role": {
                            "_id": "5d30518c822fa522c7991272",
                            "role": "mentor",
                            "desc": "This is the mentor role here",
                            "roleDisplayName": "Mentor"
                        },
                        "edu_level": {
                            "_id": "5d31c1a0822fa522c7abefca",
                            "level_name": "Level 01",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        }
                    },
                    "service": [{
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d3188206d84b8436af1ce3a"
                        },
                        {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        }
                    ]
                }],
                message: "Student Profile Info fetched Successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async getprofilementor(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d3188206d84b8436af1ce37",
                    "profile": {
                        "_id": "5d3188206d84b8436af1ce37",
                        "full_name": "Kalyan",
                        "email": "kalyan@gmail.com",
                        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                        "profile_image": "",
                        "school_name": "",
                        "education_level": "5d31c1a0822fa522c7abefca",
                        "want_to": "",
                        "address": "London",
                        "university_name": "RKMV",
                        "gender": "Male",
                        "dob": "",
                        "social_id": "",
                        "register_type": "normal",
                        "deviceToken": "",
                        "deviceType": "",
                        "isDeleted": false,
                        "isActive": true,
                        "country": "5d306dc6822fa522c79acd21",
                        "language": "5d306346822fa522c79a2dec",
                        "specialization": [{
                                "title": "project one",
                                "_id": "5d3188206d84b8436af1ce39"
                            },
                            {
                                "title": "http://www.projectone.com",
                                "_id": "5d3188206d84b8436af1ce38"
                            }
                        ],
                        "service": {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        },
                        "role": "5d30518c822fa522c7991272",
                        "project": [],
                        "activities": [],
                        "push_notification": true,
                        "short_description": "test description",
                        "createdAt": "2019-07-19T09:06:40.023Z",
                        "updatedAt": "2019-07-19T13:35:04.720Z",
                        "__v": 0,
                        "user_role": {
                            "_id": "5d30518c822fa522c7991272",
                            "role": "mentor",
                            "desc": "This is the mentor role here",
                            "roleDisplayName": "Mentor"
                        },
                        "edu_level": {
                            "_id": "5d31c1a0822fa522c7abefca",
                            "level_name": "Level 01",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        }
                    },
                    "service": [{
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d3188206d84b8436af1ce3a"
                        },
                        {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        }
                    ]
                }],
                message: "Mentor Profile Info fetched Successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async getprofiledetails(req, res) {
        try {
            return {
                status: 200,
                data: [{
                    "_id": "5d3188206d84b8436af1ce37",
                    "profile": {
                        "_id": "5d3188206d84b8436af1ce37",
                        "full_name": "Kalyan",
                        "email": "kalyan@gmail.com",
                        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                        "profile_image": "",
                        "school_name": "",
                        "education_level": "5d31c1a0822fa522c7abefca",
                        "want_to": "",
                        "address": "London",
                        "university_name": "RKMV",
                        "gender": "Male",
                        "dob": "",
                        "social_id": "",
                        "register_type": "normal",
                        "deviceToken": "",
                        "deviceType": "",
                        "isDeleted": false,
                        "isActive": true,
                        "country": "5d306dc6822fa522c79acd21",
                        "language": "5d306346822fa522c79a2dec",
                        "specialization": [{
                                "title": "project one",
                                "_id": "5d3188206d84b8436af1ce39"
                            },
                            {
                                "title": "http://www.projectone.com",
                                "_id": "5d3188206d84b8436af1ce38"
                            }
                        ],
                        "service": {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        },
                        "role": "5d30518c822fa522c7991272",
                        "project": [],
                        "activities": [],
                        "push_notification": true,
                        "short_description": "test description",
                        "createdAt": "2019-07-19T09:06:40.023Z",
                        "updatedAt": "2019-07-19T13:35:04.720Z",
                        "__v": 0,
                        "user_role": {
                            "_id": "5d30518c822fa522c7991272",
                            "role": "mentor",
                            "desc": "This is the mentor role here",
                            "roleDisplayName": "Mentor"
                        },
                        "edu_level": {
                            "_id": "5d31c1a0822fa522c7abefca",
                            "level_name": "Level 01",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        }
                    },
                    "service": [{
                            "name": "Service Two",
                            "time": "3",
                            "details": "service details two",
                            "cost": "100",
                            "_id": "5d3188206d84b8436af1ce3a"
                        },
                        {
                            "name": "Service One",
                            "time": "2",
                            "details": "service details one",
                            "cost": "75",
                            "_id": "5d3188206d84b8436af1ce3b"
                        }
                    ]
                }],
                message: "Mentor Profile Info fetched Successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async addInterest(req, res) {
        try {
            return {
                status: 200,
                data: [{
                        "tag": "Interest One"
                    },
                    {
                        "tag": "Interest Two"
                    }
                ],
                message: "Add tag of Interest successfully"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async addActivity(req, res) {
        try {
            return {
                status: 200,
                data: [{
                        "tag": "Activity One"
                    },
                    {
                        "tag": "Activity Two"
                    }
                ],
                message: "Add activity tag successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async addClubs(req, res) {
        try {
            return {
                status: 200,
                data: [{
                        "tag": "Club One"
                    },
                    {
                        "tag": "Club Two"
                    }
                ],
                message: "Add clubs tag successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async addSocieties(req, res) {
        try {
            return {
                status: 200,
                data: [{
                        "tag": "Society One"
                    },
                    {
                        "tag": "Society Two"
                    }
                ],
                message: "Add society tag successfully."
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorjobsearchcountry(req, res) {
        try {
            return {
                status: 200,
                message: "Job fetched successfully",
                data: [{
                    "_id": "5d526b57a47f2f2ab1d657a6",
                    "name": "Albina Glick",
                    "email": "albina@glick.com",
                    "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                    "profile_image": "",
                    "school_name": "Royal Grammar School",
                    "short_description": "Test Description",
                    "address": "128 Bransten Rd",
                    "university_name": "",
                    "gender": "Male",
                    "phone": "7894561236",
                    "activities": [
                        [{
                                "title": "Activity One",
                                "tag": "Interest",
                                "_id": "5d526b57a47f2f2ab1d657a8"
                            },
                            {
                                "title": "Activity Two",
                                "tag": "Club",
                                "_id": "5d526b57a47f2f2ab1d657a7"
                            }
                        ]
                    ],
                    "specialization": [{
                        "_id": "5d498adae72f7708815f0793",
                        "course_name": "Human Resourse",
                        "status": "Active",
                        "isDeleted": false,
                        "education": "5d31c1d7822fa522c7abf330",
                        "university": "5d497ab20866ed78651160a2",
                        "createdAt": "2019-08-06T14:12:42.017Z",
                        "updatedAt": "2019-08-08T11:41:55.548Z",
                        "__v": 0
                    }],
                    "country": "Germany",
                    "language": "French",
                    "want_to": {
                        "_id": "5d31c5b8822fa522c7ac5585",
                        "name": "Study medicine at university",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-04-15T08:24:40.827Z",
                        "__v": 0
                    },
                    "project": [{
                            "_id": "5d526b57a47f2f2ab1d657aa",
                            "project_name": "project two",
                            "project_url": "http://www.projecttwo.com",
                            "project_details": "project details two",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T07:48:39.390Z",
                            "__v": 0
                        },
                        {
                            "_id": "5d526b57a47f2f2ab1d657a9",
                            "project_name": "Apply For University",
                            "project_url": "www.afu.com",
                            "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T09:37:32.578Z",
                            "__v": 0
                        }
                    ]
                }]
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorjobsearchcourse(req, res) {
        try {
            return {
                status: 200,
                message: "Student list fetched successfully",
                data: [{
                    "_id": "5d526b57a47f2f2ab1d657a6",
                    "name": "Albina Glick",
                    "email": "albina@glick.com",
                    "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                    "profile_image": "",
                    "school_name": "Royal Grammar School",
                    "short_description": "Test Description",
                    "address": "128 Bransten Rd",
                    "university_name": "",
                    "gender": "Male",
                    "phone": "7894561236",
                    "activities": [
                        [{
                                "title": "Activity One",
                                "tag": "Interest",
                                "_id": "5d526b57a47f2f2ab1d657a8"
                            },
                            {
                                "title": "Activity Two",
                                "tag": "Club",
                                "_id": "5d526b57a47f2f2ab1d657a7"
                            }
                        ]
                    ],
                    "specialization": [{
                        "_id": "5d498adae72f7708815f0793",
                        "course_name": "Human Resourse",
                        "status": "Active",
                        "isDeleted": false,
                        "education": "5d31c1d7822fa522c7abf330",
                        "university": "5d497ab20866ed78651160a2",
                        "createdAt": "2019-08-06T14:12:42.017Z",
                        "updatedAt": "2019-08-08T11:41:55.548Z",
                        "__v": 0
                    }],
                    "country": "Germany",
                    "language": "French",
                    "want_to": {
                        "_id": "5d31c5b8822fa522c7ac5585",
                        "name": "Study medicine at university",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-04-15T08:24:40.827Z",
                        "__v": 0
                    },
                    "project": [{
                            "_id": "5d526b57a47f2f2ab1d657aa",
                            "project_name": "project two",
                            "project_url": "http://www.projecttwo.com",
                            "project_details": "project details two",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T07:48:39.390Z",
                            "__v": 0
                        },
                        {
                            "_id": "5d526b57a47f2f2ab1d657a9",
                            "project_name": "Apply For University",
                            "project_url": "www.afu.com",
                            "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T09:37:32.578Z",
                            "__v": 0
                        }
                    ]
                }]
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorjobsearchuniversity(req, res) {
        try {
            return {
                status: 200,
                message: "Student list fetched successfully",
                data: [{
                    "_id": "5d526b57a47f2f2ab1d657a6",
                    "name": "Albina Glick",
                    "email": "albina@glick.com",
                    "password": "$2a$08$pIh8Ic.QCIc1HxK6r3S8zO3dqUPO4y6j8cnElVYhCsI.VoqApUJgO",
                    "profile_image": "",
                    "school_name": "",
                    "short_description": "Test Description",
                    "address": "128 Bransten Rd",
                    "university_name": "Oxford",
                    "gender": "Male",
                    "phone": "7894561236",
                    "activities": [
                        [{
                                "title": "Activity One",
                                "tag": "Interest",
                                "_id": "5d526b57a47f2f2ab1d657a8"
                            },
                            {
                                "title": "Activity Two",
                                "tag": "Club",
                                "_id": "5d526b57a47f2f2ab1d657a7"
                            }
                        ]
                    ],
                    "specialization": [{
                        "_id": "5d498adae72f7708815f0793",
                        "course_name": "Human Resourse",
                        "status": "Active",
                        "isDeleted": false,
                        "education": "5d31c1d7822fa522c7abf330",
                        "university": "5d497ab20866ed78651160a2",
                        "createdAt": "2019-08-06T14:12:42.017Z",
                        "updatedAt": "2019-08-08T11:41:55.548Z",
                        "__v": 0
                    }],
                    "country": "Germany",
                    "language": "French",
                    "want_to": {
                        "_id": "5d31c5b8822fa522c7ac5585",
                        "name": "Study medicine at university",
                        "status": "Active",
                        "isDeleted": false,
                        "createdAt": "2019-04-15T08:24:40.827Z",
                        "__v": 0
                    },
                    "project": [{
                            "_id": "5d526b57a47f2f2ab1d657aa",
                            "project_name": "project two",
                            "project_url": "http://www.projecttwo.com",
                            "project_details": "project details two",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T07:48:39.390Z",
                            "__v": 0
                        },
                        {
                            "_id": "5d526b57a47f2f2ab1d657a9",
                            "project_name": "Apply For University",
                            "project_url": "www.afu.com",
                            "project_details": "As Cicero would put it, “Um, not so fast.” The placeholder text, beginning with the line “Lorem ipsum dolor sit amet,",
                            "status": "Active",
                            "isDeleted": false,
                            "user_id": "5d526b57a47f2f2ab1d657a6",
                            "createdAt": "2019-08-13T07:48:39.390Z",
                            "updatedAt": "2019-08-13T09:37:32.578Z",
                            "__v": 0
                        }
                    ]
                }]
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async mentorlisting(req, res) {
        try {
            return {
                status: 200,
                message: "Mentor list fetched successfully",
                data: [{
                        "_id": "5d31830d6d84b8436af1ce32",
                        "full_name": "Kuldip",
                        "email": "kk@gmail.com",
                        "password": "$2a$08$vtcwsvoesd6jZtMo5wdp4OxKMBdwpzuHMNwMoGQZnIRIilVPEBMnm",
                        "profile_image": "",
                        "school_name": "",
                        "education_level": "5d31c1a0822fa522c7abefca",
                        "want_to": "",
                        "address": "London",
                        "university_name": "RKMV",
                        "gender": "Male",
                        "dob": "",
                        "social_id": "",
                        "register_type": "normal",
                        "deviceToken": "",
                        "deviceType": "",
                        "isDeleted": false,
                        "isActive": true,
                        "country": "5d306dc6822fa522c79acd21",
                        "language": "5d306346822fa522c79a2dec",
                        "specialization": [{
                                "title": "project one",
                                "_id": "5d31830d6d84b8436af1ce34"
                            },
                            {
                                "title": "http://www.projectone.com",
                                "_id": "5d31830d6d84b8436af1ce33"
                            }
                        ],
                        "service": [{
                                "name": "Service One",
                                "time": "2",
                                "details": "service details one",
                                "cost": "75",
                                "_id": "5d31830d6d84b8436af1ce36"
                            },
                            {
                                "name": "Service Two",
                                "time": "3",
                                "details": "service details two",
                                "cost": "100",
                                "_id": "5d31830d6d84b8436af1ce35"
                            }
                        ],
                        "role": "5d30518c822fa522c7991272",
                        "project": [],
                        "activities": [],
                        "push_notification": true,
                        "short_description": "test description",
                        "createdAt": "2019-07-19T08:45:01.100Z",
                        "updatedAt": "2019-07-19T08:45:01.100Z",
                        "__v": 0,
                        "user_role": {
                            "_id": "5d30518c822fa522c7991272",
                            "role": "mentor",
                            "desc": "This is the mentor role here",
                            "roleDisplayName": "Mentor"
                        },
                        "edu_level": {
                            "_id": "5d31c1a0822fa522c7abefca",
                            "level_name": "Level 01",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        }
                    },
                    {
                        "_id": "5d3188206d84b8436af1ce37",
                        "full_name": "Subhra",
                        "email": "subhra@gmail.com",
                        "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                        "profile_image": "",
                        "school_name": "",
                        "education_level": "5d31c1a0822fa522c7abefca",
                        "want_to": "",
                        "address": "London",
                        "university_name": "RKMV",
                        "gender": "Male",
                        "dob": "",
                        "social_id": "",
                        "register_type": "normal",
                        "deviceToken": "",
                        "deviceType": "",
                        "isDeleted": false,
                        "isActive": true,
                        "country": "5d306dc6822fa522c79acd21",
                        "language": "5d306346822fa522c79a2dec",
                        "specialization": [{
                                "title": "project one",
                                "_id": "5d3188206d84b8436af1ce39"
                            },
                            {
                                "title": "http://www.projectone.com",
                                "_id": "5d3188206d84b8436af1ce38"
                            }
                        ],
                        "service": [{
                                "name": "Service One",
                                "time": "2",
                                "details": "service details one",
                                "cost": "75",
                                "_id": "5d3188206d84b8436af1ce3b"
                            },
                            {
                                "name": "Service Two",
                                "time": "3",
                                "details": "service details two",
                                "cost": "100",
                                "_id": "5d3188206d84b8436af1ce3a"
                            }
                        ],
                        "role": "5d30518c822fa522c7991272",
                        "project": [],
                        "activities": [],
                        "push_notification": true,
                        "short_description": "test description",
                        "createdAt": "2019-07-19T09:06:40.023Z",
                        "updatedAt": "2019-07-19T13:35:04.720Z",
                        "__v": 0,
                        "user_role": {
                            "_id": "5d30518c822fa522c7991272",
                            "role": "mentor",
                            "desc": "This is the mentor role here",
                            "roleDisplayName": "Mentor"
                        },
                        "edu_level": {
                            "_id": "5d31c1a0822fa522c7abefca",
                            "level_name": "Level 01",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        }
                    },
                    {
                        "_id": "5d3189f0719a7e481b879cb1",
                        "full_name": "Soudip",
                        "email": "soudip@gmail.com",
                        "password": "$2a$08$zPkgkuWbLqIepAojvcOW9.svpljrsvlS9bf6cBR.lM2GCYY8bMtQm",
                        "profile_image": "profile_image_1563527664279_img3.jpg",
                        "school_name": "",
                        "education_level": "5d31c1d7822fa522c7abf330",
                        "want_to": "",
                        "address": "London",
                        "university_name": "RKMV",
                        "gender": "Male",
                        "dob": "",
                        "social_id": "",
                        "register_type": "normal",
                        "deviceToken": "",
                        "deviceType": "",
                        "isDeleted": false,
                        "isActive": true,
                        "country": "5d306dc6822fa522c79acd21",
                        "language": "5d306346822fa522c79a2dec",
                        "specialization": [{
                                "title": "project one",
                                "_id": "5d3189f0719a7e481b879cb3"
                            },
                            {
                                "title": "http://www.projectone.com",
                                "_id": "5d3189f0719a7e481b879cb2"
                            }
                        ],
                        "service": [{
                                "name": "Service One",
                                "time": "2",
                                "details": "service details one",
                                "cost": "75",
                                "_id": "5d3189f0719a7e481b879cb5"
                            },
                            {
                                "name": "Service Two",
                                "time": "3",
                                "details": "service details two",
                                "cost": "100",
                                "_id": "5d3189f0719a7e481b879cb4"
                            }
                        ],
                        "role": "5d30518c822fa522c7991272",
                        "project": [],
                        "activities": [],
                        "push_notification": true,
                        "short_description": "test description",
                        "createdAt": "2019-07-19T09:14:24.393Z",
                        "updatedAt": "2019-07-19T09:14:24.393Z",
                        "__v": 0,
                        "user_role": {
                            "_id": "5d30518c822fa522c7991272",
                            "role": "mentor",
                            "desc": "This is the mentor role here",
                            "roleDisplayName": "Mentor"
                        },
                        "edu_level": {
                            "_id": "5d31c1d7822fa522c7abf330",
                            "level_name": "Level 02",
                            "status": "Active",
                            "isDeleted": false,
                            "createdAt": "2019-04-15T08:24:40.827Z",
                            "__v": 0
                        }
                    }
                ]
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async createChatNew(req, res) {
        try {
            return {
                status: 200,
                message: "Attachment send successfully",
                data: {
                    "chat_token": "dPucrZQBwqIi0o8TlTwsseaYr2sjkTEWHJHer8PGtVI",
                    "chat_date": "2019-07-23T10:46:57.731Z",
                    "_id": "5d36e5ba24bf1f6694936c0e",
                    "mentor_id": "5d31830d6d84b8436af1ce32",
                    "student_id": "5d309257ae2ba07b64ea59f4",
                    "__v": 0
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async chatSendAttachment(req, res) {
        try {
            return {
                status: 200,
                message: "Attachment send successfully",
                data: "1563802441970_ton.pdf"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async chatSendPicture(req, res) {
        try {
            return {
                status: 200,
                message: "Picture send successfully",
                data: "image_1563802441970_img2.jpg"
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async createToken(req, res) {
        try {
            return {
                status: 200,
                message: "Chat token create successfully",
                data: []
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async createSessionToken(req, res) {
        try {
            return {
                status: 200,
                message: "Chat started successfully",
                data: []
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async sremoveChat(req, res) {
        try {
            return {
                status: 200,
                message: "Chat deleted successfully",
                data: {
                    "chat_token": "cxxph92ZaCwW1LEmir9wCkmGNIiQ9xtsQ5FV4t9EAkU",
                    "chat_date": "2019-09-06T10:57:44.989Z",
                    "isDeleted": true,
                    "_id": "5d723bddc5524b394b342861",
                    "mentor_id": "5d6e75ff91b6735dc5206a35",
                    "student_id": "5d6e4b8db6c99e0151707649",
                    "__v": 0
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async messagePage(req, res) {
        try {
            return {
                status: 200,
                message: "Create message API",
                data: {
                    "_id": "5d3188206d84b8436af1ce37",
                    "mentor_id": "5d306dc6822fa522c79acd21",
                    "student_id": "5d306346822fa522c79a2dec"
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async messagestatuschange(req, res) {
        try {
            return {
                status: 200,
                message: "message status updated successfully",
                data: {
                    "readstatus": "yes",
                    "_id": "5d3188206d84b8436af1ce37"
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async messagedelete(req, res) {
        try {
            return {
                status: 200,
                message: "Delete message successfully",
                data: {},
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async requestcallbackpage(req, res) {
        try {
            return {
                status: 200,
                message: "Request call back page fetched",
                data: {},
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async reportmessage(req, res) {
        try {
            return {
                status: 200,
                message: "Request call back page fetched",
                data: {
                    "_id": "5d3188206d84b8436af1ce37",
                    "mentor_id": "5d306dc6822fa522c79acd21",
                    "student_id": "5d306346822fa522c79a2dec",
                    "count": 5,
                    "message_details": [{
                            message: "message one"
                        },
                        {
                            message: "message two"
                        }
                    ]
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async messagelist(req, res) {
        try {
            return {
                status: 200,
                message: "Message list fetched successfully",
                data: {
                    "_id": "5d3188206d84b8436af1ce37",
                    "mentor_id": "5d306dc6822fa522c79acd21",
                    "student_id": "5d306346822fa522c79a2dec",
                    "count": 5,
                    "message_details": [{
                            message: "hi"
                        },
                        {
                            message: "how are you"
                        }
                    ]
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }

    async offlineonlinestatus(req, res) {
        try {
            return {
                status: 200,
                message: "Settings update successfully",
                data: {
                    "full_name": "Josephine Darakjy",
                    "email": "josephine_darakjy@darakjy.org",
                    "password": "$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq",
                    "online_status": true,
                    "isDeleted": false,
                    "isActive": true,
                    "_id": "5d3188206d84b8436af1ce37",
                    "role": "5d30518c822fa522c7991272",
                    "createdAt": "2019-07-19T09:06:40.023Z",
                    "updatedAt": "2019-08-16T09:14:15.232Z",
                    "__v": 0
                },
            }
        } catch (error) {
            return {
                status: 500,
                message: error.message
            }
        }
    }
}

module.exports = new staticController();