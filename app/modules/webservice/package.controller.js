const packageRepo = require('package/repositories/package.repository');
const orderRepo = require('order/repositories/order.repository');
const userRepo = require('user/repositories/user.repository');
const mongoose = require('mongoose');


/* @Method: getList
// @Description: package list
*/
exports.getList = async req => {
	try {
		let condition = {
			$and: [{
				"isDeleted": false,
				"user_id": {
					$exists: false
				}
			}]
		};
		const defaultPackageData = await packageRepo.getAllByField(condition);

		if (!_.isEmpty(defaultPackageData)) {
			return {
				"status": 200,
				data: defaultPackageData,
				message: 'Packages fetched successfully'
			};
		} else {
			return {
				"status": 201,
				data: [],
				message: 'Records not found'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};

/* @Method: getPackageDetailByUser
// @Description: Package Detail By User
*/
exports.getPackageDetailByUser = async req => {
	try {

		let package_id = req.params.package_id;
		const packageData = await packageRepo.getById(package_id);
		console.log('packageData', packageData);
		if (!_.isEmpty(packageData)) {

			let purchasedOrders = await orderRepo.getAllByField({
				'student_id': mongoose.Types.ObjectId(req.user._id)
			});

			if (_.find(purchasedOrders, function (obj) {
					return obj.package_id.toString() === packageData._id.toString();
				})) {
				packageData.isPurchased = true;
			} else {
				packageData.isPurchased = false;
			}

			return {
				"status": 200,
				data: packageData,
				message: 'Packages fetched successfully'
			};
		} else {
			return {
				"status": 201,
				data: [],
				message: 'Records not found'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};


/* @Method: addpackage
// @Description: package Add
*/
exports.addCustomPackageBackUp = async req => {
	try {
		let _loggedInUserID = req.user._id;
		req.body.user_id = _loggedInUserID;
		// let checkPackage =  await packageRepo.getByField({ 'package_name': { $regex: req.body.package_name, $options: 'i' },'isDeleted':false});
		// console.log("35",checkPackage);
		// if (!_.isEmpty(checkPackage)) {
		// 	console.log("37");
		// 	return { "status": 200,data: checkPackage,message: 'Packages name already exist'};		
		// }
		// else{
		let insertpackage = await packageRepo.save(req.body);
		if (insertpackage) {
			userRepo.updateById({
				$push: {
					"service": {
						"package_id": insertpackage._id
					}
				}
			}, _loggedInUserID);
			return {
				"status": 200,
				data: insertpackage,
				message: 'Packages added successfully'
			};
		} else {
			return {
				"status": 201,
				data: [],
				message: 'Package add failed'
			};
		}
		//}
	} catch (error) {
		console.log('ererer', error);
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};


exports.addCustomPackage = async req => {
	try {
		req.body.user_id = req.user._id;
		let userInfo = await userRepo.getById(req.user._id);
		if (_.has(req.body, 'package_id')) {
			if (userInfo.service.length > 0) {
				let oldServiceArray = userInfo.service;
				var checkNewPackageExistsOrNot = oldServiceArray.some(el => el.package_id.toString() === req.body.package_id.toString());
				if (checkNewPackageExistsOrNot) {
					return {
						status: 201,
						data: [],
						message: 'This Package already exists, please choose another.'
					}
				} else {
					userRepo.updateById({
						$push: {
							"service": {
								"package_id": req.body.package_id
							}
						}
					}, req.body.user_id);
					return {
						status: 200,
						data: [],
						message: 'Package added successfully.'
					}
				}
			} else {
				userRepo.updateById({
					$push: {
						"service": {
							"package_id": req.body.package_id
						}
					}
				}, req.body.user_id);
				return {
					status: 200,
					data: [],
					message: 'Package added successfully.'
				}
			}
		} else {
			let getAllPackages = await packageRepo.getAllByField({
				'status': 'Active',
				'isDeleted': false,
				'user_id': req.user._id
			});
			var checkSamePackageNameExistsOrNot = getAllPackages.some(el => el.package_name.replace(/\s+/g, '-').toLowerCase() === req.body.package_name.replace(/\s+/g, '-').toLowerCase());
			if (checkSamePackageNameExistsOrNot) {
				return {
					status: 201,
					data: [],
					message: 'This Package already exists, please choose another.'
				}
			} else {
				let insertpackage = await packageRepo.save(req.body);
				if (insertpackage) {
					userRepo.updateById({
						$push: {
							"service": {
								"package_id": insertpackage._id
							}
						}
					}, req.body.user_id);
					return {
						"status": 200,
						data: insertpackage,
						message: 'Packages added successfully'
					};
				} else {
					return {
						"status": 201,
						data: [],
						message: 'We are unable to add the Package.'
					};
				}
			}
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};

/* @Method: getListUser
// @Description: package list
*/
exports.getListUser = async req => {
	try {
		let _loggedInUserID = req.user._id;
		const packageData = await packageRepo.getAllPackage({
			"user_id": mongoose.Types.ObjectId(_loggedInUserID)
		});
		//console.log("48>>",packageData);
		let condition = {
			$and: [{
				"isDeleted": false,
				"user_id": {
					$exists: false
				}
			}]
		};
		const defaultPackageData = await packageRepo.getAllByField(condition);
		//console.log("51>>",defaultPackageData);
		if (!_.isEmpty(packageData) || !_.isEmpty(defaultPackageData)) {
			return {
				"status": 200,
				data: {
					"custom": packageData,
					"default": defaultPackageData
				},
				message: 'Packages fetched successfully'
			};
		} else {
			return {
				"status": 201,
				data: [],
				message: 'Records not found'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};

/* @Method: getList
// @Description: package list
*/
exports.updateCustomPackage = async req => {
	try {
		let user_id = req.user._id;
		let packageId = req.body.package_id;
		const updateData = await packageRepo.updateById(req.body, {
			"_id": mongoose.Types.ObjectId(packageId)
		});
		if (!_.isEmpty(updateData)) {
			return {
				"status": 200,
				data: updateData,
				message: 'Packages updated Successfully'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};


/* @Method: getList
// @Description: package list
*/
exports.deleteCustomPackage = async req => {
	try {
		let user_id = req.user._id;
		let packageId = req.body.package_id;
		// console.log("77>>", packageId);
		const checkExist = await packageRepo.getById({
			"_id": mongoose.Types.ObjectId(packageId)
		});
		// console.log("79>>", checkExist); //process.exit();
		if (!_.isEmpty(checkExist)) {
			return {
				"status": 201,
				data: checkExist,
				message: 'You cannot delete this as this package already been used'
			};
		} else {
			const deleteData = await packageRepo.updateById({
				"isDeleted": true
			}, {
				"_id": mongoose.Types.ObjectId(packageId)
			});
			return {
				"status": 200,
				data: deleteData,
				message: 'Delete data successfully'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};