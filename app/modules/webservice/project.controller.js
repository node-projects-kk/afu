const projectRepo = require('project/repositories/project.repository');
const mongoose = require('mongoose');
/* @Method: getList
// @Description: Project list
*/
exports.getList = async req => {
	try {
		let user_id = req.user._id;
		const projectData = await projectRepo.getAllProject({user_id :mongoose.Types.ObjectId(user_id),status: 'Active'});
		return { "status": 200,data: projectData,message: 'Projects fetched Successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
	}
};

/* @Method: getList
// @Description: Project list
*/
exports.updateProject = async req => {
	try {
		let user_id = req.user._id;
		let projectId = req.body.project_id;
		const updateData = await projectRepo.updateById(req.body,{"_id" :mongoose.Types.ObjectId(projectId)});
		if (!_.isEmpty(updateData)) {
			return { "status": 200,data: updateData,message: 'Projects updated Successfully'};	
		}
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
	}
};


/* @Method: getList
// @Description: Project list
*/
exports.deleteProject = async req => {
	try {
		let user_id = req.user._id;
		let projectId = req.body.project_id;
		const checkExist = await projectRepo.getById({"_id" :mongoose.Types.ObjectId(projectId)});
		if (!_.isEmpty(checkExist)) {
			return { "status": 201,data: checkExist,message: 'You cannot delete this as this project already been used'};	
		}
		else{
			const deleteData =  await projectRepo.updateById({"isDeleted":true},{"_id" :mongoose.Types.ObjectId(projectId)});
			return { "status": 200,data: deleteData,message: 'Delete data successfully'};	
		}
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
	}
};