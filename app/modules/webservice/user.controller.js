const userRepo = require('user/repositories/user.repository');
const roleRepo = require('role/repositories/role.repository');
const settingRepo = require('setting/repositories/setting.repository');
const reviewRepo = require('review/repositories/review.repository');
const languageRepo = require('language/repositories/language.repository');
const projectRepo = require('project/repositories/project.repository');
const orderRepo = require('order/repositories/order.repository');
const menteeRepo = require('user/repositories/mentee.repository');
const mentorRepo = require('user/repositories/mentor.repository');
const packageRepo = require('package/repositories/package.repository');
const userModel = require('user/models/user.model.js');
const mailer = require('../../helpers/mailer.js');
const gm = require('gm').subClass({
	imageMagick: true
});
const fs = require('fs');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const async = require('async');
const perPage = config.PAGINATION_PERPAGE;
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const {
	join
} = require('path');
const {
	promisify
} = require('util');
const {
	readFile
} = require('fs');
var stripe = require("stripe")(config.stripe_secret_key);
const User = new userModel();
const pushHelper = require('../../helpers/native_push');
const chatRepo = require('chat/repositories/chat.repository');

class userController {
	constructor() {
		this.user = [];
	}

	async register(req, res) {
		try {
			let roleDetails = '';
			let projectArr = [];
			const type = req.body.type;
			if (type == "student") {
				roleDetails = await roleRepo.getByField({
					'role': 'student'
				});
			} else if (type == "mentor") {
				roleDetails = await roleRepo.getByField({
					'role': 'mentor'
				});
			}

			if (!_.isEmpty(roleDetails)) {
				req.body.role = roleDetails._id;
			}

			if (_.has(req, 'files')) {
				if (req.files.length > 0) {
					async.forEach(req.files, function (item, cb) {
						if (item.fieldname == "profile_image") {
							gm('public/uploads/user/' + item.filename).resize(100).write('public/uploads/user/thumb/' + item.filename, function (err) {
								if (err) throw new Error(err.message);
							});
							req.body.profile_image = item.filename;
							cb();
						}
						if (item.fieldname == "logo") {
							gm('public/uploads/logo/' + item.filename).resize(100).write('public/uploads/logo/thumb/' + item.filename, function (err) {
								if (err) throw new Error(err.message);
							});
							req.body.logo = item.filename;
							cb();
						}
					}, function (err) {});
				}
			}

			if (req.body.register_type && (req.body.register_type == "google" || req.body.register_type == "facebook" || req.body.register_type == "instagram" || req.body.register_type == "linkedin")) {
				let checkSocial = await userRepo.getByField({
					"social_id": req.body.social_id,
					"isDeleted": false
				});
				if (!_.isEmpty(checkSocial)) {
					return {
						status: 201,
						data: [],
						"message": 'User already exists.'
					};
				}
			} else {
				var p = {
					"isDeleted": false,
					'email': req.body.email
				};

				let checkEmail = await userRepo.getByField(p);
				if (!_.isEmpty(checkEmail)) {
					return {
						status: 201,
						data: [],
						"message": 'Email already exists.'
					};
				}
			}

			if (req.body.register_type && (req.body.register_type == "google" || req.body.register_type == "facebook" || req.body.register_type == "instagram" || req.body.register_type == "linkedin")) {
				req.body.social_id = req.body.social_id;
			} else {
				req.body.social_id = "";
			}
			req.body.password = User.generateHash(req.body.password);
			let userData = await userRepo.save(req.body);
			let user_id = userData._id;

			/************** User status change in firebase and user model ************/
			let usersRef = firebase_app.database().ref(`users/${String(user_id)}`);
			let getUserDataFromFirebase = await usersRef.once('value');
			let userDataFromFirebase = getUserDataFromFirebase.val();
			if (userDataFromFirebase) {
				let updateUserOfFirebase = await usersRef.update({
					isOnline: true,
				});
			} else {
				let doc = {
					isOnline: true,
				};
				let createRecordInFirebase = await usersRef.set(doc);
			}
			let updateUserOnlineStatus = await userRepo.updateById({
				onoffstatus: 'yes'
			}, user_id);
			/********************End************************/

			if (_.has(req.body, 'project')) {
				async.forEach(req.body.project, function (item, cb) {
					req.body.project_name = item.project_name;
					req.body.project_url = item.project_url;
					req.body.project_details = item.project_details;
					req.body.user_id = user_id;
					let projectData = projectRepo.save(req.body);
					cb();
				}, function (err) {});
			}
			if (!_.isEmpty(userData)) {
				const payload = {
					id: userData._id
				};
				const token = jwt.sign(payload, config.jwtSecret, {
					expiresIn: 86400
				});
				return {
					status: 200,
					data: userData,
					token: token,
					"message": 'Registration successful.'
				};
			}
		} catch (e) {
			console.log("124>>", e);
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async signin(req, res) {
		try {
			var userType = req.body.user_type;
			if (_.has(req.body, 'social_id')) {
				if (req.body.register_type === 'facebook' || req.body.register_type === 'google' || req.body.register_type === 'instagram' || req.body.register_type === 'linkedin') {
					let checkUserData = await userRepo.getByField({
						"social_id": req.body.social_id,
						"isDeleted": false
					});
					if (!_.isEmpty(checkUserData)) {
						let roleInfo = await roleRepo.getById(checkUserData.role)
						if (roleInfo.role == userType) {
							if (checkUserData.isActive && checkUserData.isActive === true) {
								const payload = {
									id: checkUserData._id
								};
								const token = jwt.sign(payload, config.jwtSecret, {
									expiresIn: 86400
								});
								/************** User status change in firebase and user model ************/
								let usersRef = firebase_app.database().ref(`users/${String(checkUserData._id)}`);
								let getUserDataFromFirebase = await usersRef.once('value');
								let userDataFromFirebase = getUserDataFromFirebase.val();
								if (userDataFromFirebase) {
									let updateUserOfFirebase = await usersRef.update({
										isOnline: true,
									});
								} else {
									let doc = {
										isOnline: true,
									};
									let createRecordInFirebase = await usersRef.set(doc);
								}
								let updateUserOnlineStatus = await userRepo.updateById({
									onoffstatus: 'yes'
								}, checkUserData._id);
								/******************** End ************************/
								return {
									status: 200,
									data: checkUserData,
									isLoggedIn: true,
									token: token,
									"message": 'Login successful.'
								};
							} else {
								return {
									status: 201,
									data: [],
									isLoggedIn: false,
									"message": 'User status is inactive.'
								};
							}
						} else {
							return {
								status: 201,
								data: [],
								isLoggedIn: false,
								"message": 'Sorry user not found.'
							};
						}
					} else {
						return {
							status: 201,
							data: [],
							isLoggedIn: false,
							"message": 'User not found.'
						};
					}
				}
			} else {
				let checkUserData = await userRepo.getUserByField({
					"email": req.body.email,
					"isDeleted": false
				});
				if (!_.isEmpty(checkUserData)) {
					if (checkUserData.role.role == userType) {
						if (checkUserData.isActive && checkUserData.isActive === true) {
							let isPasswordMatched = bcrypt.compareSync(req.body.password, checkUserData.password);
							if (!isPasswordMatched) {
								return {
									status: 201,
									data: [],
									isLoggedIn: false,
									"message": 'Password not matched.'
								};
							} else {
								const payload = {
									id: checkUserData._id
								};
								const token = jwt.sign(payload, config.jwtSecret, {
									expiresIn: 86400
								});
								if (_.has(req.body, 'deviceType') && !_.isEmpty(req.body.deviceType)) {
									let userData = {
										'deviceType': req.body.deviceType,
										'deviceToken': req.body.deviceToken
									};
									const updatedUser = await userRepo.updateById(userData, checkUserData._id);
									checkUserData.deviceType = updatedUser.deviceType;
									checkUserData.deviceToken = updatedUser.deviceToken;
								}
								/************** User status change in firebase and user model ************/
								let usersRef = firebase_app.database().ref(`users/${String(checkUserData._id)}`);
								let getUserDataFromFirebase = await usersRef.once('value');
								let userDataFromFirebase = getUserDataFromFirebase.val();
								if (userDataFromFirebase) {
									let updateUserOfFirebase = await usersRef.update({
										isOnline: true,
									});
								} else {
									let doc = {
										isOnline: true,
									};
									let createRecordInFirebase = await usersRef.set(doc);
								}
								let updateUserOnlineStatus = await userRepo.updateById({
									onoffstatus: 'yes'
								}, checkUserData._id);
								/**************End**************/
								return {
									status: 200,
									data: checkUserData,
									isLoggedIn: true,
									token: token,
									"message": 'Login successful.'
								};
							}
						} else {
							return {
								status: 201,
								data: [],
								isLoggedIn: false,
								"message": 'User status is inactive.'
							};
						}
					} else {
						return {
							status: 201,
							data: [],
							isLoggedIn: false,
							"message": 'Sorry user not found.'
						};
					}
				} else {
					return {
						status: 201,
						data: [],
						isLoggedIn: false,
						"message": 'User not found.'
					};
				}
			}
		} catch (e) {
			return res.status(500).send({
				status: 500,
				message: e.message
			});
		}
	}

	async getMyProfile(req) {
		try {
			const user = await userRepo.getByIdUser(req.user._id);
			let getReviewInfo = await reviewRepo.getAllReviewByField(req.user._id);
			let getReviewLength = getReviewInfo.length;
			let ratingAverageInfo = 0;
			if (getReviewLength > 0) {
				let addRating = 0;
				let reviewTotal = getReviewInfo.map(item => {
					addRating += item.rating;
				});
				ratingAverageInfo = addRating / getReviewLength;
			}
			let result = {};
			result = user;
			result.ratingAverageInfo = ratingAverageInfo.toFixed(2);

			return {
				status: 200,
				data: result,
				message: 'Profile Info fetched Successfully'
			};
		} catch (e) {
			return res.status(500).send({
				status: 500,
				message: e.message
			});
		}
	}

	async getDashboard(req) {
		try {
			const user = await userRepo.getById(req.user._id);

			var count_data = {};

			var top_users = [];

			if (user.role.role == 'mentor') {
				const userCount = await userRepo.getUsersCount();
				count_data.user = userCount[0].count;
			} else {
				top_users = await userRepo.getTopMentors(5);
				const mentorCount = await userRepo.getMentorCount();
				count_data.user = mentorCount[0].count;
			}



			count_data.referral = 5;
			count_data.ambassador = 7;

			var result = {
				'count': count_data,
				'top_users': top_users
			}



			return {
				status: 200,
				data: result,
				message: 'Dashboard Info fetched Successfully'
			};
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async updateProfile(req) {
		try {
			const data = await userRepo.getById(req.user._id);
			if (!_.isEmpty(data.email)) {
				if (_.has(req.body, 'email') && (data.email != req.body.email)) {
					//console.log("1");
					const result = await userRepo.getByField({
						'email': req.body.email,
						'isDeleted': false
					});
					if (!_.isEmpty(result)) {
						return {
							status: 201,
							data: {},
							"message": "This email address is already exist!"
						};
					}
				} else { //console.log("2"); process.exit();
					if (_.has(req, 'files')) {
						if (req.files.length > 0) {
							async.forEach(req.files, function (item, cb) {
								if (item.fieldname == "profile_image") {
									if (!_.isEmpty(data)) {
										if (!_.isEmpty(data) && data.profile_image != '') {
											if (fs.existsSync('public/uploads/user/' + data.profile_image)) {
												const upl_resume = fs.unlinkSync('public/uploads/user/' + data.profile_image);
											}
											if (fs.existsSync('public/uploads/user/thumb/' + data.profile_image)) {
												const upl_thumb_resume = fs.unlinkSync('public/uploads/user/thumb/' + data.profile_image);
											}
										}
									} else {
										req.body.profile_image = data.profile_image;
									}
									gm('public/uploads/user/' + item.filename).resize(100).write('public/uploads/user/thumb/' + item.filename, function (err) {
										if (err) throw new Error(err.message);
									});
									req.body.profile_image = item.filename;
									cb();
								}
								if (item.fieldname == "logo") {
									if (!_.isEmpty(data)) {
										if (!_.isEmpty(data) && data.logo != '') {
											if (fs.existsSync('public/uploads/logo/' + data.logo)) {
												const upl_resume = fs.unlinkSync('public/uploads/logo/' + data.logo);
											}
											if (fs.existsSync('public/uploads/logo/thumb/' + data.logo)) {
												const upl_thumb_resume = fs.unlinkSync('public/uploads/logo/thumb/' + data.logo);
											}
										}
									} else {
										req.body.logo = data.logo;
									}
									gm('public/uploads/logo/' + item.filename).resize(100).write('public/uploads/logo/thumb/' + item.filename, function (err) {
										if (err) throw new Error(err.message);
									});
									req.body.logo = item.filename;
									cb();
								}
							}, function (err) {

							});
						}
					}
					//if (_.has(req.body,"password")) {
					//	req.body.password = await User.generateHash(req.body.password);
					//}
					const result2 = await userRepo.updateById(req.body, req.user._id);
					return {
						status: 200,
						data: result2,
						"message": "User details updated successfully"
					};
				}
			} else {
				return {
					status: 200,
					data: [],
					"message": "User not found"
				};
			}
		} catch (e) {
			return res.status(500).send({
				status: 500,
				message: e.message
			});
		}
	}

	async updateAdditionalInfo(req) {
		try {
			const data = await userRepo.getById(req.user._id);
			if (_.has(req, 'files')) {
				if (req.files.length > 0) {
					if (!_.isEmpty(data)) {
						if (!_.isEmpty(data) && data.logo != '') {
							if (fs.existsSync('public/uploads/logo/' + data.logo)) {
								const upl_resume = await fs.unlinkSync('public/uploads/logo/' + data.logo);
							}
							if (fs.existsSync('public/uploads/logo/thumb/' + data.logo)) {
								const upl_thumb_resume = await fs.unlinkSync('public/uploads/logo/thumb/' + data.logo);
							}
						}
					}
					gm('public/uploads/logo/' + req.files[0].filename).resize(100).write('public/uploads/logo/thumb/' + req.files[0].filename, function (err) {
						if (err)
							throw new Error(err.message);
					});
					req.body.logo = req.files[0].filename;
				} else {
					req.body.logo = data.logo;
				}
			}
			const result2 = await userRepo.updateById(req.body, req.user._id);
			if (result2) {
				return {
					status: 200,
					data: result2,
					"message": "Additional info updated successfully"
				};
			}
		} catch (e) {
			console.log("274>>", error.message);
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async changePassword(req) {
		try {
			const user = await userRepo.getById(req.user._id);
			if ((!User.validPassword(req.body.currentPassword, user.password))) {
				return {
					status: 201,
					data: [],
					"message": 'Wrong current password'
				};
			} else {
				const newPassword = User.generateHash(req.body.newPassword);
				await userRepo.updateById({
					password: newPassword
				}, req.user._id);
				return {
					status: 200,
					data: [],
					"message": 'Password updated successfully'
				};
			}
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async forgotPassword(req, res) {
		try {
			const user = await userRepo.getByField({
				email: req.body.email
			});
			//console.log("357>>",user);
			if (!_.isEmpty(user)) {
				let random_pass = Math.random().toString(36).substr(2, 9);
				const readable_pass = random_pass;
				random_pass = User.generateHash(random_pass);
				const setting_data = await settingRepo.getAllByField({
					"isDeleted": false
				});
				//console.log("363>>",setting_data);
				var settingObj = {};
				if (!_.isEmpty(setting_data)) {
					setting_data.forEach(function (element) {
						settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
					});
				}
				console.log(">>", settingObj.Site_Title);
				let locals = {
					user_fullname: user.full_name,
					password: readable_pass,
					site_title: settingObj.Site_Title
				};
				let isMailSend = await mailer.sendMail('Admin<test@yopmail.com>', req.body.email, 'Reset Password', 'forgot-password', locals);

				//console.log("234>>",isMailSend); //process.exit();

				if (isMailSend) {
					await userRepo.updateById({
						password: random_pass
					}, user._id);
					return {
						status: 200,
						data: [],
						"message": 'New Password sent to your registered email id'
					};
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'No matching user found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async logout(req, res) {
		try {
			var user_id = req.body.user_id;
			const result = await userRepo.getById(user_id);
			if (!_.isEmpty(result)) {
				/******* update user status firebase and user model********/
				let usersRef = firebase_app.database().ref(`users/${String(result._id)}`);
				let getUserDataFromFirebase = await usersRef.once('value');
				let userDataFromFirebase = getUserDataFromFirebase.val();
				if (userDataFromFirebase) {
					let updateUserOfFirebase = await usersRef.update({
						isOnline: false,
					});
				} else {
					let doc = {
						isOnline: false
					};
					let createRecordInFirebase = await usersRef.set(doc);
				}
				/************ End *************/
				var updateObj = {
					"deviceToken": "",
					"deviceType": "",
					"onoffstatus": "no",
				};
				const updatedObj = await userRepo.updateById(updateObj, user_id);
				const payload = {
					id: user_id
				};
				const token = jwt.sign(payload, config.jwtSecret, {
					expiresIn: 0
				});

				return {
					status: 200,
					data: [],
					isLoggedIn: false,
					"message": 'Logout Successfully'
				};
			} else {
				return {
					status: 201,
					data: [],
					"message": 'No User Found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async mentorList(req, res) {
		try {
			let user_id = req.user._id;
			let mlist = await userRepo.getAllActiveUsersByRole("mentor");
			//console.log("354>>",mlist);
			if (!_.isEmpty(mlist)) {
				return {
					status: 200,
					data: mlist,
					"message": 'Mentor list fetched successfully'
				};
			} else {
				return {
					status: 201,
					data: [],
					message: 'No matching user found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async studentList(req, res) {
		try {
			let user_id = req.user._id;
			let course_id = req.user.course;
			let country_id = req.user.country;

			/* This is for search section */
			if (_.isObject(req.body) && _.has(req.body, 'course')) {
				if (req.body.course != '') {
					course_id = req.body.course
				}
			}

			if (_.isObject(req.body) && _.has(req.body, 'country')) {
				if (req.body.country != '') {
					country_id = req.body.country
				}
			}

			/* This is for search section */
			req.body.course = course_id;
			req.body.country = country_id;


			let menteesList = await menteeRepo.getAllByField({
				'mentor_id': user_id
			});
			let getMenteeIdLists = menteesList.map((item) => item.student_id);
			let slist = await userRepo.getAllStudetListCourseWise(req.body, getMenteeIdLists);
			if (!_.isEmpty(slist)) {
				return {
					status: 200,
					data: slist,
					"message": 'Student list fetched successfully'
				};
			} else {
				return {
					status: 200,
					data: [],
					message: 'Sorry no matching mentee found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				status: 500,
				message: e.message
			});
		}
	}

	async mentorSearch(req, res) {
		try {
			let user_id = req.user._id;
			let mentorList = await mentorRepo.getAllByField({
				'student_id': user_id
			});
			let getMentorIdLists = mentorList.map((item) => item.mentor_id);
			let mlist = await userRepo.mentorsearch(req.body, getMentorIdLists);
			if (!_.isEmpty(mlist)) {
				return {
					status: 200,
					data: mlist,
					"message": 'Mentor list fetched successfully'
				};
			} else {
				return {
					status: 200,
					data: [],
					message: 'No matching mentor found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				status: 500,
				message: e.message
			});
		}
	}

	async getMentorProfile(req) {
		try {
			const userId = req.params.id;

			let user = await userRepo.getByIdUser(userId);

			let purchasedOrders = await orderRepo.getAllByField({
				'student_id': mongoose.Types.ObjectId(req.user._id)
			});
			let getReviewInfo = await reviewRepo.getAllReviewByField(userId);
			let getReviewLength = getReviewInfo.length;
			let ratingAverageInfo = 0;
			if (getReviewLength > 0) {
				let addRating = 0;
				let reviewTotal = getReviewInfo.map(item => {
					addRating += item.rating;
				});
				ratingAverageInfo = addRating / getReviewLength;
			}
			for (var s = 0; s < user.service_val.length; s++) {
				if (_.find(purchasedOrders, function (obj) {
						return obj.package_id.toString() === user.service_val[s]._id.toString();
					})) {
					user.service_val[s]['isPurchased'] = true;
				} else {
					user.service_val[s]['isPurchased'] = false;
				}
			}

			// let data = user;
			let result = {};
			result = user;
			result.ratingAverageInfo = ratingAverageInfo.toFixed(2);
			return {
				status: 200,
				data: result,
				message: 'Profile Info fetched Successfully'
			};
		} catch (e) {
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async jobSearch(req, res) {
		try {
			let user_id = req.user._id;
			let addedmenteeList = await menteeRepo.getAllByField({
				"mentor_id": user_id,
				"isDeleted": false
			});
			console.log("455>>", addedmenteeList);
			var menteeIds = addedmenteeList.map(function (mentee) {
				return mentee.student_id;
			});
			console.log("459>>", menteeIds); //process.exit();
			let slist = await userRepo.jobsearch(req.body, menteeIds);

			if (!_.isEmpty(slist)) {
				return {
					status: 200,
					data: slist,
					"message": 'Job list fetched successfully'
				};
			} else {
				return {
					status: 201,
					data: [],
					message: 'No matching job found'
				};
			}
		} catch (e) {
			//console.log("272>>",error.message);
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async changeNotification(req, res) {
		try {
			var updateVal = {};
			let user_id = req.user._id;
			if (_.has(req.body, 'push_notification')) {
				updateVal = {
					"push_notification": req.body.push_notification
				};
			}
			if (_.has(req.body, 'display_mobile_no')) {
				updateVal = {
					"display_mobile_no": req.body.display_mobile_no
				};
			}
			if (_.has(req.body, 'push_notification') && _.has(req.body, 'display_mobile_no')) {
				updateVal = {
					"push_notification": req.body.push_notification,
					"display_mobile_no": req.body.display_mobile_no
				};
			}
			let resultData = await userRepo.updateById(updateVal, {
				"_id": user_id
			});
			if (!_.isEmpty(resultData)) {
				return {
					status: 200,
					data: resultData,
					"message": 'Settings updated successfully'
				};
			} else {
				return {
					status: 201,
					data: [],
					message: 'No matching record found'
				};
			}
		} catch (e) {
			console.log("428>>", error.message);
			return res.status(500).send({
				"message": e.message
			});
		}
	}

	async placeOrder(req, res) {
		try {
			let student_id = req.user._id;
			let mentor_id = req.body.mentor_id;
			let package_id = req.body.package_id;
			let package_name = req.body.package_name;
			let package_price = req.body.package_price;
			let checkExist = await orderRepo.getByField({
				"package_id": package_id,
				"student_id": student_id,
				"mentor_id": mentor_id
			});
			if (!_.isEmpty(checkExist)) {
				return {
					status: 201,
					data: [],
					message: 'Already placed this order'
				};
			} else {
				/************************************************************/
				var card_number = req.body.card_number;
				var expiry_month = parseInt(req.body.exp_month);
				var expiry_year = parseInt(req.body.exp_year);
				var cvv = req.body.cvv;

				var stripeCardResponse = await stripe.tokens.create({
					card: {
						number: card_number,
						exp_month: expiry_month,
						exp_year: expiry_year,
						cvc: cvv,
					}
				});

				var stripe_token = stripeCardResponse.id;
				var customer = await stripe.customers.create({
					email: req.user.email,
				});

				var source = await stripe.customers.createSource(customer.id, {
					source: stripe_token,
				});

				const stripeCharge = await stripe.charges.create({
					amount: package_price * 100,
					currency: 'usd',
					description: 'package charge',
					customer: source.customer
				});

				req.body.stripe_charge_id = stripeCharge.id;
				req.body.customer_id = customer.id;
				if (stripeCharge) {
					req.body.package_price = parseFloat(req.body.package_price)
					var resultData = await orderRepo.save(req.body);
					var packageInfo = await packageRepo.getById(resultData.package_id);
					let {
						deviceType,
						deviceToken
					} = await userRepo.getById(resultData.mentor_id);

					let {
						full_name
					} = await userRepo.getById(resultData.student_id);

					let deviceDetails = {
						deviceType,
						deviceToken
					};

					let chatRecord = await chatRepo.getByField({
						'mentor_id': resultData.mentor_id,
						'student_id': resultData.student_id
					});

					let chatToken = '';
					if (chatRecord) {
						chatToken = chatRecord.chat_token;
					}

					let sendPushNew = await pushHelper.sendPushForChat(deviceDetails, {
						title: `New Package Purchased`,
						body: `${full_name} has purchased the package ${packageInfo.package_name}`,
						clickAction: 'mentorpackagenotification'
					}, {
						sender_id: resultData.student_id,
						full_name,
						chatToken,
						type: 'payment'
					});

					let checkUserData = await mentorRepo.getByField({
						"mentor_id": mentor_id,
						"student_id": student_id,
						"isDeleted": false
					});

					if (_.isEmpty(checkUserData) || _.isNull(checkUserData)) {
						let insertObj = {};
						insertObj.student_id = student_id;
						insertObj.mentor_id = mentor_id;
						let MentorResult = await mentorRepo.save(insertObj);
						let MenteeResult = await menteeRepo.save(insertObj);
					}

					if (resultData) {
						return {
							status: 200,
							data: resultData,
							message: 'Package has purchased successfully'
						};
					}
				}
			}
		} catch (e) {
			console.log("erererererer === 957 ====", e);
			return res.status(500).send({
				status: 500,
				message: e.message
			});
		}
	}

	async requestCallBack(req, res) {
		try {
			let user_id = req.user._id;
			let mentor_id = req.body.mentor_id;
			const result = await userRepo.getById(mentor_id);
			if (!_.isEmpty(result)) {
				let mail = result.email;

				let name = req.body.name;
				let phone = req.body.phone;
				let callback_time = req.body.callback_time;
				let message = req.body.message;

				//const setting_data = await settingRepo.getAllSetting();
				//var settingObj = {};
				//if (!_.isEmpty(setting_data)) {
				//	setting_data.forEach(function (element) {
				//		settingObj[element.setting_name.replace(/\s+/g, "_")] = element.setting_value;
				//	});
				//}

				let locals = {
					fullname: name,
					phone_number: phone,
					callbacktime: callback_time,
					message: message,
					site_title: "Request call back"
				};
				let isMailSend = await mailer.sendMail('Admin<test@yopmail.com>', 'subhasish.ghosh@webskitters.com', 'Request Call back', 'request-callback', locals);

				console.log("234>>", isMailSend);

				if (isMailSend) {
					//await userRepo.updateById({password: random_pass}, user._id);
					return {
						status: 200,
						data: [],
						"message": 'Request call back send successfully'
					};
				}
			} else {
				return {
					status: 201,
					data: [],
					"message": 'User not found'
				};
			}
		} catch (e) {
			return res.status(500).send({
				message: e.message
			});
		}
	}

	async addMentees(req, res) {
		try {
			let mentor_id = req.user._id;
			let student_id = req.body.student_id;
			let checkUserData = await menteeRepo.getByField({
				"mentor_id": mentor_id,
				"student_id": student_id,
				"isDeleted": false
			});
			if (!_.isEmpty(checkUserData) || !_.isNull(checkUserData)) {
				return {
					status: 201,
					data: checkUserData,
					message: "Already added"
				}
			} else {
				req.body.mentor_id = mentor_id;
				let result = await menteeRepo.save(req.body);
				if (result) {
					return {
						status: 200,
						data: result,
						message: "Mentee added successfully"
					}
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}

	async listMentees(req, res) {
		try {
			let user_id = req.user._id;
			let menteesList = await menteeRepo.getMenteesList(user_id);
			if (!_.isEmpty(menteesList)) {
				return {
					status: 200,
					data: menteesList,
					message: "Mentees list fetched successfully"
				}
			} else {
				return {
					status: 200,
					data: [],
					message: "No record found"
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}

	async removeMentees(req, res) {
		try {
			let mentor_id = req.user._id;
			let id = req.body.id;
			let checkUserData = await menteeRepo.getByField({
				"mentor_id": mentor_id,
				"_id": id,
				"isDeleted": false
			});
			if (!_.isEmpty(checkUserData) || !_.isNull(checkUserData)) {
				let deleteData = await menteeRepo.delete(checkUserData._id);
				return {
					status: 200,
					data: checkUserData,
					message: "Mentee has removed successfully"
				}
			} else {
				return {
					status: 201,
					data: [],
					message: "Sorry data not found"
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}

	async addMentors(req, res) {
		try {
			let student_id = req.user._id;
			let mentor_id = req.body.mentor_id;
			let checkUserData = await mentorRepo.getByField({
				"mentor_id": mentor_id,
				"student_id": student_id,
				"isDeleted": false
			});
			if (!_.isEmpty(checkUserData) || !_.isNull(checkUserData)) {
				return {
					status: 201,
					data: checkUserData,
					message: "Already added"
				}
			} else {
				req.body.student_id = student_id;
				let result = await mentorRepo.save(req.body);
				if (result) {
					return {
						status: 200,
						data: result,
						message: "Mentor added successfully"
					}
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}

	async listMentors(req, res) {
		try {
			let user_id = req.user._id;
			let menteesList = await mentorRepo.getMentorsList(user_id);
			if (!_.isEmpty(menteesList)) {
				return {
					status: 200,
					data: menteesList,
					message: "Mentors list fetched successfully"
				}
			} else {
				return {
					status: 200,
					data: [],
					message: "No record found"
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}

	async removeMentors(req, res) {
		try {
			let student_id = req.user._id;
			let id = req.body.id;
			let checkUserData = await mentorRepo.getByField({
				"student_id": student_id,
				"_id": id,
				"isDeleted": false
			});
			if (!_.isEmpty(checkUserData) || !_.isNull(checkUserData)) {
				let deleteData = await mentorRepo.delete(checkUserData._id);
				return {
					status: 200,
					data: checkUserData,
					message: "Mentor has removed successfully"
				}
			} else {
				return {
					status: 201,
					data: [],
					message: "Sorry data not found"
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}

	async userOrderHistory(req, res) {
		try {
			let getUserRoleInfo = await userRepo.getById(req.user._id);
			if (getUserRoleInfo.role.role == "student") {
				let getOrderDetails = await orderRepo.getOrderHistoryByStudentId(req.user._id);
				if (!_.isEmpty(getOrderDetails)) {
					return {
						status: 200,
						data: getOrderDetails,
						message: 'Order history fetched successfully.'
					}
				} else {
					return {
						status: 201,
						data: [],
						message: 'There are no data at this moment.'
					}
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'Only students can view this, Please login with student details.'
				}
			}
		} catch (error) {
			return {
				status: 500,
				message: error.message
			}
		}
	}



}


module.exports = new userController();