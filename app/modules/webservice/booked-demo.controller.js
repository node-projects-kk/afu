const bookedDemoRepo = require('booked-demo/repositories/booked-demo.repository');
const jwt = require('jsonwebtoken');
const async = require('async');
const mongoose = require('mongoose');


class bookedDemoController {
	constructor() {
		this.user = [];
	}
	
	async bookDemo(req,res){
		try{
			
			let bookingData = await bookedDemoRepo.save(req.body);
			
			return {status: 200,data:bookingData, "message": 'Demo booked successfully.'};
			
		}
		catch(e){
			console.log(e.message);
			return res.status(500).send({"message": e.message});
		}
	}	
	
}


module.exports = new bookedDemoController();