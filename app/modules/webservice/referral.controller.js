const referralRepo = require('referral/repositories/referral.repository');
const mongoose = require('mongoose');

/* @Method: referralList
// @Description: Referral list
*/
exports.referralListOfMentor = async req => {
  try {
    let getMentorReferralInfo = await referralRepo.getAllReferralList(req.params.id);
    if (!_.isEmpty(getMentorReferralInfo)) {
      return {
        status: 200,
        data: getMentorReferralInfo,
        message: 'Referral information fetched successfully.'
      }
    } else {
      return {
        status: 201,
        data: [],
        message: 'There are no data at this moment.'
      }
    }
  } catch (error) {
    return res.status(500).send({
      message: error.message
    });
  }
};