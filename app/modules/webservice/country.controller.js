const countryRepo = require('country/repositories/country.repository');

/* @Method: getList
// @Description: Country list
*/
exports.getList = async req => {
    try {
        const countryData = await countryRepo.getAllCountry({status: 'Active'});
        return { "status": 200,data: countryData,message: 'Countries fetched Successfully'};
    } catch (error) {
        return { "status": 500, data: [], "message": error.message };
    }
};


/* @Method: getAutocompleteList
// @Description: Country Autocomplete list API
*/
exports.getAutocompleteList = async req => {
	try {
		const searchTxt = req.query.keyword;
		const countryData = await countryRepo.getAllCountry({status: 'Active',"country_name": {"$regex": searchTxt, "$options": "i"}});
		return { "status": 200,data: countryData,message: 'Countries fetched successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
    }
};