const courseRepo = require('course/repositories/course.repository');
/* @Method: getList
// @Description: Course List
*/
exports.getList = async req => {
	try {
		const courseData = await courseRepo.getAllCourse({status: 'Active'});
		return { "status": 200,data: courseData,message: 'Courses fetched Successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
	}
};


/* @Method: getAutocompleteList
// @Description: Course Autocomplete list API
*/
exports.getAutocompleteList = async req => {
	try {
		const searchTxt = req.query.keyword;
		const courseData = await courseRepo.getAllCourse({status: 'Active',"course_name": {"$regex": searchTxt, "$options": "i"}});
		return { "status": 200,data: courseData,message: 'Courses fetched Successfully'};
	}
	catch (error) {
		return { "status": 500, data: [], "message": error.message };
    }
};