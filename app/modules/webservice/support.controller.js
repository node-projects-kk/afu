const supportRepo = require('support/repositories/support.repository');
const jwt = require('jsonwebtoken');
const async = require('async');
const mongoose = require('mongoose');

class bookedDemoController {
	constructor() {
		this.user = [];
	}
	
	async submitQuery(req,res){
		try{
			req.body.user_id = req.user._id;
			let bookingData = await supportRepo.save(req.body);			
			return {status: 200,data:bookingData, "message": 'Query placed successfully.'};			
		}
		catch(e){
			console.log(e.message);
			return res.status(500).send({"message": e.message});
		}
	}	
	
}


module.exports = new bookedDemoController();