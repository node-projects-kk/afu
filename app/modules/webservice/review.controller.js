const reviewRepo = require('review/repositories/review.repository');
const userRepo = require('user/repositories/user.repository');
const mongoose = require('mongoose');

/* @Method: addReview
// @Description: Review Add
*/
exports.addReviewBackUp = async req => {
	try {
		let _loggedInUserID = req.user._id;
		let mentor_id = req.body.review_to_user_id;
		let checkReview = await reviewRepo.getByField({
			"review_from_user_id": _loggedInUserID,
			"review_to_user_id": mentor_id
		});

		if (!_.isEmpty(checkReview)) {
			let review_id = checkReview._id; //console.log("14>>",review_id); process.exit();
			req.body.review_from_user_id = _loggedInUserID;
			let updateReview = await reviewRepo.updateById(req.body, review_id);
			if (updateReview) {
				return {
					"status": 200,
					data: updateReview,
					message: 'Reviews update successfully'
				};
			} else {
				return {
					"status": 201,
					data: [],
					message: 'Review update failed'
				};
			}
		} else {
			req.body.review_from_user_id = _loggedInUserID;
			//req.body.review_to_user_id = review_to_user_id;
			let insertReview = await reviewRepo.save(req.body);
			if (insertReview) {
				return {
					"status": 200,
					data: insertReview,
					message: 'Reviews given successfully'
				};
			} else {
				return {
					"status": 201,
					data: [],
					message: 'Review add failed'
				};
			}
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};

exports.addReview = async req => {
	try {
		req.body.review_from_user_id = req.user._id;
		let getUserRoleInfo = await userRepo.getById(req.user._id);
		if (getUserRoleInfo.role.role == 'student') {
			let saveReview = await reviewRepo.save(req.body);
			if (!_.isEmpty(saveReview)) {
				return {
					status: 200,
					data: saveReview,
					message: 'Review added successfully.'
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'There are some problem, Please try again.'
				}
			}
		} else {
			return {
				status: 201,
				data: [],
				message: 'Only students can view this, Please login with student details.'
			}
		}
	} catch (error) {
		return {
			status: 500,
			message: error.message
		}
	}
};


/* @Method: getList
// @Description: Review list
*/
exports.getListBackUp = async req => {
	try {
		let _loggedInUserID = req.user._id;
		const reviewData = await reviewRepo.getAllReview({
			"review_from_user_id": mongoose.Types.ObjectId(_loggedInUserID)
		});
		// console.log("48>>", reviewData);
		if (!_.isEmpty(reviewData)) {
			return {
				"status": 200,
				data: reviewData,
				message: 'Reviews fetched successfully'
			};
		} else {
			return {
				"status": 201,
				data: [],
				message: 'Record not found'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};

exports.getReviewList = async req => {
	try {
		let getUserRoleInfo = await userRepo.getById(req.user._id);
		if (getUserRoleInfo.role.role == 'student') {
			let getReviewsList = await reviewRepo.getAllReviewByPackageId(req.params.packageId);
			if (!_.isEmpty(getReviewsList)) {
				return {
					status: 200,
					data: getReviewsList,
					message: 'Review fetched successfully.'
				}
			} else {
				return {
					status: 201,
					data: [],
					message: 'There are no reviews for this Package.'
				}
			}
		} else {
			return {
				status: 201,
				data: [],
				message: 'Only students can view this, Please login with student details.'
			}
		}
	} catch (error) {
		return {
			status: 500,
			message: error.message
		}
	}
};


/* @Method: getList
// @Description: Review list
*/
exports.updateReview = async req => {
	try {
		let user_id = req.user._id;
		let reviewId = req.body.review_id;
		const updateData = await reviewRepo.updateById(req.body, {
			"_id": mongoose.Types.ObjectId(reviewId)
		});
		if (!_.isEmpty(updateData)) {
			return {
				"status": 200,
				data: updateData,
				message: 'Reviews updated Successfully'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};


/* @Method: getList
// @Description: Review list
*/
exports.deleteReview = async req => {
	try {
		let user_id = req.user._id;
		let reviewId = req.body.review_id;
		const checkExist = await reviewRepo.getById({
			"_id": mongoose.Types.ObjectId(reviewId)
		});
		if (!_.isEmpty(checkExist)) {
			return {
				"status": 201,
				data: checkExist,
				message: 'You cannot delete this as this review already been used'
			};
		} else {
			const deleteData = await reviewRepo.updateById({
				"isDeleted": true
			}, {
				"_id": mongoose.Types.ObjectId(reviewId)
			});
			return {
				"status": 200,
				data: deleteData,
				message: 'Delete data successfully'
			};
		}
	} catch (error) {
		return {
			"status": 500,
			data: [],
			"message": error.message
		};
	}
};