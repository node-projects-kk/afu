const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["Active", "Inactive"];

const languageSchema = new Schema({
  title: { type: String, default: '' },
  code: { type: String, default: '' },
  sort_order : { type: Number, default: 0 },
  isDefault: { type: Boolean, default:true,enum:[true,false] },
  isDeleted: {type: Boolean, default: false, enum: [true, false]},
  status: { type: String, default: "Active", enum: status }
},{versionKey: false});

// For pagination
languageSchema.plugin(mongooseAggregatePaginate);

// create the model for language and expose it to our app
module.exports = mongoose.model('language', languageSchema);