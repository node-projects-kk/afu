const mongoose = require('mongoose');
const Language = require('language/models/language.model');
const perPage = config.PAGINATION_PERPAGE;

const languageRepository = {

	getAll: async (req) => {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
		
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({
					$or: [
						{ 'title': { $regex: req.body.query.generalSearch, $options: 'i' } },
						{ 'code': { $regex: req.body.query.generalSearch, $options: 'i' } },
					]
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
		
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
		
			var aggregate =  Language.aggregate([
								{ $match: conditions },
								sortOperator
							]);
		
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allLanguage = await Language.aggregatePaginate(aggregate, options);
		
			return allLanguage;
		}
		catch (e) {
			throw (e);
		}
	},

    async getAllLanguage(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Language.find(_params).sort({"sort_order":1}).lean().exec();
        } catch (error) {
            return error;
        }
    },

	save: async (data) => {
		try {
			let language = await Language.create(data);
			if (!language) {
				return null;
			}
			return language;  
		}
		catch (e){
			return e;  
		}     
	},
   
    getById: async (id) => {
        let language = await Language.findById(id).exec();
        try {
            if (!language) {
                return null;
            }
            return language;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let language = await Language.findOne(params).exec();
        try {
            if (!language) {
                return null;
            }
            return language;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let language = await Language.find(params).exec();
        try {
            if (!language) {
                return null;
            }
            return language;

        } catch (e) {
            return e;
        }
    },

    getLanguageCount: async (params) => {
        try {
            let languageCount = await Language.countDocuments(params);
            if (!languageCount) {
                return null;
            }
            return languageCount;
        } catch (e) {
            return e;
        }

    },




    delete: async (id) => {
        try {
            let language = await Language.findById(id);
            if (language) {
                let languageDelete = await Language.remove({ _id: id }).exec();
                if (!languageDelete) {
                    return null;
                }
                return languageDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let language = await Language.findByIdAndUpdate(id, data, { new: true}).exec();
            if (!language) {
                return null;
            }
            return language;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },






};

module.exports = languageRepository;