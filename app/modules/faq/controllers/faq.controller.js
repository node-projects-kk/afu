const faqRepo = require('faq/repositories/faq.repository');
const educationRepo = require('education_level/repositories/education_level.repository');
const universityRepo = require('university/repositories/university.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class faqController {
	constructor() {
		this.faq = [];
	}

	async create(req, res) {
		try {
			let educationlist = await educationRepo.getActiveEducationLevel({"isDeleted":false,"status":"Active"});
			let universitylist = await universityRepo.getActiveUniversity({"isDeleted":false,"status":"Active"});
			
			res.render('faq/views/create.ejs', {
				page_name: 'faq-list',
				page_title: 'Create Faq',
				user: req.user,
				education:educationlist,
				university:universitylist
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let faq =  await faqRepo.getByField({ 'title': { $regex: req.body.title, $options: 'i' } ,'isDeleted':false});
						
			if (_.isEmpty(faq)) {
				let faqSave = await faqRepo.save(req.body); 
				if(faqSave){
					req.flash('success', "Faq created successfully.");
					res.redirect(namedRouter.urlFor('admin.faq.list'));
				}    
			}
			else {
				req.flash('error', "This faq already exist!");
				res.redirect(namedRouter.urlFor('admin.faq.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  faq update page
    */
	async edit (req, res){
		try{
			let result = {};
			let faq = await faqRepo.getById(req.params.id);
			if (!_.isEmpty(faq)) {
				result.faq_data = faq;
				res.render('faq/views/edit.ejs', {
					page_name: 'faq-management',
					page_title: 'Edit Faq',
					user: req.user,
					response: result
				});
			}
			else {
				req.flash('error', "Sorry faq not found!");
				res.redirect(namedRouter.urlFor('admin.faq.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: update
    // @Description: faq update action
    */
    async update (req, res){
        try {
            const faqId = req.body.faq_id;
          
           	let faq =  await faqRepo.getByField({ 'title': { $regex: req.body.title, $options: 'i' } ,_id:{$ne:faqId}, 'isDeleted':false});
            if (_.isEmpty(faq)) {
                    let faqUpdate = await faqRepo.updateById(req.body,faqId)
                    if(faqUpdate) {
                        req.flash('success', "Faq updated successfully");
                        res.redirect(namedRouter.urlFor('admin.faq.list'));
                    }
                    
                }else{
                req.flash('error', "Faq is already available!");
                res.redirect(namedRouter.urlFor('admin.faq.edit', { id: faqId }));
            }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the faqs from DB
    */
    async list (req, res){
            try
            {
                res.render('faq/views/list.ejs', {
                    page_name: 'faq-list',
                    page_title: 'Faq List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let faq = await faqRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": faq.pageCount, "perpage": req.body.pagination.perpage, "total": faq.totalCount, "sort": sortOrder, "field": sortField};
			
			return {status: 200, meta: meta, data:faq.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: faq status change action
    */
	async changeStatus (req, res){
		try {			
			let faq = await faqRepo.getById(req.params.id);
			
			if(!_.isEmpty(faq)){
				let faqStatus = (faq.status == "Active") ? "Inactive" : "Active";
				let faqUpdate= await faqRepo.updateById({"status": faqStatus }, req.params.id);
				req.flash('success', "Faq status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.faq.list'));
			}
			else {
				req.flash('error', "Sorry faq not found");
				res.redirect(namedRouter.urlFor('admin.faq.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: faq delete
    */
    async destroy (req, res){
	
	try {
		const faqDelete = await faqRepo.updateById({'isDeleted': true}, req.params.id);
		if(!_.isEmpty(faqDelete)){
			req.flash('success','Faq removed successfully');
			res.redirect(namedRouter.urlFor('admin.faq.list'));
		} 
	}
	catch (error) {
		return res.status(500).send({message: e.message});  
	}
    };

}

module.exports = new faqController();