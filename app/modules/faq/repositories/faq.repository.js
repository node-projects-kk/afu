const Faq = require('faq/models/faq.model');
const perPage = config.PAGINATION_PERPAGE;

class FaqRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
								
				and_clauses.push({
					$or:
					[
						{ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'content': { $regex:  req.body.query.generalSearch, $options: 'i'} }
					]
				});
			}
			
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
			
				
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  Faq.aggregate([
				{
					$project:{
						_id :"$_id",
						title:"$title",
						content:"$content",
						status:"$status",
						isDeleted:"$isDeleted",
						createdAt:"$createdAt",
						updatedAt:"$updatedAt"
					}
				},
				{ $match: conditions },
				sortOperator
			]);
	
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allFaq = await Faq.aggregatePaginate(aggregate, options);
			return allFaq;
		}
		catch (e) {
			throw (e);
		}
	}
    
    
    
    async getAllFaq(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Faq.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await Faq.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Faq.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Faq.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Faq.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getFaqCount() {
        try {
            return await Faq.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new Faq(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActiveFaq(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Faq.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Faq.findById(id).lean().exec();
            return await Faq.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new FaqRepository();