const Support = require('support/models/support.model');
const perPage = config.PAGINATION_PERPAGE;

class SupportRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
								
				and_clauses.push({
					$or:
					[
						{ 'email': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'message': { $regex:  req.body.query.generalSearch, $options: 'i'} }
					]
				});
			}
			
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
			
				
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  Support.aggregate([
                {
                    "$lookup": {
                        "from": "users",
                        "localField": "user_id",
                        "foreignField": "_id",
                        "as": "users"
                    },
                },
				{ 
					$unwind: {
						path: "$users",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$project:{
                        _id :"$_id",
                        user_name: "$users.full_name",
						email:"$email",
						message:"$message",
						status:"$status",
						isDeleted:"$isDeleted",
						createdAt:"$createdAt",
						updatedAt:"$updatedAt"
					}
				},
				{ $match: conditions },
				sortOperator
			]);
	
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allSupport = await Support.aggregatePaginate(aggregate, options);
			return allSupport;
		}
		catch (e) {
			throw (e);
		}
	}
    
    
    
    async getAllSupport(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Support.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await Support.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Support.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Support.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Support.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getSupportCount() {
        try {
            return await Support.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new Support(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActiveSupport(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Support.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Support.findById(id).lean().exec();
            return await Support.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new SupportRepository();