const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const status = ["Active", "Inactive"];

const SupportSchema = new Schema({
  user_id : { type: Schema.Types.ObjectId, ref: 'User'},
  email : { type: String, default: '' },
  message : { type: String, default: '' },
  status: { type: String, default: "Active", enum: status },
  isDeleted: { type: Boolean, default: false, enum: deleted },
},{timestamps:true, versionKey: false});

// For pagination
SupportSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('support', SupportSchema);