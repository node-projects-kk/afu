const supportRepo = require('support/repositories/support.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class bookedDemoController {
	constructor() {
		this.support = [];
	}

	
    /* @Method: list
    // @Description: To get all the supports from DB
    */
    async list (req, res){
            try
            {
                res.render('support/views/list.ejs', {
                    page_name: 'support-list',
                    page_title: 'Support Query List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let support = await supportRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": support.pageCount, "perpage": req.body.pagination.perpage, "total": support.totalCount, "sort": sortOrder, "field": sortField};
			
			return {status: 200, meta: meta, data:support.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    
}

module.exports = new bookedDemoController();