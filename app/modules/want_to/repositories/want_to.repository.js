const WantTo = require('want_to/models/want_to.model');
const perPage = config.PAGINATION_PERPAGE;

class WantToRepository {
    constructor() {}

    async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
		
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({ 'name': { $regex: req.body.query.generalSearch, $options: 'i' }});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
		
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
		
			var aggregate =  WantTo.aggregate([
								{ $match: conditions },
								sortOperator
							]);
		
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allWantTo = await WantTo.aggregatePaginate(aggregate, options);
		
			return allWantTo;
		}
		catch (e) {
			throw (e);
		}
	}
    
    
    async getAllWantTo(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await WantTo.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await WantTo.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await WantTo.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await WantTo.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await WantTo.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getWantToCount() {
        try {
            return await WantTo.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new WantTo(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActiveWantTo(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await WantTo.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await WantTo.findById(id).lean().exec();
            return await WantTo.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new WantToRepository();