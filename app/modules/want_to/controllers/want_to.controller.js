const want_toRepo = require('want_to/repositories/want_to.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class want_toController {
	constructor() {
		this.want_to = [];
	}

	async create(req, res) {
		try {
			res.render('want_to/views/create.ejs', {
				page_name: 'want_to-list',
				page_title: 'Create Want To Section',
				user: req.user,
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let want_to = await want_toRepo.getByField({ 'name': req.body.level_name, 'isDeleted': false });
			
			if (_.isEmpty(want_to)) {
				let want_toSave = await want_toRepo.save(req.body); 
				if(want_toSave){
					req.flash('success', "Want to section created successfully.");
					res.redirect(namedRouter.urlFor('admin.want_to.list'));
				}    
			}
			else {
				req.flash('error', "This want to already exist!");
				res.redirect(namedRouter.urlFor('admin.want_to.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  want_to update page
    */
    async edit (req, res){
        try
        {
            let result = {};
            let want_to = await want_toRepo.getById(req.params.id);
            if (!_.isEmpty(want_to)) {
                result.want_to_data = want_to;
                res.render('want_to/views/edit.ejs', {
                    page_name: 'want_to-management',
                    page_title: 'Edit Want To Section',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry want to not found!");
                res.redirect(namedRouter.urlFor('admin.want_to.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: want_to update action
    */
    async update (req, res){
        try {
            const want_toId = req.body.want_to_id;
            let want_to = await want_toRepo.getByField({'name':req.body.level_name,_id:{$ne:want_toId}});
           
            if (_.isEmpty(want_to)) {
                    let want_toUpdate = await want_toRepo.updateById(req.body,want_toId)
                    if(want_toUpdate) {
                        req.flash('success', "Want to section updated successfully");
                        res.redirect(namedRouter.urlFor('admin.want_to.list'));
                    }
                    
                }else{
                req.flash('error', "Want to section is already available!");
                res.redirect(namedRouter.urlFor('admin.want_to.edit', { id: want_toId }));
            }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the want_tos from DB
    */
    async list (req, res){
            try
            {
                res.render('want_to/views/list.ejs', {
                    page_name: 'want_to-list',
                    page_title: 'Want To Section List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let want_to = await want_toRepo.getAll(req);
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {
				"page": req.body.pagination.page,
				"pages": want_to.pageCount,
				"perpage": req.body.pagination.perpage,
				"total": want_to.totalCount,
				"sort": sortOrder,
				"field": sortField
			};
			return {status: 200, meta: meta, data:want_to.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: want_to status change action
    */
	async changeStatus (req, res){
		try {
			let want_to = await want_toRepo.getById(req.params.id);
			if(!_.isEmpty(want_to)){
				let want_toStatus = (want_to.status == "Active") ? "Inactive" : "Active";
				let want_toUpdate= await want_toRepo.updateById({"status": want_toStatus }, req.params.id);
				req.flash('success', "Want to section status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.want_to.list'));
			}
			else {
				req.flash('error', "Sorry want to not found");
				res.redirect(namedRouter.urlFor('admin.want_to.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: want_to delete
    */
    async destroy (req, res){
        try{
            let want_toDelete = await want_toRepo.delete(req.params.id)
            if(!_.isEmpty(want_toDelete)){
                req.flash('success','Want to section removed successfully');
                res.redirect(namedRouter.urlFor('admin.want_to.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };

}

module.exports = new want_toController();