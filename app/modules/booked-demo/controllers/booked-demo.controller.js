const bookedDemoRepo = require('booked-demo/repositories/booked-demo.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class bookedDemoController {
	constructor() {
		this.faq = [];
	}

	
    /* @Method: list
    // @Description: To get all the faqs from DB
    */
    async list (req, res){
            try
            {
                res.render('booked-demo/views/list.ejs', {
                    page_name: 'booked-demo-list',
                    page_title: 'Booked Demo List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let faq = await bookedDemoRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": faq.pageCount, "perpage": req.body.pagination.perpage, "total": faq.totalCount, "sort": sortOrder, "field": sortField};
			
			return {status: 200, meta: meta, data:faq.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}    
}

module.exports = new bookedDemoController();