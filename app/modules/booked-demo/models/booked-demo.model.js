const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const status = ["Active", "Inactive"];

const BookedDemoSchema = new Schema({
  full_name : { type: String, default: '' },
  institution : { type: String, default: '' },
  job_title : { type: String, default: '' },
  email : { type: String, default: '' },
  phone : { type: String, default: '' },
  message : { type: String, default: '' },
  status: { type: String, default: "Active", enum: status },
  isDeleted: { type: Boolean, default: false, enum: deleted },
},{timestamps:true});

// For pagination
BookedDemoSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('booked-demo', BookedDemoSchema);