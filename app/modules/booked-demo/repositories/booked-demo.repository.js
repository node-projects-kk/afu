const BookedDemo = require('booked-demo/models/booked-demo.model');
const perPage = config.PAGINATION_PERPAGE;

class BookedDemoRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
								
				and_clauses.push({
					$or:
					[
						{ 'title': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'content': { $regex:  req.body.query.generalSearch, $options: 'i'} }
					]
				});
			}
			
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
			
				
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  BookedDemo.aggregate([
				{
					$project:{
						_id :"$_id",
						full_name:"$full_name",
                        institution:"$institution",
                        job_title : "$job_title",
                        email : "$email",
                        phone : "$phone",
                        message : "$message",
						status:"$status",
						isDeleted:"$isDeleted",
						createdAt:"$createdAt",
						updatedAt:"$updatedAt"
					}
				},
				{ $match: conditions },
				sortOperator
			]);
	
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allBookedDemo = await BookedDemo.aggregatePaginate(aggregate, options);
			return allBookedDemo;
		}
		catch (e) {
			throw (e);
		}
	}    
    
    
    async getAllBookedDemo(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await BookedDemo.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await BookedDemo.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await BookedDemo.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await BookedDemo.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await BookedDemo.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getBookedDemoCount() {
        try {
            return await BookedDemo.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new BookedDemo(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActiveBookedDemo(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await BookedDemo.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await BookedDemo.findById(id).lean().exec();
            return await BookedDemo.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new BookedDemoRepository();