const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const status = ["Active", "Inactive"];

const UniversitySchema = new Schema({
	user_id: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	university_name: {
		type: String,
		default: ''
	},
	address: {
		type: String,
		default: ''
	},
	city: {
		type: String,
		default: ''
	},
	state: {
		type: String,
		default: ''
	},
	country: {
		type: Schema.Types.ObjectId,
		ref: 'Country'
	},
	postal_code: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: ''
	},
	phone: {
		type: String,
		default: ''
	},
	website: {
		type: String,
		default: ''
	},
	registration_no: {
		type: String,
		default: ''
	},
	status: {
		type: String,
		default: "Active",
		enum: status
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: deleted
	},
}, {
	timestamps: true,
	versionKey: false
});

// For pagination
UniversitySchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('University', UniversitySchema);