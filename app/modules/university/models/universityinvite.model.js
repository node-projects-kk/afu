const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const linksend = ["Yes","No"];
const UniversityInviteSchema = new Schema({
	university_id : { type: Schema.Types.ObjectId,ref: 'University' },
	first_name : { type: String, default: '' },
	last_name : { type: String, default: '' },
	email : { type: String, default: '' },
	isLinkSend: { type: String, default: "No", enum: linksend },
	isDeleted: { type: Boolean, default: false, enum: deleted },
},{timestamps:true});

// For pagination
UniversityInviteSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('UniversityInvite', UniversityInviteSchema);