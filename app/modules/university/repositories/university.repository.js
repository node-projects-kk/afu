const University = require('university/models/university.model');
const perPage = config.PAGINATION_PERPAGE;

class UniversityRepository {
    constructor() {}

    async getAll(req) {
        try {
            var conditions = {};
            var and_clauses = [];
            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
                //and_clauses.push({ 'university_name': { $regex: req.body.query.generalSearch, $options: 'i' } });

                and_clauses.push({
                    $or: [{
                            'university_name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'registration_no': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'city': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        },
                        {
                            'phone': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                    ]
                });


            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }
                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = University.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);

            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allUniversity = await University.aggregatePaginate(aggregate, options);
            return allUniversity;
        } catch (e) {
            throw (e);
        }
    }



    async getAllUniversity(params) {
        try {
            const _params = {
                ...params,
                "isDeleted": false,
            };
            return await University.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getById(id) {
        try {
            return await University.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {
                ...params,
                "isDeleted": false,
            };
            return await University.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {
                ...params,
                "isDeleted": false,
            };
            return await University.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await University.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            console.log('university repo error', error.message);
            return error;
        }
    }

    async getUniversityByField(data) {
        try {
            let university = await University.findOne(data).exec();
            if (!university) {
                return null;
            }
            return university;
        } catch (e) {
            return e;
        }
    }

    async getUniversityCount() {
        try {
            return await University.count({
                "isDeleted": false,
                "status": "Active"
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new University(data);
            return await _save.save();
        } catch (error) {
            console.log('repo', error.message);
            return error;
        }
    }

    async getActiveUniversity(params) {
        try {
            const _params = {
                ...params,
                "isDeleted": false,
            };
            return await University.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await University.findById(id).lean().exec();
            return await University.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new UniversityRepository();