const UniversityInvite = require('university/models/universityinvite.model');
const perPage = config.PAGINATION_PERPAGE;

class UniversityInviteRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({ 'universityinviteinvite_name': { $regex: req.body.query.generalSearch, $options: 'i' } });
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
	
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  UniversityInvite.aggregate([
								{ $match: conditions },
								sortOperator
							]);
	
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allUniversityInvite = await UniversityInvite.aggregatePaginate(aggregate, options);
			return allUniversityInvite;
		}
		catch (e) {
			throw (e);
		}
	}
    
    
    
    async getAllUniversityInvite(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await UniversityInvite.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await UniversityInvite.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await UniversityInvite.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await UniversityInvite.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await UniversityInvite.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getUniversityInviteCount() {
        try {
            return await UniversityInvite.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new UniversityInvite(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActiveUniversityInvite(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await UniversityInvite.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await UniversityInvite.findById(id).lean().exec();
            return await UniversityInvite.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new UniversityInviteRepository();