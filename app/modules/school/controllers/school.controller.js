const schoolRepo = require('school/repositories/school.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class schoolController {
	constructor() {
		this.school = [];
	}

	async create(req, res) {
		try {
			res.render('school/views/create.ejs', {
				page_name: 'school-list',
				page_title: 'Create School',
				user: req.user,
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let school =  await schoolRepo.getByField({ 'school_name': { $regex: req.body.school_name, $options: 'i' } ,'isDeleted':false});
			//let school = await schoolRepo.getByField({ 'school_name': req.body.title, 'isDeleted': false });
			//console.log("29>>",school); process.exit();
			
			if (_.isEmpty(school)) {
				let schoolSave = await schoolRepo.save(req.body); 
				if(schoolSave){
					req.flash('success', "School created successfully.");
					res.redirect(namedRouter.urlFor('admin.school.list'));
				}    
			}
			else {
				req.flash('error', "This school already exist!");
				res.redirect(namedRouter.urlFor('admin.school.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  school update page
    */
    async edit (req, res){
        try
        {
            let result = {};
            let school = await schoolRepo.getById(req.params.id);
            if (!_.isEmpty(school)) {
                result.school_data = school;
                res.render('school/views/edit.ejs', {
                    page_name: 'school-management',
                    page_title: 'Edit School',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry school not found!");
                res.redirect(namedRouter.urlFor('admin.school.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: school update action
    */
    async update (req, res){
        try {
            const schoolId = req.body.school_id;
          //  let school = await schoolRepo.getByField({'school_name':req.body.school_name,_id:{$ne:schoolId}});
           let school =  await schoolRepo.getByField({ 'school_name': { $regex: req.body.school_name, $options: 'i' } ,_id:{$ne:schoolId}});
            if (_.isEmpty(school)) {
                    let schoolUpdate = await schoolRepo.updateById(req.body,schoolId)
                    if(schoolUpdate) {
                        req.flash('success', "School updated successfully");
                        res.redirect(namedRouter.urlFor('admin.school.list'));
                    }
                    
                }else{
                req.flash('error', "School is already available!");
                res.redirect(namedRouter.urlFor('admin.school.edit', { id: schoolId }));
            }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the schools from DB
    */
    async list (req, res){
            try
            {
                res.render('school/views/list.ejs', {
                    page_name: 'school-list',
                    page_title: 'School List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let school = await schoolRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": school.pageCount, "perpage": req.body.pagination.perpage, "total": school.totalCount, "sort": sortOrder, "field": sortField};
			return {status: 200, meta: meta, data:school.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: school status change action
    */
	async changeStatus (req, res){
		try {
			//console.log("147>>",req.params.id);
			let school = await schoolRepo.getById(req.params.id);
			//console.log("149>>",school); process.exit();
			if(!_.isEmpty(school)){
				let schoolStatus = (school.status == "Active") ? "Inactive" : "Active";
				let schoolUpdate= await schoolRepo.updateById({"status": schoolStatus }, req.params.id);
				req.flash('success', "School status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.school.list'));
			}
			else {
				req.flash('error', "Sorry school not found");
				res.redirect(namedRouter.urlFor('admin.school.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: school delete
    */
    async destroy (req, res){
        try{
            let schoolDelete = await schoolRepo.delete(req.params.id)
            if(!_.isEmpty(schoolDelete)){
                req.flash('success','School removed successfully');
                res.redirect(namedRouter.urlFor('admin.school.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };

}

module.exports = new schoolController();