const mongoose = require('mongoose');
const Report = require('report/models/report.model');
const perPage = config.PAGINATION_PERPAGE;

const reportRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({ "isDeleted": false });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
               
                and_clauses.push({
                    $or: [
                        { 'title': { $regex: req.body.query.generalSearch, $options: 'i' } },
                        { 'description': { $regex: req.body.query.generalSearch, $options: 'i' } },
                        { 'user.first_name': { $regex: req.body.query.generalSearch, $options: 'i' } },
                        { 'user.last_name': { $regex: req.body.query.generalSearch, $options: 'i' } },
                        { 'name': { $regex: req.body.query.generalSearch, $options: 'i' } }
                        
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({ "status": req.body.query.Status });
            }
            conditions['$and'] = and_clauses;

            var sortOperator = { "$sort": {} };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate =  Report.aggregate([
                {
                    $lookup:{
                    "from": "users",
                    "localField": "user_id",
                    "foreignField": "_id",
                    "as": "user"
                    }
                },
                { "$unwind": "$user" },  
                {$project:{
                    title:1,
                    description:1,
                    status:1,
                    isDeleted:1,
                    createdAt:1,
                    updatedAt:1,
                    user:"$user",
                    name: { $concat : [ "$user.first_name", " ", "$user.last_name" ] }}
                },
                { $match: conditions },
                sortOperator
            ]);
       
            var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
            let allReport = await Report.aggregatePaginate(aggregate, options);
     
            return allReport;
        } catch (e) {
            throw (e);
        }
    },

    getById: async (id) => {
        let report = await Report.findById(id).exec();
        try {
            if (!report) {
                return null;
            }
            return report;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let report = await Report.findOne(params).exec();
        try {
            if (!report) {
                return null;
            }
            return report;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let report = await Report.find(params).exec();
        try {
            if (!report) {
                return null;
            }
            return report;

        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let report = await Report.findById(id);
            if (report) {
                let reportDelete = await Report.remove({ _id: id }).exec();
                if (!reportDelete) {
                    return null;
                }
                return reportDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let report = await Report.findByIdAndUpdate(id, data, { new: true}).exec();
            if (!report) {
                return null;
            }
            return report;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },

    save: async (data) => {
        try {
            let report = await Report.create(data);
             if (!report) {
                return null;
             }
             return report;  
          }
          catch (e){
             return e;  
          }     
     }

};

module.exports = reportRepository;