const reportRepo = require('report/repositories/report.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const moment = require('moment');



class reportController {
    constructor() {
        this.report = [];
        
    }

    /* @Method: list
    // @Description: To get all the reports from DB
    */
    async list (req, res){
            try
            {
                res.render('report/views/list.ejs', {
                    page_name: 'report-management',
                    page_title: 'Report List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

    async getAll (req, res){
        try{
            let report = await reportRepo.getAll(req);
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }
            let meta = {"page": req.body.pagination.page, "pages": report.pageCount, "perpage": req.body.pagination.perpage, "total": report.totalCount, "sort": sortOrder, "field": sortField};
            return {status: 200, meta: meta, data:report.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }

    /*
    // @Method: edit
    // @Description:  report update page
    */
    async edit (req, res){
        try
        {
            let result = {};
            let report = await reportRepo.getById(req.params.id);
            if (!_.isEmpty(report)) {
                result.report_data = report;
                res.render('report/views/edit.ejs', {
                    page_name: 'report-management',
                    page_title: 'Edit Report',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry report not found!");
                res.redirect(namedRouter.urlFor('admin.report.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: report update action
    */
    async update (req, res){
        try {
            const reportId = req.body.report_id;
            let updateObj = {};
            updateObj.title = req.body.title;
            updateObj.description = req.body.description;
            updateObj.status = req.body.status;
            updateObj.updatedAt = moment();
            let reportUpdate = await reportRepo.updateById(updateObj,reportId)
            if(reportUpdate) {
                req.flash('success', "Report Updated Successfully");
                res.redirect(namedRouter.urlFor('admin.report.list'));
            }
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };
    /* @Method: delete
    // @Description: report delete
    */
    async destroy (req, res){
        try{
            let reportDelete = await reportRepo.delete(req.params.id)
            if(!_.isEmpty(reportDelete)){
                req.flash('success','Report Removed Successfully');
                res.redirect(namedRouter.urlFor('admin.report.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };

}

module.exports = new reportController();