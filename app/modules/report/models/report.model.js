const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const status = ["New", "Open","Solved"];

const ReportSchema = new Schema({
  user_id: {type: Schema.Types.ObjectId,ref: 'User'},
  title: { type: String, default: '' },
  description: { type: String, default: '' },
  status: { type: String, default: "Active", enum: status },
  isDeleted: {type: Boolean, default: false, enum: [true, false]},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date, default: Date.now}
});

// For pagination
ReportSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Report', ReportSchema);