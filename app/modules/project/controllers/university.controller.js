const universityRepo = require('university/repositories/university.repository');
const universityInviteRepo = require('university/repositories/universityinvite.repository');
const countryRepo = require('country/repositories/country.repository');
const settingRepo = require('setting/repositories/setting.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class universityController {
	constructor() {
		this.university = [];
	}

	async create(req, res) {
		try {
			let countrylist = await countryRepo.getActiveCountry({"isDeleted":false});
			//console.log("16>>",countrylist);
			res.render('university/views/create.ejs', {
				page_name: 'university-list',
				page_title: 'Create University',
				user: req.user,
				country:countrylist
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let university =  await universityRepo.getByField({ 'university_name': { $regex: req.body.university_name, $options: 'i' } ,'isDeleted':false});
			//let university = await universityRepo.getByField({ 'university_name': req.body.title, 'isDeleted': false });
			//console.log("29>>",university); process.exit();
			
			if (_.isEmpty(university)) {
				let universitySave = await universityRepo.save(req.body); 
				if(universitySave){
					req.flash('success', "University created successfully.");
					res.redirect(namedRouter.urlFor('admin.university.list'));
				}    
			}
			else {
				req.flash('error', "This university already exist!");
				res.redirect(namedRouter.urlFor('admin.university.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  university update page
    */
    async edit (req, res){
        try
        {
            let result = {};
	    let countrylist = await countryRepo.getActiveCountry({"isDeleted":false});
            let university = await universityRepo.getById(req.params.id);
            if (!_.isEmpty(university)) {
                result.university_data = university;
                res.render('university/views/edit.ejs', {
                    page_name: 'university-management',
                    page_title: 'Edit University',
                    user: req.user,
                    response: result,
		    country:countrylist
                });
            } else {
                req.flash('error', "Sorry university not found!");
                res.redirect(namedRouter.urlFor('admin.university.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: university update action
    */
    async update (req, res){
        try {
            const universityId = req.body.university_id;
          //  let university = await universityRepo.getByField({'university_name':req.body.university_name,_id:{$ne:universityId}});
           let university =  await universityRepo.getByField({ 'university_name': { $regex: req.body.university_name, $options: 'i' } ,_id:{$ne:universityId}});
            if (_.isEmpty(university)) {
                    let universityUpdate = await universityRepo.updateById(req.body,universityId)
                    if(universityUpdate) {
                        req.flash('success', "University updated successfully");
                        res.redirect(namedRouter.urlFor('admin.university.list'));
                    }
                    
                }else{
                req.flash('error', "University is already available!");
                res.redirect(namedRouter.urlFor('admin.university.edit', { id: universityId }));
            }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the universitys from DB
    */
    async list (req, res){
            try
            {
                res.render('university/views/list.ejs', {
                    page_name: 'university-list',
                    page_title: 'University List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let university = await universityRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": university.pageCount, "perpage": req.body.pagination.perpage, "total": university.totalCount, "sort": sortOrder, "field": sortField};
			return {status: 200, meta: meta, data:university.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: university status change action
    */
	async changeStatus (req, res){
		try {
			//console.log("147>>",req.params.id);
			let university = await universityRepo.getById(req.params.id);
			//console.log("149>>",university); process.exit();
			if(!_.isEmpty(university)){
				let universityStatus = (university.status == "Active") ? "Inactive" : "Active";
				let universityUpdate= await universityRepo.updateById({"status": universityStatus }, req.params.id);
				req.flash('success', "University status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.university.list'));
			}
			else {
				req.flash('error', "Sorry university not found");
				res.redirect(namedRouter.urlFor('admin.university.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: university delete
    */
    async destroy (req, res){
       try {
			const universityDelete = await universityRepo.updateById({'isDeleted': true}, req.params.id);
			if(!_.isEmpty(universityDelete)){
				req.flash('success','University removed successfully');
				res.redirect(namedRouter.urlFor('admin.university.list'));
			} 
		}
		catch (error) {
			return res.status(500).send({message: e.message});  
		}
    };
    
    async invite(req, res) {
		try {
			let universitylist = await universityRepo.getActiveUniversity({"isDeleted":false});
			//console.log("16>>",countrylist);
			res.render('university/views/invite.ejs', {
				page_name: 'invite-university',
				page_title: 'Invite University',
				user: req.user,
				university:universitylist
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}
	
	async inviteSend(req, res) {
		try { 
			const customUrl = `${req.protocol}://${req.headers.host}/api/usermail/email_verify`;
			const setting_data = await settingRepo.getAllByField({"isDeleted":false});
			
			var settingObj = {};
			if (!_.isEmpty(setting_data)) {
				setting_data.forEach(function(element) {
					settingObj[element.setting_name.replace(/\s+/g,"_")] = element.setting_value;
				});
			}
			let existUniversity = await universityInviteRepo.getAllUniversityInvite({"email":req.body.email,"university_id":req.body.university,"isLinkSend":"Yes"});
			if (!_.isEmpty(existUniversity)) {
				req.flash('error', "The user has already send the invitation.");
				res.redirect(namedRouter.urlFor('admin.university.invite'));
			}
			else{
				let universityval = await universityRepo.getById(req.body.university);
				let locals = { first_name:req.body.first_name,university_name:universityval.university_name,invitation_link: customUrl};
				let isMailSend = await mailer.sendMail('Admin<belasmith00@gmail.com>',req.body.email,settingObj.Site_Title+' Invitation Send', 'invite-send', locals);
			
				if(isMailSend){
					req.body.university_id = req.body.university;
					req.body.isLinkSend = "Yes";
					let universityInviteSave = await universityInviteRepo.save(req.body); 
					req.flash('success', "Invitation send successfully.");
					res.redirect(namedRouter.urlFor('admin.university.invite'));
				}
				else{
					req.flash('error', "Mail send failed!");
					res.redirect(namedRouter.urlFor('admin.university.invite'));
				}
			}
		}
		catch(e) {
			console.log("238>>", e);
			return res.status(500).send({message: e.message});  
		}
	}

}

module.exports = new universityController();