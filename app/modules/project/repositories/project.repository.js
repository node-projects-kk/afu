const Project = require('project/models/project.model');
const perPage = config.PAGINATION_PERPAGE;

class ProjectRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				//and_clauses.push({ 'project_name': { $regex: req.body.query.generalSearch, $options: 'i' } });
				and_clauses.push({
					$or:
					[
						{ 'project_name': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'registration_no': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'city': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'phone': { $regex:  req.body.query.generalSearch, $options: 'i'} }
					]
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
	
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  Project.aggregate([
								{ $match: conditions },
								sortOperator
							]);
	
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allProject = await Project.aggregatePaginate(aggregate, options);
			return allProject;
		}
		catch (e) {
			throw (e);
		}
	}
    
	async getAllProject(params) {
		try {
			const _params = {...params,"isDeleted": false,};
			return await Project.find(_params).populate('user_id',['full_name','email']).lean().exec();
		}
		catch (error) {
			return error;
		}
	}
   
	async getById(id) {
		try {
			return await Project.findById(id).lean().exec();
		}
		catch (error) {
			return error;
		}
	}

	async getByField(params) {
		try {
			const _params = {...params,"isDeleted": false,};
			return await Project.findOne(_params).lean().exec();
		}
		catch (error) {
			return error;
		}
	}

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Project.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

	async updateById(data, id) {
		try {
			return await Project.findByIdAndUpdate(id, data, {new: true,upsert: true}).lean().exec();
		}
		catch (error) {
			return error;
		}
	}
	
	async  updateByField(field, fieldValue, data) {
		try {
			let user = await User.findByIdAndUpdate(fieldValue, field, { new: true});
			if (!user) {
				return null;
			}
			return user;
		}
		catch(e) {
			return e;
		}
	}

    async getProjectCount() {
        try {
            return await Project.count({"isDeleted": false,"status":"Active"});
        }
	catch (error) {
            return error;
        }
    }

	async save(data) {
		try {
			const _save = new Project(data);
			return await _save.save();
		}
		catch (error) {
			return error;
		}
	}

    async getActiveProject(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Project.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

	async delete(id) {
		try {
			await Project.findById(id).lean().exec();
			return await Project.deleteOne({_id: id}).lean().exec();
		}
		catch (error) {
			return error;
		}
	}
}

module.exports = new ProjectRepository();