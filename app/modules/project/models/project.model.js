const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const status = ["Active", "Inactive"];

const ProjectSchema = new Schema({
	project_name : { type: String, default: '' },
	project_url:{ type: String, default: '' },
	project_details:{ type: String, default: ''},
	user_id:{type: Schema.Types.ObjectId,ref: 'User'},
	status: { type: String, default: "Active", enum: status },
	isDeleted: { type: Boolean, default: false, enum: deleted },
},{timestamps:true});

// For pagination
ProjectSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Project', ProjectSchema);