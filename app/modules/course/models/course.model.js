const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const status = ["Active", "Inactive"];

const CourseSchema = new Schema({
  course_name : { type: String, default: '' },
  education : { type: Schema.Types.ObjectId,ref: 'Education_Level' },
  university : { type: Schema.Types.ObjectId,ref: 'University' },
  status: { type: String, default: "Active", enum: status },
  isDeleted: { type: Boolean, default: false, enum: deleted },
},{timestamps:true});

// For pagination
CourseSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('Course', CourseSchema);