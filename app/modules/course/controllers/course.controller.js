const courseRepo = require('course/repositories/course.repository');
const educationRepo = require('education_level/repositories/education_level.repository');
const universityRepo = require('university/repositories/university.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);

class courseController {
	constructor() {
		this.course = [];
	}

	async create(req, res) {
		try {
			let educationlist = await educationRepo.getActiveEducationLevel({"isDeleted":false,"status":"Active"});
			let universitylist = await universityRepo.getActiveUniversity({"isDeleted":false,"status":"Active"});
			
			res.render('course/views/create.ejs', {
				page_name: 'course-list',
				page_title: 'Create Course',
				user: req.user,
				education:educationlist,
				university:universitylist
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let course =  await courseRepo.getByField({ 'course_name': { $regex: req.body.course_name, $options: 'i' } ,'isDeleted':false});
			//let course = await courseRepo.getByField({ 'course_name': req.body.title, 'isDeleted': false });
			//console.log("29>>",course); process.exit();
			
			if (_.isEmpty(course)) {
				let courseSave = await courseRepo.save(req.body); 
				if(courseSave){
					req.flash('success', "Course created successfully.");
					res.redirect(namedRouter.urlFor('admin.course.list'));
				}    
			}
			else {
				req.flash('error', "This course already exist!");
				res.redirect(namedRouter.urlFor('admin.course.list'));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

    
    /*
    // @Method: edit
    // @Description:  course update page
    */
	async edit (req, res){
		try{
			let result = {};
			let educationlist = await educationRepo.getActiveEducationLevel({"isDeleted":false,"status":"Active"});
			let universitylist = await universityRepo.getActiveUniversity({"isDeleted":false,"status":"Active"});
			let course = await courseRepo.getById(req.params.id);
			if (!_.isEmpty(course)) {
				result.course_data = course;
				res.render('course/views/edit.ejs', {
					page_name: 'course-management',
					page_title: 'Edit Course',
					user: req.user,
					response: result,
					education:educationlist,
					university:universitylist
				});
			}
			else {
				req.flash('error', "Sorry course not found!");
				res.redirect(namedRouter.urlFor('admin.course.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: update
    // @Description: course update action
    */
    async update (req, res){
        try {
            const courseId = req.body.course_id;
          //  let course = await courseRepo.getByField({'course_name':req.body.course_name,_id:{$ne:courseId}});
           let course =  await courseRepo.getByField({ 'course_name': { $regex: req.body.course_name, $options: 'i' } ,_id:{$ne:courseId}});
            if (_.isEmpty(course)) {
                    let courseUpdate = await courseRepo.updateById(req.body,courseId)
                    if(courseUpdate) {
                        req.flash('success', "Course updated successfully");
                        res.redirect(namedRouter.urlFor('admin.course.list'));
                    }
                    
                }else{
                req.flash('error', "Course is already available!");
                res.redirect(namedRouter.urlFor('admin.course.edit', { id: courseId }));
            }    
        }catch(e){
            return res.status(500).send({message: e.message});  
        }      
            
    };



    /* @Method: list
    // @Description: To get all the courses from DB
    */
    async list (req, res){
            try
            {
                res.render('course/views/list.ejs', {
                    page_name: 'course-list',
                    page_title: 'Course List',
                    user: req.user,
                    
                });
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }  
    };
   

	async getAll (req, res){
		try{
			let course = await courseRepo.getAll(req);
			
			if(_.has(req.body, 'sort')){
				var sortOrder = req.body.sort.sort;
				var sortField = req.body.sort.field;
			}
			else{
				var sortOrder = -1;
				var sortField = '_id';
			}
			let meta = {"page": req.body.pagination.page, "pages": course.pageCount, "perpage": req.body.pagination.perpage, "total": course.totalCount, "sort": sortOrder, "field": sortField};
			//console.log("145>>",course.data);
			return {status: 200, meta: meta, data:course.data, message: `Data fetched succesfully.`};
		}
		catch(e){
			return {status: 500,data: [],message: e.message};
		}
	}
    /*
    // @Method: status_change
    // @Description: course status change action
    */
	async changeStatus (req, res){
		try {
			//console.log("147>>",req.params.id);
			let course = await courseRepo.getById(req.params.id);
			//console.log("149>>",course); process.exit();
			if(!_.isEmpty(course)){
				let courseStatus = (course.status == "Active") ? "Inactive" : "Active";
				let courseUpdate= await courseRepo.updateById({"status": courseStatus }, req.params.id);
				req.flash('success', "Course status has changed successfully" );
				res.redirect(namedRouter.urlFor('admin.course.list'));
			}
			else {
				req.flash('error', "Sorry course not found");
				res.redirect(namedRouter.urlFor('admin.course.list')); 
			}
		}
		catch(e){
			return res.status(500).send({message: e.message}); 
		}
	};

    /* @Method: delete
    // @Description: course delete
    */
    async destroy (req, res){
	
	try {
		const courseDelete = await courseRepo.updateById({'isDeleted': true}, req.params.id);
		if(!_.isEmpty(courseDelete)){
			req.flash('success','Course removed successfully');
			res.redirect(namedRouter.urlFor('admin.course.list'));
		} 
	}
	catch (error) {
		return res.status(500).send({message: e.message});  
	}
    };

}

module.exports = new courseController();