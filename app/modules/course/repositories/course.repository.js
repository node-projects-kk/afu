const Course = require('course/models/course.model');
const perPage = config.PAGINATION_PERPAGE;

class CourseRepository {
    constructor() {}

	async getAll(req) {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({ "isDeleted": false });
	
			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				//and_clauses.push({ 'course_name': { $regex: req.body.query.generalSearch, $options: 'i' } });
				
				and_clauses.push({
					$or:
					[
						{ 'university_name': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'course_name': { $regex:  req.body.query.generalSearch, $options: 'i'} },
						{ 'education': { $regex:  req.body.query.generalSearch, $options: 'i'} },
					]
				});
			}
			
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				and_clauses.push({ "status": req.body.query.Status });
			}
			conditions['$and'] = and_clauses;
			
			console.log("33>>",conditions);
	
			var sortOperator = { "$sort": {} };
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				}
				else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			}
			else {
				sortOperator["$sort"]['_id'] = -1;
			}
	
			var aggregate =  Course.aggregate([
								{
									"$lookup": {
										"from": "education_levels",
										"localField": "education",
										"foreignField": "_id",
										"as": "education_list"
									},
								},
								//{$unwind:"$education_list"},
								{
									$unwind: {
										path: "$education_list",
										preserveNullAndEmptyArrays: true
									}
								},
								{
									"$lookup": {
										"from": "universities",
										"localField": "university",
										"foreignField": "_id",
										"as": "university_list"
									},
								},
								//{$unwind:"$university_list"},
								{
									$unwind: {
										path: "$university_list",
										preserveNullAndEmptyArrays: true
									}
								},
								{
									$project:{
										_id :"$_id",
										course_name:"$course_name",
										status:"$status",
										isDeleted:"$isDeleted",
										createdAt:"$createdAt",
										updatedAt:"$updatedAt",
										education:"$education_list.level_name",
										university_name:"$university_list.university_name",
										university_address:"$university_list.address",
										university_city:"$university_list.city",
										university_state:"$university_list.state",
										university_postal_code:"$university_list.postal_code",
										university_email:"$university_list.email",
										university_phone:"$university_list.phone",
										university_website:"$university_list.website",
										university_registration_no:"$university_list.registration_no",
									}
								},
								{ $match: conditions },
								sortOperator
							]);
	
			//console.log("107>>");
			var options = { page: req.body.pagination.page, limit: req.body.pagination.perpage };
			let allCourse = await Course.aggregatePaginate(aggregate, options);
			return allCourse;
		}
		catch (e) {
			throw (e);
		}
	}
    
    
    
    async getAllCourse(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Course.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }
   
    async getById(id) {
        try {
            return await Course.findById(id).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Course.findOne(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getAllByField(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Course.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async updateById(data, id) {
        try {
            return await Course.findByIdAndUpdate(id, data, {
                    new: true,
                    upsert: true
                })
                .lean().exec();
        } catch (error) {
            return error;
        }
    }

    async getCourseCount() {
        try {
            return await Course.count({
                "isDeleted": false
            });
        } catch (error) {
            return error;
        }
    }

    async save(data) {
        try {
            const _save = new Course(data);
            return await _save.save();
        } catch (error) {
            return error;
        }
    }

    async getActiveCourse(params) {
        try {
            const _params = {...params,
                "isDeleted": false,
            };
            return await Course.find(_params).lean().exec();
        } catch (error) {
            return error;
        }
    }

    async delete(id) {
        try {
            await Course.findById(id).lean().exec();
            return await Course.deleteOne({
                _id: id
            }).lean().exec();
        } catch (error) {
            return error;
        }
    }
}

module.exports = new CourseRepository();