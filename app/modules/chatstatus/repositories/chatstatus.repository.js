const mongoose = require('mongoose');
const ChatStatus = require('chatstatus/models/chatstatus.model');
const perPage = config.PAGINATION_PERPAGE;

const chatStatusRepository = {
    getAll: async  (searchQuery, sortOrder = {
        '_id': -1
    }, page) => {
        
            try {
                const query = [{
                    //"status": "Active"
                }];
                // serach by keyword
                if (_.has(searchQuery, "keyword")) {
                    if (searchQuery.keyword != '') {
                        let search1 = searchQuery.keyword.trim();
                        const search = search1.charAt(0).toUpperCase() + search1.slice(1);
    
                        query.push({
                            "$or": [{
                                'title': {
                                    '$regex': search,
                                    '$options': 'i'
                                }
                            },
                            {
                                'status': search
                            }]
                        });
                    }
                }
                searchQuery = {
                    "$and": query
                };
                const aggregate = ChatStatus.aggregate([{
                    "$sort": sortOrder
                },
                {
                    $project: {
                        _id: "$_id",
                        title: "$title",
                        content: "$content",
                        status: "$status",
                        slug: "$slug"
                    }
                },
                {
                    $match: searchQuery
                },
                ]);
                return await ChatStatus.aggregatePaginate(aggregate, {
                    page: page,
                    limit: perPage
                }); // { data, pageCount, totalCount }
            } catch (error) {
                throw error;
            }
        
    },

    getById: async (id) => {
        let chat = await ChatStatus.findById(id).lean().exec();
        try {
            if (!chat) {
                return null;
            }
            return chat;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let chat = await ChatStatus.findOne(params).exec();
        try {
            if (!chat) {
                return null;
            }
            return chat;

        } catch (e) {
            return e;
        }
    },

    getAllByField: async (params) => {
        let chat = await ChatStatus.find(params).exec();
        try {
            if (!chat) {
                return null;
            }
            return chat;

        } catch (e) {
            return e;
        }
    },

    getChatStatusCount: async (params) => {
        try {
            let chatCount = await ChatStatus.countDocuments(params);
            if (!chatCount) {
                return null;
            }
            return chatCount;
        } catch (e) {
            return e;
        }

    },

	save: async (data) => {
		try {
			let user = await ChatStatus.create(data);
			if (!user) {
				return null;
			}
			return user;  
		}
		catch (e){
			return e;  
		}     
	},
	
    delete: async (id) => {
        try {
            let chat = await ChatStatus.findById(id);
            if (chat) {
                let chatDelete = await ChatStatus.remove({ _id: id }).exec();
                if (!chatDelete) {
                    return null;
                }
                return chatDelete;
            }
        } catch (e) {
            throw e;
        }
    },

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },


    updateById: async (data, id) => {
        try {
            let chat = await ChatStatus.findByIdAndUpdate(id, data, { new: true, upsert: true }).exec();
            if (!chat) {
                return null;
            }
            return chat;
        } catch (e) {
            return e;
        }
    },

    updateByField: async (field, fieldValue, data) => {
        //todo: update by field
    },


};

module.exports = chatStatusRepository;