const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const deleted = [true, false];
const callstatus = ["free","busy"];

const ChatStatusSchema = new Schema({
	mentor_id: { type:Schema.Types.ObjectId,ref: 'User' },
	student_id: { type:Schema.Types.ObjectId,ref: 'User' },
	call_date: { type: Date, default: Date.now() },
	call_status:{type:String, default:"free",enum:callstatus},
	isDeleted: { type: Boolean, default: false, enum: deleted },
});

// For pagination
ChatStatusSchema.plugin(mongooseAggregatePaginate);
// create the model for users and expose it to our app
module.exports = mongoose.model('ChatStatus', ChatStatusSchema);