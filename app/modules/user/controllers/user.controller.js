const mongoose = require('mongoose');
const UserModel = require('user/models/user.model');
const userRepo = require('user/repositories/user.repository');
const languageRepo = require('language/repositories/language.repository');
const countryRepo = require('country/repositories/country.repository');
const eduRepo = require('education_level/repositories/education_level.repository');
const universityRepo = require('university/repositories/university.repository');
const roleRepo = require('role/repositories/role.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({ imageMagick: true });
const fs = require('fs');
const jwt = require('jsonwebtoken');
//mail send 
const { join } = require('path');
const ejs = require('ejs');
const { readFile } = require('fs');
const { promisify } = require('util');
const readFileAsync = promisify(readFile);
const mailer = require('../../../helpers/mailer');

class UserController {
    constructor() {
        this.users = [];

    }

    /* @Method: login
    // @Description: user Login Render
    */
    async login(req, res) {
        res.render('user/views/login.ejs');
    };

    /* @Method: signin
    // @Description: user Login
    */
	async signin(req, res) {
		try {
			let userData = await userRepo.findOneWithRole(req.body);
			if (userData.status == 500) {
				return res.status(201).send({ "result":[],"message":userData.message, "status":false });
				//req.flash('error', userData.message);
				//return res.redirect(namedRouter.urlFor('user.login'));
			}
		
			let user = userData.data;
			if (!_.isEmpty(user.role) && user.role.role == 'admin') {
				const payload = {id: user._id};
				let token = jwt.sign(payload, config.jwtSecret, {expiresIn: 86400});
				req.session.token = token;
				req.user = user;
				let user_details = {};
				user_details.id = user._id;
				user_details.name = user.name;
				user_details.email = user.email;
				// return the information including token as JSON
				return res.status(200).send({ "result":user_details,"message":"You have successfully logged in", "status":true });
				// req.flash('success', "You have successfully logged in");
				// res.redirect(namedRouter.urlFor('admin.user.dashboard'));
			}
			else {
				return res.status(201).send({ "result":[],"message":"Incorrect username or password. Please try again.", "status":false });
				// req.flash('error', 'Authentication failed. Wrong credentials.');
				// res.redirect(namedRouter.urlFor('user.login'));
			}
		}
		catch (e) {
			return res.status(500).send({ message: e.message });
		}
	};

    /* @Method: signup
    // @Description: user Signup
    */
    async signup(req, res) {
        try {
            
            var roleDetails = await roleRepo.getByField({'role': 'users'});
                            
            if (!_.isEmpty(roleDetails)) {
            
                const userDetails = await userRepo.getByField({
                    'email': req.body.user_email
                });

                if (_.isEmpty(userDetails) || _.isNull(userDetails)) {
                    
                    req.body.role = roleDetails._id
                    
                    const User = new UserModel();
                    req.body.password = User.generateHash(req.body.password);
                    req.body.email = req.body.user_email;
                    req.body.full_name = req.body.hdn_full_name;
                    req.body.company_name = req.body.hdn_company_name;
                    req.body.country = req.body.hdn_country;
                    req.body.phone = req.body.hdn_phone;

                    const saveUser = await userRepo.save(req.body);
                    
                    return res.status(200).send({ "result":saveUser,"message":"Registration successfull", "status":true });
                } else {
                    return res.status(201).send({ "result":{},"message":"Email already exists", "status":false });
                }
            } else {
                throw new Error("Invalid User type");
            }
        } catch (error) {
            throw error.message;
        }
    };

    
	/* @Method: Dashboard
	// @Description: User Dashboard
	*/
	async dashboard(req, res) {
		try {
			let relsultall = {};
			let languageCount = await languageRepo.getLanguageCount({"isDeleted":false,"status":"Active"});
			let countryCount = await countryRepo.getCountryCount();
			let educationCount = await eduRepo.getEducationLevelCount();
			let userCount = await userRepo.getUsersCount();
			let universityCount = await universityRepo.getUniversityCount();
			let mentorCount = await userRepo.getMentorCount();
		
			let user = await userRepo.getLimitUserByField({'isDeleted':false,_id:{$ne:mongoose.Types.ObjectId(req.user._id)}});
			relsultall.user = user;
		
			res.render('user/views/dashboard.ejs', {
				page_name: 'user-dashboard',
				page_title: 'Dashboard',
				user: req.user,
				language:(languageCount >0)?languageCount:0,
				country:(countryCount>0)?countryCount:0,
				education:(educationCount>0)?educationCount:0,
				userC:(userCount[0].count>0)?userCount[0].count:0,
				university:(universityCount >0)?universityCount:0,
				mentor:(mentorCount[0].count >0)?mentorCount[0].count:0,
				response: []                    
			});
		}
		catch (e) {
			throw (e);
		}
	};

    /* @Method: Logout
    // @Description: User Logout
    */
    async logout(req, res) {
        req.session.destroy(function (err) {
            //res.redirect('/' + process.env.ADMIN_FOLDER_NAME);
            res.redirect('/admin');
        });
        // req.session.token = "";
        // req.session.destroy();
        // return res.redirect('/');
    };

    async adminChangePassword(req, res) {
        try {
            res.render('user/views/change_password.ejs', {
                page_name: 'change-password',
                page_title: 'Change Password',
                user: req.user,
            });
            // return { status: 200, data: [], message: 'Change Password ' }
        } catch(e) {
            throw ({message: e.message});
        }
    }

    async dashboardCounts(req, res) {
        try {
            let users = await userRepo.getAllActiveUsersByRole('user');
            return { status: 200, data:users.length, message: 'Data fetched successfully' };
        } catch(e) {
            throw ({ message: e.message });
        }
    }

    async resetUserPassword(req, res) {
        try {
            let user = await userRepo.getById(req.params.id);
            if(user){
                let random_pass = Math.random().toString(36).substr(2, 9);
                let readable_pass = random_pass;
                random_pass = user.generateHash(random_pass);
                let user_details = await userRepo.updateById({password: random_pass}, user._id);
                if (!user_details) {  
                    return null;
                }
                // return readable_pass;
                let mailOptions = {
                        from: `Admin<${process.env.MAIL_USERNAME}>`,
                        to: user.email,
                        subject: 'Reset Password | Game Boosting',
                        html: `Hello ${user.first_name} ${user.last_name},<br>
                                We have received your Password Reset request.<br>
                                Here is your New Password: <b>${readable_pass}</b><br>
                                -Game Boosting Admin Team`
                };
                let sendEmail = await transporter.sendMail(mailOptions);
                req.flash('success', 'Password reset successful, email sent containing password');
                res.redirect(namedRouter.urlFor('admin.managers'));

            } else {
                req.flash('error', 'User not found');
                res.redirect(namedRouter.urlFor('admin.managers'));
            }
        } catch(e) {
            throw ({message: e.message});
        }
    }


	async list(req, res) {
		try {
			let pname = "";
			if (req.params.type=="mentor") {
				res.render('user/views/list.ejs', {
					page_name: "mentor-list",
					page_title: 'Mentor List',
					user: req.user,
					usertype:req.params.type,
					response: []
				});
			}
			else if (req.params.type=="student") {
				res.render('user/views/list.ejs', {
					page_name: "student-list",
					page_title: 'Student List',
					user: req.user,
					usertype:req.params.type,
					response: []
				});
			}
			else{
				res.render('user/views/list.ejs', {
					page_name: "users-list",
					page_title: 'Admin User List',
					user: req.user,
					usertype:req.params.type,
					response: []
				});
			}
		}
		catch(e) {
			throw ({message: e.message});
		}
	}

async getAll(req, res) {
	try {
		if (req.params.utype == "mentor") {
			req.body.role = 'mentor';
		}
		else if (req.params.utype == "student") {
			req.body.role = 'student';
		}
		else{
			req.body.role = 'admin';
		}
		
		if(_.has(req.body, 'sort')){
			var sortOrder = req.body.sort.sort;
			var sortField = req.body.sort.field;
		}
		else{
			var sortOrder = -1;
			var sortField = '_id';
		}
	
		if(!_.has(req.body, 'pagination')){
			req.body.pagination.page = 1;
			eq.body.pagination.perpage = config.PAGINATION_PERPAGE
		}
		let boosters = await userRepo.getUsersListByRole(req);
		let meta = {"page": req.body.pagination.page, "pages": boosters.pageCount, "perpage": req.body.pagination.perpage, "total": boosters.totalCount, "sort": sortOrder, "field": sortField}; 
	
		return {status: 200, meta: meta, data:boosters.data, message: `Data fetched succesfully.`};
	}
	catch(e) {
		throw({message: e.message});
	}
}
    
	async create(req, res) {
		try {
			res.render('user/views/create.ejs', {
				page_name: 'users-list',
				page_title: 'Create Admin User',
				user: req.user,
			});
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async store(req, res) {
		try {
			let user = await userRepo.getByField({ 'email': req.body.email, 'isDeleted': false });
			let role = await roleRepo.getByField({ 'role':'admin'});
			if (_.isEmpty(user)) {
				req.body.role = role._id;
				const password = req.body.password;
				req.body.password = req.user.generateHash(req.body.password);
				let userSave = await userRepo.save(req.body); 
				if(userSave){
					const templateData = {email: req.body.email,password: password,};
					const ejsTemplate = await readFileAsync(join(__dirname, '../../../', 'views/templates/email.sendregister.ejs'));
					const html = ejs.render(ejsTemplate.toString(), {data: templateData});
					await transporter.sendMail({
						from: `Admin<${process.env.MAIL_USERNAME}>`,
						//to: req.body.email,
						to: 'subhasish.ghosh@webskitters.com',
						subject: 'New Admin | Apply for University',
						html: html
					});
					req.flash('success', "Admin User created successfully.");
					res.redirect(namedRouter.urlFor('admin.users',{type:"admin"}));
				}    
			}
			else {
				req.flash('error', "This email address is already exist!");
				res.redirect(namedRouter.urlFor('admin.users',{type:"admin"}));
			}
		}
		catch(e) {
			throw ({ message: e.message });
		}
	}

	async changeStatus(req, res) {
		try {
			let user = await userRepo.getById(req.params.id);
			let role = user.role.role;
			if(!_.isEmpty(user)) {
				let status = (user.isActive == true)? false : true;
				let userUpdate = await userRepo.updateById({ isActive: status }, req.params.id);
				req.flash('success', 'User status updated successfully');
				res.redirect(namedRouter.urlFor('admin.users',{type:role}));
			}
			else {
				throw ({ message: 'Invalid user selected' });
			}
		}
		catch(e) {
			throw({ message: e.message });
		}
	}

	async edit(req,res) {
		try {
			let user = await userRepo.getById(req.params.id);
			if (!_.isEmpty(user)) {
				res.render('user/views/edit.ejs', {
					page_name: 'users-list',
					page_title: 'Edit User',
					user: req.user,
					response: user
				});
			}
		}
		catch(e) {
			throw({message: e.message});
		}
	}

	async view(req,res) {
		try {
			let usertype = req.params.utype;
			if (usertype == "mentor") {
				let user = await userRepo.getByIdMentor(req.params.id);
				
				if (!_.isEmpty(user)) {
					res.render('user/views/view.ejs', {
						page_name: 'users-list',
						page_title: 'View Mentor',
						user: req.user,
						response: user,
						user_type:"mentor"
					});
				}
			}
			else if (usertype == "student") {
				let user = await userRepo.getByIdStudent(req.params.id);
				if (!_.isEmpty(user)) {
					res.render('user/views/view.ejs', {
						page_name: 'users-list',
						page_title: 'View Student',
						user: req.user,
						response: user,
						user_type:"student"
					});
				}
			}
		}
		catch(e) {
			throw({message: e.message});
		}
	}


	async update(req, res) {
		try {
			let userId = req.body.user_id;
			let user = await userRepo.getByField({ 'email': req.body.email, 'isDeleted': false, '_id': { $ne: mongoose.Types.ObjectId(userId) } });
			if (_.isEmpty(user)) {
				let userUpdate = userRepo.updateById(req.body, userId)
				if(userUpdate) {
					req.flash('success', "User updated successfully");
					res.redirect(namedRouter.urlFor('admin.users',{type:"admin"}));
				}
			}
			else{
				req.flash('error', "This email address is already exist!");
				res.redirect(namedRouter.urlFor('admin.user.edit',{id:userId}));
			}   
		}
		catch(e) {
			throw({message: e.message});
		}
	}
    // Booster Module Ends

    /* @Method: viewmyprofile
    // @Description: To get Profile Info from db
    */
    async viewmyprofile(req, res) {
        try {
            const id = req.params.id;
            let user = await userRepo.getById(id)
            if (!_.isEmpty(user)) {
                res.render('user/views/myprofile.ejs', {
                    page_name: 'user-profile',
                    page_title: 'My Profile',
                    user: req.user,
                    response: user
                });

            }
        } catch (e) {
            throw(e.message);
            return res.status(500).send({ message: e.message });
        }
    }

    /* @Method: updateprofile
    // @Description: Update My Profile 
    */
    async updateprofile(req, res) {
        try {
            const id = req.body.id;
            let userUpdate = await userRepo.updateById(req.body, id)
            if (!_.isEmpty(userUpdate)) {
                req.flash('success', "Profile updated successfully.");
                res.redirect(namedRouter.urlFor('admin.profile', { id: id }));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    
    /* @Method: changepassword
    // @Description: user changepassword Render
    */
    async changePassword(req, res) {
        var vehicleOwner = await userRepo.getById(req.user._id);
        if (vehicleOwner) {
            res.render('user/views/change_password.ejs', {
                page_name: 'user-changepassword',
                page_title: 'Change Password',
                response: vehicleOwner,
                user: req.user
            });
        } else {
            req.flash('error', "Sorry user not found.");
            res.redirect(namedRouter.urlFor('admin.user.dashboard'));
        }

    };

    /*
    // @Method: updatepassword
    // @Description: User password change
    */

    async adminUpdatePassword(req, res) {
        try {
            let user = await userRepo.getById(req.user._id);
            if (!_.isEmpty(user)) {
                // check if password matches

                if (!user.validPassword(req.body.old_password, user.password)) {
                    req.flash('error', "Sorry old password mismatch!");
                    res.redirect(namedRouter.urlFor('admin.changepassword'));
                } else {
                    // if user is found and password is right, check if he is an admin

                    let new_password = req.user.generateHash(req.body.password);
                    let userUpdate = await userRepo.updateById({ "password": new_password }, req.body.id)
                    if (!_.isEmpty(user)) {
                        req.flash('success', "Your password has been changed successfully.");
                        res.redirect(namedRouter.urlFor('admin.user.dashboard'));
                    }
                }
            } else {
                req.flash('error', "Authentication failed. Wrong credentials.");
                res.redirect(namedRouter.urlFor('admin.changepassword'));
            }
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    };

    /*
    // @Method: forgotPassword
    // @Description: User forgotPassword
    */

	async forgotPassword(req, res) {
		try {
			let result = await userRepo.forgotPassword(req.body);
			if (result.status == 500) {
				return res.status(201).send({ "result":[],"message":"User not found", "status":false });
			}
			else{
				let locals = { password: result.data };
				let isMailSend = await mailer.sendMail('Admin<belasmith00@gmail.com>', req.body.email, 'New Password', 'forgot-password', locals);
				
				if (isMailSend) {
					return res.status(200).send({ "result":result,"message":"Mail is sending to your mail id with new password", "status":false });
					//res.redirect(namedRouter.urlFor('user.login'));
				}
			}
		}
		catch (e) {
			return res.status(500).send({ message: e.message });
		}
	};


    async getAllUserCount(req, res){
        try {
            let userCount = await userRepo.getUsersCount(req);
            return userCount;
        } catch (e) {
            return res.status(500).send({ message: e.message });
        }
    }

      /* @Method: delete
    // @Description: user delete
    */
	async destroy (req, res){
		try{
			let userDelete = await userRepo.delete(req.params.id)
			if(!_.isEmpty(userDelete)){
				req.flash('success','User removed successfully');
				res.redirect(namedRouter.urlFor('admin.users',{type:"admin"}));
			} 
		}
		catch(e){
			return res.status(500).send({message: e.message});   
		} 
	};
	
	
}

module.exports = new UserController();