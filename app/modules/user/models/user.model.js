var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const deleted = [true, false];
const notification = [true, false];
const mobileno = [true, false];
const onoff = ["yes", "no"];
const registertype = ["normal", "facebook", "google", "instagram", "linkedin"];
const devicetype = ["android", "ios", "web"];

var UserSchema = new Schema({
	full_name: {
		type: String,
		default: ''
	},
	surname: {
		type: String,
		default: ''
	},
	email: {
		type: String,
		default: ''
	},
	password: {
		type: String,
		default: ''
	},
	role: {
		type: Schema.Types.ObjectId,
		ref: 'Role'
	},
	profile_image: {
		type: String,
		default: ''
	},
	language: {
		type: Schema.Types.ObjectId,
		ref: 'Language'
	},
	country: {
		type: Schema.Types.ObjectId,
		ref: 'Country'
	},
	country_of_origin: {
		type: Schema.Types.ObjectId,
		ref: 'Country'
	},
	university_name: {
		type: String,
		default: ''
	},
	university_id: {
		type: Schema.Types.ObjectId,
		ref: 'University'
	},
	course: {
		type: Schema.Types.ObjectId,
		ref: 'Course'
	},
	//specialization:[{
	//	course:{type: Schema.Types.ObjectId,ref: 'Course'},
	//}],
	year_of_study: {
		type: String,
		default: ''
	},
	short_description: {
		type: String,
		default: ''
	},
	cv_list: [{
		file_link: {
			type: String,
			default: ''
		},
		student_id: {
			type: Schema.Types.ObjectId,
			ref: 'User'
		},
		date: {
			type: Date,
			default: Date.now()
		},
		_id: false
	}],
	service: [{
		package_id: {
			type: Schema.Types.ObjectId,
			ref: 'Package'
		},
		//time:{type: String, default: ''},
		//details:{type: String, default: ''},
		//cost:{type:Schema.Types.Double,default:0.00},
	}],
	social_id: {
		type: String,
		default: ''
	},
	register_type: {
		type: String,
		default: 'normal',
		enum: registertype
	},
	school_name: {
		type: String,
		default: ''
	},
	education_level: {
		type: Schema.Types.ObjectId,
		ref: 'Education_Level'
	},
	want_to: {
		type: Schema.Types.ObjectId,
		ref: 'Want_To'
	},
	activities: [{
		title: {
			type: String,
			default: ''
		},
		tag: {
			type: String,
			default: ''
		}
	}],
	is_ambassador: {
		type: Boolean,
		default: false,
		enum: [true, false]
	},
	logo: {
		type: String,
		default: ''
	},
	address: {
		type: String,
		default: ''
	},
	gender: {
		type: String,
		default: 'Male',
		enum: ['Male', 'Female']
	},
	phone: {
		type: String,
		default: ''
	},
	dob: {
		type: String,
		default: ''
	},
	deviceToken: {
		type: String,
		default: ''
	},
	deviceType: {
		type: String,
		default: 'web',
		enum: devicetype
	},
	push_notification: {
		type: Boolean,
		default: true,
		enum: notification
	},
	display_mobile_no: {
		type: Boolean,
		default: true,
		enum: mobileno
	},
	onoffstatus: {
		type: String,
		default: "yes",
		enum: onoff
	},
	isDeleted: {
		type: Boolean,
		default: false,
		enum: deleted
	},
	isActive: {
		type: Boolean,
		default: true,
		enum: [true, false]
	},
}, {
	timestamps: true
});

// generating a hash
UserSchema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function (password, checkPassword) {
	return bcrypt.compareSync(password, checkPassword);
	//bcrypt.compare(jsonData.password, result[0].pass
};

// For pagination
UserSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('User', UserSchema);