var mongoose = require('mongoose');
require('mongoose-double')(mongoose);
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const deleted = [true, false];

var MenteeSchema = new Schema({
	mentor_id:{type: Schema.Types.ObjectId,ref: 'User'},
	student_id:{type: Schema.Types.ObjectId,ref: 'User'},
	isDeleted: { type: Boolean, default: false, enum: deleted },
}, { timestamps: true });

// For pagination
MenteeSchema.plugin(mongooseAggregatePaginate);
// create the model for users and expose it to our app
module.exports = mongoose.model('Mentee', MenteeSchema);