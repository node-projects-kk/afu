const mongoose = require('mongoose');
const Mentee = require('user/models/mentee.model');
const role = require('role/models/role.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;

const menteeRepository = {
	findOneWithRole: async (params) => {
		try{
			let mentee = await Mentee.findOne({email: params.email, isDeleted:false, isActive:true}).populate('role').exec();
			if (!mentee) {
				throw {"status":500, data: null, "message": 'Authentication failed. Mentee not found.' }
			}
			const MenteeModel = new Mentee();
			if (!MenteeModel.validPassword(params.password,mentee.password)) {
				throw {"status":500, data: null, "message": 'Authentication failed. Wrong password.' }
			}
			else {
				throw {"status":200, data: mentee, "message": "" }
			}
		}
		catch(e){
			return e;
		}
	},

	findIsAccess: async (mentee_role, permission_id) =>{
	    try{
		let roleInfo = await rolePermission.findOne({ $and: [{ 'role': mentee_role }, { 'permissionall': { $in: [permission_id] } }] }).exec();
		let is_access = (roleInfo != null) ? true : false;
		throw {"status":200, is_access: is_access, "message": "" }
	    }catch(e)
	    {
	       return e;
	    }
	},

	getById: async (id) => {
		let mentee = await Mentee.findById(id).populate('role').exec();
		try{
			if (!mentee) {
				return null;
			} 
			return mentee;
		}
		catch(e){
			return e;
		}
	},
	
	updateById:async(data, id)=>{
		try {
			return await Mentee.findByIdAndUpdate(id, data, {new: true,upsert: true}).lean().exec();
		}
		catch (error) {
			return error;
		}
	},
	
    getByField: async (params) => {

        let mentee =  await Mentee.findOne(params).exec();
        try{
            if (!mentee) {
                return null;
            } 
            return mentee;
    
        }catch(e){
                return e;
        }
    },

    getAllByField: async (params) => {
        let mentee =  await Mentee.find(params).exec(); 
        try{
            if (!mentee) {
                return null;
            } 
            return mentee;
    
        }catch(e){
                return e;
        }
    },

    getLimitMenteeByField: async (params) => {
        let mentee =  await Mentee.find(params).populate('role').limit(5).sort({_id:-1}).exec(); 
        try{
            if (!mentee) {
                return null;
            } 
            return mentee;
    
        }catch(e){
                return e;
        }
    },
        
	delete: async (id) => {
		try { 
			let mentee = await Mentee.findById(id);
			if(mentee) {
				let menteeDelete = await Mentee.remove({_id:id}).exec();
				if (!menteeDelete) {
					return null;
				}
				return menteeDelete;
			}
		}
		catch(e){
			return e;
		}
	},

    deleteByField: async (field, fieldValue) => {
        //todo: Implement delete by field
    },

	updateById: async (data, id) =>{
		try {
			let mentee = await Mentee.findByIdAndUpdate(id, data, { new: true});
			if (!mentee) {
				return null;
			}
			return mentee;
		}
		catch(e) {
			return e;
		}
	},

    updateByField: async (field, fieldValue, data) => {
        try {
            let mentee = await Mentee.findByIdAndUpdate(fieldValue, field, { new: true});
            if (!mentee) {
               return null;
            }
            return mentee;
         } catch(e) {
            return e;
         }
    },

    save: async (data) => {
       try {
            let mentee = await Mentee.create(data);
         
            if (!mentee) {
               return null;
            }
            return mentee;  
         }
         catch (e){
            return e;  
         }     
    },


    getMentee : async(id) =>{
       try{ 
            let mentee = await Mentee.findOne({id}).exec(); 
            if (!mentee) {
            return null;
            }
            return mentee;  
        } catch(e){
            return e;
        }
    },

    getMenteeByField : async(data) =>{
        try{ 
             let mentee = await Mentee.findOne(data).populate('role').exec(); 
             if (!mentee) {
             return null;
             }
             return mentee;  
         } catch(e){
             return e;
         }
     },
     
     getMenteesList: async (id) => {
		try {
			let users = await Mentee.aggregate([
						{
							$lookup: {
								from: 'users',
								localField: 'student_id',
								foreignField: '_id',
								as: 'students'
							}
						},
						{
							$unwind: {
								path: "$students"
							}
						},
						{
							$lookup: {
								from: 'users',
								localField: 'mentor_id',
								foreignField: '_id',
								as: 'mentors'
							}
						},
						{
							$unwind: {
								path: "$mentors"
							}
						},

						{
							$lookup: {
								from: 'education_levels',
								localField: 'students.education_level',
								foreignField: '_id',
								as: 'students.education_level'
							}
						},
						{
							$unwind: {
								path: "$students.education_level",
								preserveNullAndEmptyArrays: true
							}
						},
						{
							$lookup: {
								from: 'countries',
								localField: 'students.country',
								foreignField: '_id',
								as: 'students.countries'
							}
						},
						{
							$unwind: {
								path: "$students.countries",
								preserveNullAndEmptyArrays: true
							}
						},
						{
							$lookup: {
								from: 'courses',
								localField: 'students.course',
								foreignField: '_id',
								as: 'students.course_details'
							}
						},
						{
							$unwind: {
								path: "$students.course_details",
								preserveNullAndEmptyArrays: true
							}
						},

						{
							$lookup: {
								from: 'languages',
								localField: 'students.language',
								foreignField: '_id',
								as: 'students.language'
							}
						},
						{
							$unwind: {
								path: "$students.language",
								preserveNullAndEmptyArrays: true
							}
						},

						{ $match: { "isDeleted": false,"mentor_id":mongoose.Types.ObjectId(id) } },
						//{
						//	$group:{
						//		_id:"$_id",
						//		profile:{$first:"$$ROOT"},
						//		project:{$addToSet:"$project_details"},
						//		activities:{$addToSet:"$activities"}
						//	}
						//},
						//{
						//	$project:{
						//		_id:1,
						//		full_name:"$profile.full_name",
						//		email:"$profile.email",
						//		address:"$profile.address",
						//		school_name:"$profile.school_name",
						//		gender:"$profile.gender",
						//		short_description:"$profile.short_description",
						//		phone:"$profile.phone",
						//		education:"$profile.edu_level.level_name",
						//		country:"$profile.country_details.country_name",
						//		want_to:"$profile.want_details.name",
						//		project:"$project",
						//		activities:"$activities",
						//	}
						//}
					]);
			//console.log("437>>",users);
			
			return users;
		}
		catch(e) {
			throw(e);
		}
	},

     
    

};

module.exports = menteeRepository;