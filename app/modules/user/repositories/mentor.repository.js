const mongoose = require('mongoose');
const Mentor = require('user/models/mentor.model');
const role = require('role/models/role.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;

const mentorRepository = {
	findOneWithRole: async (params) => {
		try {
			let mentor = await Mentor.findOne({
				email: params.email,
				isDeleted: false,
				isActive: true
			}).populate('role').exec();
			if (!mentor) {
				throw {
					"status": 500,
					data: null,
					"message": 'Authentication failed. Mentor not found.'
				}
			}
			const MentorModel = new Mentor();
			if (!MentorModel.validPassword(params.password, mentor.password)) {
				throw {
					"status": 500,
					data: null,
					"message": 'Authentication failed. Wrong password.'
				}
			} else {
				throw {
					"status": 200,
					data: mentor,
					"message": ""
				}
			}
		} catch (e) {
			return e;
		}
	},

	findIsAccess: async (mentor_role, permission_id) => {
		try {
			let roleInfo = await rolePermission.findOne({
				$and: [{
					'role': mentor_role
				}, {
					'permissionall': {
						$in: [permission_id]
					}
				}]
			}).exec();
			let is_access = (roleInfo != null) ? true : false;
			throw {
				"status": 200,
				is_access: is_access,
				"message": ""
			}
		} catch (e) {
			return e;
		}
	},

	getById: async (id) => {
		let mentor = await Mentor.findById(id).populate('role').exec();
		try {
			if (!mentor) {
				return null;
			}
			return mentor;
		} catch (e) {
			return e;
		}
	},

	updateById: async (data, id) => {
		try {
			return await Mentor.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			return error;
		}
	},

	getByField: async (params) => {

		let mentor = await Mentor.findOne(params).exec();
		try {
			if (!mentor) {
				return null;
			}
			return mentor;

		} catch (e) {
			return e;
		}
	},

	getAllByField: async (params) => {
		let mentor = await Mentor.find(params).exec();
		try {
			if (!mentor) {
				return null;
			}
			return mentor;

		} catch (e) {
			return e;
		}
	},

	getLimitMentorByField: async (params) => {
		let mentor = await Mentor.find(params).populate('role').limit(5).sort({
			_id: -1
		}).exec();
		try {
			if (!mentor) {
				return null;
			}
			return mentor;

		} catch (e) {
			return e;
		}
	},

	delete: async (id) => {
		try {
			let mentor = await Mentor.findById(id);
			if (mentor) {
				let mentorDelete = await Mentor.remove({
					_id: id
				}).exec();
				if (!mentorDelete) {
					return null;
				}
				return mentorDelete;
			}
		} catch (e) {
			return e;
		}
	},

	deleteByField: async (field, fieldValue) => {
		//todo: Implement delete by field
	},

	updateById: async (data, id) => {
		try {
			let mentor = await Mentor.findByIdAndUpdate(id, data, {
				new: true
			});
			if (!mentor) {
				return null;
			}
			return mentor;
		} catch (e) {
			return e;
		}
	},

	updateByField: async (field, fieldValue, data) => {
		try {
			let mentor = await Mentor.findByIdAndUpdate(fieldValue, field, {
				new: true
			});
			if (!mentor) {
				return null;
			}
			return mentor;
		} catch (e) {
			return e;
		}
	},

	save: async (data) => {
		try {
			let mentor = await Mentor.create(data);

			if (!mentor) {
				return null;
			}
			return mentor;
		} catch (e) {
			return e;
		}
	},


	getMentor: async (id) => {
		try {
			let mentor = await Mentor.findOne({
				id
			}).exec();
			if (!mentor) {
				return null;
			}
			return mentor;
		} catch (e) {
			return e;
		}
	},

	getMentorsByField: async (data) => {
		try {
			let mentor = await Mentor.findOne(data).populate('role').exec();
			if (!mentor) {
				return null;
			}
			return mentor;
		} catch (e) {
			return e;
		}
	},

	getMentorsList: async (id) => {
		try {
			let users = await Mentor.aggregate([{
					$lookup: {
						from: 'users',
						localField: 'student_id',
						foreignField: '_id',
						as: 'students'
					}
				},
				{
					$unwind: {
						path: "$students"
					}
				},
				{
					$lookup: {
						from: 'users',
						localField: 'mentor_id',
						foreignField: '_id',
						as: 'mentors'
					}
				},
				{
					$unwind: {
						path: "$mentors"
					}
				},

				{
					$lookup: {
						from: 'education_levels',
						localField: 'mentors.education_level',
						foreignField: '_id',
						as: 'mentors.education_level'
					}
				},
				{
					$unwind: {
						path: "$mentors.education_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'mentors.country',
						foreignField: '_id',
						as: 'mentors.countries'
					}
				},
				{
					$unwind: {
						path: "$mentors.countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'courses',
						localField: 'mentors.course',
						foreignField: '_id',
						as: 'mentors.course_details'
					}
				},
				{
					$unwind: {
						path: "$mentors.course_details",
						preserveNullAndEmptyArrays: true
					}
				},

				{
					$lookup: {
						from: 'languages',
						localField: 'mentors.language',
						foreignField: '_id',
						as: 'mentors.language'
					}
				},
				{
					$unwind: {
						path: "$mentors.language",
						preserveNullAndEmptyArrays: true
					}
				},

				{
					$match: {
						"isDeleted": false,
						"student_id": mongoose.Types.ObjectId(id)
					}
				},
				//{
				//	$group:{
				//		_id:"$_id",
				//		profile:{$first:"$$ROOT"},
				//		project:{$addToSet:"$project_details"},
				//		activities:{$addToSet:"$activities"}
				//	}
				//},
				//{
				//	$project:{
				//		_id:1,
				//		full_name:"$profile.full_name",
				//		email:"$profile.email",
				//		address:"$profile.address",
				//		school_name:"$profile.school_name",
				//		gender:"$profile.gender",
				//		short_description:"$profile.short_description",
				//		phone:"$profile.phone",
				//		education:"$profile.edu_level.level_name",
				//		country:"$profile.country_details.country_name",
				//		want_to:"$profile.want_details.name",
				//		project:"$project",
				//		activities:"$activities",
				//	}
				//}
			]);
			//console.log("437>>",users);

			return users;
		} catch (e) {
			throw (e);
		}
	},

	async getMenteeCount(id) {
		try {
			let menteeCount = await Mentor.find({
				'mentor_id': mongoose.Types.ObjectId(id),
				'isDeleted': false
			}).count();
			return menteeCount;
		} catch (e) {
			return e;
		}
	},

	getMenteeListWithSearch: async (data, id) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false
			});
			and_clauses.push({
				"role": {
					"$eq": "student"
				}
			});

			if (_.isObject(data) && _.has(data, 'country')) {
				if (!_.isEmpty(data.country)) {
					and_clauses.push({
						'country_id': mongoose.Types.ObjectId(data.country)
					});
				}
			}
			if (_.isObject(data) && _.has(data, 'course')) {
				if (!_.isEmpty(data.course)) {
					and_clauses.push({
						'course_id': mongoose.Types.ObjectId(data.course)
					});
				}
			}

			conditions['$and'] = and_clauses;
			return await Mentor.aggregate([{
					$match: {
						"mentor_id": mongoose.Types.ObjectId(id),
					}
				},
				{
					"$lookup": {
						"from": "users",
						"localField": "student_id",
						"foreignField": "_id",
						"as": "mentee_info"
					},
				},
				{
					$unwind: {
						path: "$mentee_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "roles",
						"localField": "mentee_info.role",
						"foreignField": "_id",
						"as": "role_info"
					},
				},
				{
					$unwind: {
						path: "$role_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "courses",
						"localField": "mentee_info.course",
						"foreignField": "_id",
						"as": "course_info"
					},
				},
				{
					$unwind: {
						path: "$course_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "countries",
						"localField": "mentee_info.country",
						"foreignField": "_id",
						"as": "country_info"
					},
				},
				{
					$unwind: {
						path: "$country_info",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$project: {
						_id: "$_id",
						full_name: "$mentee_info.full_name",
						role: "$role_info.role",
						profile_image: "$mentee_info.profile_image",
						course_id: "$course_info._id",
						course_name: "$course_info.course_name",
						country_id: "$country_info._id",
						country_name: "$country_info.country_name",
						isDeleted: "$mentee_info.isDeleted",
						createdAt: "$mentee_info.createdAt",
					}
				},
				{
					$match: conditions
				},
				{
					"$sort": {
						createdAt: -1
					}
				},
			]).exec();
		} catch (e) {
			return e;
		}
	},
};

module.exports = mentorRepository;