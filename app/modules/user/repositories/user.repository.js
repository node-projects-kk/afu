const mongoose = require('mongoose');
const User = require('user/models/user.model');
const role = require('role/models/role.model');
const rolePermission = require('permission/models/role_permission.model');
const perPage = config.PAGINATION_PERPAGE;

const userRepository = {
	findOneWithRole: async (params) => {
		try {
			let user = await User.findOne({
				email: params.email,
				isDeleted: false,
				isActive: true
			}).populate('role').exec();
			if (!user) {
				throw {
					"status": 500,
					data: null,
					"message": 'Authentication failed. User not found.'
				}
			}
			const UserModel = new User();
			if (!UserModel.validPassword(params.password, user.password)) {
				throw {
					"status": 500,
					data: null,
					"message": 'Authentication failed. Wrong password.'
				}
			} else {
				throw {
					"status": 200,
					data: user,
					"message": ""
				}
			}
		} catch (e) {
			return e;
		}
	},

	findIsAccess: async (user_role, permission_id) => {
		try {
			let roleInfo = await rolePermission.findOne({
				$and: [{
					'role': user_role
				}, {
					'permissionall': {
						$in: [permission_id]
					}
				}]
			}).exec();
			let is_access = (roleInfo != null) ? true : false;
			throw {
				"status": 200,
				is_access: is_access,
				"message": ""
			}
		} catch (e) {
			return e;
		}
	},

	getAllUsers: async (req) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false
			});
			and_clauses.push({
				"user_role.role": req.body.role
			});

			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({
					$or: [{
							'first_name': {
								$regex: req.body.query.generalSearch,
								$options: 'i'
							}
						},
						{
							'last_name': {
								$regex: req.body.query.generalSearch,
								$options: 'i'
							}
						},
						{
							'email': {
								$regex: req.body.query.generalSearch,
								$options: 'i'
							}
						}
					]
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				(req.body.query.Status == 'Active') ? and_clauses.push({
					"isActive": true
				}): and_clauses.push({
					"isActive": false
				});
				//and_clauses.push({"isActive": req.body.query.Status});
			}

			conditions['$and'] = and_clauses;

			var sortOperator = {
				"$sort": {}
			};
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				} else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}

				sortOperator["$sort"][sortField] = sortOrder;
			} else {
				sortOperator["$sort"]['_id'] = -1;
			}

			var aggregate = User.aggregate([{
					$lookup: {
						"from": "roles",
						"localField": "role",
						"foreignField": "_id",
						"as": "user_role"
					}
				},
				{
					"$unwind": "$user_role"
				},
				{
					$match: conditions
				},
				sortOperator
			]);

			var options = {
				page: req.body.pagination.page,
				limit: req.body.pagination.perpage
			};
			let allUsers = await User.aggregatePaginate(aggregate, options);

			return allUsers;
		} catch (e) {
			throw (e);
		}
	},

	getUsersListByRole: async (req) => {
		try {
			var conditions = {};
			var and_clauses = [ /*{ 'user_role.role': req.body.role }*/ ];
			var and_clauses = [];

			and_clauses.push({
				'user_role.role': req.body.role,
				'isDeleted': false
			});


			if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
				and_clauses.push({
					$or: [{
							'full_name': {
								$regex: req.body.query.generalSearch,
								$options: 'i'
							}
						},
						{
							'email': {
								$regex: req.body.query.generalSearch,
								$options: 'i'
							}
						}
					]
				});
			}
			if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
				(req.body.query.Status == 'Active') ? and_clauses.push({
					"isActive": true
				}): and_clauses.push({
					"isActive": false
				});
			}

			conditions['$and'] = and_clauses;

			var sortOperator = {
				"$sort": {}
			};
			if (_.has(req.body, 'sort')) {
				var sortField = req.body.sort.field;
				if (req.body.sort.sort == 'desc') {
					var sortOrder = -1;
				} else if (req.body.sort.sort == 'asc') {
					var sortOrder = 1;
				}
				sortOperator["$sort"][sortField] = sortOrder;
			} else {
				sortOperator["$sort"]['user_role.role'] = -1;
			}

			let users = User.aggregate([{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$match: conditions
				},
				sortOperator
			]);

			var options = {
				page: req.body.pagination.page,
				limit: req.body.pagination.perpage
			};
			let allUsers = await User.aggregatePaginate(users, options);
			//console.log("149>>",allUsers); process.exit();
			return allUsers;
		} catch (e) {
			throw (e);
		}
	},

	getAllActiveUsersByRole: async (role) => {

		try {
			return await User.aggregate([{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'want_tos',
						localField: 'want_to',
						foreignField: '_id',
						as: 'want_tos'
					}
				},
				{
					$unwind: {
						path: "$want_tos",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'countries'
					}
				},
				{
					$unwind: {
						path: "$countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'courses',
						localField: 'course',
						foreignField: '_id',
						as: 'course_details'
					}
				},
				{
					$unwind: {
						path: "$course_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: {
						'user_role.role': role,
						"isActive": true,
						"isDeleted": false
					}
				}
			]).exec();
		} catch (e) {
			console.log('repo error', e.message);
			throw (e);
		}
	},

	getAllActiveUsersByRoleChat: async (role) => {
		try {

			let users;
			if (role == "mentor") {
				users = await User.aggregate([{
						$lookup: {
							from: 'roles',
							localField: 'role',
							foreignField: '_id',
							as: 'user_role'
						}
					},
					{
						$unwind: '$user_role'
					},
					{
						$lookup: {
							from: 'chats',
							localField: '_id',
							foreignField: 'mentor_id',
							as: 'chat_details'
						}
					},
					{
						$unwind: {
							path: "$chat_details",
							preserveNullAndEmptyArrays: true
						}
					},
					{
						$match: {
							'user_role.role': role,
							"isActive": true,
							"isDeleted": false
						}
					}
				]);
			} else {
				users = await User.aggregate([{
						$lookup: {
							from: 'roles',
							localField: 'role',
							foreignField: '_id',
							as: 'user_role'
						}
					},
					{
						$unwind: '$user_role'
					},
					{
						$lookup: {
							from: 'chats',
							localField: '_id',
							foreignField: 'student_id',
							as: 'chat_details'
						}
					},
					{
						$unwind: {
							path: "$chat_details",
							preserveNullAndEmptyArrays: true
						}
					},
					{
						$match: {
							'user_role.role': role,
							"isActive": true,
							"isDeleted": false
						}
					}
				]);
			}
			return users;
		} catch (e) {
			throw (e);
		}
	},
	getAllStudetListCourseWise: async (searchBy, studentArr = '') => {
		var conditions = {};
		var and_clauses = [];
		and_clauses.push({
			'user_role.role': 'student',
			"isActive": true,
			"isDeleted": false
		});

		if (_.isObject(searchBy) && _.has(searchBy, 'course')) {
			if (!_.isEmpty(searchBy.course)) {
				and_clauses.push({
					'course': mongoose.Types.ObjectId(searchBy.course)
				});
			}
		}
		if (_.isObject(searchBy) && _.has(searchBy, 'country')) {
			if (!_.isEmpty(searchBy.country)) {
				and_clauses.push({
					'country': mongoose.Types.ObjectId(searchBy.country)
				});
			}
		}

		if (studentArr != '') {
			and_clauses.push({
				'_id': {
					'$nin': studentArr
				}
			});
		}
		conditions["$and"] = and_clauses;
		try {
			return await User.aggregate([{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'want_tos',
						localField: 'want_to',
						foreignField: '_id',
						as: 'want_tos'
					}
				},
				{
					$unwind: {
						path: "$want_tos",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'countries'
					}
				},
				{
					$unwind: {
						path: "$countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'courses',
						localField: 'course',
						foreignField: '_id',
						as: 'course_details'
					}
				},
				{
					$unwind: {
						path: "$course_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: conditions
				}
			]).exec();
		} catch (e) {
			console.log('repo error', e.message);
			throw (e);
		}
	},

	getAllStudetList: async (condition = '') => {
		var conditions = {};
		var and_clauses = [];
		and_clauses.push({
			'user_role.role': 'student',
			"isActive": true,
			"isDeleted": false
		});

		if (condition != '') {
			and_clauses.push({
				'_id': {
					'$nin': condition
				}
			});
		}
		conditions["$and"] = and_clauses;
		try {
			return await User.aggregate([{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'want_tos',
						localField: 'want_to',
						foreignField: '_id',
						as: 'want_tos'
					}
				},
				{
					$unwind: {
						path: "$want_tos",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'countries'
					}
				},
				{
					$unwind: {
						path: "$countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'courses',
						localField: 'course',
						foreignField: '_id',
						as: 'course_details'
					}
				},
				{
					$unwind: {
						path: "$course_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: conditions
				}
			]).exec();
		} catch (e) {
			console.log('repo error', e.message);
			throw (e);
		}
	},


	getUserDetails: async (id, role, loggedin_user_id) => {
		try {
			// console.log("206>>", role, id, loggedin_user_id);
			let users;
			if (role == "mentor") {
				users = await User.aggregate([{
						$match: {
							'_id': mongoose.Types.ObjectId(id),
							"isActive": true,
							"isDeleted": false
						}
					},
					{
						$lookup: {
							from: 'roles',
							localField: 'role',
							foreignField: '_id',
							as: 'user_role'
						}
					},
					{
						$unwind: '$user_role'
					},
					{
						$lookup: {
							from: 'chats',
							localField: '_id',
							foreignField: 'mentor_id',
							as: 'chat_details'
						}
					},
					{
						$unwind: {
							path: "$chat_details"
						}
					},
					{
						$match: {
							'chat_details.mentor_id': mongoose.Types.ObjectId(id),
							'chat_details.student_id': mongoose.Types.ObjectId(loggedin_user_id)
						}
					}
				]);
			} else {
				users = await User.aggregate([{
						$match: {
							'_id': mongoose.Types.ObjectId(id),
							"isActive": true,
							"isDeleted": false
						}
					},
					{
						$lookup: {
							from: 'roles',
							localField: 'role',
							foreignField: '_id',
							as: 'user_role'
						}
					},
					{
						$unwind: '$user_role'
					},
					{
						$lookup: {
							from: 'chats',
							localField: '_id',
							foreignField: 'student_id',
							as: 'chat_details'
						}
					},

					{
						$unwind: {
							path: "$chat_details"
						}
					},
					{
						$match: {
							'chat_details.student_id': mongoose.Types.ObjectId(id),
							'chat_details.mentor_id': mongoose.Types.ObjectId(loggedin_user_id)
						}
					}

				]);
			}

			// console.log("332>>", users);

			return users[0];
		} catch (e) {
			throw (e);
		}
	},

	getById: async (id) => {
		let user = await User.findById(id).populate('role').exec();
		try {
			if (!user) {
				return null;
			}
			return user;
		} catch (e) {
			return e;
		}
	},

	getByIdUser: async (id) => {
		try {
			let users = await User.aggregate([{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$unwind: {
						path: "$service",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'packages',
						localField: 'service.package_id',
						foreignField: '_id',
						as: 'service_val'
					}
				},
				{
					$unwind: {
						path: "$service_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'country_val'
					}
				},
				{
					$unwind: {
						path: "$country_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'languages',
						localField: 'language',
						foreignField: '_id',
						as: 'language_val'
					}
				},
				{
					$unwind: {
						path: "$language_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'courses',
						localField: 'course',
						foreignField: '_id',
						as: 'course_val'
					}
				},
				{
					$unwind: {
						path: "$course_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: {
						"isActive": true,
						"isDeleted": false,
						"_id": mongoose.Types.ObjectId(id)
					}
				},
				{
					$group: {
						_id: "$_id",
						profile: {
							$first: "$$ROOT"
						},
						service: {
							$addToSet: "$service"
						},
						service_val: {
							$addToSet: "$service_val"
						}
					}
				}
			]);
			return users[0];
		} catch (e) {
			throw (e);
		}
	},

	getByIdUniversity: async (id) => {
		try {
			let users = await User.aggregate([{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$lookup: {
						from: 'universities',
						localField: '_id',
						foreignField: 'user_id',
						as: 'universities'
					}
				},
				{
					$unwind: {
						path: "$universities",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'countries'
					}
				},
				{
					$unwind: {
						path: "$countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: {
						"isActive": true,
						"isDeleted": false,
						"_id": mongoose.Types.ObjectId(id)
					}
				},
				{
					$group: {
						_id: "$_id",
						university_name: {
							$first: "$universities.university_name"
						},
						profile_image: {
							$first: "$profile_image"
						},
						address: {
							$first: "$universities.address"
						},
						city: {
							$first: "$universities.city"
						},
						state: {
							$first: "$universities.state"
						},
						postal_code: {
							$first: "$universities.postal_code"
						},
						email: {
							$first: "$universities.email"
						},
						phone: {
							$first: "$universities.phone"
						},
						website: {
							$first: "$universities.website"
						},
						registration_no: {
							$first: "$universities.registration_no"
						},
						country: {
							$first: "$country"
						},
						country_name: {
							$first: "$countries.country_name"
						}
					}
				}
			]);
			return users[0];
		} catch (e) {
			throw (e);
		}
	},

	getByIdMentor: async (id) => {
		try {
			let users = await User.aggregate([{
					$unwind: '$service'
				},
				{
					$unwind: '$specialization'
				},
				{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: {
						path: "$user_role",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'country_details'
					}
				},
				{
					$unwind: {
						path: "$country_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'languages',
						localField: 'language',
						foreignField: '_id',
						as: 'language_details'
					}
				},
				{
					$unwind: {
						path: "$language_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: {
						"isActive": true,
						"isDeleted": false,
						"_id": mongoose.Types.ObjectId(id)
					}
				},
				{
					$group: {
						_id: "$_id",
						profile: {
							$first: "$$ROOT"
						},
						service: {
							$addToSet: "$service"
						},
						specialisation: {
							$addToSet: "$specialization"
						}
					}
				},
				{
					$project: {
						_id: 1,
						full_name: "$profile.full_name",
						email: "$profile.email",
						address: "$profile.address",
						university_name: "$profile.university_name",
						gender: "$profile.gender",
						short_description: "$profile.short_description",
						phone: "$profile.phone",
						education: "$profile.edu_level.level_name",
						country: "$profile.country_details.country_name",
						service: "$service",
						specialisation: "$specialisation",
					}
				}
			]);
			//console.log("289>>",users);
			//process.exit();
			return users[0];
		} catch (e) {
			throw (e);
		}
	},

	getByIdStudent: async (id) => {
		try {
			let users = await User.aggregate([{
					$unwind: '$activities'
				},
				{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: {
						path: "$user_role",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'country_details'
					}
				},
				{
					$unwind: {
						path: "$country_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'languages',
						localField: 'language',
						foreignField: '_id',
						as: 'language_details'
					}
				},
				{
					$unwind: {
						path: "$language_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'projects',
						localField: '_id',
						foreignField: 'user_id',
						as: 'project_details'
					}
				},
				{
					$unwind: {
						path: "$project_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'want_tos',
						localField: 'want_to',
						foreignField: '_id',
						as: 'want_details'
					}
				},
				{
					$unwind: {
						path: "$want_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: {
						"isActive": true,
						"isDeleted": false,
						"_id": mongoose.Types.ObjectId(id)
					}
				},
				{
					$group: {
						_id: "$_id",
						profile: {
							$first: "$$ROOT"
						},
						project: {
							$addToSet: "$project_details"
						},
						activities: {
							$addToSet: "$activities"
						}
					}
				},
				{
					$project: {
						_id: 1,
						full_name: "$profile.full_name",
						email: "$profile.email",
						address: "$profile.address",
						school_name: "$profile.school_name",
						gender: "$profile.gender",
						short_description: "$profile.short_description",
						phone: "$profile.phone",
						education: "$profile.edu_level.level_name",
						country: "$profile.country_details.country_name",
						want_to: "$profile.want_details.name",
						project: "$project",
						activities: "$activities",
					}
				}
			]);
			//console.log("437>>",users);

			return users[0];
		} catch (e) {
			throw (e);
		}
	},

	updateById: async (data, id) => {
		try {
			return await User.findByIdAndUpdate(id, data, {
				new: true,
				upsert: true
			}).lean().exec();
		} catch (error) {
			console.log('user repo error', error.message);
			return error;
		}
	},

	mentorsearch: async (params, mentorArr = '') => {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			and_clauses.push({
				"user_role.role": "mentor"
			});

			if (_.isObject(params) && _.has(params, 'country')) {
				if (!_.isEmpty(params.country)) {
					and_clauses.push({
						'country': mongoose.Types.ObjectId(params.country)
					});
				}
			}
			if (_.isObject(params) && _.has(params, 'course')) {
				if (!_.isEmpty(params.course)) {
					and_clauses.push({
						'course': mongoose.Types.ObjectId(params.course)
					});
				}
			}
			if (_.isObject(params) && _.has(params, 'university')) {
				if (!_.isEmpty(params.university)) {
					and_clauses.push({
						"university_id": mongoose.Types.ObjectId(params.university)
					});
				}
			}

			/*
			 *We off this piece of this because when we try to find the mentors based on University Id
			 *Then due to this we are unable to find the other users as well.
			 *And the reason is main developer is not able to describe why he used this piece of code
			 *over here. For this KK, with the instruction of main developer off this area because app is working fine.
			 */
			// if (mentorArr != '') {
			// 	and_clauses.push({
			// 		'_id': {
			// 			'$nin': mentorArr
			// 		}
			// 	});
			// }


			conditions['$and'] = and_clauses;
			let users = await User.aggregate([{
					$unwind: {
						path: "$service",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'packages',
						localField: 'service.package_id',
						foreignField: '_id',
						as: 'service_val'
					}
				},
				{
					$unwind: {
						path: "$service_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: '$user_role'
				},
				{
					$lookup: {
						from: 'education_levels',
						localField: 'education_level',
						foreignField: '_id',
						as: 'edu_level'
					}
				},
				{
					$unwind: {
						path: "$edu_level",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'courses',
						localField: 'course',
						foreignField: '_id',
						as: 'course_details'
					}
				},
				{
					$unwind: {
						path: "$course_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'country_val'
					}
				},
				{
					$unwind: {
						path: "$country_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'languages',
						localField: 'language',
						foreignField: '_id',
						as: 'language_val'
					}
				},
				{
					$unwind: {
						path: "$language_val",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: conditions
				},

				{
					$group: {
						_id: "$_id",
						profile: {
							$first: "$$ROOT"
						},
						service: {
							$addToSet: "$service"
						},
						service_val: {
							$addToSet: "$service_val"
						}
					}
				},
				{
					$project: {
						_id: 1,
						full_name: "$profile.full_name",
						surname: "$profile.surname",
						email: "$profile.email",
						profile_image: "$profile.profile_image",
						logo: "$profile.logo",
						language: "$profile.language_val",
						country: "$profile.country_val",
						university_name: "$profile.university_name",
						university_id: "$profile.university_id",
						course: "$profile.course_details",
						gender: "$profile.gender",
						short_description: "$profile.short_description",
						education_level: "$profile.edu_level",
						service: "$service_val",
						register_type: "$profile.register_type",
					}
				}
			]);
			return users;
		} catch (e) {
			throw (e);
		}
	},

	jobsearch: async (params, menteeIds) => {
		try {
			var conditions = {};
			var and_clauses = [];
			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			and_clauses.push({
				"user_role.role": "student"
			});
			if (_.has(params, "country")) {
				and_clauses.push({
					"country_details.country_name": {
						$regex: params.country,
						$options: 'i'
					}
				});
			}
			if (_.has(params, "course")) {
				and_clauses.push({
					"course_details.course_name": {
						$regex: params.course,
						$options: 'i'
					}
				});
			}

			conditions['$and'] = and_clauses;
			console.log("783>>", conditions); //process.exit();
			let users = await User.aggregate([{
					$unwind: '$specialization'
				},
				{
					$lookup: {
						from: 'roles',
						localField: 'role',
						foreignField: '_id',
						as: 'user_role'
					}
				},
				{
					$unwind: {
						path: "$user_role",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$lookup: {
						from: 'countries',
						localField: 'country',
						foreignField: '_id',
						as: 'country_details'
					}
				},
				{
					$unwind: {
						path: "$country_details",
						preserveNullAndEmptyArrays: true
					}
				},
				//{
				//	$lookup: {
				//		from: 'languages',
				//		localField: 'language',
				//		foreignField: '_id',
				//		as: 'language_details'
				//	}
				//},
				//{
				//	$unwind: {
				//		path: "$language_details",
				//		preserveNullAndEmptyArrays: true
				//	}
				//},
				//{
				//	$lookup: {
				//		from: 'education_levels',
				//		localField: 'education_level',
				//		foreignField: '_id',
				//		as: 'edu_level'
				//	}
				//},
				//{
				//	$unwind: {
				//		path: "$edu_level",
				//		preserveNullAndEmptyArrays: true
				//	}
				//},
				//{
				//	$lookup: {
				//		from: 'projects',
				//		localField: '_id',
				//		foreignField: 'user_id',
				//		as: 'project_details'
				//	}
				//},
				//{
				//	$unwind: {
				//		path: "$project_details",
				//		preserveNullAndEmptyArrays: true
				//	}
				//},
				//{
				//	$lookup: {
				//		from: 'want_tos',
				//		localField: 'want_to',
				//		foreignField: '_id',
				//		as: 'wantto_details'
				//	}
				//},
				//{
				//	$unwind: {
				//		path: "$wantto_details",
				//		preserveNullAndEmptyArrays: true
				//	}
				//},
				{
					$lookup: {
						from: 'courses',
						localField: 'specialization.course',
						foreignField: '_id',
						as: 'course_details'
					}
				},
				{
					$unwind: {
						path: "$course_details",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: conditions
				},
				{
					$group: {
						_id: "$_id",
						profile: {
							$first: "$$ROOT"
						},
						country: {
							$first: "$country_details"
						},
						language: {
							$first: "$language_details"
						},
						education: {
							$first: "$edu_level"
						},
						want_to: {
							$first: "$wantto_details"
						},
						activities: {
							$addToSet: "$activities"
						},
						course: {
							$addToSet: "$course_details"
						},
						projects: {
							$addToSet: "$project_details"
						},
					}
				},
				{
					$project: {
						_id: 1,
						name: "$profile.full_name",
						email: "$profile.email",
						password: "$profile.password",
						profile_image: "$profile.profile_image",
						school_name: "$profile.school_name",
						short_description: "$profile.short_description",
						address: "$profile.address",
						university_name: "$profile.university_name",
						gender: "$profile.gender",
						phone: "$profile.phone",
						role: "$user_role.role",
						push_notification: "$push_notification",
						display_phone_number: "$display_mobile_no",
						activities: "$activities",
						specialization: "$course",
						country: "$country.country_name",
						language: "$language.title",
						education: "$edu_level.level_name",
						want_to: "$want_to",
						project: "$projects"
					}
				}
			]);

			console.log("925>>", users); //process.exit();
			return users;
		} catch (e) {
			throw (e);
		}
	},

	getByField: async (params) => {
		let user = await User.findOne(params).exec();
		try {
			if (!user) {
				return null;
			}
			return user;

		} catch (e) {
			return e;
		}
	},

	getAllByField: async (params) => {
		let user = await User.find(params).exec();
		try {
			if (!user) {
				return null;
			}
			return user;

		} catch (e) {
			return e;
		}
	},

	getLimitUserByField: async (params) => {
		let user = await User.find(params).populate('role').limit(5).sort({
			_id: -1
		}).exec();
		try {
			if (!user) {
				return null;
			}
			return user;

		} catch (e) {
			return e;
		}
	},

	delete: async (id) => {
		try {
			let user = await User.findById(id);
			if (user) {
				let userDelete = await User.remove({
					_id: id
				}).exec();
				if (!userDelete) {
					return null;
				}
				return userDelete;
			}
		} catch (e) {
			return e;
		}
	},

	deleteByField: async (field, fieldValue) => {
		//todo: Implement delete by field
	},

	updateById: async (data, id) => {
		try {
			console.log(data);
			let user = await User.findByIdAndUpdate(id, data, {
				new: true
			});
			if (!user) {
				return null;
			}
			return user;
		} catch (e) {
			console.log('update by id', e.message);
			return e;
		}
	},

	updateByField: async (field, fieldValue, data) => {
		try {
			let user = await User.findByIdAndUpdate(fieldValue, field, {
				new: true
			});
			if (!user) {
				return null;
			}
			return user;
		} catch (e) {
			return e;
		}
	},

	save: async (data) => {
		try {
			let user = await User.create(data);

			if (!user) {
				return null;
			}
			return user;
		} catch (e) {
			return e;
		}
	},

	forgotPassword: async (params) => {
		try {
			//console.log("439>>",params);
			let user = await User.findOne({
				email: params.email
			}).exec();
			if (!user) {
				throw {
					"status": 500,
					data: null,
					"message": 'Authentication failed. User not found.'
				}
			} else if (user) {
				let random_pass = Math.random().toString(36).substr(2, 9);
				let readable_pass = random_pass;
				random_pass = user.generateHash(random_pass);
				let user_details = await User.findByIdAndUpdate(user._id, {
					password: random_pass
				}).exec();
				if (!user_details) {
					throw {
						"status": 500,
						data: null,
						"message": 'User not found.'
					}
				} else {
					throw {
						"status": 200,
						data: readable_pass,
						"message": ""
					}
				}
				//return readable_pass;	
			}
		} catch (e) {
			return e;
		}
	},

	getUser: async (id) => {
		try {
			let user = await User.findOne({
				id
			}).exec();
			if (!user) {
				return null;
			}
			return user;
		} catch (e) {
			return e;
		}
	},

	getUserByField: async (data) => {
		try {
			let user = await User.findOne(data).populate('role').exec();
			if (!user) {
				return null;
			}
			return user;
		} catch (e) {
			return e;
		}
	},

	getTopMentors: async (limit) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			and_clauses.push({
				"user_role.role": {
					"$eq": "mentor"
				}
			});

			conditions['$and'] = and_clauses;

			return await User.aggregate([{
					"$lookup": {
						"from": "roles",
						"localField": "role",
						"foreignField": "_id",
						"as": "user_role"
					},
				},
				{
					$unwind: {
						path: "$user_role",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "courses",
						"localField": "course",
						"foreignField": "_id",
						"as": "courses"
					},
				},
				{
					$unwind: {
						path: "$courses",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "orders",
						"localField": "_id",
						"foreignField": "mentor_id",
						"as": "orders"
					},
				},
				{
					$unwind: {
						path: "$orders",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$match: conditions
				},
				{
					$group: {
						_id: "$_id",
						full_name: {
							$first: "$full_name"
						},
						profile_image: {
							$first: "$profile_image"
						},
						earning: {
							$sum: "$orders.package_price"
						},
						createdAt: {
							$first: "$createdAt"
						},
						orders: {
							$addToSet: "$orders"
						},
					}
				},
				{
					$project: {
						_id: "$_id",
						full_name: "$full_name",
						profile_image: "$profile_image",
						earning: "$earning",
						packages_sold: {
							$size: "$orders"
						},
						active_from: "$createdAt"
					}
				},
				{
					"$sort": {
						earning: -1
					}
				},
				{
					$limit: limit
				}
			]).exec();

		} catch (e) {
			return e;
		}
	},

	getMentorsListWithSearch: async (params) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			and_clauses.push({
				"role": {
					"$eq": "mentor"
				}
			});

			if (_.isObject(params) && _.has(params, 'country')) {
				if (!_.isEmpty(params.country)) {
					and_clauses.push({
						'country_id': mongoose.Types.ObjectId(params.country)
					});
				}
			}
			if (_.isObject(params) && _.has(params, 'course')) {
				if (!_.isEmpty(params.course)) {
					and_clauses.push({
						'course_id': mongoose.Types.ObjectId(params.course)
					});
				}
			}
			// if (_.isObject(params) && _.has(params, 'university')) {
			// 	if (!_.isEmpty(params.university)) {
			// 		and_clauses.push({
			// 			"university_name": {
			// 				$regex: params.university,
			// 				$options: 'i'
			// 			}
			// 		});
			// 	}
			// }

			conditions['$and'] = and_clauses;

			return await User.aggregate([{
					"$lookup": {
						"from": "roles",
						"localField": "role",
						"foreignField": "_id",
						"as": "user_role"
					},
				},
				{
					$unwind: {
						path: "$user_role",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "orders",
						"localField": "_id",
						"foreignField": "mentor_id",
						"as": "orders"
					},
				},
				{
					$unwind: {
						path: "$orders",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "courses",
						"localField": "course",
						"foreignField": "_id",
						"as": "courses"
					},
				},
				{
					$unwind: {
						path: "$courses",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "countries",
						"localField": "country",
						"foreignField": "_id",
						"as": "countries"
					},
				},
				{
					$unwind: {
						path: "$countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "mentors",
						"localField": "_id",
						"foreignField": "mentor_id",
						"as": "mentors"
					},
				},
				{
					$unwind: {
						path: "$mentors",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$group: {
						_id: "$_id",
						full_name: {
							$first: "$full_name"
						},
						role: {
							$first: "$user_role.role"
						},
						profile_image: {
							$first: "$profile_image"
						},
						country_id: {
							$first: "$country"
						},
						country_name: {
							$first: "$countries.country_name"
						},
						course_id: {
							$first: "$course"
						},
						course_name: {
							$first: "$courses.course_name"
						},
						earning: {
							$sum: "$orders.package_price"
						},
						createdAt: {
							$first: "$createdAt"
						},
						orders: {
							$addToSet: "$orders"
						},
						mentors: {
							$addToSet: "$mentors"
						},
						isDeleted: {
							$first: "$isDeleted"
						},
						isActive: {
							$first: "$isActive"
						}
					}
				},
				{
					$project: {
						_id: "$_id",
						full_name: "$full_name",
						role: "$role",
						profile_image: "$profile_image",
						course_id: "$course_id",
						course_name: "$course_name",
						country_id: "$country_id",
						country_name: "$country_name",
						students: {
							$size: "$mentors"
						},
						earning: "$earning",
						packages_sold: {
							$size: "$orders"
						},
						active_from: "$createdAt",
						isDeleted: "$isDeleted",
						isActive: "$isActive"
					}
				},
				{
					$match: conditions
				}
			]).exec();

		} catch (e) {
			return e;
		}
	},

	getStudentsListWithSearch: async (params) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			and_clauses.push({
				"role": {
					"$eq": "student"
				}
			});

			if (_.isObject(params) && _.has(params, 'country')) {
				if (!_.isEmpty(params.country)) {
					and_clauses.push({
						'country_id': mongoose.Types.ObjectId(params.country)
					});
				}
			}
			if (_.isObject(params) && _.has(params, 'course')) {
				if (!_.isEmpty(params.course)) {
					and_clauses.push({
						'course_id': mongoose.Types.ObjectId(params.course)
					});
				}
			}
			// if (_.isObject(params) && _.has(params, 'university')) {
			// 	if (!_.isEmpty(params.university)) {
			// 		and_clauses.push({
			// 			"university_name": {
			// 				$regex: params.university,
			// 				$options: 'i'
			// 			}
			// 		});
			// 	}
			// }

			conditions['$and'] = and_clauses;

			return await User.aggregate([{
					"$lookup": {
						"from": "roles",
						"localField": "role",
						"foreignField": "_id",
						"as": "user_role"
					},
				},
				{
					$unwind: {
						path: "$user_role",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "courses",
						"localField": "course",
						"foreignField": "_id",
						"as": "courses"
					},
				},
				{
					$unwind: {
						path: "$courses",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "countries",
						"localField": "country",
						"foreignField": "_id",
						"as": "countries"
					},
				},
				{
					$unwind: {
						path: "$countries",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "mentees",
						"localField": "_id",
						"foreignField": "student_id",
						"as": "mentees"
					},
				},
				{
					$unwind: {
						path: "$mentees",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					"$lookup": {
						"from": "users",
						"localField": "mentees.mentor_id",
						"foreignField": "_id",
						"as": "users"
					},
				},
				{
					$unwind: {
						path: "$users",
						preserveNullAndEmptyArrays: true
					}
				},
				{
					$group: {
						_id: "$_id",
						full_name: {
							$first: "$full_name"
						},
						role: {
							$first: "$user_role.role"
						},
						profile_image: {
							$first: "$profile_image"
						},
						mentor_name: {
							$first: "$users.full_name"
						},
						country_id: {
							$first: "$country"
						},
						country_name: {
							$first: "$countries.country_name"
						},
						course_id: {
							$first: "$course"
						},
						course_name: {
							$first: "$courses.course_name"
						},
						createdAt: {
							$first: "$createdAt"
						},
						isDeleted: {
							$first: "$isDeleted"
						},
						isActive: {
							$first: "$isActive"
						}
					}
				},
				{
					$project: {
						_id: "$_id",
						full_name: "$full_name",
						role: "$role",
						profile_image: "$profile_image",
						mentor_name: "$mentor_name",
						course_id: "$course_id",
						course_name: "$course_name",
						country_id: "$country_id",
						country_name: "$country_name",
						active_from: "$createdAt",
						isDeleted: "$isDeleted",
						isActive: "$isActive"
					}
				},
				{
					$match: conditions
				},
				{
					"$sort": {
						mentors: -1
					}
				},
			]).exec();

		} catch (e) {
			return e;
		}
	},

	getUsersCount: async (req) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			and_clauses.push({
				"user_role.role": {
					"$eq": "student"
				}
			});

			conditions['$and'] = and_clauses;

			let users = await User.aggregate([{
					"$lookup": {
						"from": "roles",
						"localField": "role",
						"foreignField": "_id",
						"as": "user_role"
					},
				},
				{
					$unwind: "$user_role"
				},
				{
					$match: conditions
				},
				{
					$group: {
						_id: "$user_role._id",
						name: {
							$first: "$user_role.role"
						},
						count: {
							$sum: 1
						}
					}
				},
				{
					"$sort": {
						_id: 1
					}
				},
			]).exec();
			return users;
		} catch (e) {
			throw (e);
		}
	},

	getMentorCount: async (req) => {
		try {
			var conditions = {};
			var and_clauses = [];

			and_clauses.push({
				"isDeleted": false,
				"isActive": true
			});
			//and_clauses.push({"status": "Active"});
			and_clauses.push({
				"user_role.role": "mentor"
			});

			conditions['$and'] = and_clauses;

			let users = await User.aggregate([{
					"$lookup": {
						"from": "roles",
						"localField": "role",
						"foreignField": "_id",
						"as": "user_role"
					},
				},
				{
					$unwind: "$user_role"
				},
				{
					$match: conditions
				},
				{
					$group: {
						_id: "$user_role._id",
						name: {
							$first: "$user_role.role"
						},
						count: {
							$sum: 1
						}
					}
				},
				{
					"$sort": {
						_id: 1
					}
				},
			]).exec();

			return users;
		} catch (e) {
			throw (e);
		}
	},


	getUserDetailsForWebsite: async (id, role, loggedin_user_id) => {
		try {
			let users;
			if (role == "mentor") {
				users = await User.aggregate([{
						$match: {
							'_id': mongoose.Types.ObjectId(id),
							"isActive": true,
							"isDeleted": false
						}
					},
					{
						$lookup: {
							from: 'roles',
							localField: 'role',
							foreignField: '_id',
							as: 'user_role'
						}
					},
					{
						$unwind: '$user_role'
					},
					{
						$lookup: {
							from: 'chats',
							localField: '_id',
							foreignField: 'mentor_id',
							as: 'chat_details'
						}
					},
					{
						$unwind: {
							path: "$chat_details"
						}
					},
					{
						$match: {
							'chat_details.mentor_id': mongoose.Types.ObjectId(id),
							'chat_details.university_id': mongoose.Types.ObjectId(loggedin_user_id)
						}
					}
				]);
			} else {
				users = await User.aggregate([{
						$match: {
							'_id': mongoose.Types.ObjectId(id),
							"isActive": true,
							"isDeleted": false
						}
					},
					{
						$lookup: {
							from: 'roles',
							localField: 'role',
							foreignField: '_id',
							as: 'user_role'
						}
					},
					{
						$unwind: '$user_role'
					},
					{
						$lookup: {
							from: 'chats',
							localField: '_id',
							foreignField: 'university_id',
							as: 'chat_details'
						}
					},

					{
						$unwind: {
							path: "$chat_details"
						}
					},
					{
						$match: {
							'chat_details.university_id': mongoose.Types.ObjectId(id),
							'chat_details.mentor_id': mongoose.Types.ObjectId(loggedin_user_id)
						}
					}

				]);
			}
			return users[0];
		} catch (e) {
			throw (e);
		}
	},


};

module.exports = userRepository;