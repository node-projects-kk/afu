const contactusRepo = require('contactus/repositories/contactus.repository');
const express = require('express');
const routeLabel = require('route-label');
const router = express.Router();
const namedRouter = routeLabel(router);
const querystring = require('querystring');
const gm = require('gm').subClass({ imageMagick: true });
const fs = require('fs');


class contactusController {
    constructor() {
        this.contactus = [];
        
    }

    async create (req, res){
        try{
            res.render('contactus/views/add.ejs', {
                page_name: 'contactus-management',
                page_title: 'Create contactus',
                user: req.user,
            });
           
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    }

    async store(req,res){
        try {
            let contactusDup = await contactusRepo.getByField({ name: req.body.name });
            if(contactusDup) {
                req.flash('error', "A contactus with same name already Exists");
                res.redirect(namedRouter.urlFor('contactus.create'));
            } else {
                let save = await contactusRepo.save(req.body);
                if(save){
                    req.flash('success', "contactus created successfully.");
                    res.redirect(namedRouter.urlFor('contactus.list'));
                }else{
                    req.flash('error', "Something went wrong!");
                    res.redirect(namedRouter.urlFor('contactus.create'));
                }
            }
        } catch(e) {
            return res.status(500).send({ message: e.message });
        }
    }

    /*
    // @Method: edit
    // @Description:  contactus edit page
    */
    async edit (req, res){
        try
        {
            let result = {};
            let contactus_edit = await contactusRepo.getById(req.params.id);
            if (!_.isEmpty(contactus_edit)) {
                result.contactus_data = contactus_edit;
                res.render('contactus/views/edit.ejs', {
                    page_name: 'contactus-management',
                    page_title: 'Update contactus',
                    user: req.user,
                    response: result
                });
            } else {
                req.flash('error', "Sorry contactus not found!");
                res.redirect(namedRouter.urlFor('contactus.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: update
    // @Description: contactus update action
    */
   async update (req, res){
    try {
        const contactusId = req.body.contactusId;
        let contactus = await contactusRepo.getByField({'name':req.body.name,_id:{$ne:contactusId}});
        // var slug = req.body.name.toLowerCase();
        // req.body.slug=slug.split(' ').join('-');
        if (_.isEmpty(contactus)) {
                let contactusIdUpdate = contactusRepo.updateById(req.body,contactusId)
                if(contactusIdUpdate) {
                    req.flash('success', "contactus Updated Successfully");
                    res.redirect(namedRouter.urlFor('contactus.list'));
                }
                
            }else{
            req.flash('error', "contactus is already availabe!");
            res.redirect(namedRouter.urlFor('contactus.edit', { id: contactusId }));
        }    
    }catch(e){
        return res.status(500).send({message: e.message});  
    }      
        
};

    /* @Method: list
    // @Description: To get all the contactus list from DB
    */
    async list (req, res){
            try{
                res.render('contactus/views/list.ejs', {
                    page_name: 'contactus-management',
                    page_title: 'Contactus List',
                    user: req.user,
                    
                });
        } catch(e){

            return res.status(500).send({message: e.message}); 
        }  
    };
   

    async getAll (req, res){
        try{
            let contactus = await contactusRepo.getAll(req);
            if(_.has(req.body, 'sort')){
                var sortOrder = req.body.sort.sort;
                var sortField = req.body.sort.field;
            }else{
                var sortOrder = -1;
                var sortField = '_id';
            }
            if(!_.has(req.body, 'pagination')){
                req.body.pagination.page = 1;
                eq.body.pagination.perpage = config.PAGINATION_PERPAGE
            }
            let meta = {"page": req.body.pagination.page, "pages": contactus.pageCount, "perpage": req.body.pagination.perpage, "total": contactus.totalCount, "sort": sortOrder, "field": sortField};
            return {status: 200, meta: meta, data:contactus.data, message: `Data fetched succesfully.`};
        } catch(e){
            return {status: 500,data: [],message: e.message};
        }
    }
    /*
    // @Method: status_change
    // @Description: contactus status change action
    */
    async statusChange (req, res){
        try {
            let contactus = await contactusRepo.getById(req.params.id);
            if(!_.isEmpty(contactus)){
                let contactusStatus = (contactus.status == 'Active') ? 'Inactive' : 'Active';
                let contactusUpdate = contactusRepo.updateById({ status: contactusStatus }, req.params.id);
                req.flash('success', "contactus status has changed successfully" );
                // res.send(preparationUpdate);
                res.redirect(namedRouter.urlFor('contactus.list'));
            } else {
                req.flash('error', "sorry contactus not found");
                res.redirect(namedRouter.urlFor('contactus.list')); 
            }
        } catch(e){
            return res.status(500).send({message: e.message}); 
        }
    };

    /* @Method: delete
    // @Description: contactus delete
    */
    async destroy (req, res){
        try{
            
            let contactusDelete = await contactusRepo.delete(req.params.id)
            // console.log(req.params.id);
            if(!_.isEmpty(contactusDelete)){
                req.flash('success','contactus Removed Successfully');
                res.redirect(namedRouter.urlFor('contactus.list'));
            } 
        }catch(e){
            return res.status(500).send({message: e.message});   
        } 
    };


}

module.exports = new contactusController();