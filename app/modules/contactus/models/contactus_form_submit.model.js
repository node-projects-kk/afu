const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const status = ["Active", "Inactive"];

const ContactUsFormSchema = new Schema({
	name:{type:String, default:''},
	phone: { type: String, default: '' },
	email: { type: String, default: '' },
	message: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
	isDeleted: {type: Boolean, default: false, enum: [true, false]},
	status: { type: String, default: "Active", enum: status },
});

// For pagination
ContactUsFormSchema.plugin(mongooseAggregatePaginate);

// create the model for users and expose it to our app
module.exports = mongoose.model('contactus-form', ContactUsFormSchema);