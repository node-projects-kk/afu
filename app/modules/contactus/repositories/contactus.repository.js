const mongoose = require('mongoose');
const ContactUsFormModel = require('contactus/models/contactus_form_submit.model');
const perPage = config.PAGINATION_PERPAGE;

const contactusFormRepository = {

    getAll: async (req) => {
        try {
            var conditions = {};
            var and_clauses = [];

            and_clauses.push({
                "isDeleted": false
            });

            if (_.isObject(req.body.query) && _.has(req.body.query, 'generalSearch')) {
               
               
                and_clauses.push({
                    $or: [{
                            'name': {
                                $regex: req.body.query.generalSearch,
                                $options: 'i'
                            }
                        }
                        
                    ]
                });
            }
            if (_.isObject(req.body.query) && _.has(req.body.query, 'Status')) {
                and_clauses.push({
                    "status": req.body.query.Status
                });
            }
            conditions['$and'] = and_clauses;
            // console.log(JSON.stringify(conditions))

            var sortOperator = {
                "$sort": {}
            };
            if (_.has(req.body, 'sort')) {
                var sortField = req.body.sort.field;
                if (req.body.sort.sort == 'desc') {
                    var sortOrder = -1;
                } else if (req.body.sort.sort == 'asc') {
                    var sortOrder = 1;
                }

                sortOperator["$sort"][sortField] = sortOrder;
            } else {
                sortOperator["$sort"]['_id'] = -1;
            }

            var aggregate = ContactUsFormModel.aggregate([{
                    $match: conditions
                },
                sortOperator
            ]);
          
            var options = {
                page: req.body.pagination.page,
                limit: req.body.pagination.perpage
            };
            let allContactUs = await ContactUsFormModel.aggregatePaginate(aggregate, options);
          
            return allContactUs;
        } catch (e) {
            throw (e);
        }
    },



    contactUsFormSave: async (data) =>{
        try {
            let contactusform = await ContactUsFormModel.create(data);
            if (!contactusform) {
                return null;
            }
            return contactusform;
        } catch (e) {
            return e;
        }
    },

    getById: async (id) => {
        let ContactUs = await ContactUsFormModel.findById(id).exec();
        try {
            if (!ContactUs) {
                return null;
            }
            return ContactUs;

        } catch (e) {
            return e;
        }
    },

    getByField: async (params) => {
        let ContactUs = await ContactUsFormModel.findOne(params).exec();
        try {
            if (!ContactUs) {
                return null;
            }
            return ContactUs;

        } catch (e) {
            return e;
        }
    },

    
    getAllByField: async (params) => {
        let ContactUs = await ContactUsFormModel.find(params).exec();
        try {
            if (!ContactUs) {
                return null;
            }
            return ContactUs;

        } catch (e) {
            return e;
        }
    },

    delete: async (id) => {
        try {
            let ContactUs = await ContactUsFormModel.findById(id);
            if (ContactUs) {
                let ContactUsDelete = await ContactUsFormModel.remove({
                    _id: id
                }).exec();
                if (!ContactUsDelete) {
                    return null;
                }
                return ContactUsDelete;
            }
        } catch (e) {
            throw e;
        }
    },


    updateById: async (data, id) => {
        try {
            let ContactUs = await ContactUsFormModel.findByIdAndUpdate(id, data, {
                new: true,
                upsert: true
            }).exec();
            if (!ContactUs) {
                return null;
            }
            return ContactUs;
        } catch (e) {
            return e;
        }
    },

    save: async (data) => {
        try {
            let ContactUs = await ContactUsFormModel.create(data);
            if (!ContactUs) {
                return null;
            }
            return ContactUs;
        } catch (e) {
            return e;
        }
    },

    
};

module.exports = contactusFormRepository;