const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

const ChatHistorySchema = new Schema({
  user_id: { type:Schema.Types.ObjectId,ref: 'User' },
  chat_id: { type:Schema.Types.ObjectId,ref: 'Chat' },
  last_message:{type:String, default:''},
  chat_date: { type: Date, default: Date.now() }
});

// For pagination
ChatHistorySchema.plugin(mongooseAggregatePaginate);
// create the model for users and expose it to our app
module.exports = mongoose.model('ChatHistory', ChatHistorySchema);