// Class definition
var KTFormControls = function () {

	var addUserValidation = function () {
		$("#frmCreateUser").validate({
			rules: {
				first_name: {
					required: true,
					letterswithbasicpunc: true
				},
				last_name: {
					required: true,
				},
				email: {
					required: true,
					email: true,
				},
				phone: {
					required: true,
					number: true
				},
				password: {
					required: true,
					minlength : 6
				},
				confirm_password: {
					required: true,
					minlength : 6,
					equalTo : "#password"
				},
			},
			messages: {
				first_name: {
					required: "Please enter first name",
					letterswithbasicpunc: "Please enter alphabets only"
				},
				last_name: {
					required: "Please enter last name",
				},
				email: {
					required: "Please enter email",
				},
				phone: {
					required: "Please enter phone number",
				},
				password: {
					required: "Please enter password",
				},
				confirm_password: {
					required: "Please enter confirm password",
				},
			},
			//display error alert on form submit  
			invalidHandler: function (event, validator) {
				KTUtil.scrollTop();
			},
			submitHandler: function (form) {
				form[0].submit(); // submit the form
			}
		});
	}

	
	var addFaqValidation = function () {
		$("#frmCreateFaq").validate({
			rules: {
				title: {
					required: true
				},
				content: {
					required: true,
				}
			},
			messages: {
				title: {
					required: "Please enter Title"
				},
				content: {
					required: "Please enter Content",
				}
			},
			//display error alert on form submit  
			invalidHandler: function (event, validator) {
				KTUtil.scrollTop();
			},
			submitHandler: function (form) {
				form[0].submit(); // submit the form
			}
		});
	}

	var editUserValidation = function () {
		$("#editUserFrm").validate({
			// define validation rules
			rules: {
				first_name: {
					required: true,
					letterswithbasicpunc: true
				},
				last_name: {
					required: true,
				},
				email: {
					required: true,
					email: true,
				},
				phone: {
					required: true,
					number: true
				}
			},
			messages: {
				first_name: {
					required: "Please enter first name",
					letterswithbasicpunc: "Please enter alphabets only"
				},
				last_name: {
					required: "Please enter last name",
				},
				email: {
					required: "Please enter email",
				},
				phone: {
					required: "Please enter phone number",
				},
			},
			//display error alert on form submit  
			invalidHandler: function (event, validator) {
				KTUtil.scrollTop();
			},
			submitHandler: function (form) {
				form[0].submit();
			}
		});
	}


var addMembershipValidation = function () {
$("#addMembershipFrm").validate({
rules: {
title: {
required: true,
},
content: {
required: true
},
price: {
required: true
}
},
messages: {
title: {
required: "Please enter Title",
},
content: {
required: "Please enter Description",
},
price: {
required: "Please enter Price",

}
},
//display error alert on form submit  
invalidHandler: function (event, validator) {
KTUtil.scrollTop();
},

submitHandler: function (form) {
form[0].submit(); // submit the form
}
});
}


var editMembershipValidation = function () {
$("#editMembershipFrm").validate({
// define validation rules
rules: {
title: {
required: true,
},
content: {
required: true
},
price: {
required: true
}
},
messages: {
title: {
required: "Please enter Title",
},
content: {
required: "Please enter Description",
},
price: {
required: "Please enter Price",

}
},
//display error alert on form submit  
invalidHandler: function (event, validator) {
KTUtil.scrollTop();
},

submitHandler: function (form) {
form[0].submit();
}
});
}


var addPromocodeValidation = function () {
$("#addPromocodeFrm").validate({
rules: {
code: {
required: true,
},
value: {
required: true,
number: true
},
expiry_date: {
required: true
}
},
messages: {
code: {
required: "Please enter Promo code",
},
value: {
required: "Please enter Promo code value",
},
expiry_date: {
required: "Please enter Promo code Expiry date",

}
},
//display error alert on form submit  
invalidHandler: function (event, validator) {
KTUtil.scrollTop();
},

submitHandler: function (form) {
form[0].submit(); // submit the form
}
});
}

var editPromocodeValidation = function () {
$("#editPromocodeFrm").validate({
rules: {
code: {
required: true,
},
value: {
required: true,
number: true
},
expiry_date: {
required: true
}
},
messages: {
code: {
required: "Please enter Promo code",
},
value: {
required: "Please enter Promo code value",
},
expiry_date: {
required: "Please enter Promo code Expiry date",

}
},
//display error alert on form submit  
invalidHandler: function (event, validator) {
KTUtil.scrollTop();
},

submitHandler: function (form) {
form[0].submit(); // submit the form
}
});
}


return {
// public functions
init: function () {

addUserValidation();
editUserValidation();
addMembershipValidation();
editMembershipValidation();
addPromocodeValidation();
editPromocodeValidation();
addFaqValidation();
}
};
}();

jQuery(document).ready(function () {
KTFormControls.init();
});