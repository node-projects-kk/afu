"use strict";
// Class definition
var KTDatatableReport = function() {
    // Private functions
    var options = {
        // datasource definition
        data: {
            type: 'remote',
            source: {
                read: {
                    url: `http://${window.location.host}/admin/report/getall`,
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
        },

        // layout definition
        layout: {
            scroll: true, // enable/disable datatable scroll both horizontal and
            // vertical when needed.
            height: 500, // datatable's body's fixed height
            footer: false // display/hide footer
        },

        // column sorting
        sortable: true,

        pagination: true,

        // columns definition

        columns: [
        {
            field: 'createdAt',
            title: 'Date',
            template: function (row) {
                return moment(row.createdAt).format("YYYY-MM-DD");
                },
            width:100    
        },  
        {
            field: 'user',
            title: 'User',
            template: function (row) {
                return row.user.first_name+' '+row.user.last_name;
            },
            width:150
        },  
        {
            field: 'title',
            title: 'Title',
            template: '{{title}}',
            width:150
        },

        {
            field: 'description',
            title: 'Description',
            template: '{{description}}',
            width:200
        },
        {
            field: 'status',
            title: 'Status',
            // callback function support for column rendering
            template: function(row) {

                var status = {
                    "New": {
                        'title': 'New',
                        'class': 'kt-badge--brand'
                    },
                    "Open": {
                        'title': 'Open',
                        'class': ' kt-badge--danger'
                    },
                    "Solved": {
                        'title': 'Solved',
                        'class': ' kt-badge--danger'
                    },
                };
                return '<span class="kt-badge ' + status[row.status].class +
                ' kt-badge--inline kt-badge--pill" data-id="'+row._id+'" >' + status[row.status].title +
                '</span>';
            },
            width:100
        }, {
            field: 'Actions',
            title: 'Actions',
            sortable: false,
            width: 110,
            overflow: 'visible',
            textAlign: 'left',
	        autoHide: false,
            template: function(row) {
	            return '\
                    \<a href="http://'+window.location.host+'/admin/report/edit/'+row._id+'" class="btn btn-sm btn-clean btn-icon btn-icon-sm" title="Edit">\
                        <i class="flaticon-edit"></i>\
                    </a>';
            },
        }],
    };

    // basic demo
    var reportSelector = function() {

        options.search = {
            input: $('#generalSearch'),
        };

        var datatable = $('#reportRecordSelection').KTDatatable(options);

        $('#kt_form_status').on('change', function() {
            datatable.search($(this).val(), 'Status');
        });

        $('#kt_form_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_form_status,#kt_form_type').selectpicker();

        datatable.on(
            'kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',
            function(e) {
                var checkedNodes = datatable.rows('.kt-datatable__row--active').nodes();
                var count = checkedNodes.length;
                $('#kt_datatable_selected_number').html(count);
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
        });

        $('#kt_modal_fetch_id').on('show.bs.modal', function(e) {
            var ids = datatable.rows('.kt-datatable__row--active').
            nodes().
            find('.kt-checkbox--single > [type="checkbox"]').
            map(function(i, chk) {
                return $(chk).val();
            });
            var c = document.createDocumentFragment();
            for (var i = 0; i < ids.length; i++) {
                var li = document.createElement('li');
                li.setAttribute('data-id', ids[i]);
                li.innerHTML = 'Selected record ID: ' + ids[i];
                c.appendChild(li);
            }
            $(e.target).find('.kt-datatable_selected_ids').append(c);
        }).on('hide.bs.modal', function(e) {
            $(e.target).find('.kt-datatable_selected_ids').empty();
        });

        $(document).on('click', '.ktDelete', function(){
            var elemID = $(this).attr('id').replace('del-', '');
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    window.location.href = `http://${window.location.host}/admin/report/delete/${elemID}`;
                }
            });
        }); 
        
     };

    

    return {
        // public functions
        init: function() {
            reportSelector();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableReport.init();
});