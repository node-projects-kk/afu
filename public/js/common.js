// For status change //
function statusChangeFunction(type, element) {

    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes,change it!",
        cancelButtonText: "No, cancel please!",

    }).then(function () {
        statusModifier(type, element);
    }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }).catch(swal.noop);
}

function statusModifier(type, element) {
    var id = $(element).attr('data-team');
    var url = apiBaseURL + "/" + type + "/status-change";
    var redirect_url = apiBaseURL + "/" + type + "/list";
    $.ajax({
        url: url,
        type: "POST",
        data: {
            "id": id
        },
        success: function (msg) {
            if (msg) {
                location.href = redirect_url;
            }
        }
    });
}
// For status change //

// For delete data //
$(document).on("click", ".delete", function (event) {
    var redirect_url = $(this).attr('href');
    event.preventDefault(); // prevent form submit
    swal({
        title: "Are you sure?",
        type: "error",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",

    }).then(function () {
        location.href = redirect_url;
    }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }).catch(swal.noop);

})
//Creator AutoComplete Search
$(document).ready(function () {

    
});
//End Creator AutoComplete Search




// Allow only decimal number //
$('.allownumericwithdecimal').on('input', function () {
    this.value = this.value
        .replace(/[^\d.]/g, '') // numbers and decimals only
        //.replace(/(^[\d]{2})[\d]/g, '$1')   // not more than 2 digits at the beginning
        .replace(/(\..*)\./g, '$1') // decimal can't exist more than once
        .replace(/(\.[\d]{2})./g, '$1'); // not more than 4 digits after decimal
});
/*$(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
    
    
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }

});*/

//ONLY FOR USER_PROFILE PAGE
$('.attribute').on('ifChecked ifUnchecked', function (event) {
    $('.attribute').valid();
    var getTextBoxId = "#attr_val_" + $(this).attr('data-id');
    if (event.type == 'ifChecked') {
        $(getTextBoxId).show();
    } else {
        $(getTextBoxId).parent()
            .find("em.invalid")
            .remove();
        $(getTextBoxId).val('');
        $(getTextBoxId).hide();

    }
});

/////  FOR Event Image Prevention ///////
$("#event_image_image").on("change", function (event) {
    var size = $(this).attr('data-team');
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($("#event_image_image")[0].files.length > size) {
        $(this).val('');
        $(".msg").text('Please Select Upto ' + size + ' Images');
    } else if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'x-ms-bmp']) == -1) {
        $(this).val('');
        $(".msg").text('Invalid Image Type');
    } else {
        $(".msg").text('');
    }
});
//for prventing first character to be space 
$("input").on("keypress", function (e) {
    var startPos = e.currentTarget.selectionStart;
    if (e.which === 32 && startPos == 0)
        e.preventDefault();
});
$("textarea").on("keypress", function (e) {
    var startPos = e.currentTarget.selectionStart;
    if (e.which === 32 && startPos == 0)
        e.preventDefault();
});


//Delete Image
$(document).on("click", ".img_delete", function (event) {
    event.preventDefault();
    var url = $(this).attr('href');
    var redirect_url = window.location.href;
    var id = redirect_url.substring(redirect_url.lastIndexOf('/') + 1);
    var img = $(this).attr('data-team');
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",

    }).then(function () {
        $.ajax({
            url: url,
            type: "POST",
            data: {
                "id": id,
                img_name: img
            },
            success: function (msg) {
                if (msg) {
                    location.href = redirect_url;
                }
            }
        });

    }, function (dismiss) {
        // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
        if (dismiss === 'cancel') {
            swal(
                'Cancelled',
                'Your data is safe :)',
                'error'
            )
        }
    }).catch(swal.noop);

});
//for banner Image Selection
$('input[name="banner_image"]').on('click', function (event) {
    if ($(this).attr('id') == 'existing_image') {

        $(".ex_img_0").prop("checked", true);
        $("#existing_banner_img").show();
        $('#event_img').hide();
        $('#event_video').hide();
        $('#type').val('exist_image');
    } else if ($(this).attr('id') == 'banner_image') {
        $(".ex_img_0").prop("checked", false);
        $('#existing_banner_img').hide();
        $('#event_img').show();
        $('#event_video').hide();
        $('#type').val('image');

    } else if ($(this).attr('id') == 'banner_video') {
        $(".ex_img_0").prop("checked", false);
        $('#existing_banner_img').hide();
        $('#event_img').hide();
        $('#event_video').show();
        $('#type').val('video');
    } else {
        $(".ex_img_0").prop("checked", false);
        $('#existing_banner_img').hide();
        $('#event_img').hide();
        $('#event_video').hide();
    }
});

$(document).on("change", "#event_image_image", function (event) {
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'x-ms-bmp']) == -1) {
        $(this).val('');
        $(".img_msg").text('Invalid Image Type');

    } else {
        $(".img_msg").text('');

    }
});
$(document).on("change", "#event_image_video", function (event) {
    var ext = $(this).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['x-flv', 'mp4', 'x-mpegURL', 'MP2T', '3gpp', 'quicktime', 'x-msvideo', 'x-ms-wmv']) == -1) {
        $(this).val('');
        $(".video_msg").text('Invalid Video Type.');

    } else {
        $(".video_msg").text('');

    }
});




$(".allownumericwithdecimals").on("keypress keyup blur", function (event) {
    //this.value = this.value.replace(/[^0-9\.]/g,'');
    var ignoredKeys = [8, 9, 37, 38, 39, 40];
    $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
    if (ignoredKeys.indexOf(event.which) >= 0) {
        return true;
    } else if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});

/* code for category page in admin panel add category */
var parentId = $('.parentCategory').val();
if (parentId != undefined) {
    parentId = parentId;
    if (parentId == 0) {
        $('.subCategory').css('display', 'none');
        $('.categoryIcon').css('display', 'block');
    } else {
        $('.subCategory').css('display', 'block');
        $('.categoryIcon').css('display', 'none');
        /* ajax for getting sub category */
        $.ajax({
            type: "GET",
            url: apiBaseURL + "/category/sub-category/" + parentId,
            dataType: "json",
            success: function (result) {
                if (result.length > 0) {
                    var html = "<option value=''>Select a sub category</option>";
                    $.each(result, function (index, value) {

                        html += "<option value=" + value._id + ">" + value.category_name + "</option>"
                        $('.subCat').html(html);
                    });
                } else {
                    $('.subCategory').css('display', 'none');
                }

            },
        })
    }
}
$('.parentCategory').change(function () {
    let parentId = $(this).val();

    if (parentId == 0) {
        $('.subCategory').css('display', 'none');
        $('.categoryIcon').css('display', 'block');
    } else {
        $('.subCategory').css('display', 'block');
        $('.categoryIcon').css('display', 'none');
        /* ajax for getting sub category */
        $.ajax({
            type: "GET",
            url: apiBaseURL + "/category/sub-category/" + parentId,
            dataType: "json",
            success: function (result) {
                if (result.length > 0) {
                    var html = "<option value=''>Select a sub category</option>";
                    $.each(result, function (index, value) {

                        html += "<option value=" + value._id + ">" + value.category_name + "</option>"
                        $('.subCat').html(html);
                    });
                } else {
                    $('.subCategory').css('display', 'none');
                }

            },
        })
    }
});

/* end category add section */



$('#book_id').change(function () {
    alert("SSS")
    let Id = $(this).val();
    $.ajax({
        type: "GET",
        url: apiBaseURL + "/book/chapterBy/" + Id,
        dataType: "json",
        success: function (result) {
            console.log(result)
            if (result.length > 0) {
                var html = "<option value=''>Select a chapter</option>";
                $.each(result, function (value) {

                    html += "<option value=" + value.chapter_num + ">" + value.chapter_num + "</option>"
                    $('#chapter_no').html(html);
                });
            } else {
                var html = "<option value=''>Select a chapter</option>";
                $('#chapter_no').html(html);
            }

        },
    })
});
/* end section*/

/* city as per state*/
$('.state').change(function () {
    let stateId = $(this).val();

    $.ajax({
        type: "GET",
        url: apiBaseURL + "/city/" + stateId,
        dataType: "json",
        success: function (result) {
            if (result.length > 0) {
                var html = "<option value=''>Select a City</option>";
                $.each(result, function (index, value) {

                    html += "<option value=" + value._id + ">" + value.city_name + "</option>"
                    $('.city').html(html);
                });
            } else {
                var html = "<option value=''>Select a City</option>";
                $('.city').html(html);
            }

        },
    })
});


/* data table add for vehicle owner */
$('#vehicleOwner').DataTable({ "ordering": false, "paging": true });
$('#shopOwner').DataTable({ "ordering": false });
$('.cityRoadside').select2();

/* add moe for sub category question*/
let i = 1;
$('.addMore').click(function () {
    $('.appendMore').append('<div class="row onRemove"><div class="form-group col-md-6 mb-2"><label for="answer" class="control-label">Answer</label><div class="controls"><input id="answer" name="answer[' + i + ']" type="text"placeholder="Enter your Answer" class="form-control required" required data-validation-required-message="This field is required" value="" /></div></div><div></div><div><button type="button" class="btn btn-danger pull-right removeMore">Remove</button></div>');
    i++;
});

$(document).on("click", ".removeMore", function () {
    $(this).closest('.onRemove').remove()
})
/* end section */

$('.searchByBankAccounts').click(function(){
    var accId = 'all';
    var AccountName = $(this).attr('data-AccountName');
    if($(this).attr('data-AccountID')!='')
    {
        accId = $(this).attr('data-AccountID');
    }
    $.ajax({
        type: "GET",
        url: apiBaseURL + "/bank-transactions/search/ajax/" + accId,
        dataType: "json",
        success: function (result) {
            //alert(JSON.stringify(result));
            var content = '';
            
            for (var i = 0; i < result.response.transactions.length; i++) {
                var dt = (result.response.transactions[i].DateString).split('T')
                content += '<tr>';
                content += '<td>' + moment(dt[0]).format('ll') + '</td>';
                content += '<td>' + result.response.transactions[i].Contact.Name + '</td>';
                content += '<td>' + result.response.transactions[i].BankAccount.Name + '</td>';
                if(result.response.transactions[i].Type=='SPEND'){
                    content += '<td><span class="kt-font-bold kt-font-danger">' + result.response.transactions[i].Total.toFixed(2) + '</td>';
                }
                else
                {
                    content += '<td><span class="kt-font-bold kt-font-success">' + result.response.transactions[i].Total.toFixed(2) + '</td>';
                }
                content += '</tr>';
            }
            $('#bank_transaction_table tbody').html(content);
            //alert(AccountName);
            $('#transactionSearchBy').html(AccountName);

        },
    });
    //alert($(this).attr('data-AccountID'));
    //alert($(this).attr('data-AccountName'));
});


$("#searchByBankAccID").change(function(){
    var accId = 'all';
    if($(this).val()!='')
    {
        accId = $(this).val();
    }
    
        
        $.ajax({
            type: "GET",
            url: apiBaseURL + "/bank-transactions/search/ajax/" + accId,
            dataType: "json",
            success: function (result) {
                //alert(JSON.stringify(result));
                var content = '';
                
                for (var i = 0; i < result.response.transactions.length; i++) {
                    var dt = (result.response.transactions[i].DateString).split('T')
                    content += '<tr>';
                    content += '<td>' + moment(dt[0]).format('ll') + '</td>';
                    content += '<td>' + result.response.transactions[i].Contact.Name + '</td>';
                    content += '<td>' + result.response.transactions[i].BankAccount.Name + '</td>';
                    if(result.response.transactions[i].Type=='SPEND'){
                        content += '<td><span class="kt-font-bold kt-font-danger">' + result.response.transactions[i].Total.toFixed(2) + '</td>';
                    }
                    else
                    {
                        content += '<td><span class="kt-font-bold kt-font-success">' + result.response.transactions[i].Total.toFixed(2) + '</td>';
                    }
                    content += '</tr>';
                }
                $('#bank_transaction_table tbody').html(content);
                /*
                if (result.length > 0) {
                    var html = "<option value=''>Select a City</option>";
                    $.each(result, function (index, value) {

                        html += "<option value=" + value._id + ">" + value.city_name + "</option>"
                        $('.city').html(html);
                    });
                } else {
                    var html = "<option value=''>Select a City</option>";
                    $('.city').html(html);
                }
                */

            },
        });
    
});

$(document).on('click', '.ctrlTransactionDetail', function(){
    var transactionID = $(this).attr('data-id');

    $.ajax({
        type: "GET",
        url: apiBaseURL + "/bank-transactions/detail/ajax/" + transactionID,
        dataType: "json",
        success: function (result) {
            console.log(result);
            var str = '';
            str += '<div class="kt-widget12">';
            str += '<div class="kt-widget12__content">';
            str += '<div class="kt-widget12__item">';
            str += '<div class="kt-widget12__info">';
            str += '<span class="kt-widget12__desc">Account</span>';
            str += '<span class="kt-widget12__value">'+result.data.BankAccount.Name+'</span>';
            str += '</div>';
            str += '<div class="kt-widget12__info">';
            str += '<span class="kt-widget12__desc">Vendor</span>';
            str += '<span class="kt-widget12__value">'+result.data.Contact.Name+'</span>';
            str += '</div>';            
            str += '</div>';
            str += '<div class="kt-widget12__item">';
            str += '<div class="kt-widget12__info">';
            str += '<span class="kt-widget12__desc">Status</span>';
            str += '<span class="kt-widget12__value">'+result.data.Status+'</span>';
            str += '</div>';
            str += '<div class="kt-widget12__info">';
            str += '<span class="kt-widget12__desc">Type</span>';
            str += '<span class="kt-widget12__value">'+result.data.Type+'</span>';
            str += '</div>';
            str += '</div>';

            str += '<div class="kt-widget12__item">';
            str += '<div class="kt-widget12__info">';
            str += '<span class="kt-widget12__desc">Sub Total + Tax</span>';
            str += '<span class="kt-widget12__value">'+result.data.SubTotal+'('+result.data.CurrencyCode+') + ' +result.data.TotalTax+'('+result.data.CurrencyCode+')</span>';
            str += '</div>';
            str += '<div class="kt-widget12__info">';
            str += '<span class="kt-widget12__desc">Total</span>';
            str += '<span class="kt-widget12__value">'+result.data.Total+'('+result.data.CurrencyCode+')</span>';
            str += '</div>';
            str += '</div>';

            str += '</div>';
            str += '</div>';
												
            
            $('#transactionDetailBody').html(str);
        },
    });
});


