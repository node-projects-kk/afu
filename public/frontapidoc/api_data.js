define({ "api": [
  {
    "type": "get",
    "url": "/chat/createSessionToken",
    "title": "Intregate firebase chat",
    "version": "1.0.0",
    "group": "REST_API_CHAT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Chat started successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_CHAT",
    "name": "GetChatCreatesessiontoken"
  },
  {
    "type": "get",
    "url": "/chat/createToken/:sessionId",
    "title": "Intregate Tokbox chat",
    "version": "1.0.0",
    "group": "REST_API_CHAT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": [],\n    \"message\": \"Chat token create successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_CHAT",
    "name": "GetChatCreatetokenSessionid"
  },
  {
    "type": "post",
    "url": "/chat/new/create",
    "title": "Chat page",
    "version": "1.0.0",
    "group": "REST_API_CHAT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "mentor_id",
            "description": "<p>Mentor ID</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "student_id",
            "description": "<p>Student ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"chat_token\": \"dPucrZQBwqIi0o8TlTwsseaYr2sjkTEWHJHer8PGtVI\",\n        \"chat_date\": \"2019-07-23T10:46:57.731Z\",\n        \"_id\": \"5d36e5ba24bf1f6694936c0e\",\n        \"mentor_id\": \"5d31830d6d84b8436af1ce32\",\n        \"student_id\": \"5d309257ae2ba07b64ea59f4\",\n        \"__v\": 0\n    },\n    \"message\": \"Chat start\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_CHAT",
    "name": "PostChatNewCreate"
  },
  {
    "type": "post",
    "url": "/chat/sendattachment",
    "title": "Send Attachment API",
    "version": "1.0.0",
    "group": "REST_API_CHAT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "file",
            "description": "<p>File</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": \"1563802441970_ton.pdf\",\n    \"message\": \"Attachment send successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_CHAT",
    "name": "PostChatSendattachment"
  },
  {
    "type": "post",
    "url": "/chat/sendpicture",
    "title": "Send Picture API",
    "version": "1.0.0",
    "group": "REST_API_CHAT",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "file",
            "description": "<p>Image</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": \"image_1563802441970_img2.jpg\",\n    \"message\": \"Picture send successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_CHAT",
    "name": "PostChatSendpicture"
  },
  {
    "type": "GET",
    "url": "/messagelist",
    "title": "Message List(Inbox)",
    "version": "1.0.0",
    "group": "REST_API_MESSAGE",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access Token</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n\t\"_id\":\"5d3188206d84b8436af1ce37\",\n\t\"mentor_id\":\"5d306dc6822fa522c79acd21\",\n\t\"student_id\":\"5d306346822fa522c79a2dec\",\n\t\"count\":5,\n\t\"message_details\":[\n\t\t{\n\t\t\tmessage:\"hi\"\n\t\t},\n\t\t{\n\t\t\tmessage:\"how are you\"\n\t\t}\n\t]\n  }\n    \"message\": \"Message list fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_MESSAGE",
    "name": "GetMessagelist"
  },
  {
    "type": "post",
    "url": "/message/changestatus",
    "title": "Read/Unread status Message API",
    "version": "1.0.0",
    "group": "REST_API_MESSAGE",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "mentor_id",
            "description": "<p>Mentor ID</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "student_id",
            "description": "<p>Student ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n\t\"readstatus\":\"yes\"\n\t\"_id\":\"5d3188206d84b8436af1ce37\"\n  }\n    \"message\": \"message status updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_MESSAGE",
    "name": "PostMessageChangestatus"
  },
  {
    "type": "post",
    "url": "/message/delete",
    "title": "Delete Message API",
    "version": "1.0.0",
    "group": "REST_API_MESSAGE",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "mentor_id",
            "description": "<p>Mentor ID</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "student_id",
            "description": "<p>Student ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n  }\n    \"message\": \"Delete message successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_MESSAGE",
    "name": "PostMessageDelete"
  },
  {
    "type": "post",
    "url": "/message/reportmessage",
    "title": "Report Message API",
    "version": "1.0.0",
    "group": "REST_API_MESSAGE",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "mentor_id",
            "description": "<p>Mentor ID</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "student_id",
            "description": "<p>Student ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n\t\"_id\":\"5d3188206d84b8436af1ce37\",\n\t\"mentor_id\":\"5d306dc6822fa522c79acd21\",\n\t\"student_id\":\"5d306346822fa522c79a2dec\",\n\t\"count\":5,\n\t\"message_details\":[\n\t\t{\n\t\t\tmessage:\"message one\"\n\t\t},\n\t\t{\n\t\t\tmessage:\"message two\"\n\t\t}\n\t]\n  }\n    \"message\": \"Report message list successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_MESSAGE",
    "name": "PostMessageReportmessage"
  },
  {
    "type": "post",
    "url": "/student/offlineonline",
    "title": "Offline Online Icon",
    "version": "1.0.0",
    "group": "REST_API_USER",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's Access token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "online_status",
            "description": "<p>Online Status</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"full_name\": \"Josephine Darakjy\",\n        \"email\": \"josephine_darakjy@darakjy.org\",\n        \"password\": \"$2a$08$6q976DlalgWZIzfeTteN5Of0BBTNfQu.6wqOZhXm8krdtCJeOYOfq\",\n        \"online_status\":true,\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5d3188206d84b8436af1ce37\",\n        \"role\": \"5d30518c822fa522c7991272\",\n        \"createdAt\": \"2019-07-19T09:06:40.023Z\",\n        \"updatedAt\": \"2019-08-16T09:14:15.232Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Settings update successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_USER",
    "name": "PostStudentOfflineonline"
  },
  {
    "type": "post",
    "url": "/student/signin",
    "title": "Student Login",
    "version": "1.0.0",
    "group": "REST_API_USER",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>Student Email</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "password",
            "description": "<p>Student Password</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "social_id",
            "description": "<p>Social ID (If Social Login)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "register_type",
            "description": "<p>Register Type (facebook/google/normal/linkedin/instagram)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"full_name\": \"Subhra\",\n        \"email\": \"subhra11@gmail.com\",\n        \"password\": \"$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye\",\n        \"school_name\": \"\",\n        \"dob\": \"\",\n        \"social_id\": \"\",\n        \"register_type\": \"normal\",\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"push_notification\": true,\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5d35bb4a38104765642d2b21\",\n        \"role\": \"5d30518c822fa522c7991272\",\n        \"createdAt\": \"2019-07-22T13:34:02.101Z\",\n        \"updatedAt\": \"2019-07-22T13:34:02.101Z\",\n        \"__v\": 0\n    },\n    \"isLoggedIn\": true,\n    \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkMzE4ODIwNmQ4NGI4NDM2YWYxY2UzNyIsImlhdCI6MTU2MzUzMjM4NCwiZXhwIjoxNTYzNjE4Nzg0fQ.5JvxFtAsNlBRPjOFgsW_ZRaYwjw-w665S_iRpxCKaY0\",\n    \"message\": \"Login successfull Student.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_USER",
    "name": "PostStudentSignin"
  },
  {
    "type": "post",
    "url": "/student/step1",
    "title": "Signup API step One",
    "version": "1.0.0",
    "group": "REST_API_USER",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "full_name",
            "description": "<p>Student Name</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "email",
            "description": "<p>Student Email</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "password",
            "description": "<p>Student Password</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "social_id",
            "description": "<p>Social ID (If Social Login)</p>"
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "register_type",
            "description": "<p>Register Type (facebook/google/normal/linkedin/instagram)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success Student",
          "content": "{\n    \"status\": 200,\n    \"data\": {\n        \"full_name\": \"Subhra\",\n        \"email\": \"subhra11@gmail.com\",\n        \"password\": \"$2a$08$5y0.VRcXrKLyznMQcxIagekdEY4n/XKg/xv1I5mM5RIk3.mCG0Jye\",\n        \"school_name\": \"\",\n        \"dob\": \"\",\n        \"social_id\": \"\",\n        \"register_type\": \"normal\",\n        \"deviceToken\": \"\",\n        \"deviceType\": \"\",\n        \"push_notification\": true,\n        \"isDeleted\": false,\n        \"isActive\": true,\n        \"_id\": \"5d35bb4a38104765642d2b21\",\n        \"role\": \"5d30518c822fa522c7991272\",\n        \"createdAt\": \"2019-07-22T13:34:02.101Z\",\n        \"updatedAt\": \"2019-07-22T13:34:02.101Z\",\n        \"__v\": 0\n    },\n    \"message\": \"Registration step one successfull.\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/routes/front/user.routes.js",
    "groupTitle": "REST_API_USER",
    "name": "PostStudentStep1"
  }
] });
